﻿using CDatos;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using CEntidades;
namespace CNegocios
{
    public class IndicadoresBussines
    {
        #region Clases
        IndicadoresData ObjIndicadores = new IndicadoresData();

        #endregion
        public int INSERTARINDICADOR(IndicadoresEntities ObjeECalidad)
        {
            return ObjIndicadores.CrearIndicador(ObjeECalidad);
        }

        public DataSet CONSULTARINDICADORES(int idindicador)
        {
            return ObjIndicadores.CONSULTARINDICADORES(idindicador);
        }

        public DataSet CONSULTARCATEGORIA()
        {
            return ObjIndicadores.CONSULTARCATEGORIAS();
        }


        public DataSet DTABLASMAESTRASESQUEMA(string Tabla)
        {
            return ObjIndicadores.CONSULTARTABLEMASTER(Tabla);
        }

        public DataSet DTABLAUSUARIOS()
        {
            return ObjIndicadores.CONSULTARTABLAUSERS();
        }

        public DataSet DTABLESROLE()
        {
            return ObjIndicadores.CONSULTARROLES();
        }


        public DataSet DCONSULTAPDF(int IDINDICADOR)
        {
            return ObjIndicadores.CONSULTAINDICADORPDF(IDINDICADOR);
        }

        public DataSet DCONSULTAPDFVAR(int IDINDICADOR)
        {
            return ObjIndicadores.CONSULTAINDICADORPDFVAR(IDINDICADOR);
        }
        public DataSet DTABLESPERMISOS()
        {
            return ObjIndicadores.CONSULTARPERMISOS();
        }

        public DataSet DINDICADORES(int IDPROCESO)
        {
            return ObjIndicadores.CONSULTARINDICADORES(IDPROCESO);
        }

        public DataSet DVAR(int IDINDICADOR)
        {
            return ObjIndicadores.CONSULTARVARIABLES(IDINDICADOR);
        }

        public DataSet DVARPER(int IDINDICADOR)
        {
            return ObjIndicadores.CONSULTARVARIABLES_PER(IDINDICADOR);
        }

        public DataSet DVIGENCIA(int IDVIGENCIA)
        {
            return ObjIndicadores.CONSULTARVIGENCIA(IDVIGENCIA);
        }

        public DataSet DMENU(int IDROL)
        {
            return ObjIndicadores.CONSULTARMENU(IDROL);
        }

        public DataSet DBUSQUEDATXT(int ID_INDICADOR,int ID_PERIODO)
        {
            return ObjIndicadores.CONSULTARPARTINDICADORESTXT(ID_INDICADOR,ID_PERIODO);
        }

        public DataSet DCONSULTAINDICADORESHIS(int ID_INDICADORES)
        {
            return ObjIndicadores.CONSULTARPARTINDICADORESHIS(ID_INDICADORES);
        }

        public DataSet DBUSQUEDAPROCESO(int IDPROCESO,int IDPERIODO)
        {
            return ObjIndicadores.CONSULTARPARTINDICADORESPROCESOS(IDPROCESO, IDPERIODO);
        }

        public DataSet DBUSQUEDASECCIONAL( int IDPERIODO, int IDSECCIONAL, int IDINDICADOR)
        {
            return ObjIndicadores.CONSULTARSECCIONALES(IDPERIODO,IDSECCIONAL,IDINDICADOR);
        }
        public DataSet DBUSQUEDATIPO(int TIPO, int IDPERSPECTIVA,int IDPERIODO)
        {
            return ObjIndicadores.CONSULTARTIPOINDICADOR(TIPO, IDPERSPECTIVA,IDPERIODO);
        }



        public List<PA_MENU_GET_Result> GetMenu(int? idRol, int? idusuario)
        {
            using (SCORBOARDEntities context = new SCORBOARDEntities())
            {
                return context.PA_MENU_GET(idRol,idusuario).ToList();
            }
        }

        public DataSet DBUSQUEDAVARIABLE(int IDINDICADOR)
        {
            return ObjIndicadores.CONSULTARVARIABLE(IDINDICADOR);
        }

        public DataSet DBUSQUEDAVARIABLE_DET(int IDPERIODO, int IDINDICADOR,int IDSECCIONAL)
        {
            return ObjIndicadores.CONSULTARVARIABLEPERIODO(IDPERIODO, IDINDICADOR, IDSECCIONAL);
        }

        public DataSet DACTUALIZAR_VALORES_VARIABLES(int IDVARIABLE, float VALOR, int PERIODO)
        {
            return ObjIndicadores.ACTUALIZAR_VALOR_VAR(IDVARIABLE, VALOR, PERIODO);
        }


        public DataSet DPERIDOSCMBVARIABLE(int VIGENCIA)
        {
            return ObjIndicadores.CONSULTAPERIODOCMB(VIGENCIA);
        }

        public DataSet DCONSULTATIPO(int TIPO)
        {
            return ObjIndicadores.CONSULTATIPO(TIPO);
        }


        public DataSet DCONSULTASECCIONAL(int CASO)
        {
            return ObjIndicadores.CONSULTASECCIONALES(CASO);
        }

        public DataSet DUSUARIOSDA()
        {
            return ObjIndicadores.CONSULTAUSUARIOS();
        }

        public int CREATEROLESINDICADOR(string IDUSUARIO, int IDINDICADOR, int IDROL, bool TODOS)
        {
            return ObjIndicadores.CREARASIGNACION(IDUSUARIO, IDINDICADOR, IDROL,TODOS);
        }


        public int CREATEMENUUSUARIO(int IDUSUARIO, int IDMENU, int IDROL)
        {
            return ObjIndicadores.CREARASIGNACIONMENU(IDUSUARIO, IDMENU, IDROL);
        }


        public DataSet DCONSULTARRESULTADO(int IDINDICADOR)
        {
            return ObjIndicadores.CONSULTARESULTADO(IDINDICADOR);
        }

        public DataSet DCONSULTARRESULTADO_R(int IDINDICADOR,int PERIODO)
        {
            return ObjIndicadores.CONSULTARESULTADO_R(IDINDICADOR, PERIODO);
        }

        public DataSet DCONSULTARRESULTADO_S(int IDINDICADOR,int PERIODO)
        {
            return ObjIndicadores.CONSULTARESULTADO_R2(IDINDICADOR,PERIODO); 
        }



        public DataSet DPARAMETRO(int IDPARAMETRO)
        {
            return ObjIndicadores.CONSULTAPARAMETROS(IDPARAMETRO);
        }


        public DataSet DCONSULTAUSR_NAME(string NAME)
        {
            return ObjIndicadores.CONSULTAUSUARIOSPER(NAME);
        }

        public DataSet DCUMPLIMIENTOINDICADORES(int IDINDICADORES)
        {
            return ObjIndicadores.CONSULTACUMPLIMIENTO(IDINDICADORES);
        }

        public DataSet DCUMPLIMIENTOINDICADORESR(int IDINDICADORES)
        {
            return ObjIndicadores.CONSULTACUMPLIMIENTOR(IDINDICADORES);
        }

        public DataSet DBUSQUEDAREGIONAL(int PERIODO ,int IDREGIONAL, int ID_INDICADOR)
        {
            return ObjIndicadores.CONSULTARPARTINDICADORESREGIONAL(PERIODO, IDREGIONAL, ID_INDICADOR);
        }


        public DataSet FRMREGIONAL(int PERIODO, int IDREGIONAL)
        {
            return ObjIndicadores.FORMULAPREGIONAL(PERIODO, IDREGIONAL);
        }


        public DataSet DCONSULTAINDUCTORES(int IDINDICADOR)
        {
            return ObjIndicadores.CONSULTARINDUCTORES(IDINDICADOR);
        }


        public int DVARIABLES(int IDINDICADOR,string NOMBREVAR,int IDFUENTE,int IDTIPODATO,string OBSERVACION)
        {
            return ObjIndicadores.CREARVARIABLES(IDINDICADOR,NOMBREVAR,IDFUENTE,IDTIPODATO, OBSERVACION);
        }
        public int UPDATEDVARIABLES(int IDINDICADOR, string NOMBREVAR, int IDFUENTE, int IDTIPODATO)
        {
            return ObjIndicadores.UPDATEVARIABLES(IDINDICADOR, NOMBREVAR, IDFUENTE, IDTIPODATO);
        }

        public DataSet DCONSULTANOTAS(int IDPERIODO, int IDINDICADOR)
        {
            return ObjIndicadores.CONSULTANOTAS(IDPERIODO, IDINDICADOR);
        }

        public DataSet DACTUALIZAR_NOTAS(int IDNOTA, string NOTA, int PERIODO, int INDICADOR)
        {
            return ObjIndicadores.ACTUALIZAR_NOTA(IDNOTA, NOTA, PERIODO, INDICADOR);
        }


        public DataSet DACONSULTATMAESTRAS( string TABLA)
        {
            return ObjIndicadores.CONSULTAPARAMETROS(TABLA);
        }

        public DataSet DACONSULTAASINGANCION(int IDINDICADOR)
        {
            return ObjIndicadores.CONSULTAASIGNACION(IDINDICADOR);
        }

        public DataSet DADELETEVARIABLE(int IDVARIABLE)
        {
            return ObjIndicadores.DELETEVARIABLE(IDVARIABLE);
        }

        public DataSet DENVIARCORREO(string IDENTIFICACION,int ID_INDICADOR)
        {
            return ObjIndicadores.ENVIARCORREO(IDENTIFICACION,ID_INDICADOR);
        }
        public DataSet DUSUARIOS2(string IDUSUARIO)
        {
            return ObjIndicadores.CONSULTAUSUARIOS2(IDUSUARIO);
        }
        public DataSet DROLESGETMENU(int ROL,string IDUSUARIO)
        {
            return ObjIndicadores.PAGETMENU2(ROL,IDUSUARIO);
        }
        public int CREATEDOCUMENTO(int IDINDICADOR, string RUTA)
        {
            return ObjIndicadores.CREATEDOCUMENTOINDICADOR(IDINDICADOR, RUTA);
        }
        public DataSet DUSUARIOS3(int rol, string IDUSUARIO)
        {
            return ObjIndicadores.PAGETMENU2(rol, IDUSUARIO);
        }
        public DataSet DCONSULTAPER(int rol, string IDUSUARIO)
        {
            return ObjIndicadores.CONSULTAPERMISOSINDICADORES(rol, IDUSUARIO);
        }

        public DataSet DCONSULMENU()
        {
            return ObjIndicadores.PAGETMENU3();
        }
        public DataSet DCONSULTAPERMISOSTIPO(int idproceso, int idtipo)
        {
            return ObjIndicadores.CONSULTAPERMISOS(idproceso,idtipo);
        }

        public DataSet DCONSULTAVAR(int INDICADOR)
        {
            return ObjIndicadores.CONSULTAVARV(INDICADOR);
        }

        public DataSet DACTUALIZARFSQL(int INDICADOR, string VALOR)
        {
            return ObjIndicadores.UPDATE_FSQL(INDICADOR, VALOR);
        }

        public DataSet DCONSULTARVALORESVAR(int IDINDICADOR,int PERIODO,int IDSECCIONAL)
        {
            return ObjIndicadores.CONSULTARVARIABLESVALORES(IDINDICADOR,PERIODO,IDSECCIONAL);
        }
        public DataSet DCONSULTARVALORESVAR2(int IDINDICADOR, int PERIODO)
        {
            return ObjIndicadores.CONSULTARVARIABLESVALORES2(IDINDICADOR, PERIODO);
        }
        

        public DataSet DUPDATETABLE(string TABLA,string VALOR, string VALOR_FILTRO)
        {
            return ObjIndicadores.ACTUALIZARVALORESTABLA(TABLA, VALOR, VALOR_FILTRO);
        }

        public DataSet DINSERTARTABLEM(string TABLA, string VALOR)
        {
            return ObjIndicadores.INSERTVALUESTM(TABLA, VALOR);
        }

        public DataSet DDELETEM(string TABLA, string VALOR)
        {
            return ObjIndicadores.DELETETM(TABLA, VALOR);
        }

        public DataSet DCONSULTAINDICADORES(int TIPO, int PROCESO)
        {
            return ObjIndicadores.CONSULTAMODIFICAR(TIPO, PROCESO);
        }

        public DataSet DINSERTARNOTAINDICADOR(int IDINDICADOR,int IDPERIODO,string NOTA)
        {
            return ObjIndicadores.INSERTNOTASCUALITATIVAS(IDINDICADOR,IDPERIODO,NOTA);
        }

        public DataSet DELETEINDICADOR(int ID)
        {
            return ObjIndicadores.DELETEINDICADOR(ID);
        }

        public DataSet DELETEINDICADORMOD(int ID)
        {
            return ObjIndicadores.DELETEINDICADORMOD(ID);
        }


        public DataSet DELETEINDICADOROL(int ID)
        {
            return ObjIndicadores.DELETEINDICADORROL(ID);
        }

        public DataSet INSERTARDOCUMENTO(string RUTA, int ID)
        {
            return ObjIndicadores.CREATEDOCUMENTOS(RUTA,ID);
        }

        public int UPDATEINDICADOR(IndicadoresEntities ObjeECalidad)
        {
            return ObjIndicadores.UPDATEACTUALIZAR(ObjeECalidad);
        }


        public DataSet DEVALUARSINTAXIS(string EXPRESION)
        {
            return ObjIndicadores.DEVALUAREXPRESION(EXPRESION);
        }

        public DataSet DCONSULTARDOCUMENTO(int IDTINDICADOR)
        {
            return ObjIndicadores.CONSULTADOCUMENTOS(IDTINDICADOR);
        }
        public DataSet DCREARVARINDICADOR(int IDTINDICADOR)
        {
            return ObjIndicadores.CREARVARIABLEPORINDICADOR(IDTINDICADOR);
        }

   

        public DataSet DACTUALIZARINDICADORR(int IDINDICADOR,int IDPERIODO, int IDSECCIONAL, float VALOR, int IDVARIABLE)
        {
            return ObjIndicadores.UPDATEFORMULA(IDINDICADOR, IDPERIODO,IDSECCIONAL,VALOR,IDVARIABLE);
        }
        public DataSet DACTUALIZARINDICADORR2(int IDINDICADOR, int IDPERIODO)
        {
            return ObjIndicadores.UPDATEFORMULA2(IDINDICADOR, IDPERIODO);
        }

        public DataSet DACTUALIZARSECCIONAL(int IDINDICADOR, int IDPERIODO)
        {
            return ObjIndicadores.UPDATEFORMULA1(IDINDICADOR, IDPERIODO);
        }
        public DataSet DACTUALIZARSECCIONALREGIONAL(int IDINDICADOR, int IDPERIODO)
        {
            return ObjIndicadores.UPDATEFORMULA2(IDINDICADOR, IDPERIODO);
        }


        public DataSet DACTUALIZARNORMAL(int IDINDICADOR, int IDPERIODO)
        {
            return ObjIndicadores.UPDATEFORMULANORMAL(IDINDICADOR, IDPERIODO);
        }

        public DataSet DFORMULANACIONAL(int IDINDICADOR, int IDPERIODO)
        {
            return ObjIndicadores.UPDATEFORMULANACIONAL(IDINDICADOR, IDPERIODO);
        }




        public DataSet DACTUALIZARCUMPLIMIENTO(int IDINDICADOR, int IDPERIODO)
        {
            return ObjIndicadores.UPDATECUMPLIMIENTO(IDINDICADOR, IDPERIODO);
        }

        public DataSet DACTUALIZARCUMPLIMIENTOS(int IDINDICADOR, int IDPERIODO)
        {
            return ObjIndicadores.UPDATECUMPLIMIENTOS(IDINDICADOR, IDPERIODO);
        }


        public DataSet DACTUALIZARCUMPLIMIENTOR(int IDINDICADOR, int IDPERIODO)
        {
            return ObjIndicadores.UPDATECUMPLIMIENTOR(IDINDICADOR, IDPERIODO);
        }
        public DataSet DACONSULTARNACIONAL(int IDINDICADOR)
        {
            return ObjIndicadores.CONSULTARNACIONAL(IDINDICADOR);
        }


        public DataSet DACONSULTA_VAR_INDICADOR(int IDINDICADOR,int PERIODO, int SECCIONAL)
        {
            return ObjIndicadores.CONSULTA_VAR_INDICADOR(IDINDICADOR,PERIODO,SECCIONAL);
        }

        public DataSet DCONSULTARUSUARIOS()
        {
            return ObjIndicadores.CONSULTARUSUARIOS();
        }

    

        public DataSet DCONSULTARVERSION()
        {
            return ObjIndicadores.CONSULTARNOMBRESINDICADORES();
        }
        public DataSet DCONSULTARVERSIONES(int IDINDICADOR)
        {
            return ObjIndicadores.CONSULTARVERIN(IDINDICADOR);
        }

        public DataSet DCONSULTARLIMITE( int IDINDICADOR, double PEOR, double MEJOR, double ALCANCE, double ROJA)
        {
            return ObjIndicadores.CONSULTA_LIMITE(IDINDICADOR,PEOR,MEJOR,ALCANCE,ROJA);

        }

        public DataSet DRESULTADO(int IDINDICADOR, int REGIONAL, int SECCIONAL, int PERIODO)
        {
            return ObjIndicadores.CONSULTARESULTADOS(IDINDICADOR, REGIONAL, SECCIONAL,PERIODO);
        }
        public DataSet DUPDATERESULTADOS(int IDINDICADOR,int REGIONAL,int SECCIONAL,int PERIODO, double RESULTADO,double META)
        {
            return ObjIndicadores.UPDATERESULTADOS( IDINDICADOR, REGIONAL,SECCIONAL, PERIODO, RESULTADO, META);
        }

    }
}
