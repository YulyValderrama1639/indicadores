﻿using System.Net.Mail;

namespace CEntidades
{
    public class EEnvioCorreo
    {
        #region Atributos
        private string para1;
        private string para2;
        private string para3;
        private string para4;
        private string copiaO;
        private string asunto;
        private string mensaje;
        private string adjunto;
        private AlternateView imagen;
        #endregion

        #region Constructor
        public EEnvioCorreo()
        {
            para1 = string.Empty;
            para2 = string.Empty;
            para3 = string.Empty;
            para4 = string.Empty;
            copiaO = string.Empty;
            asunto = string.Empty;
            mensaje = string.Empty;
            adjunto = string.Empty;
        }
        #endregion

        #region Encapsulamientos
        public string Para1
        {
            get { return para1; }
            set { para1 = value; }
        }
        public string Para2
        {
            get { return para2; }
            set { para2 = value; }
        }
        public string Para3
        {
            get { return para3; }
            set { para3 = value; }
        }
        public string Para4
        {
            get { return para4; }
            set { para4 = value; }
        }
        public string Asunto
        {
            get { return asunto; }
            set { asunto = value; }
        }
        public string Mensaje
        {
            get { return mensaje; }
            set { mensaje = value; }
        }
        public string Adjunto
        {
            get { return adjunto; }
            set { adjunto = value; }
        }
        public AlternateView Imagen
        {
            get { return imagen; }
            set { imagen = value; }
        }
        public string CopiaO
        {
            get
            {
                return copiaO;
            }

            set
            {
                copiaO = value;
            }
        }
        #endregion
    }
}