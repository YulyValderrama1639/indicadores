﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEntidades
{
    public class IndicadoresEntities
    {
        #region Atributos
        private int idindicador;
        private int idperiodicidad;
        private int idunidad;
        private int idtipoindicador;
        private int idproceso;        
        private string nombreindicador;
        private string proposito;
        private string formula;                
        private int idtendencia;
        private int perspectiva;        
        private string meta;               
        private int idclasificacion;
        private int idempresa;
        private int idperanalisis;
        private int idalcance;
        private int idtipomedicion;
        private string porcentajecumplimiento;
        private int peso;
        private int inductor;
        private string usuario;
        private int calculado;
        private DateTime fechacreacion;

        #endregion
        #region Constructor
        public IndicadoresEntities()
        {

            idproceso = 0;
            idperiodicidad = 0;
            idunidad = 0;
            idtipoindicador = 0;
            nombreindicador = string.Empty;
            proposito = string.Empty;
            calculado = 0;
            
      
        }
        #endregion
        #region Encapsulamientos

        public int IDPROCESO
        {
            get { return idproceso; }
            set { idproceso = value; }
        }

        public int IDPERIODICIDAD
        {
            get { return idperiodicidad; }
            set { idperiodicidad = value; }
        }
        public int IDUNIDAD
        {
            get { return idunidad; }
            set { idunidad = value; }
        }
        public int TIPOINDICADOR
        {
            get { return idtipoindicador; }
            set { idtipoindicador = value; }
        }


        public string NOMBREINDICADOR
        {
            get { return nombreindicador; }
            set { nombreindicador = value; }
        }

        public string PROPOSITOINDICADOR
        {
            get { return proposito; }
            set { proposito = value; }
        }
        public int IDPERSPECTIVA
        {
            get { return perspectiva; }
            set { perspectiva = value; }
        }



        public string FORMULA
        {
            get { return formula; }
            set { formula = value; }
        }



        public int IDTENDENCIA
        {
            get { return idtendencia; }
            set { idtendencia = value; }
        }

 
        public string META
        {
            get { return meta; }
            set { meta = value; }
        }
        

        public int IDINDICADOR
        {
            get { return idindicador; }
            set { idindicador = value; }
        }

        public int IDCLASIFICACION
        {
            get { return idclasificacion; }
            set { idclasificacion = value; }
        }


        public int IDEMPRESA
        {
            get { return idempresa; }
            set { idempresa = value; }
        }

        public int IDPERANALISI
        {
            get { return idperanalisis; }
            set { idperanalisis = value; }
        }

        public int IDALCANCE
        {
            get { return idalcance; }
            set { idalcance = value; }
        }
        public int IDTIPOMEDICION
        {
            get { return idtipomedicion; }
            set { idtipomedicion = value; }
        }

        public string CUMPLIMIENTO
        {
            get { return porcentajecumplimiento; }
            set { porcentajecumplimiento = value; }
        }

        public string PORCENTAJEC
        {
            get { return porcentajecumplimiento; }
            set { porcentajecumplimiento = value; }
        }

        public int PESO
        {
            get { return peso; }
            set { peso = value; }
        }

        public int IDINDUCTOR
        {
            get { return inductor; }
            set { inductor = value; }
        }

        public string USUARIO
        {
            get { return usuario; }
            set { usuario = value; }
        }

        public int CALCULADO
        {
            get { return calculado; }
            set { calculado = value; }
        }
        public DateTime FECHAC
        {
            get { return fechacreacion; }
            set { fechacreacion = value; }
        }


        #endregion
    }
}
