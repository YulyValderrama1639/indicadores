﻿using System;
using System.Net.Mail;
using CEntidades;
using System.Collections.Generic;

namespace CDatos
{
    public class DEnvioCorreo
    {
        public DEnvioCorreo() { }
        public string EnvioCorreo(EEnvioCorreo objEEnvioCorreo)
        {
            MailMessage email = new MailMessage();
            email.To.Add(new MailAddress(objEEnvioCorreo.Para1));
            if (objEEnvioCorreo.Para2 != string.Empty)
            {
                email.To.Add(new MailAddress(objEEnvioCorreo.Para2));
            }
            if (objEEnvioCorreo.Para3 != string.Empty)
            {
                email.To.Add(new MailAddress(objEEnvioCorreo.Para3));
            }
            if (objEEnvioCorreo.Para4 != string.Empty)
            {
                email.To.Add(new MailAddress(objEEnvioCorreo.Para4));
            }
            email.From = new MailAddress("infocalidad@scare.org.co", "INDICADORES CALIDAD");
            email.Subject = objEEnvioCorreo.Asunto;
            email.Body = objEEnvioCorreo.Mensaje;
            email.IsBodyHtml = true;
            email.Priority = MailPriority.High;
            email.Attachments.Add(new Attachment(objEEnvioCorreo.Adjunto));
             SmtpClient smtp = new SmtpClient();
            smtp.Host = "gcsbog-excs00";
            smtp.Port = 25;
            smtp.EnableSsl = false;
            smtp.UseDefaultCredentials = false;

            string output = null;
            try
            {
                smtp.Send(email);
                email.Dispose();
                output = "OK";
            }
            catch (Exception ex)
            {
                output = "Error enviando correo electrónico: " + ex.Message;
            }
            return (output);
        }
    }
}
