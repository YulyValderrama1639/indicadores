﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CEntidades;

namespace CDatos
{
    public class IndicadoresData : DConexionBD
    {

        public int CrearIndicador(IndicadoresEntities ObjeECalidad)
        {
            int resultado = 0;
            SqlCommand cmd = new SqlCommand("P_SAVE_INDICATOR", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ID_PERIODICIDAD", ObjeECalidad.IDPERIODICIDAD);
            cmd.Parameters.AddWithValue("@ID_UNIDADMEDIDA", ObjeECalidad.IDUNIDAD);
            cmd.Parameters.AddWithValue("@ID_TIPO_INDICADOR", ObjeECalidad.TIPOINDICADOR);
            cmd.Parameters.AddWithValue("@ID_PROCESO", ObjeECalidad.IDPROCESO);
            cmd.Parameters.AddWithValue("@NOMBRE_INDICADOR", ObjeECalidad.NOMBREINDICADOR);
            cmd.Parameters.AddWithValue("@PROPOSITO_INDICADOR", ObjeECalidad.PROPOSITOINDICADOR);
            cmd.Parameters.AddWithValue("@FORMULA", ObjeECalidad.FORMULA);
            cmd.Parameters.AddWithValue("@ID_TENDENCIA", ObjeECalidad.IDTENDENCIA);
            cmd.Parameters.AddWithValue("@ID_INDUCTOR", ObjeECalidad.IDINDUCTOR);
            cmd.Parameters.AddWithValue("@ID_PERSPECTIVA", ObjeECalidad.IDPERSPECTIVA);
            cmd.Parameters.AddWithValue("@META", ObjeECalidad.META);
            cmd.Parameters.AddWithValue("@FECHA_CREACION", ObjeECalidad.FECHAC);
            cmd.Parameters.AddWithValue("@CUMPLIMIENTO", ObjeECalidad.CUMPLIMIENTO);
            cmd.Parameters.AddWithValue("@ID_CLASIFICACION", ObjeECalidad.IDCLASIFICACION);
            cmd.Parameters.AddWithValue("@ID_EMPRESA", ObjeECalidad.IDEMPRESA);
            cmd.Parameters.AddWithValue("@ID_PER_ANALISIS", ObjeECalidad.IDPERANALISI);
            cmd.Parameters.AddWithValue("@ID_ALCANCE", ObjeECalidad.IDALCANCE);
            cmd.Parameters.AddWithValue("@ID_TIPO_MEDICION", ObjeECalidad.IDTIPOMEDICION);
            cmd.Parameters.AddWithValue("@POR_CUMPLIMIENTO", ObjeECalidad.PORCENTAJEC);
            cmd.Parameters.AddWithValue("@ID_CALCULADO", ObjeECalidad.CALCULADO);

            SqlParameter output = new SqlParameter("@ID_INDICADOR", SqlDbType.Int);
            output.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(output);
            try
            {
                AbrirConexion();
                resultado = cmd.ExecuteNonQuery();
                var outputd = output.SqlValue;

            }
            catch (Exception e)
            {
                throw new Exception("Error, al crear indicador ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return Convert.ToInt32(cmd.Parameters["@ID_INDICADOR"].Value);
        }

        public int UPDATEACTUALIZAR(IndicadoresEntities ObjeECalidad)
        {
            int resultado = 0;
            SqlCommand cmd = new SqlCommand("[P_ACTUALIZAR_INDICATOR]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ID_INDICADOR", ObjeECalidad.IDINDICADOR);
            cmd.Parameters.AddWithValue("@ID_PERIODICIDAD", ObjeECalidad.IDPERIODICIDAD);
            cmd.Parameters.AddWithValue("@ID_UNIDADMEDIDA", ObjeECalidad.IDUNIDAD);
            cmd.Parameters.AddWithValue("@ID_PER_ANALISIS", ObjeECalidad.IDPERANALISI);
            cmd.Parameters.AddWithValue("@ID_TIPO_MEDICION", ObjeECalidad.IDTIPOMEDICION);
            cmd.Parameters.AddWithValue("@TENDENCIA", ObjeECalidad.IDTENDENCIA);
            cmd.Parameters.AddWithValue("@META", ObjeECalidad.META);
            cmd.Parameters.AddWithValue("@USUARIO", ObjeECalidad.USUARIO);
            cmd.Parameters.AddWithValue("@DESCRIPCIONFORMULA", ObjeECalidad.FORMULA);
            cmd.Parameters.AddWithValue("@NOMBRE", ObjeECalidad.NOMBREINDICADOR);
            try
            {
                AbrirConexion();
                resultado = cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("Error, al crear indicador ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return resultado;
        }


        public DataSet CONSULTARTABLASMAESTRAS()
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[DBO].[PA_TABLES_MASTERS]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al realizar Consulta ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }


        public DataSet CONSULTAINDICADORPDF(int IDINDICADOR)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[DBO].[PA_CONSULTAINDICADORES_PDF]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDINDICADOR", IDINDICADOR);
            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al realizar Consulta ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }




        public DataSet CONSULTAINDICADORPDFVAR(int IDINDICADOR)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_CONSULTAINDICADORES_PDF_VAR]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDINDICADOR", IDINDICADOR);
            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al realizar Consulta ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }



        public DataSet CONSULTARTABLAUSERS()
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[DBO].[PA_USER_GET]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al realizar Consulta ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }


        public DataSet CONSULTARCATEGORIAS()
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_TABLE_CATEGORIA]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al realizar Consulta ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }
        public DataSet CONSULTARNOMBRESINDICADORES()
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_CONSULTA_INDI_VER]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al realizar Consulta ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }


        public DataSet CONSULTARVERIN(int IDINDICADOR)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_CONSULTA_VERSIONES]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDINDICADOR", IDINDICADOR);
            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al realizar Consulta ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }


        public DataSet CONSULTARTABLEMASTER(string NTabla)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[ESQUEMA_TABLASM_GET]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TABLA", NTabla);

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al realizar Consulta ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }

        public DataSet CONSULTARROLES()
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_ROLES]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al realizar Consulta ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }


        public DataSet CONSULTARPERMISOS()
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_PERMISOS]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al realizar Consulta ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }


        public DataSet CONSULTARINDICADORES(int IDPROCESO)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_CONSULTAINDICADORES]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDPROCESO", IDPROCESO);

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al realizar Consulta ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }


        public DataSet CONSULTARVARIABLES(int IDINDICADOR)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_CONSULTAVAR]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDINDICADOR", IDINDICADOR);

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al realizar Consulta ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }


        public DataSet CONSULTARVARIABLES_PER(int IDINDICADOR)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_CONSULTAVAR_PER]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDINDICADOR", IDINDICADOR);

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al realizar Consulta ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }



        public DataSet CONSULTARVIGENCIA(int IDVIGENCIA)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_CONSULTA_VIGENCIA_PR]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDVIGENCIA", IDVIGENCIA);

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al realizar Consulta ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }


        public DataSet CONSULTARMENU(int IDROL)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_MENU_GET]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDROL", IDROL);


            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al realizar Consulta ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }


        public DataSet CONSULTARPARTINDICADORESTXT(int ID_INDICADOR,int ID_PERIODO)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_CONSULTAINDICADORES_TXT]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ID_INDICADOR", ID_INDICADOR);
            cmd.Parameters.AddWithValue("@PERIODO", ID_PERIODO);
            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }

        public DataSet CONSULTARPARTINDICADORESPROCESOS(int IDPROCESO, int IDPERIODO)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_CONSULTAINDICADORES_PROCESO]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDPROCESO", IDPROCESO);
            cmd.Parameters.AddWithValue("@PERIODO", IDPERIODO);

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }

        public DataSet CONSULTARSECCIONALES(int IDPERIODO, int IDSECCIONAL, int ID_INDICADOR)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_CONSULTAINDICADORES_SECCIONAL]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ID_PERIODO", IDPERIODO);
            cmd.Parameters.AddWithValue("@SECCIONAL", IDSECCIONAL);
            cmd.Parameters.AddWithValue("@IDINDICADOR", ID_INDICADOR);

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }


        public DataSet CONSULTARPARTINDICADORESHIS(int ID_INDICADOR)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_CONSULTAINDICADORESHIS]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ID_INDICADOR", ID_INDICADOR);
            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }


        public DataSet FORMULAPREGIONAL(int IDPERIODO, int ID_REGIONAL)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_CONSULTAINDICADORES_REGIONAL]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ID_PERIODO", IDPERIODO);
            cmd.Parameters.AddWithValue("@REGIONAL", ID_REGIONAL);
            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }

        public DataSet FORMULASECCIONAL(int IDPERIODO, int ID_REGIONAL)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[P_FORMULA_RESULTADO_SEC]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ID_PERIODO", IDPERIODO);
            cmd.Parameters.AddWithValue("@REGIONAL", ID_REGIONAL);
            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }


        public DataSet CONSULTARPARTINDICADORESREGIONAL(int IDPERIODO, int ID_REGIONAL, int ID_INDICADOR)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_CONSULTAINDICADORES_REGIONAL]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ID_PERIODO", IDPERIODO);
            cmd.Parameters.AddWithValue("@REGIONAL", ID_REGIONAL);
            cmd.Parameters.AddWithValue("@ID_INDICADOR", ID_INDICADOR);
            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }




        public DataSet CONSULTARTIPOINDICADOR(int IDTIPO, int IDPERSPECTIVA, int PERIODO)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_CONSULTAINDICADORES_ESTRATEGICO]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@IDTIPO", IDTIPO);
            cmd.Parameters.AddWithValue("@IDPERSPECTIVA", IDPERSPECTIVA);
            cmd.Parameters.AddWithValue("@PERIODO", PERIODO);

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }

        public DataSet CONSULTARVARIABLE(int IDINDICADOR)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_CONSULTAINDICADORES_VARIABLES]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@IDINDICADOR", IDINDICADOR);

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }


        public DataSet CONSULTARVARIABLEPERIODO(int IDPERIODO, int ID_INDICADOR, int IDSECCIONAL)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_CONSULTAINDICADORES_VAR_DET]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@ID_INDICADOR", ID_INDICADOR);
            cmd.Parameters.AddWithValue("@IDPERIODO", IDPERIODO);
            cmd.Parameters.AddWithValue("@ID_SECCIONAL", IDSECCIONAL);

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }

        public DataSet ACTUALIZAR_VALOR_VAR(int IDVARIABLE, float VALOR, int PERIODO)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_UPDATE_VAR_DET]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@IDVARIABLE", IDVARIABLE);
            cmd.Parameters.AddWithValue("@VALOR", VALOR);
            cmd.Parameters.AddWithValue("@PERIODO", PERIODO);

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }

        public DataSet CONSULTAPERIODOCMB(int VIGENCIA)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_CONSULTA_PERIODO_CMB]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@VIGENCIA", VIGENCIA);

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }

        public DataSet CONSULTATIPO(int TIPO)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[PA_CONSULTA_TIPO_P]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TIPO", TIPO);

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }



        public DataSet CONSULTAUSUARIOS()
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[USER_DA]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }



        public DataSet CONSULTASECCIONALES(int CASO)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_SECCIONALES]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CASO", CASO);

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }


        public DataSet CONSULTAUSUARIOS2(string IDUSUARIO)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[sp_get_users_adb]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDUSUARIO", IDUSUARIO);
            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }





        public int CREARASIGNACION(string IDUSUARIO, int IDINDICADOR, int IDROL, bool TODOS)
        {
            int resultado;

            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[P_SAVE_ROLINDI]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@IDUSUARIO", IDUSUARIO);
            cmd.Parameters.AddWithValue("@IDINDICADOR", IDINDICADOR);
            cmd.Parameters.AddWithValue("@IDROL", IDROL);
            cmd.Parameters.AddWithValue("@TODO", TODOS);

            try
            {
                AbrirConexion();
                resultado = cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return resultado;
        }
        public int CREARASIGNACIONMENU(int IDUSUARIO, int IDMENU, int IDROL)
        {
            int resultado;

            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[P_SAVE_ROLUSUARIO]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@IDUSUARIO", IDUSUARIO);
            cmd.Parameters.AddWithValue("@IDMENU", IDMENU);
            cmd.Parameters.AddWithValue("@IDROL", IDROL);

            try
            {
                AbrirConexion();
                resultado = cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return resultado;
        }

        public DataSet CONSULTARESULTADO(int IDINDICADOR)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_CONSULTA_RESULTADO]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDINDICADOR", IDINDICADOR);
            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }


        public DataSet CONSULTARESULTADO_R(int IDINDICADOR, int PERIODO)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_CONSULTA_RESULTADO_R]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDINDICADOR", IDINDICADOR);
            cmd.Parameters.AddWithValue("@PERIODO", PERIODO);
            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }


        public DataSet CONSULTARESULTADO_R2(int IDINDICADOR, int PERIODO)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_CONSULTA_RESULTADO_R2]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDINDICADOR", IDINDICADOR);
            cmd.Parameters.AddWithValue("@PERIODO", PERIODO);
            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }




        public DataSet CONSULTAPARAMETROS(int ID_PARAMETRO)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_CONSULTA_PARAMETROS]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDPARAMETRO", ID_PARAMETRO);

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }

        public DataSet CONSULTAUSUARIOSPER(string USR_NAME)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_USUARIOS_PER]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@USER_NAME", USR_NAME);
            try
            {
                AbrirConexion();    
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
              throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }

        public DataSet CONSULTACUMPLIMIENTO(int IDINDICADOR)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[PA_CONSULTACUMPLIMIENTO_R]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDINDICADOR", IDINDICADOR);

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }


        public DataSet CONSULTACUMPLIMIENTOR(int IDINDICADOR)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_CONSULTACUMPLIMIENTO_R]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDINDICADOR", IDINDICADOR);

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }



        public int CREARINDICADORES(int IDUSUARIO, int IDINDICADOR, int IDROL)
        {
            int resultado;

            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[P_SAVE_ROLINDI]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@IDUSUARIO", IDUSUARIO);
            cmd.Parameters.AddWithValue("@IDINDICADOR", IDINDICADOR);
            cmd.Parameters.AddWithValue("@IDROL", IDROL);

            try
            {
                AbrirConexion();
                resultado = cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return resultado;
        }


        public DataSet CONSULTARUTA(int IDRUTA)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[P_RUTA_REPORTE]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDPARAMETRO", IDRUTA);

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }



        public DataSet CONSULTARINDUCTORES(int IDINDICADOR)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[DBO].[PA_CONSULTAINDUCTORES]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDINDICADOR", IDINDICADOR);

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al realizar Consulta ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }


        public int CREARVARIABLES(int IDINDICADOR, string NOMBREVARIABLE, int IDFUENTE, int IDTIPODATO, string OBSERVACION)
        {
            int resultado;

            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[PA_CREATE_VARIABLE]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@IDINDICADOR", IDINDICADOR);
            cmd.Parameters.AddWithValue("@NOMBREVARIABLE", NOMBREVARIABLE);
            cmd.Parameters.AddWithValue("@IDFUENTE", IDFUENTE);
            cmd.Parameters.AddWithValue("@IDTIPODATO", IDTIPODATO);
            cmd.Parameters.AddWithValue("@OBSERVACION", OBSERVACION);


            try
            {
                AbrirConexion();
                resultado = cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return resultado;
        }



        public int UPDATEVARIABLES(int IDINDICADOR, string NOMBREVARIABLE, int IDFUENTE, int IDTIPODATO)
        {
            int resultado;

            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_UPDATE_VARIABLE]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@IDINDICADOR", IDINDICADOR);
            cmd.Parameters.AddWithValue("@NOMBREVARIABLE", NOMBREVARIABLE);
            cmd.Parameters.AddWithValue("@IDFUENTE", IDFUENTE);
            cmd.Parameters.AddWithValue("@IDTIPODATO", IDTIPODATO);


            try
            {
                AbrirConexion();
                resultado = cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return resultado;
        }


        public DataSet CONSULTANOTAS(int IDPERIODO, int IDINDICADOR)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_CONSULTA_NOTAS]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDPERIODO", IDPERIODO);
            cmd.Parameters.AddWithValue("@IDINDICADOR", IDINDICADOR);       

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }

        public DataSet ACTUALIZAR_NOTA(int IDNOTA, string NOTA, int PERIODO, int INDICADOR)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_UPDATE_NOTA]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@IDNOTA", IDNOTA);
            cmd.Parameters.AddWithValue("@NOTA", NOTA);
            cmd.Parameters.AddWithValue("@PERIODO", PERIODO);
            cmd.Parameters.AddWithValue("@IDINDICADOR", INDICADOR);

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }

        public DataSet CONSULTAPARAMETROS(string TABLA)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_CONSULTA_T_MAESTRAS]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TABLA", TABLA);

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }

        public DataSet CONSULTAR(int IDNOTA, string NOTA, int PERIODO, int INDICADOR)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_UPDATE_NOTA]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@IDNOTA", IDNOTA);
            cmd.Parameters.AddWithValue("@NOTA", NOTA);
            cmd.Parameters.AddWithValue("@PERIODO", PERIODO);
            cmd.Parameters.AddWithValue("@IDINDICADOR", INDICADOR);

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }


        public DataSet CONSULTAASIGNACION(int IDINDICADOR)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_CONSULTAASIGNACIONES]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDINDICADOR", IDINDICADOR);

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al realizar Consulta ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }

        public DataSet CONSULTAVARV(int IDINDICADOR)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_CONSULTAINDICADORES_VARIABLES]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDINDICADOR", IDINDICADOR);

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al realizar Consulta ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }


        public DataSet DELETEVARIABLE(int IDVARIABLE)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_DELETE]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDVARIABLE", IDVARIABLE);

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al realizar Consulta ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }
        public DataSet ENVIARCORREO(string IDENTIFICACION, int IDINDICADOR)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[P_ENVIARCORREO]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDENTIFICACION", IDENTIFICACION);
            cmd.Parameters.AddWithValue("@ID_INDICADOR", IDINDICADOR);

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al realizar Consulta ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }

        public DataSet PAGETMENU2(int IDROL, string IDENTIFICACION)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_MENU_GET_2]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDROL", IDROL);
            cmd.Parameters.AddWithValue("@IDUSUARIO", IDENTIFICACION);

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al realizar Consulta ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }
        public DataSet PAGETMENU3()
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_MENU_GET_3]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al realizar Consulta ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }

        public DataSet CONSULTAPERMISOSINDICADORES(int IDROL, string IDENTIFICACION)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_PERMISOS_INDI]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDROL", IDROL);
            cmd.Parameters.AddWithValue("@IDUSUARIO", IDENTIFICACION);

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al realizar Consulta ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }



        public int CREATEDOCUMENTOINDICADOR(int IDINDICADOR, string RUTA)
        {
            int resultado = 0;
            SqlCommand cmd = new SqlCommand("[P_SAVE_DOCUMENTO]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDINDICADOR", IDINDICADOR);
            cmd.Parameters.AddWithValue("@RUTA", RUTA);

            try
            {
                AbrirConexion();
                resultado = cmd.ExecuteNonQuery();


            }
            catch (Exception e)
            {
                throw new Exception("Error, al crear indicador ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return resultado;
        }
        public DataSet CONSULTAPERMISOS(int IDROL, string IDENTIFICACION)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_PERMISOS_INDI]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDROL", IDROL);
            cmd.Parameters.AddWithValue("@IDUSUARIO", IDENTIFICACION);

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al realizar Consulta ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }


        public DataSet CONSULTAPERMISOS(int idproceso, int idtipo)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[PA_PERMISOS_VARIOS]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDPROCESO", idproceso);
            cmd.Parameters.AddWithValue("@IDTIPO", idtipo);

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }

        public DataSet UPDATE_FSQL(int IDINDICADOR, string VALOR)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_UPDATE_F_SQL]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDINDICADOR", IDINDICADOR);
            cmd.Parameters.AddWithValue("@VALOR", VALOR);
            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al realizar Consulta ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }

        public DataSet CONSULTARVARIABLESVALORES(int IDINDICADOR, int IDPERIODO, int IDSECCIONAL)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_CONSULTAVARIABLES]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDINDICADOR", IDINDICADOR);
            cmd.Parameters.AddWithValue("@PERIODO", IDPERIODO);
            cmd.Parameters.AddWithValue("@IDSECCIONAL", IDSECCIONAL);
            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }


        public DataSet CONSULTARVARIABLESVALORES2(int IDINDICADOR, int IDPERIODO)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_CONSULTAVARIABLES2]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDINDICADOR", IDINDICADOR);
            cmd.Parameters.AddWithValue("@PERIODO", IDPERIODO);

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }




        public DataSet ACTUALIZARVALORESTABLA(string TABLA, string VALOR, string ID_VALOR)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[P_UPDATE_TABLE]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TABLA", TABLA);
            cmd.Parameters.AddWithValue("@VALOR", VALOR);
            cmd.Parameters.AddWithValue("@VALOR_FILTRO", ID_VALOR);
            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }

        public DataSet INSERTVALUESTM(string TABLA, string VALOR)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[P_INSERT_TABLE]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TABLA", TABLA);
            cmd.Parameters.AddWithValue("@VALOR_FILTRO", VALOR);

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }

        public DataSet DELETETM(string TABLA, string ID)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[P_DELETE_TABLE]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TABLA", TABLA);
            cmd.Parameters.AddWithValue("@VALOR_FILTRO", ID);

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }
        public DataSet CONSULTAMODIFICAR(int TIPO,int PROCESO)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_CONSULTA_EDITARINDICADOR]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TIPOINDICADOR", TIPO);
            cmd.Parameters.AddWithValue("@PROCESO", PROCESO);
            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }

        public DataSet INSERTNOTASCUALITATIVAS(int IDINDICADOR, int IDPERIODO, string NOTA)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[P_SAVE_ANALISIC]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDINDICADOR", IDINDICADOR);
            cmd.Parameters.AddWithValue("@IDPERIODO", IDPERIODO);
            cmd.Parameters.AddWithValue("@NOTAS", NOTA);

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }
        public DataSet DELETEINDICADOR(int ID)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_DELETE_INDICADORES]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDINDICADOR", ID);


            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }

        public DataSet DELETEINDICADORMOD(int ID)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_DELETE_INDICADORES_MODIFICADOS]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDINDICADOR", ID);


            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }


        public DataSet DELETEINDICADORROL(int ID)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_DELETE_INDICADORES_ROL]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ID_ROLESIN", ID);


            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }

        public DataSet CREATEDOCUMENTOS(string RUTA, int ID)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[P_INSERT_DOCUMENTO]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@RUTA", RUTA);
            cmd.Parameters.AddWithValue("@IDINDICADOR", ID);

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }
        public DataSet DEVALUAREXPRESION(string EXPRESION)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_VALIDAR_SINTAXIS]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EXPRESION", EXPRESION);
            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }

        public DataSet CONSULTADOCUMENTOS(int IDTIPOINDICADOR)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_CONSULTADOCUMENTOS]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDTINDICADOR", IDTIPOINDICADOR);
            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al realizar Consulta ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }
        public DataSet CREARVARIABLEPORINDICADOR(int IDINDICADOR)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_UPDATE_VAR_INDI]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDINDICADOR", IDINDICADOR);
            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al realizar Consulta ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }

        public DataSet UPDATEFORMULA(int IDINDICADOR, int IDPERIODO, int IDSECCIONAL, float VALOR, int IDVARIABLE)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[P_SAVE_DATOS]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ID_INDICADOR", IDINDICADOR);
            cmd.Parameters.AddWithValue("@ID_PERIODICIDAD", IDPERIODO);
            cmd.Parameters.AddWithValue("@IDSECCIONAL", IDSECCIONAL);
            cmd.Parameters.AddWithValue("@VALOR", VALOR);
            cmd.Parameters.AddWithValue("@IDVARIABLE", IDVARIABLE);


            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al realizar Consulta ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }


        public DataSet UPDATEFORMULA1(int IDINDICADOR, int IDPERIODO)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[P_FORMULA_RESULTADO_SECCIONAL]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ID_INDICADOR", IDINDICADOR);
            cmd.Parameters.AddWithValue("@PERIODO", IDPERIODO);

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al realizar Consulta ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }
        public DataSet UPDATEFORMULA2(int IDINDICADOR, int IDPERIODO)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[P_FORMULA_RESULTADO_REGIONAL]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ID_INDICADOR", IDINDICADOR);
            cmd.Parameters.AddWithValue("@PERIODO", IDPERIODO);


            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al realizar Consulta ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }

        public DataSet UPDATEFORMULANORMAL(int IDINDICADOR, int IDPERIODO)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[P_FORMULA_RESULTADO_NORMAL]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ID_INDICADOR", IDINDICADOR);
            cmd.Parameters.AddWithValue("@MESAP", IDPERIODO);


            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al realizar Consulta ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }
        public DataSet UPDATEFORMULANACIONAL(int IDINDICADOR, int IDPERIODO)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[P_FORMULA_RESULTADO_NACIONAL]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ID_INDICADOR", IDINDICADOR);
            cmd.Parameters.AddWithValue("@PERIODO", IDPERIODO);


            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al realizar Consulta ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }



        public DataSet UPDATECUMPLIMIENTO(int IDINDICADOR, int IDPERIODO)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[P_FORMULA_CUMPLIMIENTO]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ID_INDICADOR", IDINDICADOR);
            cmd.Parameters.AddWithValue("@ID_PERIODO", IDPERIODO);
            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al realizar Consulta ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }


        public DataSet UPDATECUMPLIMIENTOR(int IDINDICADOR, int IDPERIODO)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[P_FORMULA_CUMPLIMIENTO_REGIONAL]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ID_INDICADOR", IDINDICADOR);
            cmd.Parameters.AddWithValue("@ID_PERIODO", IDPERIODO);
            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al realizar Consulta ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }


        public DataSet UPDATECUMPLIMIENTOS(int IDINDICADOR, int IDPERIODO)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[P_FORMULA_CUMPLIMIENTO_SECCIONAL]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ID_INDICADOR", IDINDICADOR);
            cmd.Parameters.AddWithValue("@ID_PERIODO", IDPERIODO);
            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al realizar Consulta ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }




        public DataSet CONSULTARNACIONAL(int IDINDICADOR)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_CONSULTAR_NACIONAL]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDINDICADOR", IDINDICADOR);

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }

        public DataSet CONSULTARUSUARIOS()
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_CONSULTAR_RESPONSABLES]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }

        public DataSet CONSULTA_VAR_INDICADOR(int IDINDICADOR,int PERIODO,int SECCIONAL)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_VAR_INDICADOR]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDINDICADOR", IDINDICADOR);
            cmd.Parameters.AddWithValue("@PERIODO", PERIODO);
            cmd.Parameters.AddWithValue("@IDSECCIONAL", SECCIONAL);

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }


        public DataSet CONSULTA_LIMITE(int IDINDICADOR,double PEOR,double MEJOR,double ALCANCE,double ROJA)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_CONSULTA_LIMITE]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDINDICADOR", IDINDICADOR);
            cmd.Parameters.AddWithValue("@PEOR", PEOR);
            cmd.Parameters.AddWithValue("@MEJOR", MEJOR);
            cmd.Parameters.AddWithValue("@ALCANCE", ALCANCE);
            cmd.Parameters.AddWithValue("@ROJA", ROJA);

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;


        }
        public DataSet CONSULTARESULTADOS(int IDINDICADOR, int REGIONAL, int SECCIONAL, int PERIODO)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_CONSULTARESULTADO]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDINDICADOR", IDINDICADOR);
            cmd.Parameters.AddWithValue("@REGIONAL", REGIONAL);
            cmd.Parameters.AddWithValue("@SECCIONAL", SECCIONAL);
            cmd.Parameters.AddWithValue("@PERIODO", PERIODO);

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }

        public DataSet UPDATERESULTADOS(int IDINDICADOR, int REGIONAL, int SECCIONAL, int PERIODO, double RESULTADO, double META)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("[dbo].[PA_UPDATE_RESULTADO]", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDINDICADOR", IDINDICADOR);
            cmd.Parameters.AddWithValue("@REGIONAL", REGIONAL);
            cmd.Parameters.AddWithValue("@SECCIONAL", SECCIONAL);
            cmd.Parameters.AddWithValue("@PERIODO", PERIODO);
            cmd.Parameters.AddWithValue("@RESULTADO", RESULTADO);
            cmd.Parameters.AddWithValue("@META", META);

            try
            {
                AbrirConexion();
                da.SelectCommand = cmd;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception("Error, al leer Personal ", e);
            }
            finally
            {
                CerrarConexion();
                cmd.Dispose();
            }
            return ds;
        }


    }

}
