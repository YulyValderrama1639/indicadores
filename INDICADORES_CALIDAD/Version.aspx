﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Version.aspx.cs" Inherits="INDICADORES_CALIDAD.Version" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
   
<body>
    <form id="form1" runat="server">
             <link href="Content/StyleCustom.css" rel="stylesheet" />
        <asp:ScriptManager runat="server"></asp:ScriptManager>
          <div class="col-sm-12" runat="server" id="DivAlert">
            <div class="DivGeneral DivColor">
                 <asp:Image runat="server" ImageUrl="~/images/undo.png" />
                <asp:Label Text="INDICADORES MODIFICADOS" Font-Bold="true" Font-Size="20pt" runat="server" Font-Names="Arial Narrow"></asp:Label>
                          
            </div>

        </div>

        <div class="Contencheckin" style="align-content: center; text-align: center">

            <telerik:RadGrid runat="server" ID="Gversion" AutoGenerateColumns="false" Skin="Bootstrap">
                <MasterTableView>
                    <Columns>
                         <telerik:GridBoundColumn DataField="FECHA_CREACION" HeaderText="FECHA CREACIÓN">
                            <HeaderStyle Font-Bold="true" Font-Size="8pt" />
                            <ItemStyle Font-Size="8pt" ForeColor="#003366" Font-Bold="true" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="FECHAMODIFICA" HeaderText="FECHA DE MODIFICACIÓN">
                            <HeaderStyle Font-Bold="true" Font-Size="8pt"  />
                              <ItemStyle Font-Size="8pt" ForeColor="#ff0000" Font-Bold="true" />
                        </telerik:GridBoundColumn>
                           <telerik:GridBoundColumn DataField="DESCRIPCIONFORMULA" HeaderText="FORMULA ACTUAL">
                            <HeaderStyle Font-Bold="true" Font-Size="8pt" />
                             <ItemStyle Font-Size="8pt" ForeColor="#ff0000" Font-Bold="true" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="FORMULA" HeaderText="FORMULA ANTERIOR">
                            <HeaderStyle Font-Bold="true" Font-Size="8pt" />
                            <ItemStyle Font-Size="8pt" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="VERAC" HeaderText="UNIDAD MEDIDA VERSIÓN ACTUAL">
                            <HeaderStyle Font-Bold="true" Font-Size="8pt" />
                               <ItemStyle Font-Size="8pt" ForeColor="#ff0000" Font-Bold="true" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="VERAN" HeaderText="UNIDAD MEDIDA VERSIÓN ANTERIOR">
                            <HeaderStyle Font-Bold="true" Font-Size="8pt" />
                            <ItemStyle Font-Size="8pt" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="PERAC" HeaderText="PERIODO ACTUAL">
                            <HeaderStyle Font-Bold="true" Font-Size="8pt" />
                                <ItemStyle Font-Size="8pt" ForeColor="#ff0000" Font-Bold="true" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="PERAN" HeaderText="PERIODO ANTERIOR">
                            <HeaderStyle Font-Bold="true" Font-Size="8pt" />
                            <ItemStyle Font-Size="8pt" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="VERAAC" HeaderText="PERIODICIDAD ANÁLISIS ACTUAL">
                            <HeaderStyle Font-Bold="true" Font-Size="8pt" />
                                <ItemStyle Font-Size="8pt" ForeColor="#ff0000" Font-Bold="true" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="VERAAN" HeaderText="PERIODICIDAD ANÁLISIS ANTERIOR">
                            <HeaderStyle Font-Bold="true" Font-Size="8pt" />
                            <ItemStyle Font-Size="8pt" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="METAAC" HeaderText="META ACTUAL">
                            <HeaderStyle Font-Bold="true" Font-Size="8pt" />
                                <ItemStyle Font-Size="8pt" ForeColor="#ff0000" Font-Bold="true" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="METAA" HeaderText="META ANTERIOR">
                            <HeaderStyle Font-Bold="true" Font-Size="8pt" />
                            <ItemStyle Font-Size="8pt" />
                        </telerik:GridBoundColumn>
                     
                       
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
        </div>
    </form>


</body>
</html>
