﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CNegocios;
using System.Data.Entity;
using Telerik.Web.UI;

using System.Data.SqlClient;
using System.Configuration;

namespace INDICADORES_CALIDAD
{
    public partial class Analisis : System.Web.UI.Page
    {
        public SqlConnection Conexion;
        public IndicadoresBussines ObjGIndicadores = new IndicadoresBussines();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CargarCombosMaestros("T_PERIODICIDAD", 1, 2, 2, CmbVigencia);
                string Valor = Request.QueryString["ID"];
                Session["ID_INDICADOR"] = Valor;


                DataSet ds = new DataSet();
                ds = ObjGIndicadores.DACONSULTARNACIONAL(Convert.ToInt32(Valor));
                if (ds.Tables[0].Rows.Count > 0)
                {
                    
                    CargarCombosMaestros("T_PERIODICIDAD", 1, 2, 2, CmbVigencia);
                    CmbVigencia.SelectedValue = ds.Tables[0].Rows[0]["ID_PERIODICIDAD"].ToString();
                    CmbVigencia.Enabled = false;
                    CmbPeriodo.DataSource = ObjGIndicadores.DCONSULTATIPO(Convert.ToInt32(CmbVigencia.SelectedValue));
                    CmbPeriodo.DataValueField = "ID_DET_PERIODO";
                    CmbPeriodo.DataTextField = "PERIODO";
                    CmbPeriodo.DataBind();
                }
            }
            string Valorrr = Request.QueryString["ID_INDICADOR"];
            Session["ID_INDICADOR"] = Valorrr;

        }

        private void CargarCombosMaestros(string Tabla, int campo1, int campo2, int orden, RadComboBox Combo, int? campo3 = null, int? campo4 = null)
        {
            Conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["SCORBOARD"].ConnectionString);
            SqlCommand cmd = new SqlCommand("P_CARGA_COMBOS", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TABLA", Tabla);
            cmd.Parameters.AddWithValue("@CAMPO1", campo1);
            cmd.Parameters.AddWithValue("@CAMPO2", campo2);
            cmd.Parameters.AddWithValue("@ORDEN", orden);
            cmd.Parameters.AddWithValue("@CAMPO_FILTRO", campo3);
            cmd.Parameters.AddWithValue("@VALOR_FILTRO", campo4);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {

                da.Fill(ds);
                Combo.DataSource = ds;
                Combo.DataValueField = "1";
                Combo.DataTextField = "2";
                Combo.DataBind();

            }
            catch (Exception ex)
            {

            }

        }
        protected void CmbVigencia_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            CmbPeriodo.DataSource = ObjGIndicadores.DCONSULTATIPO(Convert.ToInt32(CmbVigencia.SelectedValue));
            CmbPeriodo.DataValueField = "ID_DET_PERIODO";
            CmbPeriodo.DataTextField = "PERIODO";
            CmbPeriodo.DataBind();
        }     
      
        protected void btn_crear_Click(object sender, EventArgs e)
        {
           

            ObjGIndicadores.DINSERTARNOTAINDICADOR(Convert.ToInt32(Session["ID_INDICADOR"]),Convert.ToInt32(CmbPeriodo.SelectedValue),TxtAnalisis.Text);
            TxtAnalisis.Text = "";
            DResultado.Visible = true;
            LblGuardado.Text = "!El análisis ha sido guardado correctamente!";


        }

        protected void btn_eliminar_Click(object sender, EventArgs e)
        {
            CmbPeriodo.Text = "";
            CmbVigencia.Text = "";
            CmbPeriodo.SelectedValue = "0";
            CmbVigencia.SelectedValue = "0";
            TxtAnalisis.Text = "";
            DResultado.Visible = false;


        }
    }
}