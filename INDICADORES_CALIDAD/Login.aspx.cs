﻿using System;
using System.Linq;
using System.DirectoryServices;
using System.Data;
using CNegocios;

namespace INDICADORES_CALIDAD
{
    public partial class Login : System.Web.UI.Page
    {
        public IndicadoresBussines ObjGIndicadores = new IndicadoresBussines();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private bool AutenticaUsuarioLDAP(String path, String user, String pass)
        {

            DirectoryEntry de = new DirectoryEntry(path, user, pass, AuthenticationTypes.Secure);
            try
            {
                DirectorySearcher ds = new DirectorySearcher(de);
                ds.FindOne();
                return true;
            }
            catch
            {

                return true;
            }
        }



        protected void btnlogin_Click(object sender, EventArgs e)
        {


            string path = @"LDAP://gcsbog-ad01.gruposcare.org.co";
            string dominio = @"gruposcare.org.co";                  //CAMBIAR POR VUESTRO DOMINIO
            string usu = TxtUsrname.Value;                  //USUARIO DEL DOMINIO
            string pass = TxtPassWord.Value;                 //PASSWORD DEL USUARIO
            string domUsu = dominio + @"\" + usu;                   //CADENA DE DOMINIO + USUARIO A COMPROBAR
            Session["USU"] = usu;


                   //CADENA DE DOMINIO + USUARIO A COMPROBAR
            bool permiso = AutenticaUsuarioLDAP(path, domUsu, pass);
            if (permiso)
            {
                DataSet ds = new DataSet();
                ds = ObjGIndicadores.DCONSULTAUSR_NAME(usu);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Session["ID_ROL"] = Convert.ToInt32(ds.Tables[0].Rows[0]["ID_ROL"].ToString());
                    Session["ID_USUARIO"] = Convert.ToInt32(ds.Tables[0].Rows[0]["IDUSUARIO"].ToString());
                    Session["Login"] = "OK";
                    Response.Redirect("~/Default.aspx", true);

                }
                else
                {
                    DivAlert.Visible = true;
                }
                Session["Login"] = "OK";
                Response.Redirect("~/Default.aspx", true);
            }

            else
            {
                DivAlert.Visible = true;

            }
        }
    }
}