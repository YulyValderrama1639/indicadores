﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CNegocios;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Telerik.Web.UI;
using CEntidades;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Text;

namespace INDICADORES_CALIDAD
{
    public partial class CreateIndicator : System.Web.UI.Page
    {
        public SqlConnection Conexion;
        public IndicadoresBussines ObjGIndicadores = new IndicadoresBussines();
        public IndicadoresEntities ObjGIndicadoresE = new IndicadoresEntities();
        public NEnvioCorreo correo = new NEnvioCorreo();
        public EEnvioCorreo ObjEntEnvioCorreo = new EEnvioCorreo();
        public string FECHA_CREACION;
        public string FECHA_SISTEMA;
        public string NOMBRE_INDICADOR;
        public string FORMULA;
        public string META;
        public string NOMBRE_PERODICIDADA;
        public string NOMBRE_PERODICIDADB;
        public string PROPOSITO_INDICADOR;
        public string PESO;
        public string UNIDAD;
        public string DES_ALCANCE;
        public string DES_CLASIFICACION;
        public string DESTIPOME;
        public string ETIQUETA;
        private string TIPOINDICADOR;
        private string PROCESO;
        private string NOMBRE_VARIABLE;
        private string FUENTE;
        private string RESPONSABLE;
        private string INDUCTOR;
        private int ID_INDICADOR;


        DataSet ObjIndicadoresds = new DataSet();
        public int ItemCargado = 1;
        protected void Page_Load(object sender, EventArgs e)
        {


            if (!Page.IsPostBack)
            {
                CargarCombosMaestros("T_PERIODICIDAD", 1, 2, 2, Cmbperiodicidad);
                CargarCombosMaestros("T_UNIDAD", 1, 2, 2, CmbUnidad);
                CargarCombosMaestros("T_TIPOINDICADOR", 1, 2, 2, CmbTipoIndicador);
                CargarCombosMaestros("T_PROCESO", 1, 2, 2, CmbProceso,3,1);
                CargarCombosMaestros("T_ETIQUETAS", 1, 2, 2, CmbTendenciaI);
                CargarCombosMaestros("T_FUENTE", 1, 2, 2, CmbFuente);
                CargarCombosMaestros("T_ROLES", 1, 2, 2, CmbRol);
                CargarCombosMaestros("T_CLASIFICACION", 1, 2, 2, CmbClasificacion);
                CargarCombosMaestros("T_EMPRESA", 1, 2, 2, CmbEmpresa);
                CargarCombosMaestros("T_PERIODO_ANALISI", 1, 2, 2, Cmbperiodicidada);
                CargarCombosMaestros("T_ALCANCE", 1, 2, 2, CmbAlcance,3,2);
                CargarCombosMaestros("T_TIPO_MEDICION", 1, 2, 2, CmtipoMedicion);
                CargarCombosMaestros("T_TIPO_DATO", 1, 2, 2, CmbTipoDato);
                CargarCombosMaestros("T_CALCULO", 1, 2, 2, CmbLimite);
                CmbUsuario.DataSource = ObjGIndicadores.DCONSULTARUSUARIOS();
                CmbUsuario.DataValueField = "IDENTIFICACION";
                CmbUsuario.DataTextField = "NOMBRERESPONSABLE";
                CmbUsuario.DataBind();



            }

        }

        private void CargarCombosMaestros(string Tabla, int campo1, int campo2, int orden, RadComboBox Combo, int? campo3 = null, int? campo4 = null)
        {
            Conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["SCORBOARD"].ConnectionString);
            SqlCommand cmd = new SqlCommand("P_CARGA_COMBOS", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TABLA", Tabla);
            cmd.Parameters.AddWithValue("@CAMPO1", campo1);
            cmd.Parameters.AddWithValue("@CAMPO2", campo2);
            cmd.Parameters.AddWithValue("@ORDEN", orden);
            cmd.Parameters.AddWithValue("@CAMPO_FILTRO", campo3);
            cmd.Parameters.AddWithValue("@VALOR_FILTRO", campo4);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {

                da.Fill(ds);
                Combo.DataSource = ds;
                Combo.DataValueField = "1";
                Combo.DataTextField = "2";
                Combo.DataBind();

            }
            catch (Exception ex)
            {

            }

        }


        protected void btn_agregar_Click(object sender, EventArgs e)
        {
            int nactualizar;

            if (TxtNombreVariable.Text == "")
            {
                Lblvariablesc.Text = "Debe ingresar nombre de la variable";
                Lblvariablesc.Visible = true;
                DivErrorC.Visible = true;
                return;
            }

            if (CmbFuente.SelectedValue == "" || CmbFuente.SelectedValue == "0")
            {
                Lblvariablesc.Text = "Debe seleccionar la fuente de datos";
                Lblvariablesc.Visible = true;
                DivErrorC.Visible = true;

                return;
            }

            if (CmbTipoDato.SelectedValue == "" || CmbTipoDato.SelectedValue == "0")
            {
                Lblvariablesc.Text = "Debe seleccionar la tipo de datos";
                Lblvariablesc.Visible = true;
                DivErrorC.Visible = true;
                return;
            }


            nactualizar = ObjGIndicadores.DVARIABLES(Convert.ToInt32(Session["IDINDICADOR"]), TxtNombreVariable.Text, Convert.ToInt32(CmbFuente.SelectedValue), Convert.ToInt32(CmbTipoDato.SelectedValue), txtObservacion.Text);
            if (nactualizar == -1)
            {
                CmbTipoDato.SelectedValue = "0";
                TxtNombreVariable.Text = "";
                CmbTipoDato.Text = "";
                CmbFuente.SelectedValue = "0";
                CmbFuente.Text = "";
                txtObservacion.Text = "";
                DivErrorC.Visible = false;
                GrvVariables.Visible = true;
                GrvVariables.Rebind();
            }
            else
            {
                CmbTipoDato.SelectedValue = "0";
                TxtNombreVariable.Text = "";
                CmbTipoDato.Text = "";
                CmbFuente.SelectedValue = "0";
                CmbFuente.Text = "";
                txtObservacion.Text = "";
                DivErrorC.Visible = false;
                GrvVariables.Visible = true;
                GrvVariables.Rebind();


            }
        }
        protected void btn_guardar_Click(object sender, EventArgs e)
        {
            bool resultado;
            bool resultado2;
            resultado = validarindicadores();
            if (resultado)
            {
                DivError.Visible = false;
                VarObjIndicaores();
                int nactualizar;
                nactualizar = ObjGIndicadores.INSERTARINDICADOR(ObjGIndicadoresE);

                Session["IDINDICADOR"] = nactualizar;
                enabledcampos();

                RadWizardStep2.Enabled = true;
                DataSet ds = new DataSet();
                TxtPeor.Text = "0";
                TxtMejor.Text = "0";
                TxtAlcance.Text = "0";
                TxtRoja.Text = "0";


                if (Convert.ToInt32(CmbLimite.SelectedValue) == 3)
                {
                    lblRojo.Visible = false;
                    lblpeor.Text = "Limite superior";
                    Lblalcance.Text = "Limite inferior";
                    lblmejor.Visible = false;
                    lblpeor.Visible = true;
                    Lblalcance.Visible = true;
                    TxtAlcance.Visible = true;
                    TxtPeor.Visible = true;
                    TxtRoja.Visible = false;
                    TxtMejor.Visible = false;
                }

                
                if (Convert.ToInt32(CmbLimite.SelectedValue) == 1 || CmbLimite.SelectedValue == "2")
                {
                    ds = ObjGIndicadores.DCONSULTARLIMITE(Convert.ToInt32(Session["IDINDICADOR"]), Convert.ToDouble(TxtPeor.Text), Convert.ToDouble(TxtMejor.Text), Convert.ToDouble(TxtAlcance.Text), Convert.ToDouble(TxtRoja.Text));
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        TxtAlcance.Text = ds.Tables[0].Rows[0]["ALCANCE"].ToString();
                        TxtPeor.Text = ds.Tables[0].Rows[0]["PEOR"].ToString();
                        TxtRoja.Text = ds.Tables[0].Rows[0]["ROJA"].ToString();
                        TxtMejor.Text = ds.Tables[0].Rows[0]["MEJOR"].ToString();
                        lblMeta.Text = ds.Tables[0].Rows[0]["META"].ToString();
                    }
                }
            }


        }

        private void enabledcampos()
        {

            CmbAlcance.Enabled = false;
            btn_actualizar.Visible = true;
            btn_guardar.Visible = false;

        }

        private void enabledcampos2()
        {
            TxtNombre.Enabled = true;
            CmbTipoIndicador.Enabled = true;
            CmbClasificacion.Enabled = true;
            CmbEmpresa.Enabled = true;
            radFechainic.Enabled = true;
            Cmbperspectiva.Enabled = true;
            cmbinductor.Enabled = true;
            CmbProceso.Enabled = true;
            Cmbperiodicidad.Enabled = true;
            CmbUnidad.Enabled = true;
            Cmbperiodicidada.Enabled = true;
            CmbAlcance.Enabled = true;
            CmtipoMedicion.Enabled = true;
            TxtFormula.Enabled = true;


            TxtMeta.Enabled = true;
            CmbTendenciaI.Enabled = true;
            //Fl_SubirArchivo1.Enabled = true;
            btn_guardar.Visible = true;

        }


        private bool validarindicadores()
        {
            if (TxtNombre.Text == "")
            {
                LblValidar.Text = "Debe ingresar nombre del indicador";
                LblValidar.Visible = true;
                DivError.Visible = true;
                return false;
            }

            if (radFechainic.SelectedDate == null)
            {
                LblValidar.Text = "Debe seleccionar fecha de inicio";
                LblValidar.Visible = true;
                DivError.Visible = true;
                return false;

            }

            if (CmbUnidad.SelectedValue == "")
            {
                LblValidar.Text = "Debe seleccionar la unidad de medida";
                LblValidar.Visible = true;
                DivError.Visible = true;
                return false;
            }

            if (Cmbperiodicidad.SelectedValue == "")
            {
                LblValidar.Text = "Debe seleccionar la periodicidad";
                LblValidar.Visible = true;
                DivError.Visible = true;
                return false;
            }



            if (CmbTipoIndicador.SelectedValue == "")
            {
                LblValidar.Text = "Debe seleccionar el tipo de indicador";
                LblValidar.Visible = true;
                DivError.Visible = true;
                return false;
            }


            if (CmbEmpresa.SelectedValue == "1")
            {

                if (CmbProceso.SelectedValue == "")
                {
                    LblValidar.Text = "Debe seleccionar el proceso!";
                    LblValidar.Visible = true;
                    DivError.Visible = true;

                    return false;
                }

            }



            if (CmbTendenciaI.SelectedValue == "")
            {
                LblValidar.Text = "Debe seleccionar la tendencia";
                LblValidar.Visible = true;
                DivError.Visible = true;
                return false;
            }

            if (TxtMeta.Text == "")
            {
                LblValidar.Text = "Debe ingresar la meta";
                LblValidar.Visible = true;
                DivError.Visible = true;
                return false;
            }

            if (CmbAlcance.SelectedValue == "")
            {
                LblValidar.Text = "Debe seleccionar el alcance";
                LblValidar.Visible = true;
                DivError.Visible = true;
                return false;
            }
            if (Cmbperiodicidada.SelectedValue == "")
            {
                LblValidar.Text = "Debe seleccionar la periodicidad de analisis";
                LblValidar.Visible = true;
                DivError.Visible = true;
                return false;
            }
            if (CmtipoMedicion.SelectedValue == "")
            {
                LblValidar.Text = "Debe seleccionar tipo de medíción";
                LblValidar.Visible = true;
                DivError.Visible = true;
                return false;
            }
            if (TxtFormula.Text == "")
            {
                LblValidar.Text = "Debe ingresar la formula";
                LblValidar.Visible = true;
                DivError.Visible = true;
                return false;
            }


            return true;
        }







        protected void VarObjIndicaores()
        {
            ObjGIndicadoresE.IDINDICADOR = Convert.ToInt32(Session["IDINDICADOR"]);
            ObjGIndicadoresE.USUARIO = Convert.ToString(Session["USUARIO"]);
            ObjGIndicadoresE.IDPERIODICIDAD = Convert.ToInt32(Cmbperiodicidad.SelectedValue);

            ObjGIndicadoresE.IDUNIDAD = Convert.ToInt32(CmbUnidad.SelectedValue);

            ObjGIndicadoresE.FECHAC = Convert.ToDateTime(radFechainic.SelectedDate);

            ObjGIndicadoresE.TIPOINDICADOR = Convert.ToInt32(CmbTipoIndicador.SelectedValue);
            if (CmbProceso.SelectedValue == "")
            {
                ObjGIndicadoresE.IDPROCESO = 18;
            }
            else
            {
                ObjGIndicadoresE.IDPROCESO = Convert.ToInt32(CmbProceso.SelectedValue);
            }

            ObjGIndicadoresE.NOMBREINDICADOR = TxtNombre.Text;
            ObjGIndicadoresE.PROPOSITOINDICADOR = TxtProposito.Text;
            ObjGIndicadoresE.FORMULA = TxtFormula.Text;
            ObjGIndicadoresE.IDTENDENCIA = Convert.ToInt32(CmbTendenciaI.SelectedValue);
            if (cmbinductor.SelectedValue == "")
            {
                ObjGIndicadoresE.IDINDUCTOR = 13;
            }
            else
            {
                ObjGIndicadoresE.IDINDUCTOR = Convert.ToInt32(cmbinductor.SelectedValue);
            }

            if (Cmbperspectiva.SelectedValue == "")
            {
                ObjGIndicadoresE.IDPERSPECTIVA = 5;
            }
            else
            {
                ObjGIndicadoresE.IDPERSPECTIVA = Convert.ToInt32(Cmbperspectiva.SelectedValue);
            }


            if (CmbClasificacion.SelectedValue == "")
            {
                ObjGIndicadoresE.IDCLASIFICACION = 4;
            }
            else
            {
                ObjGIndicadoresE.IDCLASIFICACION = Convert.ToInt32(CmbClasificacion.SelectedValue);
            }
            if (CmbEmpresa.SelectedValue == "")
            {
                ObjGIndicadoresE.IDEMPRESA = 5;
            }
            else
            {
                ObjGIndicadoresE.IDEMPRESA = Convert.ToInt32(CmbEmpresa.SelectedValue);
            }

            if (CmbLimite.SelectedValue == "")
            {
                ObjGIndicadoresE.CALCULADO = 0;
            }
            else
            {
                ObjGIndicadoresE.CALCULADO = Convert.ToInt32(CmbLimite.SelectedValue);
            }


            ObjGIndicadoresE.META = (TxtMeta.Text.Replace(",", "."));
            ObjGIndicadoresE.CUMPLIMIENTO = "0";
            ObjGIndicadoresE.PORCENTAJEC = "0";
            ObjGIndicadoresE.IDTIPOMEDICION = Convert.ToInt32(CmtipoMedicion.SelectedValue);
            ObjGIndicadoresE.IDALCANCE = Convert.ToInt32(CmbAlcance.SelectedValue);
            ObjGIndicadoresE.IDPERANALISI = Convert.ToInt32(Cmbperiodicidada.SelectedValue);

        }

        protected void CmbTipoIndicador_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {

            if (CmbTipoIndicador.SelectedValue == "1" && CmbEmpresa.SelectedValue == "1")
            {
                Cmbperspectiva.Visible = true;
                CargarCombosMaestros("T_PERSPECTIVA", 1, 2, 2, Cmbperspectiva);
                lblperspectiva.Visible = true;
                LblInductor.Visible = true;
                cmbinductor.Visible = true;


            }
            else
            {
                Cmbperspectiva.Visible = false;
                lblperspectiva.Visible = false;
                LblInductor.Visible = false;
                cmbinductor.Visible = false;
            }
        }

        private void Limpiar()
        {

            CmbProceso.SelectedValue = "0";
            CmbProceso.Text = "";
            Cmbperiodicidad.SelectedValue = "0";
            Cmbperiodicidad.Text = "";
            CmbUnidad.SelectedValue = "0";
            CmbUnidad.Text = "";
            CmbTipoIndicador.SelectedValue = "0";
            CmbTipoIndicador.Text = "";
            TxtNombre.Text = "";
            TxtNombre.Text = "";
            TxtFormula.Text = "";
            CmbTendenciaI.SelectedValue = "0";
            CmbTendenciaI.Text = "";
            Cmbperspectiva.SelectedValue = "0";
            Cmbperspectiva.Text = "";
            CmbFuente.SelectedValue = "0";
            CmbFuente.Text = "";
            TxtMeta.Text = "";
            TxtNombreVariable.Text = "";
            GrvVariables.DataSource = "";
            GrvVariables.Visible = false;
            Gvrolesu.Visible = false;
            CmbRol.SelectedValue = "0";
            CmbRol.Text = "";
            CmbUsuario.SelectedValue = "0";
            CmbUsuario.Text = "";
            CmbAlcance.SelectedValue = "0";
            CmbAlcance.Text = "";
            CmbEmpresa.SelectedValue = "0";
            CmbEmpresa.Text = "";
            Cmbperiodicidad.SelectedValue = "0";
            Cmbperiodicidad.Text = "";
            radFechainic.SelectedDate = null;
            CmbClasificacion.SelectedValue = "0";
            CmbClasificacion.Text = "";
            Cmbperspectiva.Visible = false;
            cmbinductor.SelectedValue = "0";
            cmbinductor.Text = "";
            cmbinductor.Visible = false;
            CmtipoMedicion.SelectedValue = "0";
            CmtipoMedicion.Text = "";
            Cmbperiodicidada.SelectedValue = "0";
            Cmbperiodicidada.Text = "";
            lblperspectiva.Visible = false;
            LblInductor.Visible = false;
            TxtVariables.Text = "";
            TxtFormula.Enabled = true;
            btnagregar.Enabled = true;
            DivError.Visible = false;
            CmbLimite.SelectedValue = "0";
            CmbLimite.Text = "";
            TxtMejor.Text = "";
            TxtPeor.Text = "";
            TxtAlcance.Text = "";
            TxtRoja.Text = "";
            DivResultado.Visible = false;
            TxtProposito.Text = "";

        }

        private void EnviarCorreo(string ruta)
        {
            try
            {

                NEnvioCorreo correo = new NEnvioCorreo();
                ObjEntEnvioCorreo.Para1 = Convert.ToString(Session["CorreoAdmin"]);
                 ObjEntEnvioCorreo.Asunto = "INDICADORES CALIDAD";
                Session["MensajeCorreo"] = @"<html><body> <div> "
                            + "Reciba un cordial saludo" + "</b>.<br><br>"
                            + "Se registro correctamente el siguiente indicador " + NOMBRE_INDICADOR + ".<br><br>"
                            + "Se adjunta el documento  en donde esta el detalle del mismo.<br><br>"
                            + "Para acceder a la plataforma y administrar los datos del indicador debe ir al siguiente link: "
                            + "<a href=http://gcsbog-dev00.gruposcare.org.co:82/Login target=_blank>INDICADORES CALIDAD S.C.A.R.E.</a> <br><br>"
                            + "</div>"
                            + "</body></html>";
                ObjEntEnvioCorreo.Mensaje = Convert.ToString(Session["MensajeCorreo"]);
                ObjEntEnvioCorreo.Adjunto = ruta;
                string textoCorreo = correo.EnvioCorreo(ObjEntEnvioCorreo);

                if (textoCorreo != "OK")
                {
                    lblMensaje.Text = textoCorreo;
                    lblMensaje.Visible = true;
                    lblMensaje.ForeColor = System.Drawing.Color.Red;
                }
            }
            catch (Exception e)
            {
                lblMensaje.Text = "Error al enviar el correo. " + e;
                lblMensaje.Visible = true;
                lblMensaje.ForeColor = System.Drawing.Color.Red;
            }
        }




        protected void RadCrearindicador_FinishButtonClick(object sender, WizardEventArgs e)
        {

            DataSet ds = new DataSet();
            //VALIDAR SINTAXIS 
            ds = ObjGIndicadores.DEVALUARSINTAXIS(TxtVariables.Text);
            if (ds.Tables[0].Rows.Count > 0)
            {
                bool resultado = Convert.ToBoolean(ds.Tables[0].Rows[0][0].ToString());
                if (resultado == true)
                {
                    DataSet ds1 = new DataSet();
                    ds1 = ObjGIndicadores.DACTUALIZARFSQL(Convert.ToInt32(Session["IDINDICADOR"]), TxtVariables.Text);

                    LblValidarf.Text = "Sintaxis Valida";
                    LblValidarf.ForeColor = System.Drawing.Color.Green;
                    LblValidarf.Visible = true;
                    TxtFormula.Enabled = false;
                    btnagregar.Enabled = false;

                }
                else
                {
                    LblValidarf.Text = "Sintaxis invalidad";
                    LblValidarf.ForeColor = System.Drawing.Color.Red;
                    LblValidarf.Visible = true;
                }

                ObjGIndicadores.DCREARVARINDICADOR(Convert.ToInt32(Session["IDINDICADOR"]));

                DataSet ds2 = new DataSet();
                ds2 = ObjGIndicadores.DACONSULTAASINGANCION(Convert.ToInt32(Session["IDINDICADOR"]));
                if (ds2.Tables[0].Rows.Count > 0)
                {

                    for (int i = 0; i < ds2.Tables[0].Rows.Count; ++i)
                    {

                        Session["CorreoAdmin"] = (ds2.Tables[0].Rows[i]["CORREOADMINISTRADOR"]);

                        if( Convert.ToString(Session["CorreoAdmin"])!= "")
                         {
                            string archivo = "IND-" + (ds2.Tables[0].Rows[0]["ID_INDICADOR"]) + ".pdf";
                            string ruta = Server.MapPath("Pdf\\") + archivo;
                            CreatePdf();
                            EnviarCorreo(ruta);
                        }
                        
                    }
                }

                string script1 = "function f(){radopen(null, 'rwBusqueda'); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "customConfirmOpener", script1, true);

                Limpiar();
                enabledcampos2();
            }

            Response.Redirect("Default.aspx");
        }
        protected void btnagregar_Click(object sender, EventArgs e)
        {
            if (CmbUsuario.SelectedValue != "" && CmbRol.SelectedValue != "")
            {
                int nactualizar;

                nactualizar = ObjGIndicadores.CREATEROLESINDICADOR((CmbUsuario.SelectedValue.ToString()), Convert.ToInt32(Session["IDINDICADOR"]), Convert.ToInt32(CmbRol.SelectedValue), false);
                DataSet ds = new DataSet();
                ds = ObjGIndicadores.DACONSULTAASINGANCION(Convert.ToInt32(Session["IDINDICADOR"]));
                Gvrolesu.DataSource = ds;
                Gvrolesu.DataBind();
                Gvrolesu.Visible = true;
                CmbUsuario.SelectedValue = "0";
                CmbUsuario.Text = "";
                CmbRol.SelectedValue = "0";
                CmbRol.Text = "";

            }
            else
            {
                lblMensaje.Text = "Debe seleccionar el rol y el usuario para asignar";
                lblMensaje.ForeColor = System.Drawing.Color.Red;
                Gvrolesu.Visible = false;
            }


        }


        protected void CreatePdf()
        {
            DataSet ds = new DataSet();
            ds = ObjGIndicadores.DCONSULTAPDF(Convert.ToInt32(Session["IDINDICADOR"]));
            if (ds.Tables[0].Rows.Count > 0)
            {


                Session["ID_INDICADOR"] = (Convert.ToInt32(ds.Tables[0].Rows[0]["ID_INDICADOR"]));
                FECHA_CREACION = (ds.Tables[0].Rows[0]["FECHA_CREACION"].ToString());
                FECHA_SISTEMA = (ds.Tables[0].Rows[0]["FECHA_SISTEMA"].ToString());
                NOMBRE_INDICADOR = (ds.Tables[0].Rows[0]["NOMBRE_INDICADOR"].ToString());
                FORMULA = (ds.Tables[0].Rows[0]["FORMULA"].ToString());
                META = (ds.Tables[0].Rows[0]["META"].ToString());
                NOMBRE_PERODICIDADA = (ds.Tables[0].Rows[0]["NOMBRE_PERODICIDAD"].ToString());
                NOMBRE_PERODICIDADB = (ds.Tables[0].Rows[0]["ANALISISPER"].ToString());
                PROPOSITO_INDICADOR = (ds.Tables[0].Rows[0]["PROPOSITO_INDICADOR"].ToString());
                INDUCTOR = (ds.Tables[0].Rows[0]["NOMBRE_INDUCTOR"].ToString());
                UNIDAD = (ds.Tables[0].Rows[0]["UNIDAD_MEDIDA"].ToString());
                DES_ALCANCE = (ds.Tables[0].Rows[0]["DES_ALCANCE"].ToString());
                DES_CLASIFICACION = (ds.Tables[0].Rows[0]["DES_CLASIFICACION"].ToString());
                DESTIPOME = (ds.Tables[0].Rows[0]["DESTIPOME"].ToString());
                ETIQUETA = (ds.Tables[0].Rows[0]["NOMBRE_ETIQUETA"].ToString());
                TIPOINDICADOR = (ds.Tables[0].Rows[0]["NOMBRE_TIPOINDICADOR"].ToString());
                PROCESO = (ds.Tables[0].Rows[0]["NOMBRE_PROCESO"].ToString());
                RESPONSABLE = (ds.Tables[0].Rows[0]["RESPONSABLE"].ToString());

            }


            DataSet ds2 = new DataSet();
            ds2 = ObjGIndicadores.DCONSULTAPDFVAR(Convert.ToInt32(Session["IDINDICADOR"]));
            DataTable dt = ds2.Tables[0];
            if (ds2.Tables[0].Rows.Count > 0)
            {
                StringBuilder builder = new StringBuilder();
                StringBuilder builder2 = new StringBuilder();
                for (int i = 0; i < ds2.Tables[0].Rows.Count; ++i)
                {
                    builder.AppendLine(i + "-" + (ds2.Tables[0].Rows[i]["NOMBRE_VAR"].ToString()));
                    builder2.AppendLine(i + "-" + (ds2.Tables[0].Rows[i]["FUENTE"].ToString()));
                }
                NOMBRE_VARIABLE = builder.ToString();
                FUENTE = builder2.ToString();
                Document documento = new Document();
                Paragraph parrafo = new Paragraph();
                iTextSharp.text.Image hayimagen = default(iTextSharp.text.Image);
                PdfPTable tabla1 = new PdfPTable(3);
                PdfPTable tabla2 = new PdfPTable(3);

                PdfPCell cell;
                string path = Server.MapPath("Pdf");
                string pathimagenes = Server.MapPath("images");
                iTextSharp.text.pdf.PdfWriter pdfw = default(iTextSharp.text.pdf.PdfWriter);

                pdfw = iTextSharp.text.pdf.PdfWriter.GetInstance(documento, new System.IO.FileStream(path + "/IND-" + Session["ID_INDICADOR"] + ".pdf", System.IO.FileMode.Create));
                documento.Open();
                hayimagen = iTextSharp.text.Image.GetInstance(pathimagenes + "/membrete.png");

                hayimagen.SetAbsolutePosition(0, 0);
                hayimagen.ScaleAbsoluteWidth(600);
                hayimagen.ScaleAbsoluteHeight(845);
                documento.Add(hayimagen);

                documento.Add(new Paragraph(" "));
                documento.Add(new Paragraph(" "));
                documento.Add(new Paragraph(" "));

                parrafo.Alignment = Element.ALIGN_CENTER;
                parrafo.Font = FontFactory.GetFont("Arial", 14, Font.BOLD);
                parrafo.Add("INFORME INDICADOR");
                documento.Add(parrafo);
                parrafo.Clear();

                documento.Add(new Paragraph(" "));
                documento.Add(new Paragraph(" "));

                parrafo.Alignment = Element.ALIGN_JUSTIFIED;
                parrafo.Font = FontFactory.GetFont("Arial", 9);
                parrafo.Leading = 10;
                parrafo.Add("Fecha creación en el sistema:");
                parrafo.Add(FECHA_SISTEMA);
                documento.Add(parrafo);
                parrafo.Clear();

                documento.Add(new Paragraph(" "));
                documento.Add(new Paragraph(" "));

                tabla1.DefaultCell.HorizontalAlignment = Element.ALIGN_LEFT;
                tabla1.DefaultCell.BorderColor = BaseColor.BLACK;
                tabla1.DefaultCell.Border = Rectangle.BOX;
                tabla1.DefaultCell.BorderWidth = 1;

                cell = new PdfPCell();
                cell.Colspan = 4;
                cell.Border = 1;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.Phrase = new Phrase("DATOS INDICADOR", new Font(Font.FontFamily.HELVETICA, 12, 1, new BaseColor(System.Drawing.Color.White)));
                cell.BackgroundColor = new BaseColor(System.Drawing.Color.FromArgb(52, 179, 74));
                cell.PaddingBottom = 10;
                tabla1.AddCell(cell);




                tabla1.AddCell(new PdfPCell(new Paragraph("NOMBRE INDICADOR:", FontFactory.GetFont("Arial", 10, Font.BOLD))));
                cell = new PdfPCell(new Paragraph(NOMBRE_INDICADOR, FontFactory.GetFont("Arial", 10)));
                cell.Colspan = 2;
                cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                tabla1.AddCell(cell);

                tabla1.AddCell(new PdfPCell(new Paragraph("FECHA CREACIÓN INICIO:", FontFactory.GetFont("Arial", 10, Font.BOLD))));
                cell = new PdfPCell(new Paragraph(FECHA_CREACION, FontFactory.GetFont("Arial", 10)));
                cell.Colspan = 2;
                cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                tabla1.AddCell(cell);


                tabla1.AddCell(new PdfPCell(new Paragraph("RESPONSABLE:", FontFactory.GetFont("Arial", 10, Font.BOLD))));
                cell = new PdfPCell(new Paragraph(RESPONSABLE, FontFactory.GetFont("Arial", 10)));
                cell.Colspan = 2;
                cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                tabla1.AddCell(cell);




                tabla1.AddCell(new PdfPCell(new Paragraph("PROPOSITO:", FontFactory.GetFont("Arial", 10, Font.BOLD))));
                cell = new PdfPCell(new Paragraph(PROPOSITO_INDICADOR, FontFactory.GetFont("Arial", 10)));
                cell.Colspan = 2;
                cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                tabla1.AddCell(cell);

                tabla1.AddCell(new PdfPCell(new Paragraph("OBJETIVO ESTRATEGICO:", FontFactory.GetFont("Arial", 10, Font.BOLD))));
                cell = new PdfPCell(new Paragraph(INDUCTOR, FontFactory.GetFont("Arial", 10)));
                cell.Colspan = 2;
                cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                tabla1.AddCell(cell);
                tabla1.AddCell(new PdfPCell(new Paragraph("PERIODICIDAD MEDIDA", FontFactory.GetFont("Arial", 10, Font.BOLD))));
                cell = new PdfPCell(new Paragraph(NOMBRE_PERODICIDADA, FontFactory.GetFont("Arial", 10)));
                cell.Colspan = 2;
                cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                tabla1.AddCell(cell);

                tabla1.AddCell(new PdfPCell(new Paragraph("PERIODICIDAD ANÁLISIS:", FontFactory.GetFont("Arial", 10, Font.BOLD))));
                cell = new PdfPCell(new Paragraph(NOMBRE_PERODICIDADB, FontFactory.GetFont("Arial", 10)));
                cell.Colspan = 2;
                cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                tabla1.AddCell(cell);

                tabla1.AddCell(new PdfPCell(new Paragraph("UNIDAD DE MEDIDA:", FontFactory.GetFont("Arial", 10, Font.BOLD))));
                cell = new PdfPCell(new Paragraph(UNIDAD, FontFactory.GetFont("Arial", 10)));
                cell.Colspan = 2;
                cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                tabla1.AddCell(cell);

                tabla1.AddCell(new PdfPCell(new Paragraph("TIPO DE ALCANCE:", FontFactory.GetFont("Arial", 10, Font.BOLD))));
                cell = new PdfPCell(new Paragraph(DES_ALCANCE, FontFactory.GetFont("Arial", 10)));
                cell.Colspan = 2;
                cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                tabla1.AddCell(cell);

                tabla1.AddCell(new PdfPCell(new Paragraph("CLASIFICACIÓN:", FontFactory.GetFont("Arial", 10, Font.BOLD))));
                cell = new PdfPCell(new Paragraph(DES_CLASIFICACION, FontFactory.GetFont("Arial", 10)));
                cell.Colspan = 2;
                cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                tabla1.AddCell(cell);

                tabla1.AddCell(new PdfPCell(new Paragraph("TIPO MEDIDA:", FontFactory.GetFont("Arial", 10, Font.BOLD))));
                cell = new PdfPCell(new Paragraph(DESTIPOME, FontFactory.GetFont("Arial", 10)));
                cell.Colspan = 2;
                cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                tabla1.AddCell(cell);

                tabla1.AddCell(new PdfPCell(new Paragraph("FORMULA:", FontFactory.GetFont("Arial", 10, Font.BOLD))));
                cell = new PdfPCell(new Paragraph(FORMULA, FontFactory.GetFont("Arial", 10)));
                cell.Colspan = 2;
                cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                tabla1.AddCell(cell);

                tabla1.AddCell(new PdfPCell(new Paragraph("META:", FontFactory.GetFont("Arial", 10, Font.BOLD))));
                cell = new PdfPCell(new Paragraph(META, FontFactory.GetFont("Arial", 10)));
                cell.Colspan = 2;
                cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                tabla1.AddCell(cell);

                tabla1.AddCell(new PdfPCell(new Paragraph("TENDENCIA:", FontFactory.GetFont("Arial", 10, Font.BOLD))));
                cell = new PdfPCell(new Paragraph(ETIQUETA, FontFactory.GetFont("Arial", 10)));
                cell.Colspan = 2;
                cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                tabla1.AddCell(cell);

                tabla1.AddCell(new PdfPCell(new Paragraph("TIPO INDICADOR:", FontFactory.GetFont("Arial", 10, Font.BOLD))));
                cell = new PdfPCell(new Paragraph(TIPOINDICADOR, FontFactory.GetFont("Arial", 10)));
                cell.Colspan = 2;
                cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                tabla1.AddCell(cell);

                tabla1.AddCell(new PdfPCell(new Paragraph("PROCESO:", FontFactory.GetFont("Arial", 10, Font.BOLD))));
                cell = new PdfPCell(new Paragraph(PROCESO, FontFactory.GetFont("Arial", 10)));
                cell.Colspan = 2;
                cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                tabla1.AddCell(cell);
                documento.Add(tabla1);

                documento.Add(new Paragraph(" "));
                documento.Add(new Paragraph(" "));

                cell = new PdfPCell();
                cell.Colspan = 4;
                cell.Border = 1;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.Phrase = new Phrase("VARIABLES", new Font(Font.FontFamily.HELVETICA, 12, 1, new BaseColor(System.Drawing.Color.White)));
                cell.BackgroundColor = new BaseColor(System.Drawing.Color.FromArgb(52, 179, 74));
                cell.PaddingBottom = 10;
                tabla2.AddCell(cell);


                tabla2.DefaultCell.HorizontalAlignment = Element.ALIGN_LEFT;
                tabla2.DefaultCell.BorderColor = BaseColor.BLACK;
                tabla2.DefaultCell.Border = Rectangle.BOX;
                tabla2.DefaultCell.BorderWidth = 1;

                tabla2.AddCell(new PdfPCell(new Paragraph("VARIABLES:", FontFactory.GetFont("Arial", 10, Font.BOLD))));
                cell = new PdfPCell(new Paragraph(NOMBRE_VARIABLE, FontFactory.GetFont("Arial", 10)));
                cell.Colspan = 2;
                cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                tabla2.AddCell(cell);

                tabla2.AddCell(new PdfPCell(new Paragraph("FUENTE:", FontFactory.GetFont("Arial", 10, Font.BOLD))));
                cell = new PdfPCell(new Paragraph(FUENTE, FontFactory.GetFont("Arial", 10)));
                cell.Colspan = 2;
                cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                tabla2.AddCell(cell);

                documento.Add(tabla2);

                documento.Close();

            }
        }




        protected void GrvVariables_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            GridDataItem item = (GridDataItem)e.Item;
            int IDVARIABLE = Convert.ToInt32(item.GetDataKeyValue("ID_VARIABLE").ToString());
            ObjGIndicadores.DADELETEVARIABLE(IDVARIABLE);
            GrvVariables.Rebind();

        }

        protected void GrvVariables_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            DataSet ds = new DataSet();
            ds = ObjGIndicadores.DBUSQUEDAVARIABLE(Convert.ToInt32(Session["IDINDICADOR"]));
            GrvVariables.DataSource = ds;



        }

        protected void btn_agregar2_Click(object sender, EventArgs e)
        {

            DataSet ds = new DataSet();
            //VALIDAR SINTAXIS 
            ds = ObjGIndicadores.DEVALUARSINTAXIS(TxtVariables.Text);
            if (ds.Tables[0].Rows.Count > 0)
            {
                bool resultado = Convert.ToBoolean(ds.Tables[0].Rows[0][0].ToString());
                if (resultado == true)
                {
                    DataSet ds1 = new DataSet();
                    ds1 = ObjGIndicadores.DACTUALIZARFSQL(Convert.ToInt32(Session["IDINDICADOR"]), TxtVariables.Text);

                    DivResultado.Visible = true;
                    LblValidarSin.Text = "Sintaxis Valida";
                    LblValidarSin.ForeColor = System.Drawing.Color.Green;
                    LblValidarSin.Visible = true;
                    RadWizardStep3.Enabled = true;
                }
                else
                {
                    DivResultado.Visible = true;
                    LblValidarSin.Text = "Sintaxis invalida por favor verificar";
                    LblValidarSin.ForeColor = System.Drawing.Color.Red;
                    LblValidarSin.Visible = true;
                }


            }



        }



        protected void Cmbperspectiva_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            CargarCombosMaestros("T_INDUCTOR", 1, 2, 2, cmbinductor, 3, Convert.ToInt32(Cmbperspectiva.SelectedValue));
        }

        protected void CmbEmpresa_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (CmbEmpresa.SelectedValue == "1")
            {
                CmbProceso.Visible = true;
                LblProceso.Visible = true;
                Cmbperspectiva.Visible = true;
                lblperspectiva.Visible = true;
                LblInductor.Visible = true;
                cmbinductor.Visible = true;
                CargarCombosMaestros("T_PERSPECTIVA", 1, 2, 2, Cmbperspectiva);

            }
            else
            {
                CmbProceso.Visible = false;
                LblProceso.Visible = false;
                Cmbperspectiva.Visible = false;
                lblperspectiva.Visible = false;
                LblInductor.Visible = false;
                cmbinductor.Visible = false;

            }
        }


        protected void Gvrolesu_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            string ID = e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID_ROLESIN"].ToString();
            ObjGIndicadores.DELETEINDICADOROL(Convert.ToInt32(ID));

            Gvrolesu.Rebind();
        }



        protected void btn_actualizar_Click(object sender, EventArgs e)
        {
            VarObjIndicaores();
            int nactualizar;
            nactualizar = ObjGIndicadores.UPDATEINDICADOR(ObjGIndicadoresE);

          
                DataSet ds = new DataSet();
                ds = ObjGIndicadores.DCONSULTARLIMITE(Convert.ToInt32(Session["IDINDICADOR"]), Convert.ToDouble(TxtPeor.Text), Convert.ToDouble(TxtMejor.Text), Convert.ToDouble(TxtAlcance.Text), Convert.ToDouble(TxtRoja.Text));
                if (ds.Tables[0].Rows.Count > 0)
                {
                    TxtAlcance.Text = ds.Tables[0].Rows[0]["ALCANCE"].ToString();
                    TxtPeor.Text = ds.Tables[0].Rows[0]["PEOR"].ToString();
                    TxtRoja.Text = ds.Tables[0].Rows[0]["ROJA"].ToString();
                    TxtMejor.Text = ds.Tables[0].Rows[0]["MEJOR"].ToString();
                    lblMeta.Text = ds.Tables[0].Rows[0]["META"].ToString(); ;
                }
            
            

            if (nactualizar == -1)
            {
                //   DResultado.Visible = true;


            }
            else
            {

                //DResultado.Visible = true;

            }
        }

        protected void btnG_Click(object sender, EventArgs e)
        {
             if (CmbLimite.SelectedValue=="3")
            {
                TxtAlcance.Text = TxtAlcance.Text;
                TxtPeor.Text = TxtPeor.Text;
                TxtRoja.Text = "0";
                TxtMejor.Text ="0";
                DataSet ds2 = new DataSet();
                ds2 = ObjGIndicadores.DCONSULTARLIMITE(Convert.ToInt32(Session["IDINDICADOR"]), Convert.ToDouble(TxtPeor.Text), Convert.ToDouble(TxtMejor.Text), Convert.ToDouble(TxtAlcance.Text), Convert.ToDouble(TxtRoja.Text));
                if (ds2.Tables[0].Rows.Count > 0)
                {
                    TxtAlcance.Text = ds2.Tables[0].Rows[0]["ALCANCE"].ToString();
                    TxtPeor.Text = ds2.Tables[0].Rows[0]["PEOR"].ToString();
                    TxtRoja.Text = ds2.Tables[0].Rows[0]["ROJA"].ToString();
                    TxtMejor.Text = ds2.Tables[0].Rows[0]["MEJOR"].ToString();
                    lblMeta.Text= ds2.Tables[0].Rows[0]["META"].ToString();
                }

            }
            else
            {

             DataSet ds = new DataSet();
            ds = ObjGIndicadores.DCONSULTARLIMITE(Convert.ToInt32(Session["IDINDICADOR"]), Convert.ToDouble(TxtPeor.Text), Convert.ToDouble(TxtMejor.Text), Convert.ToDouble(TxtAlcance.Text), Convert.ToDouble(TxtRoja.Text));
            if (ds.Tables[0].Rows.Count > 0)
            {
                TxtAlcance.Text = ds.Tables[0].Rows[0]["ALCANCE"].ToString();
                TxtPeor.Text = ds.Tables[0].Rows[0]["PEOR"].ToString();
                TxtRoja.Text = ds.Tables[0].Rows[0]["ROJA"].ToString();
                TxtMejor.Text = ds.Tables[0].Rows[0]["MEJOR"].ToString();
                    lblMeta.Text = ds.Tables[0].Rows[0]["META"].ToString();
                }
            }



        }

        protected void CmbProceso_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if(CmbProceso.SelectedValue=="9")
            {
                CargarCombosMaestros("T_ALCANCE", 1, 2, 2, CmbAlcance,3,1);
            }
        }
    }
}
