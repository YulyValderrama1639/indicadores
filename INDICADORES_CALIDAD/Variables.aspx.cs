﻿using System;
using System.Data;
using CNegocios;
using Telerik.Web.UI;
using System.Data.SqlClient;
using System.Configuration;
namespace INDICADORES_CALIDAD
{
    public partial class Variables : System.Web.UI.Page
    {
        public SqlConnection Conexion;
        public IndicadoresBussines ObjGIndicadores = new IndicadoresBussines();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                string Valor = Request.QueryString["ID_INDICADOR"];
                Session["VALOR"] = Valor;

                if ((Session["SECCIONAL"] == ""))
                {
                    GVariables.DataSource = ObjGIndicadores.DACONSULTA_VAR_INDICADOR(Convert.ToInt32(Valor), Convert.ToInt32(Session["PERIODO"]), 0);
                    GVariables.DataBind();
                    GVariables.Visible = true;
                }
                else
                {
                    GVariables.DataSource = ObjGIndicadores.DACONSULTA_VAR_INDICADOR(Convert.ToInt32(Valor), Convert.ToInt32(Session["PERIODO"]), Convert.ToInt32(Session["SECCIONAL"]));

                    GVariables.DataBind();
                    GVariables.Visible = true;
                }
            }

        }
        private void CargarCombosMaestros(string Tabla, int campo1, int campo2, int orden, RadComboBox Combo, int? campo3 = null, int? campo4 = null)
        {
            Conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["SCORBOARD"].ConnectionString);
            SqlCommand cmd = new SqlCommand("P_CARGA_COMBOS", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TABLA", Tabla);
            cmd.Parameters.AddWithValue("@CAMPO1", campo1);
            cmd.Parameters.AddWithValue("@CAMPO2", campo2);
            cmd.Parameters.AddWithValue("@ORDEN", orden);
            cmd.Parameters.AddWithValue("@CAMPO_FILTRO", campo3);
            cmd.Parameters.AddWithValue("@VALOR_FILTRO", campo4);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {

                da.Fill(ds);
                Combo.DataSource = ds;
                Combo.DataValueField = "1";
                Combo.DataTextField = "2";
                Combo.DataBind();

            }
            catch (Exception ex)
            {

            }

        }


    }
}
