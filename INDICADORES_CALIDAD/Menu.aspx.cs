﻿using CNegocios;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace INDICADORES_CALIDAD
{

    public partial class Menu : System.Web.UI.Page
    {
        

        protected void Page_Load(object sender, EventArgs e)
        {
        
        }

        [WebMethod(EnableSession = true)]

        public static string GetMenu()
        {
            IndicadoresBussines indicadores = new IndicadoresBussines();

            
            var menuListItems = indicadores.GetMenu(Convert.ToInt32(HttpContext.Current.Session["ID_ROL"].ToString()), Convert.ToInt32( HttpContext.Current.Session["ID_USUARIO"].ToString()));

            List<MenuItem> menuList = CreateMenu(menuListItems, null, menuListItems).ToList();


            //List<MenuItem> menuList = new List<MenuItem>();

            //MenuItem opt1 = new MenuItem
            //{
            //    ID = 1,
            //    Text = "UI elements",
            //    Class = "menu-title",
            //    IsCategory = true,
            //    Items = new List<MenuItem>()
            //};

            //MenuItem c1o1 = new MenuItem() { ID = 11, URL = "ui-buttons.html", Class = "fa fa-puzzle-piece", Text = "Buttons" };
            //MenuItem c1o2 = new MenuItem() { ID = 12, URL = "ui-badges.html", Class = "fa fa-id-badge", Text = "Badges" };
            //MenuItem c1o3 = new MenuItem() { ID = 13, URL = "ui-tabs.html", Class = "fa fa-bars", Text = "Tabs" };
            //MenuItem c1o4 = new MenuItem() { ID = 14, URL = "ui-social-buttons.html", Class = "fa fa-share-square-o", Text = "Social Buttons" };
            //MenuItem c1o5 = new MenuItem() { ID = 15, URL = "ui-cards.html", Class = "fa fa-id-card-o", Text = "Cards" };
            //MenuItem c1o6 = new MenuItem() { ID = 16, URL = "ui-alerts.html", Class = "fa fa-exclamation-triangle", Text = "Alerts" };
            //MenuItem c1o7 = new MenuItem() { ID = 17, URL = "ui-progressbar.html", Class = "fa fa-spinner", Text = "Progress Bars" };
            //MenuItem c1o8 = new MenuItem() { ID = 18, URL = "ui-modals.html", Class = "fa fa-fire", Text = "Modals" };

            //MenuItem c1 = new MenuItem() { ID = 18, Class = "menu-icon fa fa-laptop", Text = "Components", IsParent = true };
            //c1.Items = new List<MenuItem> { c1o1, c1o2, c1o3, c1o4, c1o5, c1o6, c1o7, c1o8 };

            //MenuItem c1o9 = new MenuItem() { ID = 19, URL = "tables-basic.html", Class = "fa fa-table", Text = "Basic Table" };
            //MenuItem c1o10 = new MenuItem() { ID = 110, URL = "tables-data.html", Class = "fa fa-table", Text = "Data Table" };

            //MenuItem c11 = new MenuItem() { ID = 18, Class = "menu-icon fa fa-table", Text = "Tables", IsParent = true };
            //c11.Items = new List<MenuItem> { c1o9, c1o10 };

            //opt1.Items.Add(c1);
            //opt1.Items.Add(c11);
            //menuList.Add(opt1);

            //MenuItem opt2 = new MenuItem
            //{
            //    ID = 1,
            //    Text = "Icons",
            //    Class = "menu-title",
            //    IsCategory = true,
            //    Items = new List<MenuItem>()
            //};

            //MenuItem c2o1 = new MenuItem() { ID = 11, URL = "font-fontawesome.html", Class = "menu-icon fa fa-fort-awesome", Text = "Font Awesome" };
            //MenuItem c2o2 = new MenuItem() { ID = 12, URL = "font-themify.html", Class = "menu-icon ti-themify-logo", Text = "Themefy Icons" };

            //MenuItem c2 = new MenuItem() { ID = 18, Class = "menu-icon fa fa-tasks", Text = "Icons", IsParent = true };
            //c2.Items = new List<MenuItem> { c2o1, c2o2 };

            //MenuItem c2o3 = new MenuItem() { ID = 19, URL = "charts-chartjs.html", Class = "menu-icon fa fa-line-chart", Text = "Chart JS" };
            //MenuItem c2o4 = new MenuItem() { ID = 110, URL = "charts-flot.html", Class = "menu-icon fa fa-area-chart", Text = "Flot Chart" };
            //MenuItem c2o5 = new MenuItem() { ID = 110, URL = "charts-peity.html", Class = "menu-icon fa fa-pie-chart", Text = "Peity Chart" };

            //MenuItem c22 = new MenuItem() { ID = 18, Class = "menu-icon fa fa-bar-chart", Text = "Charts", IsParent = true };
            //c22.Items = new List<MenuItem> { c2o3, c2o4, c2o5 };

            //opt2.Items.Add(c2);
            //opt2.Items.Add(c22);
            //menuList.Add(opt2);

            return JsonConvert.SerializeObject(menuList);
        }

        private static List<MenuItem> CreateMenu(IEnumerable<CDatos.PA_MENU_GET_Result> data, MenuItem parent, IEnumerable<CDatos.PA_MENU_GET_Result> allData)
        {
            List<MenuItem> menuList = new List<MenuItem>();

            foreach (CDatos.PA_MENU_GET_Result item in data.Where(p => parent != null || !p.ID_ORDEN_MENU.HasValue))
            {
                
                MenuItem mItem = new MenuItem() { ID = item.ID_MENU, URL = item.ELEMENTO, Class =item.ICONOP, Text = item.NOMBRE_MENU, ItemCategory = item.ID_TIPOMENU };

                if (item.ID_TIPOMENU == 1)
                {
                    mItem.Class = "menu-title";
                    mItem.IsCategory = true;
                }


                var childItems = allData.Where(p => p.ID_ORDEN_MENU.HasValue && p.ID_ORDEN_MENU.Value == item.ID_MENU);

                if (parent != null)
                {
                    if (parent.Items == null)
                        parent.Items = new List<MenuItem>();
                    parent.Items.Add(mItem);
                }

                if (childItems.Count() > 0)
                {
                    //mItem.Class = "fa fa-puzzle-piece";

                    mItem.IsParent = true;
                    
                    if (parent == null)
                        menuList.Add(mItem);

                    menuList.AddRange(CreateMenu(childItems, mItem, allData));
                }
                else if (parent == null)
                    menuList.Add(mItem);
            }
            return menuList;
        }
    }

    public class MenuItem
    {
        public int ID { get; set; }
        public string Icon { get; set; }
        public string Class { get; set; }
        public string Text { get; set; }
        public string URL { get; set; }
        public bool IsCategory { get; set; }
        public bool IsParent { get; set; }
        public int ItemCategory { get; set; }
        public IList<MenuItem> Items { get; set; }
    }
}