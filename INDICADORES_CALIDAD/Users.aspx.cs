﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CNegocios;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Telerik.Web.UI;

namespace INDICADORES_CALIDAD
{
    public partial class Users : System.Web.UI.Page
    {
        public SqlConnection Conexion;
        public IndicadoresBussines ObjGIndicadores = new IndicadoresBussines();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Convert.ToString(this.Session["Login"]) != "OK")

            {
                Response.Redirect("~/Login.aspx", true);
            }

            if (!IsPostBack)
            {
                //CargarCombosMaestros("T_INDICADORES", 1, 6, 2, CmbIndicador);
                CargarCombosMaestros("T_TIPOINDICADOR", 1, 2, 2, CmbTipoIndicador);
                CargarCombosMaestros("T_ROLES", 1, 2, 2, CmbRol);
                CargarCombosMaestros("T_ROLES", 1, 2, 2, CmbRolP);
                CargarCombosMaestros("T_PROCESO", 1, 2, 2, Cmbproceso);
                CmbUsuario.DataSource = ObjGIndicadores.DUSUARIOSDA();
                CmbUsuario.DataValueField = "ID_RESPONSABLE";
                CmbUsuario.DataTextField = "NOMBRE_RESPONSABLE";
                CmbUsuario.DataBind();

                CmbUsuarioP.DataSource = ObjGIndicadores.DUSUARIOSDA();
                CmbUsuarioP.DataValueField = "ID_RESPONSABLE";
                CmbUsuarioP.DataTextField = "NOMBRE_RESPONSABLE";
                CmbUsuarioP.DataBind();

                BindToDataTable(RadtreePer);

            }

        }

        private void BindToDataTable(RadTreeView treeView)
        {

            SqlDataAdapter adapter = new SqlDataAdapter("SELECT t1.ID_MENU ,t1.NOMBRE_MENU,t1.ID_ESTADO,t1.ID_ORDEN_MENU FROM T_MENU T1",
                   ConfigurationManager.ConnectionStrings["SCORBOARD"].ConnectionString);

            DataSet links = new DataSet();

            adapter.Fill(links);
            RadtreePer.DataTextField = "NOMBRE_MENU";
            RadtreePer.DataFieldID = "ID_MENU";
            RadtreePer.DataFieldParentID = "ID_ORDEN_MENU";
            RadtreePer.DataSource = links;
            RadtreePer.DataBind();

        }

        protected void gvUsuarios_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            gvUsuarios.DataSource = ObjGIndicadores.DUSUARIOSDA();
        }


        private void CargarCombosMaestros(string Tabla, int campo1, int campo2, int orden, RadComboBox Combo, int? campo3 = null, int? campo4 = null)
        {
            Conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["SCORBOARD"].ConnectionString);
            SqlCommand cmd = new SqlCommand("P_CARGA_COMBOS", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TABLA", Tabla);
            cmd.Parameters.AddWithValue("@CAMPO1", campo1);
            cmd.Parameters.AddWithValue("@CAMPO2", campo2);
            cmd.Parameters.AddWithValue("@ORDEN", orden);
            cmd.Parameters.AddWithValue("@CAMPO_FILTRO", campo3);
            cmd.Parameters.AddWithValue("@VALOR_FILTRO", campo4);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {

                da.Fill(ds);
                Combo.DataSource = ds;
                Combo.DataValueField = "1";
                Combo.DataTextField = "2";
                Combo.DataBind();

            }
            catch (Exception ex)
            {

            }

        }






        protected void gvUsuarios_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                ImageButton btn_detalle = (ImageButton)e.Item.FindControl("btn_detalle");
                btn_detalle.Attributes["href"] = "javascript:void(0);";
                btn_detalle.Attributes["onclick"] = String.Format("return ShowEditForm('{0}','{1}');", e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IDENTIFICACION"], e.Item.ItemIndex);

            }
        }

        protected void btn_asigMenu_Click(object sender, EventArgs e)
        {
            int resultado;
            if (CmbUsuario.SelectedValue == "" || CmbRol.SelectedValue == "")
            {
                Div1.Visible = true;

            }

            else
            {
                for (int i = 1; i < RadtreePer.CheckedNodes.Count; i++)
                {

                    foreach (RadTreeNode node in RadtreePer.CheckedNodes)
                    {
                        if (node.Checked)
                        {
                            ObjGIndicadores.CREATEMENUUSUARIO(Convert.ToInt32(CmbUsuario.SelectedValue), Convert.ToInt32(i), Convert.ToInt32(CmbRol.SelectedValue));
                        }

                        i = i + 1;
                    }


                }

            }



            Div2.Visible = true;
            Div1.Visible = false;
            CmbUsuario.SelectedValue = "0";
            CmbUsuario.Text = "";
            CmbRol.SelectedValue = "0";
            CmbRol.Text = "";
            RadtreePer.DataSource = "";
        }





        protected void btn_consultar_Click(object sender, EventArgs e)
        {

            if (Cmbproceso.SelectedValue != "")
            {
                DivError.Visible = false;
                CmbTipoIndicador.SelectedValue = "0";
                GVINDICADOR.Rebind();
                btn_asignar.Enabled = true;
                lblMensajeAsignacion.Text = "Puede seleccionar los indicadores para asignar";
            }
            else
            {
                if (CmbTipoIndicador.SelectedValue != "")
                {
                    DivError.Visible = false;
                    Cmbproceso.SelectedValue = "0";
                    GVINDICADOR.Rebind();
                    btn_asignar.Enabled = true;
                    lblMensajeAsignacion.Text = "Puede seleccionar los indicadores para asignar";
                }
            }

            if (Cmbproceso.SelectedValue != "" || CmbTipoIndicador.SelectedValue != "")
            {
                DivError.Visible = false;
                GVINDICADOR.Rebind();
                btn_asignar.Enabled = true;
                lblMensajeAsignacion.Text = "Puede seleccionar los indicadores para asignar";

            }

            if (Cmbproceso.SelectedValue == "" || CmbTipoIndicador.SelectedValue == "")
            {
                DivError.Visible = true;
                btn_asignar.Enabled = false;

            }
        }

        protected void GVINDICADOR_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {

            GVINDICADOR.DataSource = ObjGIndicadores.DCONSULTAPERMISOSTIPO(Convert.ToInt32(Cmbproceso.SelectedValue), Convert.ToInt32(CmbTipoIndicador.SelectedValue));
            GVINDICADOR.Visible = true;


        }

        protected void ChckTodos_CheckedChanged(object sender, EventArgs e)
        {
            foreach (GridDataItem item in GVINDICADOR.MasterTableView.Items)
            {
                CheckBox chkbx = (CheckBox)item["Seleccion"].FindControl("ChckSeleecionar");
                chkbx.Checked = !chkbx.Checked;
            }
        }

        protected void btn_limpiar_Click(object sender, EventArgs e)
        {
            Limpiar();


        }

        protected void Limpiar()
        {
            lblMensajeAsignacion.Visible = false;
            GVINDICADOR.Visible = false;
            GVINDICADOR.DataSource = "";
            CmbTipoIndicador.SelectedValue = "0";
            CmbTipoIndicador.Text = "";
            CmbUsuarioP.SelectedValue = "0";
            CmbUsuarioP.Text = "";
            Cmbproceso.SelectedValue = "0";
            Cmbproceso.Text = "";
            CmbRolP.SelectedValue = "0";
            CmbRolP.Text = "";
            DivError.Visible = false;
            btn_asignar.Enabled = false;

        }


        protected void GVINDICADOR_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            List<String> idList = new List<string>();
            if (Session["idList"] != null)
            {
                idList = (List<string>)Session["idList"];
            }

            foreach (GridDataItem item in GVINDICADOR.MasterTableView.Items)
            {
                CheckBox chckPreseleccion = (CheckBox)item["Seleccion"].Controls[1];
                String IdIndicador = item["ID_INDICADOR"].Text;


                if (chckPreseleccion.Checked)
                {
                    if (!idList.Contains(IdIndicador))
                        idList.Add(IdIndicador);
                }
                else
                {
                    if (idList.Contains(IdIndicador))
                        idList.Remove(IdIndicador);
                }
            }

            Session["idList"] = idList;
        }

        protected void GVINDICADOR_PreRender(object sender, EventArgs e)
        {
            if (Session["idList"] != null)
            {
                List<String> idList = (List<string>)Session["idList"];

                foreach (GridDataItem item in GVINDICADOR.MasterTableView.Items)
                {
                    CheckBox chckPreseleccion = (CheckBox)item["Seleccion"].Controls[1];
                    String identificacion = item["ID_INDICADOR"].Text;


                    if (idList.Contains(identificacion))
                        chckPreseleccion.Checked = true;
                }

            }

        }

        protected void GVINDICADOR_DataBound(object sender, EventArgs e)
        {
            if (Session["idList"] != null)
            {
                List<String> idList = (List<string>)Session["idList"];

                foreach (GridDataItem item in GVINDICADOR.MasterTableView.Items)
                {
                    CheckBox chckPreseleccion = (CheckBox)item["Seleccion"].Controls[1];
                    String identificacion = item["ID_INDICADOR"].Text;


                    if (idList.Contains(identificacion))
                        chckPreseleccion.Checked = true;
                }

            }

        }

        protected void btn_asignar_Click(object sender, EventArgs e)
        {
            if ((CmbUsuarioP.SelectedValue == "0" || CmbUsuarioP.SelectedValue == "") ||( CmbRolP.SelectedValue == "0" || CmbRolP.SelectedValue == ""))
            {
                lblMensajeAsignacion.Text = "Debe seleccionar el usuario y el rol";
                lblMensajeAsignacion.ForeColor = System.Drawing.Color.Red;
                lblMensajeAsignacion.Visible = true;
            }
            else
            {
                DivError.Visible = false;
                foreach (GridDataItem item in GVINDICADOR.MasterTableView.Items)
                {
                    CheckBox chckPreseleccion = (CheckBox)item["Seleccion"].Controls[1];
                    String Idindicador = item["ID_INDICADOR"].Text;

                    if (chckPreseleccion.Checked)
                    {
                        ObjGIndicadores.CREATEROLESINDICADOR(Convert.ToInt32(CmbUsuarioP.SelectedValue), Convert.ToInt32(Idindicador), Convert.ToInt32(CmbRolP.SelectedValue), true);
                    }
                }

                string script1 = "function f(){radopen(null, 'rw_customConfirm'); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "customConfirmOpener", script1, true);
                Limpiar();

            }

        }
    }
}