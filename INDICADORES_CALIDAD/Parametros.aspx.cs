﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CNegocios;
using System.Data.Entity;
using Telerik.Web.UI;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;

namespace INDICADORES_CALIDAD
{
    public partial class Parametros : System.Web.UI.Page
    {
        public SqlConnection Conexion;
        public IndicadoresBussines ObjGIndicadores = new IndicadoresBussines();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CargarCombosMaestros("T_TABLAS", 1, 2, 2, cmbtablam);
            }


        }

        protected void BtnConsultar_Click(object sender, EventArgs e)
        {
            if (cmbtablam.SelectedValue!="")
            {
                GVTMAESTRA.Rebind();
                GVTMAESTRA.Visible = true;

            }

        }

        private void CargarCombosMaestros(string Tabla, int campo1, int campo2, int orden, RadComboBox Combo, int? campo3 = null, int? campo4 = null)
        {
            Conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["SCORBOARD"].ConnectionString);
            SqlCommand cmd = new SqlCommand("P_CARGA_COMBOS", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TABLA", Tabla);
            cmd.Parameters.AddWithValue("@CAMPO1", campo1);
            cmd.Parameters.AddWithValue("@CAMPO2", campo2);
            cmd.Parameters.AddWithValue("@ORDEN", orden);
            cmd.Parameters.AddWithValue("@CAMPO_FILTRO", campo3);
            cmd.Parameters.AddWithValue("@VALOR_FILTRO", campo4);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {

                da.Fill(ds);
                Combo.DataSource = ds;
                Combo.DataValueField = "1";
                Combo.DataTextField = "2";
                Combo.DataBind();

            }
            catch (Exception ex)
            {

            }

        }

        protected void GVTMAESTRA_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GVTMAESTRA.DataSource = ObjGIndicadores.DACONSULTATMAESTRAS(cmbtablam.Text);
        }

        protected void GVTMAESTRA_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);
            editableItem.GetDataKeyValue("1");
            var valorvar = (int)editableItem.GetDataKeyValue("1");
            var textBox = ((GridTextBoxColumnEditor)editableItem.EditManager.GetColumnEditor("2")).TextBoxControl;
            InputSetting inputSetting = RadInputManager1.GetSettingByBehaviorID("TxtNombreparametro");
            inputSetting.TargetControls.Add(new TargetInput(textBox.UniqueID, true));
            inputSetting.InitializeOnClient = true;
            inputSetting.Validation.IsRequired = true;

            ObjGIndicadores.DUPDATETABLE(cmbtablam.Text, textBox.Text, Convert.ToString(valorvar));
            GVTMAESTRA.Rebind();
        }


        protected void GVTMAESTRA_InsertCommand(object sender, GridCommandEventArgs e)
        {
            GridEditableItem item = e.Item as GridEditableItem;
            var editableItem = ((GridEditableItem)e.Item);
            Hashtable newValues = new Hashtable();
            e.Item.OwnerTableView.ExtractValuesFromItem(newValues, editableItem);
            string vDescripcion = (newValues["2"].ToString());
            ObjGIndicadores.DINSERTARTABLEM(cmbtablam.Text, vDescripcion);
            GVTMAESTRA.Rebind();
        }

        private void SetupInputManagerP(GridEditableItem editableItem)
        {

            var textBox = ((GridTextBoxColumnEditor)editableItem.EditManager.GetColumnEditor("2")).TextBoxControl;
            InputSetting inputSetting = RadInputManager1.GetSettingByBehaviorID("TxtNombreparametro");
            inputSetting.TargetControls.Add(new TargetInput(textBox.UniqueID, true));
            inputSetting.InitializeOnClient = true;
            inputSetting.Validation.IsRequired = true;


        }

        protected void GVTMAESTRA_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            string ID = e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["1"].ToString();
            ObjGIndicadores.DDELETEM(cmbtablam.Text,ID);
            GVTMAESTRA.Rebind();
        }
    }
}