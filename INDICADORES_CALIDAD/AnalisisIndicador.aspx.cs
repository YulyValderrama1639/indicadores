﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CNegocios;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Telerik.Web.UI;
using CNegocios;

namespace INDICADORES_CALIDAD
{
    public partial class AnalisisIndicador : System.Web.UI.Page
    {
        public IndicadoresBussines ObjGIndicadores = new IndicadoresBussines();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
              
                string Valor = Request.QueryString["ID_INDICADOR"];
                 Session["ID_INDICADOR"] = Valor;
                GVMeses.DataSource = ObjGIndicadores.DCONSULTANOTAS(0, Convert.ToInt32(Session["ID_INDICADOR"]));
                GVMeses.Rebind();
            }

        }

     
    }
}