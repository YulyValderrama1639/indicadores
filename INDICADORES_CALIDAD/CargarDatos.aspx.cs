﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CNegocios;
using System.Data.Entity;
using Telerik.Web.UI;

using System.Data.SqlClient;
using System.Configuration;
using System.Collections;

namespace INDICADORES_CALIDAD
{
    public partial class CargarDatos : System.Web.UI.Page
    {

        public SqlConnection Conexion;
        public IndicadoresBussines ObjGIndicadores = new IndicadoresBussines();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CargarCombosMaestros("T_PERIODICIDAD", 1, 2, 2, CmbMedida);
                CargarCombosMaestros("T_PERIODICIDAD", 1, 2, 2, CmbVigencia);

                CmbIndicadores.DataSource = ObjGIndicadores.DCONSULTAPER(Convert.ToInt32(Session["ID_ROL"]), Convert.ToString(Session["ID_USUARIO"]));
                CmbIndicadores.DataValueField = "ID_INDICADOR";
                CmbIndicadores.DataTextField = "NOMBRE_INDICADOR";
                CmbIndicadores.DataBind();

                CmbTipoIndicador2.DataSource = ObjGIndicadores.DCONSULTAPER(Convert.ToInt32(Session["ID_ROL"]), Convert.ToString(Session["ID_USUARIO"]));
                CmbTipoIndicador2.DataValueField = "ID_INDICADOR";
                CmbTipoIndicador2.DataTextField = "NOMBRE_INDICADOR";
                CmbTipoIndicador2.DataBind();


            }
        }
        private void CargarCombosMaestros(string Tabla, int campo1, int campo2, int orden, RadComboBox Combo, int? campo3 = null, int? campo4 = null)
        {
            Conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["SCORBOARD"].ConnectionString);
            SqlCommand cmd = new SqlCommand("P_CARGA_COMBOS", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TABLA", Tabla);
            cmd.Parameters.AddWithValue("@CAMPO1", campo1);
            cmd.Parameters.AddWithValue("@CAMPO2", campo2);
            cmd.Parameters.AddWithValue("@ORDEN", orden);
            cmd.Parameters.AddWithValue("@CAMPO_FILTRO", campo3);
            cmd.Parameters.AddWithValue("@VALOR_FILTRO", campo4);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {

                da.Fill(ds);
                Combo.DataSource = ds;
                Combo.DataValueField = "1";
                Combo.DataTextField = "2";
                Combo.DataBind();

            }
            catch (Exception ex)
            {

            }

        }

        protected void btn_consultar_Click(object sender, EventArgs e)
        {
            GrvCargardatos.Rebind();
            GrvCargardatos.Visible = true;
        }

        protected void GrvCargardatos_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (CmbIndicadores.SelectedValue == "" || CmbPeriodo.SelectedValue == "")
            {
                Lblresultado.Text = "Debe seleccionar el indicador y/o el periodo en que seran cargados ";
                Lblresultado.ForeColor = System.Drawing.Color.Red;
                Lblresultado.Visible = true;
            }
            else
            {

                int varalcance;
                DataSet ds = new DataSet();
                ds = ObjGIndicadores.DACONSULTARNACIONAL(Convert.ToInt32(CmbIndicadores.SelectedValue));
                if (ds.Tables[0].Rows.Count > 0)
                {
                    varalcance = Convert.ToInt32(ds.Tables[0].Rows[0]["ID_ALCANCE"].ToString());
                    if (varalcance == 1 || varalcance == 5 || varalcance == 4 || varalcance == 6)
                    {
                        GrvCargardatos.DataSource = ObjGIndicadores.DCONSULTARVALORESVAR(Convert.ToInt32(CmbIndicadores.SelectedValue), Convert.ToInt32(CmbPeriodo.SelectedValue), Convert.ToInt32(Cmbseccional.SelectedValue));

                    }
                    else
                    {
                        GrvCargardatos.DataSource = ObjGIndicadores.DCONSULTARVALORESVAR2(Convert.ToInt32(CmbIndicadores.SelectedValue), Convert.ToInt32(CmbPeriodo.SelectedValue));
                    }

                }
            }

        }

        protected void CmbMedida_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            CmbPeriodo.DataSource = ObjGIndicadores.DCONSULTATIPO(Convert.ToInt32(CmbMedida.SelectedValue));
            CmbPeriodo.DataValueField = "ID_DET_PERIODO";
            CmbPeriodo.DataTextField = "PERIODO";
            CmbPeriodo.DataBind();

        }


        protected void CmbIndicadores_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            DataSet ds = new DataSet();
            ds = ObjGIndicadores.DACONSULTARNACIONAL(Convert.ToInt32(CmbIndicadores.SelectedValue));
            if (ds.Tables[0].Rows.Count > 0)
            {

                int varalcance;
                varalcance = Convert.ToInt32(ds.Tables[0].Rows[0]["ID_ALCANCE"].ToString());
                switch (varalcance)
                {
                    case 1:
                        lblseccional.Visible = true;
                        Cmbseccional.Visible = true;
                        Cmbseccional.DataSource = ObjGIndicadores.DCONSULTASECCIONAL(1);
                        Cmbseccional.DataValueField = "ID_SECCIONAL";
                        Cmbseccional.DataTextField = "NOMBRE_SECCIONAL";
                        Cmbseccional.DataBind();
                        break;
                    case 5:
                        lblseccional.Visible = true;
                        Cmbseccional.Visible = true;
                        Cmbseccional.DataSource = ObjGIndicadores.DCONSULTASECCIONAL(5);
                        Cmbseccional.DataValueField = "ID_SECCIONAL";
                        Cmbseccional.DataTextField = "NOMBRE_SECCIONAL";
                        Cmbseccional.DataBind();
                        break;
                    case 4:
                        lblseccional.Visible = true;
                        Cmbseccional.Visible = true;
                        Cmbseccional.DataSource = ObjGIndicadores.DCONSULTASECCIONAL(4);
                        Cmbseccional.DataValueField = "ID_SECCIONAL";
                        Cmbseccional.DataTextField = "NOMBRE_SECCIONAL";
                        Cmbseccional.DataBind();
                        break;
                    case 6:
                        lblseccional.Visible = true;
                        Cmbseccional.Visible = true;
                        Cmbseccional.DataSource = ObjGIndicadores.DCONSULTASECCIONAL(6);
                        Cmbseccional.DataValueField = "ID_SECCIONAL";
                        Cmbseccional.DataTextField = "NOMBRE_SECCIONAL";
                        Cmbseccional.DataBind();
                        break;
                    default:
                        lblseccional.Visible = false;
                        Cmbseccional.Visible = false;
                        break;
                }
                                                    
                CargarCombosMaestros("T_PERIODICIDAD", 1, 2, 2, CmbMedida);
                CmbMedida.SelectedValue = ds.Tables[0].Rows[0]["ID_PERIODICIDAD"].ToString();
                CmbMedida.Enabled = false;
                CmbPeriodo.DataSource = ObjGIndicadores.DCONSULTATIPO(Convert.ToInt32(CmbMedida.SelectedValue));
                CmbPeriodo.DataValueField = "ID_DET_PERIODO";
                CmbPeriodo.DataTextField = "PERIODO";
                CmbPeriodo.DataBind();
            }
        }

        protected void btn_limpiar_Click(object sender, EventArgs e)
        {
            Cmbseccional.Visible = false;
            CmbIndicadores.SelectedValue = "";
            CmbMedida.SelectedValue = "";
            GrvCargardatos.Visible = false;
            Cmbseccional.Text = "";
            CmbIndicadores.Text = "";
            CmbMedida.Text = "";
            CmbPeriodo.Text = "";
            CmbPeriodo.SelectedValue = "";
            CmbMedida.Enabled = true;
            LblVerificar.Visible = false;
            Lblresultado.Visible = false;
            lblseccional.Visible = false;

        }

        protected void gvrIndicadoresnotas_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            gvrIndicadoresnotas.DataSource = ObjGIndicadores.DCONSULTANOTAS(0, Convert.ToInt32(CmbTipoIndicador2.SelectedValue));
        }

        protected void btnConsultar2_Click(object sender, EventArgs e)

        {
            if (CmbTipoIndicador2.SelectedValue == "")
            {
                Diverroranalisis.Visible = true;
            }
            else
            {
                gvrIndicadoresnotas.Rebind();
                gvrIndicadoresnotas.Visible = true;
                Diverroranalisis.Visible = false;
            }


        }





        protected void btn_guardar_Click(object sender, EventArgs e)
        {
            Div2.Visible = true;


            foreach (GridDataItem item in GrvCargardatos.Items)
            {
                String IDVARIABLE = item.GetDataKeyValue("ID_VARIABLE").ToString();
                string VALOR;
                TextBox VALORTXT = (TextBox)(item.FindControl("VALOR"));
                VALOR = VALORTXT.Text.Replace(",",".");
                int varalcance;
                DataSet ds = new DataSet();
                ds = ObjGIndicadores.DACONSULTARNACIONAL(Convert.ToInt32(CmbIndicadores.SelectedValue));
                if (ds.Tables[0].Rows.Count > 0)
                {
                    varalcance = Convert.ToInt32(ds.Tables[0].Rows[0]["ID_ALCANCE"].ToString());
                    Session["VARALCANCE"] = varalcance;
                    if (varalcance == 1 || varalcance == 5 || varalcance == 4 || varalcance == 6)
                    {
                        ObjGIndicadores.DACTUALIZARINDICADORR(Convert.ToInt32(CmbIndicadores.SelectedValue), Convert.ToInt32(CmbPeriodo.SelectedValue), Convert.ToInt32(Cmbseccional.SelectedValue), float.Parse(VALOR), Convert.ToInt32(IDVARIABLE));
                       
                    }
                    else
                    {
                        ObjGIndicadores.DACTUALIZARINDICADORR(Convert.ToInt32(CmbIndicadores.SelectedValue), Convert.ToInt32(CmbPeriodo.SelectedValue), 0, float.Parse(VALOR), Convert.ToInt32(IDVARIABLE));
                       
                    }

                }
                
            }
          
    

            if (Convert.ToInt32(Session["VARALCANCE"]) == 1 || Convert.ToInt32(Session["VARALCANCE"]) == 5 || Convert.ToInt32(Session["VARALCANCE"]) == 4 || Convert.ToInt32(Session["VARALCANCE"]) == 6)
            {
                ObjGIndicadores.DFORMULANACIONAL(Convert.ToInt32(CmbIndicadores.SelectedValue), Convert.ToInt32(CmbPeriodo.SelectedValue));
                ObjGIndicadores.DACTUALIZARSECCIONAL(Convert.ToInt32(CmbIndicadores.SelectedValue), Convert.ToInt32(CmbPeriodo.SelectedValue));
                ObjGIndicadores.DACTUALIZARSECCIONALREGIONAL(Convert.ToInt32(CmbIndicadores.SelectedValue), Convert.ToInt32(CmbPeriodo.SelectedValue));
                ObjGIndicadores.DACTUALIZARCUMPLIMIENTOR(Convert.ToInt32(CmbIndicadores.SelectedValue), Convert.ToInt32(CmbPeriodo.SelectedValue));
                ObjGIndicadores.DACTUALIZARCUMPLIMIENTOS(Convert.ToInt32(CmbIndicadores.SelectedValue), Convert.ToInt32(CmbPeriodo.SelectedValue));
            }
            else
            {
                ObjGIndicadores.DACTUALIZARNORMAL(Convert.ToInt32(CmbIndicadores.SelectedValue), Convert.ToInt32(CmbPeriodo.SelectedValue));
            }
            string script = "function f(){radopen(null, 'rwBusqueda'); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "customConfirmOpener", script, true);
            Div2.Visible = false;
            GrvCargardatos.Rebind();
            ObjGIndicadores.DACTUALIZARCUMPLIMIENTO(Convert.ToInt32(CmbIndicadores.SelectedValue), Convert.ToInt32(CmbPeriodo.SelectedValue));

        }

        protected void GrvCargardatos_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.BatchEditCommandName)
            {
                GridBatchEditingEventArgument argument = e.CommandArgument as GridBatchEditingEventArgument;
                Hashtable oldValues = argument.OldValues;
                Hashtable newValues = argument.NewValues;
                string newFirstName = newValues["FirstName"].ToString();
            }

        }

        protected void GrvCargardatos_ItemDataBound(object sender, GridItemEventArgs e)
        {
            foreach (GridDataItem item in GrvCargardatos.MasterTableView.Items)
            {
                TextBox TXTVALOR = (TextBox)(item.FindControl("VALOR"));
                string VALOR = item.GetDataKeyValue("VALOR").ToString();
                TXTVALOR.Text = VALOR;

                if (Convert.ToDouble(VALOR) > 0)
                {
                    TXTVALOR.Enabled = false;

                }
                else
                {
                    TXTVALOR.Enabled = true;
                }
            }

        }

        protected void btn_ok_Click(object sender, EventArgs e)
        {
            GrvCargardatos.Visible = false;
            //GrvCargardatos.ItemDataBound();

        }

        protected void btn_limpiar_Click1(object sender, EventArgs e)
        {
            CmbTipoIndicador2.SelectedValue = "";
            CmbTipoIndicador2.Text = "";
            gvrIndicadoresnotas.Visible = false;
            gvrIndicadoresnotas.DataSource = "";
            Cmbperiodo2.SelectedValue = "";
            CmbMedida.SelectedValue = "";
            CmbMedida.Text = "";

        }

        protected void CmbVigencia_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            Cmbperiodo2.DataSource = ObjGIndicadores.DCONSULTATIPO(Convert.ToInt32(CmbVigencia.SelectedValue));
            Cmbperiodo2.DataValueField = "ID_DET_PERIODO";
            Cmbperiodo2.DataTextField = "PERIODO";
            Cmbperiodo2.DataBind();
        }

        protected void btn_crear_Click(object sender, EventArgs e)
        {

            ObjGIndicadores.DINSERTARNOTAINDICADOR(Convert.ToInt32(CmbTipoIndicador2.SelectedValue), Convert.ToInt32(Cmbperiodo2.SelectedValue), TxtAnalisis.Text);
            TxtAnalisis.Text = "";
            CmbVigencia.SelectedValue = "";
            CmbVigencia.Text = "";
            Cmbperiodo2.SelectedValue = "";
            Cmbperiodo2.Text = "";
            //DResultado.Visible = true;
            //LblGuardado.Text = "!El análisis ha sido guardado correctamente!";


        }
    }
}



