﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CNegocios;
using System.Data.Entity;
using Telerik.Web.UI;

using System.Data.SqlClient;
using System.Configuration;
using System.Collections;

namespace INDICADORES_CALIDAD
{
    public partial class AdministradorResultados : System.Web.UI.Page
    {
        public SqlConnection Conexion;
        public IndicadoresBussines ObjGIndicadores = new IndicadoresBussines();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                CargarCombosMaestros("T_PERIODICIDAD", 1, 2, 2, CmbMedida);
                CargarCombosMaestros("T_TIPOINDICADOR", 1, 2, 2, CmbTipoIndicador);
                CargarCombosMaestros("T_PROCESO", 1, 2, 2, CmbProceso);

                CargarCombosMaestros("T_PERIODICIDAD", 1, 2, 2, CmbMedida);
                CargarCombosMaestros("T_REGIONAL", 1, 2, 2, CmbRegional);



            }

        }

        private void CargarCombosMaestros(string Tabla, int campo1, int campo2, int orden, RadComboBox Combo, int? campo3 = null, int? campo4 = null)
        {
            Conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["SCORBOARD"].ConnectionString);
            SqlCommand cmd = new SqlCommand("P_CARGA_COMBOS", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TABLA", Tabla);
            cmd.Parameters.AddWithValue("@CAMPO1", campo1);
            cmd.Parameters.AddWithValue("@CAMPO2", campo2);
            cmd.Parameters.AddWithValue("@ORDEN", orden);
            cmd.Parameters.AddWithValue("@CAMPO_FILTRO", campo3);
            cmd.Parameters.AddWithValue("@VALOR_FILTRO", campo4);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {

                da.Fill(ds);
                Combo.DataSource = ds;
                Combo.DataValueField = "1";
                Combo.DataTextField = "2";
                Combo.DataBind();

            }
            catch (Exception ex)
            {

            }

        }


        private void CargarPeriodicidad()
        {
            if (CmbMedida.SelectedValue == "7")
            {
                CmbPeriodo.Enabled = false;

            }
            else
            {
                if (CmbMedida.SelectedValue != "")
                {
                    CmbPeriodo.Enabled = true;
                    CmbPeriodo.DataSource = ObjGIndicadores.DCONSULTATIPO(Convert.ToInt32(CmbMedida.SelectedValue));
                    CmbPeriodo.DataValueField = "ID_DET_PERIODO";
                    CmbPeriodo.DataTextField = "PERIODO";
                    CmbPeriodo.DataBind();
                }

            }

        }

        protected void CmbIndicadores_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            DataSet ds = new DataSet();
            ds = ObjGIndicadores.DACONSULTARNACIONAL(Convert.ToInt32(CmbIndicadores.SelectedValue));
            if (ds.Tables[0].Rows.Count > 0)
            {
                int varalcance;
                varalcance = Convert.ToInt32(ds.Tables[0].Rows[0]["ID_ALCANCE"].ToString());
                if (varalcance == 1)
                {
                    LblSeccional.Visible = true;
                    lblregional.Visible = true;
                    CmbRegional.Visible = true;
                    CmbSeccional.Visible = true;





                }
                else
                {
                    LblSeccional.Visible = false;
                    lblregional.Visible = false;
                    CmbRegional.Visible = false;
                    CmbSeccional.Visible = false;

                }

                CargarCombosMaestros("T_PERIODICIDAD", 1, 2, 2, CmbMedida);
                CmbMedida.SelectedValue = ds.Tables[0].Rows[0]["ID_PERIODICIDAD"].ToString();
                CmbMedida.Enabled = false;
                CmbPeriodo.DataSource = ObjGIndicadores.DCONSULTATIPO(Convert.ToInt32(CmbMedida.SelectedValue));
                CmbPeriodo.DataValueField = "ID_DET_PERIODO";
                CmbPeriodo.DataTextField = "PERIODO";
                CmbPeriodo.DataBind();
            }
        }

        protected void CmbProceso_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {

            DataSet ds = new DataSet();
            ds = ObjGIndicadores.DBUSQUEDAPROCESO(Convert.ToInt32(CmbProceso.SelectedValue), Convert.ToInt32(CmbTipoIndicador.SelectedValue));
            if (ds.Tables[0].Rows.Count > 0)
            {
                CmbIndicadores.DataSource = ds;
                CmbIndicadores.DataValueField = "ID_INDICADOR";
                CmbIndicadores.DataTextField = "NOMBRE_INDICADOR";
                CmbIndicadores.DataBind();

            }


        }

        protected void CmbMedida_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            CargarPeriodicidad();

        }

        protected void GrvCargardatos_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (CmbRegional.SelectedValue == "" && CmbSeccional.SelectedValue == "")
            {
                GrvCargardatos.DataSource = ObjGIndicadores.DRESULTADO(Convert.ToInt32(CmbIndicadores.SelectedValue), 0, 0, 0);
            }
            else
            {
                if (CmbRegional.SelectedValue != "" && CmbPeriodo.SelectedValue != "")
                {
                    GrvCargardatos.DataSource = ObjGIndicadores.DRESULTADO(Convert.ToInt32(CmbIndicadores.SelectedValue), Convert.ToInt32(CmbRegional.SelectedValue), 0, Convert.ToInt32(CmbPeriodo.SelectedValue));
                }
                else
                {
                    GrvCargardatos.DataSource = ObjGIndicadores.DRESULTADO(Convert.ToInt32(CmbIndicadores.SelectedValue), 0, Convert.ToInt32(CmbSeccional.SelectedValue), Convert.ToInt32(CmbPeriodo.SelectedValue));

                }
                if (CmbRegional.SelectedValue == "" && CmbSeccional.SelectedValue == "" && CmbPeriodo.SelectedValue != "")
                {
                    GrvCargardatos.DataSource = ObjGIndicadores.DRESULTADO(Convert.ToInt32(CmbIndicadores.SelectedValue), 0, 0, Convert.ToInt32(CmbPeriodo.SelectedValue));
                }

            }

        }

        protected void btn_consultar_Click(object sender, EventArgs e)
        {
            GrvCargardatos.Visible = true;
            GrvCargardatos.Rebind();
        }

        protected void btn_limpiar_Click(object sender, EventArgs e)
        {
            CmbTipoIndicador.SelectedValue = "";
            CmbTipoIndicador.Text = "";
            CmbProceso.SelectedValue = "";
            CmbProceso.Text = "";
            CmbIndicadores.SelectedValue = "";
            CmbIndicadores.Text = "";
            GrvCargardatos.DataSource = "";
            GrvCargardatos.Visible = false;
            CmbMedida.SelectedValue = "";
            CmbMedida.Text = "";
            CmbMedida.Enabled = true;
            CmbPeriodo.SelectedValue = "";
            CmbPeriodo.Text = "";
            CmbRegional.SelectedValue = "";
            CmbRegional.Text = "";
            CmbSeccional.SelectedValue = "";
            CmbSeccional.Text = "";
            Lblresultado.Visible = false;

        }

        protected void GrvCargardatos_ItemDataBound(object sender, GridItemEventArgs e)
        {

            foreach (GridDataItem item in GrvCargardatos.MasterTableView.Items)
            {
                TextBox TXTVALOR = (TextBox)(item.FindControl("RESULTADO"));
                string VALOR = item.GetDataKeyValue("RESULTADO").ToString();
                TXTVALOR.Text = VALOR;


                TextBox TXTMETA = (TextBox)(item.FindControl("META"));
                string META = item.GetDataKeyValue("META").ToString();
                TXTMETA.Text = META;
            }
        }

        protected void btn_guardar_Click(object sender, EventArgs e)
        {
            foreach (GridDataItem item in GrvCargardatos.Items)
            {
                string RESULTADO;
                TextBox VALORTXT = (TextBox)(item.FindControl("RESULTADO"));
                RESULTADO = VALORTXT.Text;

                TextBox TXTMETA = (TextBox)(item.FindControl("META"));
                string META = item.GetDataKeyValue("META").ToString();
                META = TXTMETA.Text;

                if (CmbRegional.SelectedValue != "" && CmbPeriodo.SelectedValue != "")
                {

                    DataSet ds = new DataSet();
                    ds = ObjGIndicadores.DUPDATERESULTADOS(Convert.ToInt32(CmbIndicadores.SelectedValue), Convert.ToInt32(CmbRegional.SelectedValue), 0, Convert.ToInt32(CmbPeriodo.SelectedValue), Convert.ToDouble(RESULTADO), Convert.ToDouble(META));
                    Lblresultado.Text = "Se actualizo correctamente";
                    Lblresultado.ForeColor = System.Drawing.Color.Green;
                    Lblresultado.Visible = true;


                }
                else
                {
                    if (CmbSeccional.SelectedValue != "" && CmbPeriodo.SelectedValue != "")
                    {

                        DataSet ds = new DataSet();
                        ds = ObjGIndicadores.DUPDATERESULTADOS(Convert.ToInt32(CmbIndicadores.SelectedValue), 0, Convert.ToInt32(CmbSeccional.SelectedValue), Convert.ToInt32(CmbPeriodo), Convert.ToDouble(RESULTADO), Convert.ToDouble(META));
                        Lblresultado.Text = "Se actualizo correctamente";
                        Lblresultado.ForeColor = System.Drawing.Color.Green;
                        Lblresultado.Visible = true;
                    }
                    else
                    {
                        if (CmbRegional.SelectedValue == "" && CmbSeccional.SelectedValue == "" && CmbPeriodo.SelectedValue != "")
                        {
                            DataSet ds = new DataSet();
                            ds = ObjGIndicadores.DUPDATERESULTADOS(Convert.ToInt32(CmbIndicadores.SelectedValue), 0, 0, Convert.ToInt32(CmbPeriodo), Convert.ToDouble(RESULTADO), Convert.ToDouble(META));
                            Lblresultado.Text = "Se actualizo correctamente";
                            Lblresultado.ForeColor = System.Drawing.Color.Green;
                            Lblresultado.Visible = true;
                        }

                    }

                }


            }
        }

        protected void CmbRegional_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (CmbRegional.SelectedValue != "0")
            {

                CargarCombosMaestros("T_SECCIONAL", 1, 2, 2, CmbSeccional,3,Convert.ToInt32(CmbRegional.SelectedValue));
            }
            else
            {

                Dvbusqueda1.Visible = true;
                LblError.Text = "Debe elegir una regional";
            }
        }

     
    }


}
