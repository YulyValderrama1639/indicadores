﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CargarDatos.aspx.cs" Inherits="INDICADORES_CALIDAD.CargarDatos" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <telerik:RadWindowManager runat="server" ID="RwAyudas" Modal="True" Font-Names="Agency FB" Behaviors="Close,Move"
        RenderMode="Lightweight"   Skin="Bootstrap">
        <Windows>

            <telerik:RadWindow ID="RwAnalisis"  Width="900px" Height="300px" IconUrl="~/images/icons/favicon.ico" Behaviors="Default" Font-Names="Agency FB" RenderMode="Mobile" Title="INDICADORES-CALIDAD" Font-Bold="true" Skin="Bootstrap" Modal="true" CenterIfModal="true" VisibleStatusbar="false"
                NavigateUrl="~/Analisis.aspx" runat="server">
            </telerik:RadWindow>

            <telerik:RadWindow ID="rwBusqueda" IconUrl="~/images/icons/favicon.ico" Behaviors="Default" Font-Names="Agency FB" RenderMode="Mobile" Title="INDICADORES-CALIDAD" Font-Bold="true" Skin="Bootstrap" Modal="true" CenterIfModal="true" VisibleStatusbar="false"
                runat="server">
                <ContentTemplate>
                    <div style="align-content: center; text-align: center">
                        <asp:Label runat="server" ID="LblVerificar" Text="¡Esta seguro de guardados los datos no podran ser modificados!" Font-Bold="true" Font-Size="12pt" Font-Names="Helvetica"></asp:Label>
                        <br />
                        <asp:Button runat="server" ID="btn_ok" OnClick="btn_ok_Click" Text="OK" />
                    </div>
                </ContentTemplate>
            </telerik:RadWindow>

        </Windows>



    </telerik:RadWindowManager>

    <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel1"></telerik:RadAjaxLoadingPanel>
    <telerik:RadAjaxPanel runat="server" PostBackControls="ImageButton1">
        <div class="col-sm-12" runat="server" id="DivAlert">
            <div class="DivGeneral DivColor">
                <asp:Image runat="server" ImageUrl="~/images/play.png" />
                <asp:Label Text="DATOS" Font-Bold="true" Font-Size="20pt" runat="server" Font-Names="Arial Narrow"></asp:Label>


            </div>
        </div>

        <div class="col-sm-12">
            <telerik:RadTabStrip runat="server" ID="RadUsuarios" Font-Names="Agency FB" MultiPageID="RadMultiPage1"  Skin="MetroTouch"  SelectedIndex="1" >
                <Tabs>
                    <telerik:RadTab Text="1.PASO CARGAR DATOS" ImageUrl="~/images/subir.png" Font-Bold="true" Width="250px"  runat="server" Selected="True"></telerik:RadTab>
                    <telerik:RadTab Text="2.PASO ANALISIS CUALITATIVO" Font-Bold="true" Width="250px" runat="server"></telerik:RadTab>


                </Tabs>
            </telerik:RadTabStrip>
        </div>



        <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="0" >

            <telerik:RadPageView ID="Pagina1" runat="server">
                <div class="ContendDivRegistro" style="width: 1079px; height: auto">
                    <h2 class="Contendh3">DATOS INDICADORES</h2>

                    <div style="text-align: center; align-content: center; height: auto">
                        <table style="width: 100%">
                            <tr>
                                <td colspan="4">

                                    <asp:Label runat="server" Text="Seleccione  indicador" CssClass="Labels" Font-Bold="true"></asp:Label>
                                    <br />
                                    <telerik:RadComboBox ID="CmbIndicadores" AutoPostBack="true" EmptyMessage="Seleccionar indicador" Filter="Contains" OnSelectedIndexChanged="CmbIndicadores_SelectedIndexChanged" Width="450px" runat="server" Skin="Bootstrap"></telerik:RadComboBox>
                                    <br />
                                    <asp:Label runat="server" Text="Seleccione seccional " Visible="false" ID="lblseccional" CssClass="Labels" Font-Bold="true"></asp:Label>
                                    <br />
                                    <telerik:RadComboBox ID="Cmbseccional" Filter="Contains" Visible="false" EmptyMessage="Seleccionar seccional" Width="450px" runat="server" Skin="Bootstrap"></telerik:RadComboBox>
                                </td>

                            </tr>
                            <tr>
                                <td colspan="2">


                                    <asp:Label Text="Seleccionar medida" Font-Bold="true" Font-Size="12pt" runat="server" Font-Names="Arial Narrow"></asp:Label>
                                    <br />
                                    <telerik:RadComboBox runat="server" ID="CmbMedida" AutoPostBack="true" Width="250px" EmptyMessage="Seleccione analisis de medida" OnSelectedIndexChanged="CmbMedida_SelectedIndexChanged" Skin="Bootstrap">
                                    </telerik:RadComboBox>


                                </td>
                                <td>
                                    <asp:Label Text="Seleccionar Periodo" Font-Bold="true" Font-Size="12pt" runat="server" Font-Names="Arial Narrow"></asp:Label>
                                    <br />
                                    <telerik:RadComboBox runat="server" Filter="StartsWith" ID="CmbPeriodo" Skin="Bootstrap" Width="250px" EmptyMessage="Seleccione periodo">
                                    </telerik:RadComboBox>
                                </td>
                            </tr>




                        </table>
                        <br />

                        <telerik:RadButton runat="server" ID="btn_consultar" Text="Cargar datos" ForeColor="#ffffff" BackColor="#660066" Skin="BlackMetroTouch" OnClick="btn_consultar_Click"></telerik:RadButton>

                        <telerik:RadButton runat="server" ID="btn_guardar" Text="Guardar" ForeColor="#ffffff" BackColor="#006600" Skin="BlackMetroTouch" OnClick="btn_guardar_Click"></telerik:RadButton>
                        <telerik:RadButton runat="server" ID="btn_limpiar" Text="Limpiar" Skin="BlackMetroTouch" OnClick="btn_limpiar_Click"></telerik:RadButton>
                        <br />
                        <asp:Label runat="server" ID="Lblresultado" Visible="false"></asp:Label>
                        <br />
                        <br />


                    </div>
                    <div class="col-sm-12" runat="server" id="Div2" visible="false">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <span class="badge badge-pill badge-danger">Alerta</span> ¡Verifique los datos, ya que una vez guardados no se podran modificar!
                   
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                     

                 
                    <telerik:RadGrid runat="server" ID="GrvCargardatos" Visible="false" Font-Size="10pt" OnItemCommand="GrvCargardatos_ItemCommand" OnItemDataBound="GrvCargardatos_ItemDataBound" PageSize="20" MasterTableView-DataKeyNames="ID_VARIABLE" AllowAutomaticUpdates="True" OnNeedDataSource="GrvCargardatos_NeedDataSource" AllowPaging="True" AllowSorting="True" Skin="MetroTouch">
                        <AlternatingItemStyle BackColor="#F0F0F0" />
                        <MasterTableView CommandItemDisplay="None" DataKeyNames="ID_VARIABLE,VALOR"
                            AutoGenerateColumns="False">

                            <CommandItemStyle BackColor="#e5e5e5" VerticalAlign="Middle" />
                            <BatchEditingSettings EditType="Cell" />
                            <SortExpressions>
                                <telerik:GridSortExpression FieldName="ID_VARIABLE" SortOrder="None" />
                            </SortExpressions>
                            <Columns>

                                <telerik:GridBoundColumn DataField="ID_VARIABLE" HeaderText=" ID_VARIABLE " ReadOnly="true">
                                    <HeaderStyle Width="100px" Font-Bold="true" />

                                    <ItemStyle Font-Size="10pt" Font-Names="Arial" ForeColor="Black" Height="50px" Width="100px" HorizontalAlign="Justify" />

                                </telerik:GridBoundColumn>

                                <telerik:GridBoundColumn DataField="NOMBRE_VAR" HeaderText="NOMBRE VARIABLE" ReadOnly="true">
                                    <HeaderStyle Width="100px" Font-Bold="true" />
                                    <ItemStyle Font-Size="12pt" ForeColor="Black" Height="50px" Width="100px" HorizontalAlign="Justify" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="NMES" HeaderText="MES" ReadOnly="true">
                                    <HeaderStyle Width="100px" Font-Bold="true" />
                                    <ItemStyle Font-Size="10pt" Font-Names="Arial" Height="50px" Width="20px" HorizontalAlign="Justify" />
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn   HeaderStyle-Width="80px" HeaderText="VALOR">
                                    <ItemTemplate>
                                    <asp:TextBox ID="VALOR" runat="server"></asp:TextBox>
        
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="True" Width="80px" />
                                </telerik:GridTemplateColumn>
                            </Columns>
                        </MasterTableView>
                        <GroupingSettings CollapseAllTooltip="Collapse all groups" />
                        <ClientSettings AllowKeyboardNavigation="true"></ClientSettings>
                        <HeaderStyle BackColor="#33BDA9" Font-Names="Arial Narrow" ForeColor="White" />
                        <ItemStyle BackColor="White" />
                    </telerik:RadGrid>
                      <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                        <ProgressTemplate>
                            <div class="text-center">
                                <span style="font-size: 14pt; color: grey">Procesando....</span>
                                <asp:Image ID="imgLoader" runat="server" ImageUrl="~/images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </div>
            </telerik:RadPageView>


            <telerik:RadPageView ID="Pagina2" runat="server">
                <div class="ContendDivRegistro" style="width: 1079px; height: auto">
                    <h2 class="Contendh3">DATOS INDICADORES</h2>

                    <div style="text-align: center; align-content: center">
                        <table style="width: 100%">
                            <tr>
                                <td colspan="2">
                                    <asp:Label Text="INDICADOR" Font-Bold="true" Font-Size="12pt" runat="server" Font-Names="Arial Narrow"></asp:Label>
                                    <br />

                                    <telerik:RadComboBox ID="CmbTipoIndicador2" Filter="Contains" AutoPostBack="true" runat="server" EmptyMessage="Seleccione el tipo de indicador" Skin="Bootstrap" Width="450px"></telerik:RadComboBox>
                                   </td>
                            </tr>
                         
                        </table>
                          <table style="width: 100%">
                    <tr>
                        <td colspan="1">
                            <asp:Label Text="Análisis de medida" Font-Bold="true" ForeColor="#767676" runat="server" Font-Size="15pt" Font-Names="Agency FB">
                            </asp:Label>
                            <br />
                            <telerik:RadComboBox runat="server" ID="CmbVigencia" AutoPostBack="true" Width="250px" EmptyMessage="Seleccione analisis de medida" OnSelectedIndexChanged="CmbVigencia_SelectedIndexChanged" Skin="Bootstrap">
                            </telerik:RadComboBox>
                       
                        </td>
                        <td>
                            <asp:Label Text="Periodo" Font-Bold="true" ForeColor="#767676" runat="server" Font-Size="15pt" Font-Names="Agency FB"></asp:Label>
                            <br />
                            <telerik:RadComboBox runat="server" ID="Cmbperiodo2" Skin="Bootstrap" Width="250px" EmptyMessage="Seleccione periodo">
                            </telerik:RadComboBox>
                        </td>
                    </tr>

                      

                </table>
                        <br />

                            <telerik:RadTextBox ID="TxtAnalisis" Width="800px" Height="100px" TextMode="MultiLine" runat="server"></telerik:RadTextBox>
                        <br />
                        <br />
                        <telerik:RadButton runat="server" ID="btn_crear" RenderMode="Lightweight" Skin="BlackMetroTouch" Width="100px" Text="Crear" Height="30px"  OnClick="btn_crear_Click" ></telerik:RadButton>
                        <telerik:RadButton ID="btnConsultar2" runat="server" BackColor="#660066" CssClass="TextosSitio" Font-Size="12pt" Height="30px" OnClick="btnConsultar2_Click" RenderMode="Lightweight" Skin="BlackMetroTouch" Text="Consultar" Width="100px"></telerik:RadButton>
                        <telerik:RadButton runat="server" ID="btn_limpiar1" RenderMode="Lightweight" Skin="BlackMetroTouch" Width="100px" Text="Limpiar" Height="30px" OnClick="btn_limpiar_Click1"></telerik:RadButton>

                         <div class="col-sm-12" runat="server" id="Diverroranalisis" visible="false">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <span class="badge badge-pill badge-danger">Alerta</span> ¡Debe seleccionar el indicador!
                   
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                    </div>
                    <br />


                    <br />
                </div>
                <br />
               
                       
                <br />


                <telerik:RadGrid ID="gvrIndicadoresnotas" runat="server" AllowPaging="true" Font-Names="Arial" AutoGenerateColumns="false" OnNeedDataSource="gvrIndicadoresnotas_NeedDataSource" PageSize="50" Skin="MetroTouch" Visible="false" Width="100%">
                    <ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true">
                    </ClientSettings>
                    <PagerStyle Mode="NextPrevAndNumeric"></PagerStyle>
                    <MasterTableView>
                        <PagerStyle Mode="NextPrevNumericAndAdvanced"></PagerStyle>
                        <Columns>

                           

                            <telerik:GridBoundColumn AllowFiltering="true" DataField="NOTA" HeaderText=" ANÁLISIS CUALITATIVO DEL INDICADOR" >
                                <HeaderStyle Font-Bold="true" Wrap="true" Width="800px" Font-Names="Arial" ForeColor="Black" HorizontalAlign="Center" Font-Size="8pt" />
                                <ItemStyle Font-Size="8pt" Wrap="true" Width="800px" Font-Names="Arial" />
                            </telerik:GridBoundColumn>
                            
                            <telerik:GridBoundColumn AllowFiltering="true" DataField="MES" HeaderText="PERIODO">
                                <HeaderStyle Font-Bold="true" Wrap="true" Width="800px" Font-Names="Arial" ForeColor="Black" HorizontalAlign="Center" Font-Size="8pt" />
                                <ItemStyle Font-Size="8pt" Wrap="true" Width="800px" Font-Names="Arial" />
                            </telerik:GridBoundColumn>

                                <telerik:GridBoundColumn AllowFiltering="true" DataField="VIGENCIA" HeaderText="VIGENCIA">
                                <HeaderStyle Font-Bold="true" Wrap="true" Width="800px" Font-Names="Arial" ForeColor="Black" HorizontalAlign="Center" Font-Size="8pt" />
                                <ItemStyle Font-Size="8pt" Wrap="true" Width="800px" Font-Names="Arial" />
                            </telerik:GridBoundColumn>



                        </Columns>

                    </MasterTableView>
                    <HeaderStyle CssClass="HeaderStyleGrid" />
                    <FooterStyle CssClass="footerStyle" />
                </telerik:RadGrid>
          
            </telerik:RadPageView>


        </telerik:RadMultiPage>



    </telerik:RadAjaxPanel>

    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function ShowAnalisis()  {
                var ID = $find("<%= CmbIndicadores.ClientID %>").get_value();  
                alert(ID);
                   window.radopen("Analisis.aspx?ID_INDICADOR=" + ID );
                return false;
            }
           </script>

    </telerik:RadCodeBlock>
</asp:Content>
