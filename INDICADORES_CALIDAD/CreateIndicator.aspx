﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CreateIndicator.aspx.cs" Inherits="INDICADORES_CALIDAD.CreateIndicator" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script>
        function keyPress(sender, args) {
            var text = sender.get_value() + args.get_keyCharacter();
            if (!text.match('^[0-9.]+$'))
                args.set_cancel(true);

        }
    </script>
    <telerik:RadWindowManager runat="server" ID="RwAyudas" Modal="True" Behaviors="Close,Move"
        RenderMode="Lightweight" Width="400px" Height="200px" Skin="Bootstrap">
        <Windows>
            <telerik:RadWindow ID="rwBusqueda" Behaviors="Default" Font-Names="Arial" RenderMode="Mobile" Title="INDICADORES-CALIDAD" Font-Bold="true" Modal="true" CenterIfModal="true" VisibleStatusbar="false" runat="server">
                <ContentTemplate>
                    <div style="align-content: center; text-align: center">
                        <asp:Label runat="server" ID="LblVerificar" Text="Se realizo la creación del indicador con éxito </br> Adjunto al correo electrónico encontrara el detalle del indicador " Font-Size="15pt" Font-Names="agency fb"></asp:Label>
                    </div>
                </ContentTemplate>

            </telerik:RadWindow>


            <telerik:RadWindow ID="rwGuardado" IconUrl="~/Imagenes/IconosFormularios/faviconC.png" Behaviors="Close" RenderMode="Mobile" Title="Registro-Congreso" Skin="Bootstrap" Modal="true" CenterIfModal="true" VisibleStatusbar="false"
                runat="server">
            </telerik:RadWindow>

        </Windows>
    </telerik:RadWindowManager>
    <telerik:RadAjaxPanel runat="server">
        <div runat="server" id="DivAlert">
            <div class="DivGeneral DivColor">
                <asp:Image runat="server" ImageUrl="~/images/play.png" />
                <asp:Label Text="CREAR INDICADORES" Font-Bold="true" Font-Size="20pt" runat="server" Font-Names="Arial Narrow"></asp:Label>

            </div>
        </div>

        <telerik:RadWizard RenderMode="Lightweight" ID="RadCrearindicador" OnFinishButtonClick="RadCrearindicador_FinishButtonClick" runat="server" Height="950px" Skin="MetroTouch">
            <WizardSteps>

                <telerik:RadWizardStep ID="RadWizardStep1" Title="PASO1.CREAR DATOS" Font-Names="Arial" Font-Size="12pt" ImageUrl="images/add-documents.png" Font-Bold="true" runat="server" StepType="Start">
                    <div class="col-sm-12" runat="server" id="Div1">
                        <table style="width: 100%">
                            <tr>

                                <td>
                                    <asp:Label runat="server" Text="Nombre indicador" CssClass="Labels" Font-Bold="true"></asp:Label>
                                    <br>
                                    <telerik:RadTextBox runat="server" ID="TxtNombre" EmptyMessage="Nombre de indicador" Width="350px" Skin="Bootstrap"></telerik:RadTextBox>
                                </td>

                                <td>
                                    <asp:Label runat="server" Text="Empresa" CssClass="Labels" Font-Bold="true"></asp:Label>
                                    <br />
                                    <telerik:RadComboBox ID="CmbEmpresa" Width="350px" runat="server" EmptyMessage="Seleccionar empresa" AutoPostBack="true" OnSelectedIndexChanged="CmbEmpresa_SelectedIndexChanged" Skin="Bootstrap"></telerik:RadComboBox>

                                </td>


                            </tr>
                            <tr>

                                <td>
                                    <asp:Label runat="server" Text="Clasificación del indicador" CssClass="Labels" Font-Bold="true"></asp:Label>
                                    <br />
                                    <telerik:RadComboBox ID="CmbClasificacion" Width="350px" runat="server" EmptyMessage="Seleccionar clasificación" Skin="Bootstrap"></telerik:RadComboBox>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text="Tipo de indicador" Font-Bold="true" CssClass="Labels"></asp:Label>
                                    <br />
                                    <telerik:RadComboBox ID="CmbTipoIndicador" Width="350px" runat="server" AutoPostBack="true" EmptyMessage="Seleccionar Tipo de indicador" Skin="Bootstrap" OnSelectedIndexChanged="CmbTipoIndicador_SelectedIndexChanged"></telerik:RadComboBox>

                                </td>
                            </tr>
                            <tr>

                                <td>
                                    <asp:Label runat="server" Text="Fecha creación" CssClass="Labels" Font-Bold="true"></asp:Label>
                                    <br />
                                    <telerik:RadDatePicker runat="server" ID="radFechainic" Calendar-CultureInfo="es-CO"></telerik:RadDatePicker>
                                </td>

                                <td>
                                    <asp:Label runat="server" Text="Propósito" CssClass="Labels" Font-Bold="true"></asp:Label>
                                    <br />
                                    <telerik:RadTextBox runat="server" Width="350px" ID="TxtProposito" Skin="Bootstrap"></telerik:RadTextBox>
                                </td>
                            </tr>





                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lblperspectiva" Text="Perspectiva" Visible="false" Font-Bold="true" CssClass="Labels"></asp:Label>
                                    <br />
                                    <telerik:RadComboBox ID="Cmbperspectiva" Visible="false" Width="350px" runat="server" AutoPostBack="true" EmptyMessage="Seleccionar perspectiva" OnSelectedIndexChanged="Cmbperspectiva_SelectedIndexChanged" Skin="Bootstrap"></telerik:RadComboBox>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="LblInductor" Text="Inductor" Visible="false" Font-Bold="true" CssClass="Labels"></asp:Label>
                                    <br />
                                    <telerik:RadComboBox ID="cmbinductor" Visible="false" Width="350px" runat="server" EmptyMessage="Seleccione el inductor" Skin="Bootstrap"></telerik:RadComboBox>
                                </td>

                            </tr>

                            <tr>

                                <td>
                                    <asp:Label runat="server" Text="Proceso" ID="LblProceso" Font-Bold="true" CssClass="Labels"></asp:Label>
                                    <br />
                                    <telerik:RadComboBox ID="CmbProceso" Width="350px" AutoPostBack="true" EmptyMessage="Seleccionar proceso" runat="server" Skin="Bootstrap" OnSelectedIndexChanged="CmbProceso_SelectedIndexChanged"></telerik:RadComboBox>
                                </td>


                            </tr>
                            <tr>

                                <td>
                                    <asp:Label runat="server" Text="Periodicidad de medición" CssClass="Labels" Font-Bold="true"></asp:Label>
                                    <br />
                                    <telerik:RadComboBox ID="Cmbperiodicidad" EmptyMessage="Seleccionar periodicidad" Width="350px" runat="server" Skin="Bootstrap"></telerik:RadComboBox>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text="Unidad Medida" Font-Bold="true" CssClass="Labels"> </asp:Label>
                                    <br />
                                    <telerik:RadComboBox ID="CmbUnidad" Width="350px" runat="server" Skin="Bootstrap" EmptyMessage="Seleccionar unidad de medida"></telerik:RadComboBox>
                                </td>

                            </tr>

                            <tr>

                                <td>
                                    <asp:Label runat="server" Text="Periodicidad de análisis" Font-Bold="true" CssClass="Labels"></asp:Label>
                                    <br />
                                    <telerik:RadComboBox ID="Cmbperiodicidada" Width="350px" EmptyMessage="Seleccionar periodicidad de análisis" runat="server" Skin="Bootstrap"></telerik:RadComboBox>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text="Alcance de medición" Font-Bold="true" CssClass="Labels"></asp:Label>
                                    <br />
                                    <telerik:RadComboBox ID="CmbAlcance" EmptyMessage="Seleccionar alcance" Width="350px" runat="server" Skin="Bootstrap"></telerik:RadComboBox>
                                </td>

                            </tr>
                            <tr>
                                <td>

                                    <asp:Label runat="server" Text="Tipo de medición" Font-Bold="true" CssClass="Labels"></asp:Label>
                                    <br />
                                    <telerik:RadComboBox ID="CmtipoMedicion" EmptyMessage="Seleccionar alcance" Width="350px" runat="server" Skin="Bootstrap"></telerik:RadComboBox>


                                </td>

                                <td>
                                    <asp:Label runat="server" Text="Descripción de la fórmula matemática" Font-Bold="true" CssClass="Labels"></asp:Label>
                                    <br />
                                    <telerik:RadTextBox runat="server" ID="TxtFormula" Width="350px" Height="80px" TextMode="MultiLine" Skin="Bootstrap"></telerik:RadTextBox>
                                <td />


                            </tr>


                            <tr>
                                <td>
                                    <asp:Label runat="server" Text="Meta" Font-Bold="true" CssClass="Labels"></asp:Label>
                                    <br />
                                    <telerik:RadTextBox runat="server" ID="TxtMeta" Width="350px" Skin="Bootstrap">
                                        <ClientEvents OnKeyPress="keyPress" />
                                    </telerik:RadTextBox>
                                </td>
                                <td>

                                    <asp:Label runat="server" Text="Tendencia del resultado" Font-Bold="true" CssClass="Labels"></asp:Label>
                                    <br />
                                    <telerik:RadComboBox ID="CmbTendenciaI" EmptyMessage="Seleccionar tendencia" Width="350px" runat="server" Skin="Bootstrap"></telerik:RadComboBox>


                                </td>

                            </tr>


                            <tr>
                                <td>
                                    <asp:Label runat="server" Text="Limite" Font-Bold="true" CssClass="Labels"></asp:Label>
                                    <br />
                                    <telerik:RadComboBox runat="server" Width="250px" ID="CmbLimite" EmptyMessage="Seleccionar Limite" Skin="Bootstrap"></telerik:RadComboBox>
                                </td>

                            </tr>
                        </table>

                    </div>
                    <div style="align-content: center; text-align: center">
                        <telerik:RadButton runat="server" Text="Guardar" ID="btn_guardar" OnClick="btn_guardar_Click" Width="250px" Skin="BlackMetroTouch"></telerik:RadButton>
                        <telerik:RadButton runat="server" Text="Actualizar" ID="btn_actualizar" Visible="false" OnClick="btn_actualizar_Click" Width="250px" Skin="BlackMetroTouch"></telerik:RadButton>

                    </div>
                    <br />
                    <div class="col-sm-12" style="width: 500px" runat="server" id="DivError" visible="false">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <span class="badge badge-pill badge-danger">Error</span>
                            <asp:Label runat="server" ID="LblValidar" Visible="false"></asp:Label>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                    <asp:UpdateProgress ID="UpdateProgress4" runat="server">
                        <ProgressTemplate>
                            <div class="text-center">
                                <span style="font-size: 14pt; color: grey">Procesando datos....</span>
                                <asp:Image ID="imgLoader12" runat="server" ImageUrl="~/images/Progress.gif" Width="105px" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>

                </telerik:RadWizardStep>
                <telerik:RadWizardStep ID="RadWizardStep4" Title="PASO2.LIMITES" Font-Names="Arial" Font-Size="12pt" ImageUrl="~/images/barras.png" Font-Bold="true" runat="server">
                    <div class="ContendDivRegistro" style="width: 850px; height: auto">
                        <h2 class="Contendh3">LIMITES</h2>
                           <div>
                             <asp:Label ID="Label1"  Text="META PARA EL INDICADOR"  Font-Bold="true" runat="server"></asp:Label>
                            <asp:Label ID="lblMeta"  Font-Size="20pt"  Font-Names="Agency FB" ForeColor="#000099" Font-Bold="true" runat="server"></asp:Label>
                        </div>
                        <br />
                        <table style="width: 50%">

                            <tr>
                                <td>
                                    <asp:Label runat="server" Text="Bandera Roja" ID="lblRojo" ForeColor="Red" CssClass="Labels" Font-Bold="true"></asp:Label>
                                    <br />
                                    <telerik:RadTextBox runat="server" ID="TxtRoja" Skin="Bootstrap"></telerik:RadTextBox>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <asp:Label runat="server" Text="Peor" ID="lblpeor" ForeColor="#ffcc00" CssClass="Labels" Font-Bold="true"></asp:Label>
                                    <br />
                                    <telerik:RadTextBox runat="server" ID="TxtPeor" Skin="Bootstrap"></telerik:RadTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" Text="Alcance" ID="Lblalcance" ForeColor="Green" CssClass="Labels" Font-Bold="true"></asp:Label>
                                    <br />
                                    <telerik:RadTextBox runat="server" ID="TxtAlcance" Skin="Bootstrap"></telerik:RadTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" Text="Mejor" ID="lblmejor" ForeColor="#0066ff" CssClass="Labels" Font-Bold="true"></asp:Label>
                                    <br />
                                    <telerik:RadTextBox runat="server" ID="TxtMejor" Skin="Bootstrap"></telerik:RadTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>

                                </td>
                            </tr>
                        </table>
                        
                     
                        <br />
                               <telerik:RadButton runat="server" ID="btnG" Text="Actualizar" Width="250px" Skin="BlackMetroTouch" OnClick="btnG_Click"></telerik:RadButton>
                    </div>
                      <asp:UpdateProgress ID="UpdateProgress5" runat="server">
                        <ProgressTemplate>
                            <div class="text-center">
                                <span style="font-size: 14pt; color: grey">Actualizando datos....</span>
                                <asp:Image ID="imgLoader13" runat="server" ImageUrl="~/images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>


                </telerik:RadWizardStep>
                <telerik:RadWizardStep ID="RadWizardStep2" Title="PASO3. CREAR VARIABLES" Font-Names="Arial" Font-Size="12pt" ImageUrl="~/images/formula.png" Font-Bold="true" runat="server">
                    <div class="ContendDivRegistro" style="width: 850px; height: auto">
                        <h2 class="Contendh3">CREAR VARIABLES</h2>
                        <div style="text-align: center; align-content: center">
                            <table style="width: 100%">
                                <tr>
                                    <td colspan="3">
                                        <br />
                                        <asp:Label runat="server" Text="Nombre de variable" CssClass="Labels" Font-Bold="true"></asp:Label>
                                        <br />
                                        <telerik:RadTextBox runat="server" ID="TxtNombreVariable" EmptyMessage="Seleccionar unidad de medida" Width="350px" Skin="Bootstrap"></telerik:RadTextBox>

                                    </td>

                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server" Text="Fuente de origen" CssClass="Labels" Font-Bold="true"></asp:Label>
                                        <br />
                                        <telerik:RadComboBox ID="CmbFuente" EmptyMessage="Seleccionar fuente" Width="250px" runat="server" Skin="Bootstrap"></telerik:RadComboBox>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" Text="Tipo de dato" CssClass="Labels" Font-Bold="true"></asp:Label>
                                        <br />
                                        <telerik:RadComboBox ID="CmbTipoDato" EmptyMessage="Seleccionar Tipo" Width="250px" runat="server" Skin="Bootstrap"></telerik:RadComboBox>
                                    </td>

                                    <td>
                                        <asp:Label runat="server" Text="Observacion" CssClass="Labels" Font-Bold="true"></asp:Label>
                                        <br />
                                        <telerik:RadTextBox runat="server" ID="txtObservacion" Skin="Bootstrap" Width="250px"></telerik:RadTextBox>
                                    </td>

                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <br />
                                        <telerik:RadButton ID="btn_agregar" runat="server" Text="Agregar" Skin="BlackMetroTouch" OnClick="btn_agregar_Click"></telerik:RadButton>



                                    </td>
                                </tr>
                            </table>

                            <div style="align-content: center; text-align: center;">
                                <div class="col-sm-12" style="width: 600px; text-align: center" runat="server" id="DivErrorC" visible="false">
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        <asp:Label runat="server" ID="Lblvariablesc" Font-Size="10pt" Visible="false"></asp:Label>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <br />




                        </div>
                    </div>
                    <br />

                    <telerik:RadGrid ID="GrvVariables" runat="server" AllowPaging="True" AllowSorting="True" Font-Size="10pt" OnDeleteCommand="GrvVariables_DeleteCommand" OnNeedDataSource="GrvVariables_NeedDataSource" PageSize="1" Skin="Vista" Visible="false">
                        <MasterTableView AutoGenerateColumns="False" DataKeyNames="ID_VARIABLE" TableLayout="Fixed" Width="100%">
                            <Columns>
                                <telerik:GridButtonColumn CommandName="Delete" HeaderStyle-Width="50px" Text="Delete" UniqueName="DeleteColumn" Visible="false" />
                                <telerik:GridBoundColumn DataField="ID_VARIABLE" HeaderText="ID_VARIABLE">
                                    <HeaderStyle Font-Bold="true" ForeColor="#003399" Width="150px" />
                                    <ItemStyle Font-Size="10pt" ForeColor="#000000" Height="50px" HorizontalAlign="Justify" Width="100px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridNumericColumn DataField="VALOR" HeaderText="VALOR">
                                    <HeaderStyle Font-Bold="true" ForeColor="#003399" Width="150px" />
                                    <ItemStyle Font-Size="10pt" ForeColor="#000000" Height="50px" HorizontalAlign="Justify" Width="100px" />
                                </telerik:GridNumericColumn>
                                <telerik:GridBoundColumn DataField="FUENTE" HeaderText="FUENTE">
                                    <HeaderStyle Font-Bold="true" ForeColor="#003399" Width="150px" />
                                    <ItemStyle Font-Size="10pt" ForeColor="#000000" Height="50px" HorizontalAlign="Justify" Width="100px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="DES_TIPOD" HeaderText="TIPO DATO">
                                    <HeaderStyle Font-Bold="true" ForeColor="#003399" Width="150px" />
                                    <ItemStyle Font-Size="10pt" ForeColor="#000000" Height="50px" HorizontalAlign="Justify" Width="100px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FILA" HeaderText="ALIAS">
                                    <HeaderStyle Font-Bold="true" ForeColor="#003399" Width="150px" />
                                    <ItemStyle Font-Size="10pt" ForeColor="#000000" Height="50px" HorizontalAlign="Justify" Width="100px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="OBSERVACION" HeaderText="OBSERVACIÓN">
                                    <HeaderStyle Font-Bold="true" ForeColor="#003399" Width="150px" />
                                    <ItemStyle Font-Size="10pt" ForeColor="#000000" Height="50px" HorizontalAlign="Justify" Width="100px" />
                                </telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>

                    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                        <ProgressTemplate>
                            <div class="text-center">
                                <span style="font-size: 14pt; color: grey">Procesando....</span>
                                <asp:Image ID="imgLoader1" runat="server" ImageUrl="~/images/Progress.gif" Height="78px" Width="89px" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>

                </telerik:RadWizardStep>
                <telerik:RadWizardStep ID="RadwizarVariables" runat="server" Title="PASO4. CREAR FORMULA" Font-Names="Arial">

                    <div class="ContendDivRegistro" style="width: 700px; height: auto">
                        <h2 class="Contendh3">CONSTRUCCIÓN DE FORMULA</h2>
                        <div style="text-align: center; align-content: center">

                            <div id="DivAdvertencia" runat="server" visible="true">
                                <div role="alert" style="width: 650px; align-content: center; text-align: center; background: #E4ECE7">
                                    <span class="badge badge-pill badge-info">¡Advertencia!</span>
                                    <br />

                                    <asp:Label runat="server" ForeColor="#145A32 " ID="LblValidarf" Text="La formula se construye con la siguiente sintaxis, <br/> utilizando la letra 'V' numerando  las variables que tenga la formula <br/>Ejemplo (V1+V2/V3) " Font-Bold="true"></asp:Label>
                                    <button aria-label="Close" class="close" data-dismiss="alert" type="button"></button>
                                </div>
                            </div>
                        </div>

                        <br />
                        <br />
                        <table style="width: 100%">
                            <tr>
                                <td colspan="4" style="align-content: center; text-align: center">

                                    <asp:Label runat="server" Text="Escribir fórmula" Font-Bold="true"></asp:Label>

                                    <telerik:RadTextBox runat="server" ID="TxtVariables" EmptyMessage="Escribir variables" Width="600px" Skin="Bootstrap" TextMode="MultiLine"></telerik:RadTextBox>
                                    <br />
                                    <br />
                                    <telerik:RadButton ID="btn_agregar2" runat="server" Text="Validar sintaxis" Skin="BlackMetroTouch" OnClick="btn_agregar2_Click"></telerik:RadButton>
                                    <br />
                                    <br />
                                    <div id="DivResultado" runat="server" visible="false" style="width: 400px; align-content: center; text-align: center">
                                        <div class="alert-success" role="alert" style="width: 400px; align-content: center; text-align: center">
                                            <span class="alert-heading">¡Validación Sintaxis!</span>
                                            <br />
                                            <asp:Label runat="server" ForeColor="#009900" ID="LblValidarSin"></asp:Label>
                                        </div>
                                    </div>
                                </td>

                            </tr>


                        </table>

                        <asp:UpdateProgress ID="UpdateProgress3" runat="server">
                            <ProgressTemplate>
                                <div class="text-center">
                                    <span style="font-size: 14pt; color: grey">Procesando datos....</span>
                                    <asp:Image ID="imgLoader10" runat="server" ImageUrl="~/images/Progress.gif" />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>

                    </div>

                </telerik:RadWizardStep>
                <telerik:RadWizardStep ID="RadWizardStep3" Title="PASO5.CREAR RESPONSABLES" Font-Size="12pt" Enabled="false" Font-Names="Arial" runat="server" ImageUrl="~/images/team.png">
                    <div class="ContendDivRegistro" style="width: 700px; height: auto">
                        <h2 class="Contendh3">ASIGNAR USUARIOS</h2>
                        <table style="width: 50%">

                            <tr>
                                <td>
                                    <asp:Label Text="USUARIO ASIGNADO" CssClass="Labels" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <telerik:RadComboBox runat="server" ID="CmbUsuario" Filter="Contains" EmptyMessage="Seleccionar usuario" Width="250px" Skin="Metro"></telerik:RadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label Text="ROL ASIGNADO" CssClass="Labels" Width="350px" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <telerik:RadComboBox runat="server" ID="CmbRol" Skin="Metro" EmptyMessage="Seleccionar rol" Width="250px"></telerik:RadComboBox>
                                </td>
                            </tr>

                        </table>
                        <br />
                        <div style="align-content; text-align: center">
                            <telerik:RadButton ID="btnagregar" runat="server" Text="Agregar" Skin="BlackMetroTouch" OnClick="btnagregar_Click"></telerik:RadButton>
                            <br />
                            <br />

                            <telerik:RadGrid runat="server" ID="Gvrolesu" Visible="false" MasterTableView-DataKeyNames="ID_ROLESIN" OnDeleteCommand="Gvrolesu_DeleteCommand" AutoGenerateColumns="false" Skin="Vista">
                                <MasterTableView>
                                    <Columns>

                                        <telerik:GridButtonColumn CommandName="Delete" Text="Eliminar" HeaderText="INACTIVAR" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="Black" UniqueName="DeleteColumn" ButtonType="ImageButton" ImageUrl="images/delete.png" ItemStyle-Width="10px" />
                                        <telerik:GridBoundColumn DataField="ID_ROLES_IN" Visible="false">
                                            <HeaderStyle Font-Bold="true" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="NOMBRE" HeaderText="NOMBRE USUARIO">
                                            <HeaderStyle Font-Bold="true" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="NOMBRE_ROL" HeaderText="ROL">
                                            <HeaderStyle Font-Bold="true" />
                                        </telerik:GridBoundColumn>

                                    </Columns>
                                </MasterTableView>
                            </telerik:RadGrid>


                            <asp:Label ID="lblMensaje" CssClass="Labels" Width="350px" runat="server"></asp:Label>

                        </div>
                    </div>
                    <asp:UpdateProgress ID="UpdateProgress2" runat="server">
                        <ProgressTemplate>
                            <div class="text-center">
                                <span style="font-size: 14pt; color: grey">Procesando datos....</span>
                                <asp:Image ID="imgLoader" runat="server" ImageUrl="~/images/Progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>

                </telerik:RadWizardStep>
            </WizardSteps>
        </telerik:RadWizard>


    </telerik:RadAjaxPanel>


</asp:Content>
