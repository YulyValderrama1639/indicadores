﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AnalisisIndicador.aspx.cs" Inherits="INDICADORES_CALIDAD.AnalisisIndicador" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
       <form id="form1" runat="server">
        <asp:ScriptManager runat="server"></asp:ScriptManager>

       
        <div style="align-content: center; text-align: center">
            <asp:Label runat="server" ID="LblNombre" Font-Size="20pt"  Font-Names="Agency FB" Font-Bold="true"></asp:Label>
        </div>

        <div class="col-sm-12">
            <telerik:RadTabStrip runat="server" ID="RadUsuarios" CssClass="html .rtsTop .rtsLevel1" Font-Names="Agency FB" MultiPageID="RadMultiPage1" SelectedIndex="0" BorderColor="#99CCFF" Skin="MetroTouch" PerTabScrolling="True" ReorderTabsOnSelect="True" ShowBaseLine="True">
                <Tabs>
                    <telerik:RadTab Text="ANÀLISIS DEL INDICADOR" ImageUrl="~/images/pie-chart.png" Font-Bold="true" Width="250px"  runat ="server" Selected="True"></telerik:RadTab>
               


                </Tabs>
            </telerik:RadTabStrip>
        </div>


        <telerik:RadMultiPage ID="RadMultiPage1" SelectedIndex="0" runat="server">

            <telerik:RadPageView ID="Pagina1" runat="server">
                            

                <telerik:RadGrid ID="GVMeses" runat="server" AllowPaging="true"  AutoGenerateColumns="false" PageSize="12" Skin="MetroTouch"  Width="100%">
                    <GroupingSettings CollapseAllTooltip="Collapse all groups"></GroupingSettings>

                   

                    <MasterTableView >
                        <Columns>

                             <telerik:GridBoundColumn DataField="NOTA" HeaderText="NOTA" AllowFiltering="false">
                                <HeaderStyle Font-Bold="true" Font-Size="12pt" />
                             
                            </telerik:GridBoundColumn>
                           
                            <telerik:GridBoundColumn DataField="MES" HeaderText="PERIODO" AllowFiltering="true">
                                <HeaderStyle Font-Bold="true" Font-Size="12pt" />
                                <ItemStyle Font-Size="12pt" />
                            </telerik:GridBoundColumn>
                           
                                                 

                                
                        </Columns>
                    </MasterTableView>
                    <HeaderStyle Font-Names="Agency FB" />
                    <ItemStyle Font-Names="Arial Narrow" />
                </telerik:RadGrid>
            </telerik:RadPageView>
        </telerik:RadMultiPage>












    </form>
</body>
</html>
