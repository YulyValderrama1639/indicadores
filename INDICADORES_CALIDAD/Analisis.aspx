﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Analisis.aspx.cs" Inherits="INDICADORES_CALIDAD.Analisis" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <link href="Content/StyleCustom.css" rel="stylesheet" />
    <link href="Content/bootstrap.css" rel="stylesheet" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>


    <form id="form1" runat="server">
        <telerik:RadAjaxPanel runat="server" style="margin-top: 49px">
            <asp:ScriptManager runat="server"></asp:ScriptManager>

            <br />
            <div class="col-sm-12" runat="server" id="DivAlert">
                <div class="DivGeneral DivColor">
                    <asp:Label Text="CREAR ANÁLISIS CUALITATIVO" Font-Bold="true" Font-Size="15pt" runat="server" Font-Names="Arial Narrow"></asp:Label>

                </div>
            </div>
            <div class="col-sm-12" runat="server" id="Div2" style="text-align: center; align-content: center">
                <asp:Label runat="server" ID="LblNombre" Font-Size="11pt" Font-Bold="true"></asp:Label>
            </div>

            <div class="col-sm-12" runat="server" id="Div1" style="text-align: center; align-content: center">
                <table style="width: 100%">
                    <tr>
                        <td colspan="1">
                            <asp:Label Text="Análisis de medida" Font-Bold="true" ForeColor="#767676" runat="server" Font-Size="15pt" Font-Names="Agency FB">
                            </asp:Label>
                            <br />
                            <telerik:RadComboBox runat="server" ID="CmbVigencia" AutoPostBack="true" Width="250px" EmptyMessage="Seleccione analisis de medida" OnSelectedIndexChanged="CmbVigencia_SelectedIndexChanged" Skin="Bootstrap">
                            </telerik:RadComboBox>
                       
                        </td>
                        <td>
                            <asp:Label Text="Periodo" Font-Bold="true" ForeColor="#767676" runat="server" Font-Size="15pt" Font-Names="Agency FB"></asp:Label>
                            <br />
                            <telerik:RadComboBox runat="server" ID="CmbPeriodo" Skin="Bootstrap" Width="250px" EmptyMessage="Seleccione periodo">
                            </telerik:RadComboBox>
                        </td>
                    </tr>


                </table>
                <br />

               <table style="width:100%">
                   <tr>
                       <td align="center">
                   <telerik:RadTextBox ID="TxtAnalisis" Width="800px" Height="100px" TextMode="MultiLine" runat="server"></telerik:RadTextBox>
             </td>
                       </tr>
                       </table>



            </div>
            <div>
            </div>

            <div style="align-content: center; text-align: center">
                <telerik:RadButton runat="server" ID="btn_crear" Text="CREAR" OnClick="btn_crear_Click" Font-Names="Arial Narrow" Skin="BlackMetroTouch"></telerik:RadButton>

                <telerik:RadButton runat="server" ID="btn_eliminar" Text="LIMPIAR" OnClick="btn_eliminar_Click" Font-Names="Arial Narrow" Skin="BlackMetroTouch"></telerik:RadButton>

            </div>
            <div id="DResultado" runat="server" visible="false">
                <div class="alert-success" role="alert" style="align-content: center; text-align: center;">
                    <span class="badge badge-pill badge-info">¡Guardado!</span>
                    <br />
                    <asp:Label runat="server" ID="LblGuardado"></asp:Label>
                    <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span aria-hidden="true">×</span></button>
                </div>
            </div>







        </telerik:RadAjaxPanel>

    </form>
</body>
</html>
