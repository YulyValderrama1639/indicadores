﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CNegocios;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Telerik.Web.UI;

namespace INDICADORES_CALIDAD
{
    public partial class Search : System.Web.UI.Page
    {
        public IndicadoresBussines ObjGIndicadores = new IndicadoresBussines();
        public SqlConnection Conexion;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Convert.ToString(this.Session["Login"]) != "OK")

            {
                Response.Redirect("~/Login.aspx", true);
            }

            if (!Page.IsPostBack)
            {
                CargarCombosMaestros("T_PROCESO", 1, 2, 2, CmbProceso);
              
                CargarCombosMaestros("T_TIPOINDICADOR", 1, 2, 2, CmbtipoIndicadors);
                CargarCombosMaestros("T_TIPOINDICADOR", 1, 2, 2, CmbtipoIndicadors);
                CargarCombosMaestros("T_TIPOINDICADOR", 1, 2, 2, CmbTipoRegional);
                CargarCombosMaestros("T_PROCESO", 1, 2, 2, CmbProcesoR);
                CargarCombosMaestros("T_SECCIONAL", 1, 2, 2, cmbseccional);       
                               
                CargarCombosMaestros("T_PERIODICIDAD", 1, 2, 2, CmMedida7);               
               
                CargarCombosMaestros("T_REGIONAL", 1, 2, 2, CmbRegional);
                CargarCombosMaestros("T_TIPOINDICADOR", 1, 2, 2, CmbTipoS);
                CargarCombosMaestros("T_PROCESO", 1, 2, 2, CmbProcesoS);
             
              

            }

        }



       


       
        private void CargarPeriodicidadRegional()
        {
            if (CmbMedidaR.SelectedValue == "7")
            {
                CmbPeriodoR.Enabled = false;

            }
            else
            {
                if (CmbMedidaR.SelectedValue != "")
                {
                    CmbPeriodoR.Enabled = true;
                    CmbPeriodoR.DataSource = ObjGIndicadores.DCONSULTATIPO(Convert.ToInt32(CmbMedidaR.SelectedValue));
                    CmbPeriodoR.DataValueField = "ID_DET_PERIODO";
                    CmbPeriodoR.DataTextField = "PERIODO";
                    CmbPeriodoR.DataBind();
                }

            }

        }



        private void CargarPeriodicidadSeccional()
        {
            if (CmMedida7.SelectedValue == "7")
            {
                Cmperiodo7.Enabled = false;

            }
            else
            {
                if (CmMedida7.SelectedValue != "")
                {
                    Cmperiodo7.Enabled = true;
                    Cmperiodo7.DataSource = ObjGIndicadores.DCONSULTATIPO(Convert.ToInt32(CmMedida7.SelectedValue));
                    Cmperiodo7.DataValueField = "ID_DET_PERIODO";
                    Cmperiodo7.DataTextField = "PERIODO";
                    Cmperiodo7.DataBind();
                }

            }


        }


 




        private void CargarCombosMaestros(string Tabla, int campo1, int campo2, int orden, RadComboBox Combo, int? campo3 = null, int? campo4 = null)
        {
            Conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["SCORBOARD"].ConnectionString);
            SqlCommand cmd = new SqlCommand("P_CARGA_COMBOS", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TABLA", Tabla);
            cmd.Parameters.AddWithValue("@CAMPO1", campo1);
            cmd.Parameters.AddWithValue("@CAMPO2", campo2);
            cmd.Parameters.AddWithValue("@ORDEN", orden);
            cmd.Parameters.AddWithValue("@CAMPO_FILTRO", campo3);
            cmd.Parameters.AddWithValue("@VALOR_FILTRO", campo4);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {

                da.Fill(ds);
                Combo.DataSource = ds;
                Combo.DataValueField = "1";
                Combo.DataTextField = "2";
                Combo.DataBind();

            }
            catch (Exception ex)
            {

            }

        }

        protected void rbConsultar_Click(object sender, EventArgs e)
        {

            BUSQUEDAINDICADORES();


        }


   

        protected void btn_consulta5_Click(object sender, EventArgs e)
        {
           
        }



      


        private void BUSQUEDAINDICADORES()
        {
            GvIndicadores.Rebind();
            GvIndicadores.Visible = true;
        }


        protected void btnlimpiar_Click(object sender, EventArgs e)
        {
            GvIndicadores.Visible = false;           
            CmbMedida.SelectedValue = "0";
            CmbMedida.Text = "";        
            CmbProceso.SelectedValue = "0";
            CmbProceso.Text = "";                          
            Dvbusqueda1.Visible = false;
         

        }

      
        protected void GvIndicadores_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                ImageButton btn_detalle = (ImageButton)e.Item.FindControl("btn_detalle");
                btn_detalle.Attributes["href"] = "javascript:void(0);";
                btn_detalle.Attributes["onclick"] = String.Format("return ShowEditForm('{0}','{1}');", e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID_INDICADOR"], e.Item.ItemIndex);

                ImageButton btn_Analisis = (ImageButton)e.Item.FindControl("btn_Analisis");
                btn_Analisis.Attributes["href"] = "javascript:void(0);";
                btn_Analisis.Attributes["onclick"] = String.Format("return ShowAnalisis('{0}','{1}');", e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID_INDICADOR"], e.Item.ItemIndex);

                //ImageButton btn_Variables = (ImageButton)e.Item.FindControl("btn_Variables");
                //btn_Variables.Attributes["href"] = "javascript:void(0);";
                //btn_Variables.Attributes["onclick"] = String.Format("return ShowVariables('{0}','{1}');", e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID_INDICADOR"], e.Item.ItemIndex);

                ImageButton btn_Resultado = (ImageButton)e.Item.FindControl("btn_resultado");
                btn_Resultado.Attributes["href"] = "javascript:void(0);";
                btn_Resultado.Attributes["onclick"] = String.Format("return ShowResultado('{0}','{1}');", e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID_INDICADOR"], e.Item.ItemIndex);


                ImageButton btn_Cumplimiento = (ImageButton)e.Item.FindControl("btn_Cumplimiento");
                btn_Cumplimiento.Attributes["href"] = "javascript:void(0);";
                btn_Cumplimiento.Attributes["onclick"] = String.Format("return ShowCumplmiento('{0}','{1}');", e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID_INDICADOR"], e.Item.ItemIndex);

                ImageButton btn_grafico = (ImageButton)e.Item.FindControl("btn_grafico");
                btn_grafico.Attributes["href"] = "javascript:void(0);";
                btn_grafico.Attributes["onclick"] = String.Format("return Showgrafica('{0}','{1}');", e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID_INDICADOR"], e.Item.ItemIndex);



            }
        }



     

        protected void GvIndicadores_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (CmbPeriodo.SelectedValue !="" )
            {
                GvIndicadores.DataSource = ObjGIndicadores.DBUSQUEDATXT(Convert.ToInt32(CmbIndicadores.SelectedValue), Convert.ToInt32(CmbPeriodo.SelectedValue));
            }
            else
            {
                GvIndicadores.DataSource = ObjGIndicadores.DBUSQUEDATXT(Convert.ToInt32(CmbIndicadores.SelectedValue), 0);
            }
        }





       

        protected void GrV_regional_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if ((CmbRegional.SelectedValue == "0") || (CmbRegional.SelectedValue == ""))
            {

                GrV_regional.DataSource = ObjGIndicadores.DBUSQUEDAREGIONAL(0, 0, Convert.ToInt32(CmbIndicadorRegional.SelectedValue));

                GrV_regional.Visible = true;              

            }
            else
            {
                GrV_regional.DataSource = ObjGIndicadores.DBUSQUEDAREGIONAL(0, Convert.ToInt32(CmbRegional.SelectedValue), Convert.ToInt32(CmbIndicadorRegional.SelectedValue));

                GrV_regional.Visible = true;

            }
        }

   

     
        protected void GvIndicadores_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {

                GridDataItem item = (GridDataItem)e.Item;
                var RESULTADO = item.OwnerTableView.DataKeyValues[item.ItemIndex]["RESULTADO1"].ToString().Replace("%", "");
                var PEOR = item.OwnerTableView.DataKeyValues[item.ItemIndex]["PEOR"].ToString();
                var ROJA = item.OwnerTableView.DataKeyValues[item.ItemIndex]["ROJA"].ToString();
                var ALCANCE = item.OwnerTableView.DataKeyValues[item.ItemIndex]["ALCANCE"].ToString();
                var MEJOR = item.OwnerTableView.DataKeyValues[item.ItemIndex]["MEJOR"].ToString();
                var LIMITE = item.OwnerTableView.DataKeyValues[item.ItemIndex]["LIMITE"].ToString();

                decimal RESULTADO1 = Convert.ToDecimal(RESULTADO);
                decimal PEOR1 = Convert.ToDecimal(PEOR);
                decimal ROJA1 = Convert.ToDecimal(ROJA);
                decimal ALCANCE1 = Convert.ToDecimal(ALCANCE);
                decimal MEJOR1 = Convert.ToDecimal(MEJOR);
                decimal LIMITEC  = Convert.ToDecimal(LIMITE);


                if(LIMITEC>=3)
                {

                    if (RESULTADO1 == 0)
                    {
                        item["SEMAFORO"].Controls.Clear();
                        Image img = new Image();
                        img.ImageUrl = "~/Images/gris.png";
                        item["SEMAFORO"].Controls.Add(img);
                    }

                    if (RESULTADO1 < PEOR1 || RESULTADO1 > ALCANCE1)
                    {
                        item["SEMAFORO"].Controls.Clear();
                        Image img = new Image();
                        img.ImageUrl = "~/Images/Rojo.png";
                        item["SEMAFORO"].Controls.Add(img);
                    }

                    if (RESULTADO1 >= PEOR1 && RESULTADO1 <= ALCANCE1)
                    {
                        item["SEMAFORO"].Controls.Clear();
                        Image img = new Image();
                        img.ImageUrl = "~/Images/Verde.png";
                        item["SEMAFORO"].Controls.Add(img);
                    }

                }
                else
                {


                    if (RESULTADO1 == 0)
                {
                    item["SEMAFORO"].Controls.Clear();
                    Image img = new Image();
                    img.ImageUrl = "~/Images/gris.png";
                    item["SEMAFORO"].Controls.Add(img);
                }

                if (RESULTADO1 <= ROJA1)
                {
                    item["SEMAFORO"].Controls.Clear();
                    Image img = new Image();
                    img.ImageUrl = "~/Images/Rojo.png";
                    item["SEMAFORO"].Controls.Add(img);
                }

                if (RESULTADO1 > ROJA1 && RESULTADO1 < PEOR1)

                {
                    item["SEMAFORO"].Controls.Clear();
                    Image img = new Image();
                    img.ImageUrl = "~/Images/Amarillo.png";
                    item["SEMAFORO"].Controls.Add(img);
                }

                if (RESULTADO1 >= PEOR1 && RESULTADO1 <= ALCANCE1)
                {
                    item["SEMAFORO"].Controls.Clear();
                    Image img = new Image();
                    img.ImageUrl = "~/Images/Verde.png";
                    item["SEMAFORO"].Controls.Add(img);
                }
                if (RESULTADO1 > ALCANCE1 && RESULTADO1 <= MEJOR1 || RESULTADO1 > ALCANCE1 && RESULTADO1 > MEJOR1)
                {
                    item["SEMAFORO"].Controls.Clear();
                    Image img = new Image();
                    img.ImageUrl = "~/Images/Azul.png";
                    item["SEMAFORO"].Controls.Add(img);
                }

            }
        }
        }



        protected void GvIndicadoresEs_ItemDataBound(object sender, GridItemEventArgs e)
        {

            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                double resultado = Convert.ToDouble(item["RESULTADO"].Text.Replace("%", ""));

                if (resultado == 0)
                {
                    item["SEMAFORO"].Controls.Clear();
                    Image img = new Image();
                    img.ImageUrl = "~/Images/gris.png";
                    item["SEMAFORO"].Controls.Add(img);
                }
                if (resultado <= 80 && resultado != 0)
                {
                    item["SEMAFORO"].Controls.Clear();
                    Image img = new Image();
                    img.ImageUrl = "~/Images/Rojo.png";
                    item["SEMAFORO"].Controls.Add(img);
                }

                if (resultado > 80 && resultado < 120)
                {
                    item["SEMAFORO"].Controls.Clear();
                    Image img = new Image();
                    img.ImageUrl = "~/Images/Amarillo.png";
                    item["SEMAFORO"].Controls.Add(img);
                }
                if (resultado >= 100)
                {
                    item["SEMAFORO"].Controls.Clear();
                    Image img = new Image();
                    img.ImageUrl = "~/Images/Amarillo.png";
                    item["SEMAFORO"].Controls.Add(img);
                }

                if (resultado >= 120)
                {
                    item["SEMAFORO"].Controls.Clear();
                    Image img = new Image();
                    img.ImageUrl = "~/Images/Verde.png";
                    item["SEMAFORO"].Controls.Add(img);
                }
                if (resultado >= 140)
                {
                    item["SEMAFORO"].Controls.Clear();
                    Image img = new Image();
                    img.ImageUrl = "~/Images/Azul.png";
                    item["SEMAFORO"].Controls.Add(img);
                }
            }
        }

        protected void GrV_regional_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {

                GridDataItem item = (GridDataItem)e.Item;
                var RESULTADO = item.OwnerTableView.DataKeyValues[item.ItemIndex]["RESULTADO"].ToString().Replace("%", "");
                var PEOR = item.OwnerTableView.DataKeyValues[item.ItemIndex]["PEOR"].ToString();
                var ROJA = item.OwnerTableView.DataKeyValues[item.ItemIndex]["ROJA"].ToString();
                var ALCANCE = item.OwnerTableView.DataKeyValues[item.ItemIndex]["ALCANCE"].ToString();
                var MEJOR = item.OwnerTableView.DataKeyValues[item.ItemIndex]["MEJOR"].ToString();

                double RESULTADO1 = Convert.ToDouble(RESULTADO);
                double PEOR1 = Convert.ToDouble(PEOR);
                double ROJA1 = Convert.ToDouble(ROJA);
                double ALCANCE1 = Convert.ToDouble(ALCANCE);
                double MEJOR1 = Convert.ToDouble(MEJOR);
                if (RESULTADO1 == 0)
                {
                    item["SEMAFORO"].Controls.Clear();
                    Image img = new Image();
                    img.ImageUrl = "~/Images/gris.png";
                    item["SEMAFORO"].Controls.Add(img);
                }
                if (RESULTADO1 <= ROJA1)
                {
                    item["SEMAFORO"].Controls.Clear();
                    Image img = new Image();
                    img.ImageUrl = "~/Images/Rojo.png";
                    item["SEMAFORO"].Controls.Add(img);
                }

                if (RESULTADO1 > ROJA1 && RESULTADO1 < PEOR1)

                {
                    item["SEMAFORO"].Controls.Clear();
                    Image img = new Image();
                    img.ImageUrl = "~/Images/Amarillo.png";
                    item["SEMAFORO"].Controls.Add(img);
                }

                if (RESULTADO1 >= PEOR1 && RESULTADO1 <= ALCANCE1)
                {
                    item["SEMAFORO"].Controls.Clear();
                    Image img = new Image();
                    img.ImageUrl = "~/Images/Verde.png";
                    item["SEMAFORO"].Controls.Add(img);
                }
                if (RESULTADO1 > ALCANCE1 && RESULTADO1 <= MEJOR1 || RESULTADO1 > ALCANCE1 && RESULTADO1 > MEJOR1)
                {
                    item["SEMAFORO"].Controls.Clear();
                    Image img = new Image();
                    img.ImageUrl = "~/Images/Azul.png";
                    item["SEMAFORO"].Controls.Add(img);
                }
            }
        }

        protected void GvIndicadoresEs_ItemCreated2(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {

                //  Session["PERIODO"] = Convert.ToInt32(Cmbperiodo.SelectedValue);

                ImageButton btn_detalle = (ImageButton)e.Item.FindControl("btn_detalle");
                btn_detalle.Attributes["href"] = "javascript:void(0);";
                btn_detalle.Attributes["onclick"] = String.Format("return ShowEditForm3('{0}','{1}');", e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID_INDICADOR"], e.Item.ItemIndex);

                ImageButton btn_Analisis = (ImageButton)e.Item.FindControl("btn_Analisis");
                btn_Analisis.Attributes["href"] = "javascript:void(0);";
                btn_Analisis.Attributes["onclick"] = String.Format("return ShowAnalisis3('{0}','{1}');", e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID_INDICADOR"], e.Item.ItemIndex);

                ImageButton btn_Variables = (ImageButton)e.Item.FindControl("btn_Variables");
                btn_Variables.Attributes["href"] = "javascript:void(0);";
                btn_Variables.Attributes["onclick"] = String.Format("return ShowVariables3('{0}','{1}');", e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID_INDICADOR"], e.Item.ItemIndex);

                ImageButton btn_Resultado = (ImageButton)e.Item.FindControl("btn_resultado");
                btn_Resultado.Attributes["href"] = "javascript:void(0);";
                btn_Resultado.Attributes["onclick"] = String.Format("return ShowResultado3('{0}','{1}');", e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID_INDICADOR"], e.Item.ItemIndex);


                ImageButton btn_Cumplimiento = (ImageButton)e.Item.FindControl("btn_Cumplimiento");
                btn_Cumplimiento.Attributes["href"] = "javascript:void(0);";
                btn_Cumplimiento.Attributes["onclick"] = String.Format("return ShowCumplmiento3('{0}','{1}');", e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID_INDICADOR"], e.Item.ItemIndex);

                ImageButton btn_grafico = (ImageButton)e.Item.FindControl("btn_grafico");
                btn_grafico.Attributes["href"] = "javascript:void(0);";
                btn_grafico.Attributes["onclick"] = String.Format("return Showgrafica3('{0}','{1}');", e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID_INDICADOR"], e.Item.ItemIndex);


                ImageButton btn_inductor = (ImageButton)e.Item.FindControl("btn_inductor");
                btn_inductor.Attributes["href"] = "javascript:void(0);";
                btn_inductor.Attributes["onclick"] = String.Format("return ShowInductor('{0}','{1}');", e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID_INDICADOR"], e.Item.ItemIndex);





            }

        }

        protected void GvIndicadoresproceso_ItemCreated1(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                ImageButton btn_detalle = (ImageButton)e.Item.FindControl("btn_detalle");
                btn_detalle.Attributes["href"] = "javascript:void(0);";
                btn_detalle.Attributes["onclick"] = String.Format("return ShowEditForm2('{0}','{1}');", e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID_INDICADOR"], e.Item.ItemIndex);

                ImageButton btn_Analisis = (ImageButton)e.Item.FindControl("btn_Analisis");
                btn_Analisis.Attributes["href"] = "javascript:void(0);";
                btn_Analisis.Attributes["onclick"] = String.Format("return ShowAnalisis2('{0}','{1}');", e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID_INDICADOR"], e.Item.ItemIndex);

                ImageButton btn_Variables = (ImageButton)e.Item.FindControl("btn_Variables");
                btn_Variables.Attributes["href"] = "javascript:void(0);";
                btn_Variables.Attributes["onclick"] = String.Format("return ShowVariables2('{0}','{1}');", e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID_INDICADOR"], e.Item.ItemIndex);

                ImageButton btn_Resultado = (ImageButton)e.Item.FindControl("btn_resultado");
                btn_Resultado.Attributes["href"] = "javascript:void(0);";
                btn_Resultado.Attributes["onclick"] = String.Format("return ShowResultado2('{0}','{1}');", e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID_INDICADOR"], e.Item.ItemIndex);


                ImageButton btn_Cumplimiento = (ImageButton)e.Item.FindControl("btn_Cumplimiento");
                btn_Cumplimiento.Attributes["href"] = "javascript:void(0);";
                btn_Cumplimiento.Attributes["onclick"] = String.Format("return ShowCumplmiento2('{0}','{1}');", e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID_INDICADOR"], e.Item.ItemIndex);

                ImageButton btn_grafico = (ImageButton)e.Item.FindControl("btn_grafico");
                btn_grafico.Attributes["href"] = "javascript:void(0);";
                btn_grafico.Attributes["onclick"] = String.Format("return Showgrafica2('{0}','{1}');", e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID_INDICADOR"], e.Item.ItemIndex);



            }
        }

     
        protected void GrV_regional_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                ImageButton btn_detalle = (ImageButton)e.Item.FindControl("btn_detalle");
                btn_detalle.Attributes["href"] = "javascript:void(0);";
                btn_detalle.Attributes["onclick"] = String.Format("return ShowEditForm4('{0}','{1}');", e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID_INDICADOR"], e.Item.ItemIndex);

                ImageButton btn_Analisis = (ImageButton)e.Item.FindControl("btn_Analisis");
                btn_Analisis.Attributes["href"] = "javascript:void(0);";
                btn_Analisis.Attributes["onclick"] = String.Format("return ShowAnalisis4('{0}','{1}');", e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID_INDICADOR"], e.Item.ItemIndex);

                
                ImageButton btn_grafico = (ImageButton)e.Item.FindControl("btn_grafico");
                btn_grafico.Attributes["href"] = "javascript:void(0);";
                btn_grafico.Attributes["onclick"] = String.Format("return ShowVariablesRegional('{0}','{1}');", e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID_INDICADOR"], e.Item.ItemIndex);


            }
        }


        protected void TxtBuscar_TextChanged1(object sender, EventArgs e)
        {
            BUSQUEDAINDICADORES();

        }

    
      

        protected void btnconsultar6_Click(object sender, EventArgs e)
        {
            if ((CmbTipoS.SelectedValue == "0") || (CmbTipoS.SelectedValue == ""))
            {

                Lblresultado.Text = "Debe seleccionar el tipo de indicador";
                Lblresultado.Visible = true;
                return;
            }

            if ((CmbProcesoS.SelectedValue == "0") || (CmbProcesoS.SelectedValue == ""))
            {

                Lblresultado.Text = "Debe seleccionar el proceso";
                Lblresultado.Visible = true;
                return;
            }


            if ((CmbIndicadoresS.SelectedValue == "0") || (CmbIndicadoresS.SelectedValue == ""))
            {

                Lblresultado.Text = "Debe seleccionar el indicador";
                Lblresultado.Visible = true;
                return;
            }
            Lblresultado.Visible = false;

            GvSeccional.Rebind();
            GvSeccional.Visible = true;
        }





    
        protected void GvSeccional_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {

                GridDataItem item = (GridDataItem)e.Item;
                var RESULTADO = item.OwnerTableView.DataKeyValues[item.ItemIndex]["RESULTADO"].ToString().Replace("%", "");
                var PEOR = item.OwnerTableView.DataKeyValues[item.ItemIndex]["PEOR"].ToString();
                var ROJA = item.OwnerTableView.DataKeyValues[item.ItemIndex]["ROJA"].ToString();
                var ALCANCE = item.OwnerTableView.DataKeyValues[item.ItemIndex]["ALCANCE"].ToString();
                var MEJOR = item.OwnerTableView.DataKeyValues[item.ItemIndex]["MEJOR"].ToString();

                double RESULTADO1 = Convert.ToDouble(RESULTADO);
                double PEOR1 = Convert.ToDouble(PEOR);
                double ROJA1 = Convert.ToDouble(ROJA);
                double ALCANCE1 = Convert.ToDouble(ALCANCE);
                double MEJOR1 = Convert.ToDouble(MEJOR);
                if (RESULTADO1 == 0)
                {
                    item["SEMAFORO"].Controls.Clear();
                    Image img = new Image();
                    img.ImageUrl = "~/Images/gris.png";
                    item["SEMAFORO"].Controls.Add(img);
                }
                if (RESULTADO1 <= ROJA1)
                {
                    item["SEMAFORO"].Controls.Clear();
                    Image img = new Image();
                    img.ImageUrl = "~/Images/Rojo.png";
                    item["SEMAFORO"].Controls.Add(img);
                }

                if (RESULTADO1 > ROJA1 && RESULTADO1 < PEOR1)

                {
                    item["SEMAFORO"].Controls.Clear();
                    Image img = new Image();
                    img.ImageUrl = "~/Images/Amarillo.png";
                    item["SEMAFORO"].Controls.Add(img);
                }

                if (RESULTADO1 >= PEOR1 && RESULTADO1 <= ALCANCE1)
                {
                    item["SEMAFORO"].Controls.Clear();
                    Image img = new Image();
                    img.ImageUrl = "~/Images/Verde.png";
                    item["SEMAFORO"].Controls.Add(img);
                }
                if (RESULTADO1 > ALCANCE1 && RESULTADO1 <= MEJOR1 || RESULTADO1 > ALCANCE1 && RESULTADO1 > MEJOR1)
                {
                    item["SEMAFORO"].Controls.Clear();
                    Image img = new Image();
                    img.ImageUrl = "~/Images/Azul.png";
                    item["SEMAFORO"].Controls.Add(img);
                }
            }
        }

        protected void GvSeccional_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                ImageButton btn_detalle = (ImageButton)e.Item.FindControl("btn_detalle");
                btn_detalle.Attributes["href"] = "javascript:void(0);";
                btn_detalle.Attributes["onclick"] = String.Format("return ShowEditForm10('{0}','{1}');", e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID_INDICADOR"], e.Item.ItemIndex);

                ImageButton btn_Analisis = (ImageButton)e.Item.FindControl("btn_Analisis");
                btn_Analisis.Attributes["href"] = "javascript:void(0);";
                btn_Analisis.Attributes["onclick"] = String.Format("return ShowAnalisis10('{0}','{1}');", e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID_INDICADOR"], e.Item.ItemIndex);

             

                ImageButton btn_grafico = (ImageButton)e.Item.FindControl("btn_grafico");
                btn_grafico.Attributes["href"] = "javascript:void(0);";
                btn_grafico.Attributes["onclick"] = String.Format("return Showgrafica('{0}','{1}');", e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID_INDICADOR"], e.Item.ItemIndex);



            }

        }

        protected void GvSeccional_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
          if ((Cmperiodo7.SelectedValue == "0") || (Cmperiodo7.SelectedValue == ""))
                {
                GvSeccional.DataSource = ObjGIndicadores.DBUSQUEDASECCIONAL(0,0, Convert.ToInt32(CmbIndicadoresS.SelectedValue));
                GvSeccional.Visible = true;
            }
            else
            {

                GvSeccional.DataSource = ObjGIndicadores.DBUSQUEDASECCIONAL(Convert.ToInt32(Cmperiodo7.SelectedValue), Convert.ToInt32(cmbseccional.SelectedValue), Convert.ToInt32(CmbIndicadoresS.SelectedValue));
                GvSeccional.Visible = true;
            }

        }

        protected void btnlimpiars_Click(object sender, EventArgs e)
        {
            CmMedida7.SelectedValue = "0";
            CmMedida7.Text = "";
            Cmperiodo7.SelectedValue = "0";
            Cmperiodo7.Text = "";
            GvSeccional.DataSource = "";
            GvSeccional.Visible = false;
            cmbseccional.SelectedValue = "0";
            cmbseccional.Text = "";
        }


        protected void btnlimpiars_Click1(object sender, EventArgs e)
        {
            cmbseccional.SelectedValue = "0";
            cmbseccional.Text = "";
            CmbIndicadoresS.SelectedValue = "0";
            CmbIndicadoresS.Text = "";
            CmMedida7.SelectedValue = "0";
            CmMedida7.Text = "";
            Cmperiodo7.SelectedValue = "0";
            Cmperiodo7.Text = "";
            GvSeccional.DataSource = "";
            GvSeccional.Visible = false;
            Session["SECCIONAL"] = "0";
            CmbTipoS.SelectedValue = "0";
            CmbTipoS.Text = "0";
            CmbProcesoS.SelectedValue = "0";
            CmbProcesoS.Text = "";


        }

        protected void btn_consultar_Click(object sender, EventArgs e)
        {
            if ((CmbtipoIndicadors.SelectedValue == "0") || (CmbtipoIndicadors.SelectedValue == ""))
            {

                Lblresultado.Text = "Debe seleccionar el tipo de indicador";
                Lblresultado.Visible = true;
                return;
            }

            if ((CmbProceso.SelectedValue == "0") || (CmbProceso.SelectedValue == ""))
            {

                Lblresultado.Text = "Debe seleccionar el proceso";
                Lblresultado.Visible = true;
                return;
            }


            if ((CmbIndicadores.SelectedValue == "0") || (CmbIndicadores.SelectedValue == ""))
            {

                Lblresultado.Text = "Debe seleccionar el indicador";
                Lblresultado.Visible = true;
                return;
            }
            Lblresultado.Visible = false;

            GvIndicadores.Rebind();
            GvIndicadores.Visible = true;
        }



        protected void CmbProceso_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            DataSet ds = new DataSet();
            ds = ObjGIndicadores.DBUSQUEDAPROCESO(Convert.ToInt32(CmbProceso.SelectedValue), Convert.ToInt32(CmbtipoIndicadors.SelectedValue));
            if (ds.Tables[0].Rows.Count > 0)
            {
                CmbIndicadores.DataSource = ds;
                CmbIndicadores.DataValueField = "ID_INDICADOR";
                CmbIndicadores.DataTextField = "NOMBRE_INDICADOR";
                CmbIndicadores.DataBind();

            }
        }

        protected void CmbIndicadores_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            DataSet ds = new DataSet();
            ds = ObjGIndicadores.DACONSULTARNACIONAL(Convert.ToInt32(CmbIndicadores.SelectedValue));
            CargarCombosMaestros("T_PERIODICIDAD", 1, 2, 2, CmbMedida);
            CmbMedida.SelectedValue = ds.Tables[0].Rows[0]["ID_PERIODICIDAD"].ToString();
            CmbMedida.Enabled = false;
            CmbPeriodo.DataSource = ObjGIndicadores.DCONSULTATIPO(Convert.ToInt32(CmbMedida.SelectedValue));
            CmbPeriodo.DataValueField = "ID_DET_PERIODO";
            CmbPeriodo.DataTextField = "PERIODO";
            CmbPeriodo.DataBind();
        }

        protected void btn_limpiar_Click(object sender, EventArgs e)
        {
            CmbProceso.SelectedValue = "0";
            CmbProceso.Text = "";
            CmbIndicadores.SelectedValue = "0";
            CmbIndicadores.Text = "";
            CmbMedida.SelectedValue="0";
            CmbMedida.Text = "";
            CmbPeriodo.SelectedValue = "0";
            CmbPeriodo.Text = "0"; 
            GvIndicadores.Visible = false;



        }

        protected void CmbIndicadorRegional_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            DataSet ds = new DataSet();
            ds = ObjGIndicadores.DACONSULTARNACIONAL(Convert.ToInt32(CmbIndicadorRegional.SelectedValue));
            if (ds.Tables[0].Rows.Count > 0)
            {
                int varalcance;
                varalcance = Convert.ToInt32(ds.Tables[0].Rows[0]["ID_ALCANCE"].ToString());
                if (varalcance == 1)
                {
                 
                    CmbRegional.Visible = true;
             

               }
                else
                {
                    //LblSeccional.Visible = false;
                    //lblregional.Visible = false;
                    //CmbRegional.Visible = false;
                    //CmbSeccional.Visible = false;

                }

                CargarCombosMaestros("T_PERIODICIDAD", 1, 2, 2, CmbMedidaR);
                CmbMedidaR.SelectedValue = ds.Tables[0].Rows[0]["ID_PERIODICIDAD"].ToString();
                CmbMedidaR.Enabled = false;
                CmbPeriodoR.DataSource = ObjGIndicadores.DCONSULTATIPO(Convert.ToInt32(CmbMedidaR.SelectedValue));
                CmbPeriodoR.DataValueField = "ID_DET_PERIODO";
                CmbPeriodoR.DataTextField = "PERIODO";
                CmbPeriodoR.DataBind();
            }
        }

        protected void CmbMedidaR_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            CargarPeriodicidadRegional();
       
            }

        protected void CmbProcesoR_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            DataSet ds = new DataSet();
            ds = ObjGIndicadores.DBUSQUEDAPROCESO(Convert.ToInt32(CmbProcesoR.SelectedValue), Convert.ToInt32(CmbTipoRegional.SelectedValue));
            if (ds.Tables[0].Rows.Count > 0)
            {
                CmbIndicadorRegional.DataSource = ds;
                CmbIndicadorRegional.DataValueField = "ID_INDICADOR";
                CmbIndicadorRegional.DataTextField = "NOMBRE_INDICADOR";
                CmbIndicadorRegional.DataBind();

            }
        }

      

        protected void btn_consultarR_Click(object sender, EventArgs e)
        {

            if ((CmbTipoRegional.SelectedValue == "0") || (CmbTipoRegional.SelectedValue == ""))
            {

                LblResultadoR.Text = "Debe seleccionar el tipo de indicador";
                Lblresultado.Visible = true;
                return;
            }


            if ((CmbIndicadorRegional.SelectedValue == "0") || (CmbIndicadorRegional.SelectedValue == ""))
            {

                LblResultadoR.Text = "Debe seleccionar el indicador";
                LblResultadoR.Visible = true;
                return;
            }


            if ((CmbProcesoR.SelectedValue == "0") || (CmbProcesoR.SelectedValue == ""))
            {

                LblResultadoR.Text = "Debe seleccionar el proceso";
                Lblresultado.Visible = true;
                return;
            }
            LblResultadoR.Visible = false;

            GrV_regional.Rebind();
            GrV_regional.Visible = true;
        }


        protected void Btn_limpiaRegional_Click(object sender, EventArgs e)
        {

            CmbTipoRegional.SelectedValue = "0";
            CmbTipoRegional.Text = "";
            CmbIndicadorRegional.SelectedValue = "0";
            CmbIndicadorRegional.Text = "";
            CmbPeriodoR.SelectedValue = "0";
            CmbPeriodoR.Text = "";
            CmbRegional.SelectedValue = "0";
            CmbRegional.Text = "";
            GrV_regional.DataSource = "";
            GrV_regional.Visible = false;
            CmbProcesoR.SelectedValue = "0";    
            CmbProcesoR.Text = "";

        }

        protected void CmbProcesoS_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            DataSet ds = new DataSet();
            ds = ObjGIndicadores.DBUSQUEDAPROCESO(Convert.ToInt32(CmbProcesoS.SelectedValue), Convert.ToInt32(CmbTipoS.SelectedValue));
            if (ds.Tables[0].Rows.Count > 0)
            {
                CmbIndicadoresS.DataSource = ds;
                CmbIndicadoresS.DataValueField = "ID_INDICADOR";
                CmbIndicadoresS.DataTextField = "NOMBRE_INDICADOR";
                CmbIndicadoresS.DataBind();

            }
        }

        protected void CmMedida7_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            CargarPeriodicidadSeccional();
        }

        protected void CmbIndicadoresS_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {

            DataSet ds = new DataSet();
            ds = ObjGIndicadores.DACONSULTARNACIONAL(Convert.ToInt32(CmbIndicadoresS.SelectedValue));
            if (ds.Tables[0].Rows.Count > 0)
            {
                int varalcance;
                varalcance = Convert.ToInt32(ds.Tables[0].Rows[0]["ID_ALCANCE"].ToString());
                if (varalcance == 1)
                {

                    cmbseccional.Visible = true;


                }
                else
                {
                    //LblSeccional.Visible = false;
                    //lblregional.Visible = false;
                    //CmbRegional.Visible = false;
                    //CmbSeccional.Visible = false;

                }

                CargarCombosMaestros("T_PERIODICIDAD", 1, 2, 2, CmMedida7);
                CmMedida7.SelectedValue = ds.Tables[0].Rows[0]["ID_PERIODICIDAD"].ToString();
                CmMedida7.Enabled = false;
                Cmperiodo7.DataSource = ObjGIndicadores.DCONSULTATIPO(Convert.ToInt32(CmMedida7.SelectedValue));
                Cmperiodo7.DataValueField = "ID_DET_PERIODO";
                Cmperiodo7.DataTextField = "PERIODO";
                Cmperiodo7.DataBind();
            }
        }
    }
}
