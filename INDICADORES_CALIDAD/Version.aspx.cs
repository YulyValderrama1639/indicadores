﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CNegocios;
using System.Configuration;

namespace INDICADORES_CALIDAD
{
    public partial class Version : System.Web.UI.Page
    {
        public IndicadoresBussines ObjGIndicadores = new IndicadoresBussines();


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string Valor = Request.QueryString["ID_INDICADOR"];
                Session["IDINDICADOR"] = Valor;
                Gversion.DataSource=ObjGIndicadores.DCONSULTARVERSIONES(Convert.ToInt32(Valor));
                Gversion.DataBind();
            }
        }
       
    }
}