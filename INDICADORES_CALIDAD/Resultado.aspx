﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Resultado.aspx.cs" Inherits="INDICADORES_CALIDAD.Resultado" EnableEventValidation="false" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<!DOCTYPE html>
<link href="Content/StyleCustom.css" rel="stylesheet" />
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server"></asp:ScriptManager>

       
        <div style="align-content: center; text-align: center">
            <asp:Label runat="server" ID="LblNombre" Font-Size="20pt"  Font-Names="Agency FB" Font-Bold="true"></asp:Label>
        </div>

        <div class="col-sm-12">
            <telerik:RadTabStrip runat="server" ID="RadUsuarios" CssClass="html .rtsTop .rtsLevel1" Font-Names="Agency FB" MultiPageID="RadMultiPage1" SelectedIndex="0" BorderColor="#99CCFF" Skin="MetroTouch" PerTabScrolling="True" ReorderTabsOnSelect="True" ShowBaseLine="True">
                <Tabs>
                    <telerik:RadTab Text="RESULTADO REGIONAL" ImageUrl="~/images/pie-chart.png" Font-Bold="true" Width="250px"  runat ="server" Selected="True"></telerik:RadTab>
               


                </Tabs>
            </telerik:RadTabStrip>
        </div>


        <telerik:RadMultiPage ID="RadMultiPage1" SelectedIndex="0" runat="server">

            <telerik:RadPageView ID="Pagina1" runat="server">
                <telerik:RadGrid ID="GVMeses" runat="server" AllowPaging="true" OnNeedDataSource="GVMeses_NeedDataSource"  OnItemDataBound="GVMeses_ItemDataBound" AutoGenerateColumns="false" PageSize="12" Skin="MetroTouch" Visible="false" Width="100%">
                    <GroupingSettings CollapseAllTooltip="Collapse all groups"></GroupingSettings>

                   

                    <MasterTableView DataKeyNames="RESULTADO,PEOR,ROJA,ALCANCE,MEJOR">
                        <Columns>
                            <telerik:GridBoundColumn DataField="NOMBRE_REGIONAL" HeaderText="REGIONAL" >
                               
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="PERIODO" HeaderText="PERIODO" >
                              
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="RESULTADO" HeaderText="RESULTADO" AllowFiltering="false">
                                
                            </telerik:GridBoundColumn>
                                                 
                                    <telerik:GridBoundColumn DataField="CUMPLIMIENTO" HeaderText="CUMPLIMIENTO" >
                           
                              
                            </telerik:GridBoundColumn>
                                  <telerik:GridBoundColumn DataField="SEMAFORO" HeaderText="SEMAFORO" >
                              
                             
                            </telerik:GridBoundColumn>
                                  <telerik:GridBoundColumn DataField="PEOR" HeaderText="PEOR"  Visible="false">
                                        </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="ROJA" HeaderText="ROJA"  Visible="false">
                                        </telerik:GridBoundColumn>
                                  <telerik:GridBoundColumn DataField="ALCANCE" HeaderText="ALCANCE"  Visible="false">
                                        </telerik:GridBoundColumn>
                                  <telerik:GridBoundColumn DataField="MEJOR" HeaderText="MEJOR"  Visible="false" >
                                        </telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                    <HeaderStyle  VerticalAlign="Middle" Font-Names="Agency FB" Font-Size="14pt" />
                  
               
                </telerik:RadGrid>
            </telerik:RadPageView>
        </telerik:RadMultiPage>












    </form>
</body>
</html>
