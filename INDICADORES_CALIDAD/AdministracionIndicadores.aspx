﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdministracionIndicadores.aspx.cs" Inherits="INDICADORES_CALIDAD.AdministracionIndicadores" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>


<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <telerik:RadAjaxPanel runat="server">

        <telerik:RadWindowManager runat="server" Width="900px" Height="550px" ID="RwAyudas" Modal="True" Font-Names="Agency FB" Behaviors="Close,Move"
            RenderMode="Lightweight">
            <Windows>
                <telerik:RadWindow ID="rwBusqueda" Width="500px" IconUrl="~/images/icons/favicon.ico" Behaviors="Default" Font-Names="Agency FB" RenderMode="Mobile" Title="INDICADORES-CALIDAD" Font-Bold="true" Skin="Bootstrap" Modal="true" CenterIfModal="true" VisibleStatusbar="false"
                    runat="server">
                </telerik:RadWindow>
                <telerik:RadWindow ID="rwGuardado" IconUrl="~/Imagenes/IconosFormularios/faviconC.png" Behaviors="Close" RenderMode="Mobile" Title="INDICADORES-CALIDAD" Skin="Bootstrap" Modal="true" CenterIfModal="true" VisibleStatusbar="false"
                    runat="server">
                </telerik:RadWindow>

            </Windows>
        </telerik:RadWindowManager>
        <link href="Content/StyleCustom.css" rel="stylesheet" />

        <div class="col-sm-12" runat="server" id="DivAlert">
            <div class="DivGeneral DivColor">
                <asp:Image runat="server" ImageUrl="~/images/play.png" />
                <asp:Label Text="ADMINISTRADOR DE INDICADORES" Font-Bold="true" Font-Size="20pt" runat="server" Font-Names="Arial Narrow"></asp:Label>

            </div>

        </div>
        <div class="col-sm-12" runat="server" id="Div1">
            <telerik:RadTabStrip runat="server" ID="RadTabStrip1" Font-Names="Agency FB" MultiPageID="RadMultiPage1" Skin="Glow" SelectedIndex="1">
                <Tabs>
                    <telerik:RadTab Text="ADMINISTRACIÓN DE INDICADORES" Font-Bold="true" Width="250px" runat="server" Selected="true"></telerik:RadTab>
                    <telerik:RadTab Text="CONTROL DE CAMBIO DE INDICADORES" Font-Bold="true" Width="250px" runat="server" Visible="false"></telerik:RadTab>
                    <telerik:RadTab Text="HOJAS DE VIDA INDICADORES" Font-Bold="true" Width="250px" runat="server" Visible="false"></telerik:RadTab>
                </Tabs>
            </telerik:RadTabStrip>
            <telerik:RadMultiPage ID="RadMultiPage1" SelectedIndex="0" runat="server" CssClass="outerMultiPage">
                <telerik:RadPageView ID="Pagina1" runat="server">
                    <div class="Contencheckin" style="align-content: center; text-align: center">
                        <table style="width: 100%">
                            <tr>
                                <td>
                                    <asp:Label Text="Tipo de indicador" Font-Bold="true" Font-Size="12pt" runat="server" Font-Names="Arial Narrow"></asp:Label>
                                    <br />
                                    <telerik:RadComboBox ID="CmbTipoIndicador" AutoPostBack="true" runat="server" EmptyMessage="Seleccione el tipo de indicador" Skin="Bootstrap" Width="250px"></telerik:RadComboBox>

                                </td>
                                <td>
                                    <asp:Label Text="Proceso" Font-Bold="true" Font-Size="12pt" runat="server" Font-Names="Arial Narrow"></asp:Label>
                                    <br />
                                    <telerik:RadComboBox ID="CmbProceso" AutoPostBack="true" runat="server" EmptyMessage="Seleccione el tipo de indicador" Skin="Bootstrap" Width="250px"></telerik:RadComboBox>

                                </td>
                            </tr>
                        </table>


                        <br />

                        <br />
                        <br />
                          
                        <telerik:RadButton ID="rbConsultar" runat="server" CssClass="TextosSitio" Font-Size="12pt" Height="30px" OnClick="rbConsultar_Click" RenderMode="Lightweight" Skin="BlackMetroTouch" Text="Consultar" Width="200px"></telerik:RadButton>
                           <telerik:RadButton ID="rbEliminar" runat="server" CssClass="TextosSitio" Font-Size="12pt" Height="30px" OnClick="rbEliminar_Click" RenderMode="Lightweight" Skin="BlackMetroTouch" Text="Limpiar" Width="200px"></telerik:RadButton>
                  <br />
                        <asp:Label ID="LblInactivo" Visible="false" Font-Bold="true" Font-Size="12pt" runat="server" Font-Names="Arial Narrow"></asp:Label>
                                 <br />
                    </div>
                      <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                        <ProgressTemplate>
                            <div class="text-center">
                                <span style="font-size: 14pt; color: grey">Inactivando Indicador....</span>
                                <asp:Image ID="imgLoader" runat="server" ImageUrl="~/images/Progress.gif" Height="49px" Width="53px" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                       <telerik:RadGrid ID="GvIndicadores" runat="server" AllowPaging="true" Skin="Bootstrap" AutoGenerateColumns="false" MasterTableView-DataKeyNames="ID_INDICADOR" OnDeleteCommand="GvIndicadores_DeleteCommand" OnItemCreated="GvIndicadores_ItemCreated" OnNeedDataSource="GvIndicadores_NeedDataSource" PageSize="5" Visible="false" Width="100%" ForeColor="#660033">
                        <MasterTableView >
                            <Columns>
                                <telerik:GridButtonColumn CommandName="Delete" Text="Eliminar" HeaderText="INACTIVAR" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="Black" UniqueName="DeleteColumn" ButtonType="ImageButton" ImageUrl="images/delete.png" ItemStyle-Width="10pt" />
                                <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="EDITAR">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btn_detalle" runat="server" ImageUrl="~/images/editar.png" ImageAlign="NotSet" />
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="true" Wrap="true" Width="50px" Font-Size="10pt" />
                                    <ItemStyle Font-Size="10pt" Wrap="true" Width="50px" HorizontalAlign="Center" />
                                </telerik:GridTemplateColumn>

                                <telerik:GridBoundColumn DataField="NOMBRE_INDICADOR" AutoPostBackOnFilter="false" CurrentFilterFunction="equalto" FilterControlWidth="60px" HeaderText="NOMBRE INDICADOR" AllowFiltering="true" AllowSorting="true">
                                    <HeaderStyle Font-Bold="true" Wrap="true" Width="800px" ForeColor="Black" HorizontalAlign="Center" Font-Size="10pt" />
                                    <ItemStyle Font-Size="10pt" Wrap="true" Width="800px" ForeColor="#000000" />
                                </telerik:GridBoundColumn>

                            </Columns>

                        </MasterTableView>
                        <HeaderStyle CssClass="HeaderStyleGrid" />
                        <FooterStyle CssClass="footerStyle" />
                    </telerik:RadGrid>
                </telerik:RadPageView>
                 
                   
                      
                <telerik:RadPageView ID="Pagina2" runat="server">
                    <div class="Contencheckin" style="align-content: center; text-align: center">
                        <telerik:RadGrid ID="GvrHistorico" runat="server" AllowPaging="true" AutoGenerateColumns="false" OnItemCreated="GvrHistorico_ItemCreated" MasterTableView-DataKeyNames="ID_INDICADOR" OnDeleteCommand="GvrHistorico_DeleteCommand" OnNeedDataSource="GvrHistorico_NeedDataSource" PageSize="50" Skin="Bootstrap" Width="100%">
                            <ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true">
                            </ClientSettings>
                            <PagerStyle Mode="NextPrevAndNumeric"></PagerStyle>
                            <MasterTableView>
                                <PagerStyle Mode="NextPrevNumericAndAdvanced"></PagerStyle>
                                <Columns>
                                    <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="EDITAR">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btn_detalle" runat="server" ImageUrl="~/images/editar.png" />
                                        </ItemTemplate>
                                        <HeaderStyle Font-Bold="true" Wrap="true" Width="50px" Font-Size="8pt" />
                                        <ItemStyle Font-Size="8pt" Wrap="true" Width="50px" HorizontalAlign="Center" />
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridBoundColumn AllowFiltering="true" DataField="NOMBRE_INDICADOR" HeaderText="NOMBRE INDICADOR" FilterControlWidth="200px" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                        FilterDelay="2000" ShowFilterIcon="false">
                                        <HeaderStyle Font-Bold="true" Wrap="true" Width="800px" ForeColor="Black" HorizontalAlign="Center" Font-Size="10pt" />
                                        <ItemStyle Font-Size="8pt" Wrap="true" Width="800px" />
                                    </telerik:GridBoundColumn>



                                </Columns>

                            </MasterTableView>
                            <HeaderStyle CssClass="HeaderStyleGrid" />
                            <FooterStyle CssClass="footerStyle" />
                        </telerik:RadGrid>
                    </div>
                </telerik:RadPageView>
                <telerik:RadPageView ID="Pagina3" runat="server">
                    <div class="Contencheckin" style="align-content: center; text-align: center">
                        <asp:Label Text="INDICADORES" Font-Bold="true" Font-Size="12pt" runat="server" Font-Names="Arial Narrow"></asp:Label>
                        <br />

                        <telerik:RadComboBox ID="CmbConsultarTipo" AutoPostBack="true" runat="server" EmptyMessage="Seleccione el tipo de indicador" Skin="Bootstrap" Width="250px"></telerik:RadComboBox>


                        <br />
                        <br />
                        <telerik:RadButton ID="btn_Consulta3" runat="server" CssClass="TextosSitio" Font-Size="12pt" Height="30px" OnClick="btn_Consulta3_Click" RenderMode="Lightweight" Skin="BlackMetroTouch" Text="Consultar" Width="200px"></telerik:RadButton>
                    </div>
                    <br />
                    <div style="align-content: center">
                        <telerik:RadGrid runat="server" ID="RadDocumentos" OnItemCommand="RadDocumentos_ItemCommand1" Width="950px" AllowMultiRowSelection="true" MasterTableView-DataKeyNames="ID_INDICADOR" Visible="false" PageSize="10" AllowPaging="True" AllowSorting="True"
                            AllowFilteringByColumn="true"
                            CellSpacing="0" GridLines="None" OnNeedDataSource="RadDocumentos_NeedDataSource1" Skin="Bootstrap">
                            <GroupingSettings CaseSensitive="false" />
                            <MasterTableView AutoGenerateColumns="false" TableLayout="Fixed">
                                <HeaderStyle Width="80px" />
                                <Columns>

                                    <telerik:GridBoundColumn DataField="NOMBRE_INDICADOR" HeaderText="NOMBRE INDICADOR"
                                        ColumnGroupName="GeneralInformation">
                                        <HeaderStyle ForeColor="blue" />
                                        <ItemStyle Font-Size="9pt" Font-Bold="true" />
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn DataField="FECHA" HeaderText="FECHA DE CARGUE"
                                        ColumnGroupName="GeneralInformation">
                                        <HeaderStyle ForeColor="blue" />
                                        <ItemStyle ForeColor="Black" Font-Size="9pt" Font-Bold="true" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn Visible="false" DataField="RUTA" HeaderText="doc"
                                        ColumnGroupName="GeneralInformation">
                                        <HeaderStyle ForeColor=" #80807f" />
                                        <ItemStyle ForeColor="Black" Font-Size="9pt" Font-Bold="true" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="HOJA DE VIDA INDICADORES" ColumnGroupName="GeneralInformation" DataField="Certificados">
                                        <HeaderStyle ForeColor="blue" Font-Bold="true" Width="120px" HorizontalAlign="Center" />
                                        <ItemTemplate>

                                            <asp:ImageButton ImageAlign="Middle" ImageUrl="~/images/icons/file.png" runat="server" Width="32px" Height="32px" />
                                            <asp:LinkButton ID="lnkDownload" Text="HvIndicador" ForeColor="Blue" CommandArgument='<%# Eval("RUTA") %>' runat="server" CommandName="DownloadMyFile"></asp:LinkButton>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                </Columns>

                            </MasterTableView>

                        </telerik:RadGrid>

                    </div>
                </telerik:RadPageView>
            </telerik:RadMultiPage>
        </div>

    </telerik:RadAjaxPanel>


    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            var identificacion;
            function ShowEditForm(id, rowIndex) {
                var grid = $find("<%= GvIndicadores.ClientID %>");
                var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element();
                identificacion = rowControl;
                grid.get_masterTableView().selectItem(rowControl, true);
                window.radopen("DetalleIndicador.aspx?ID_INDICADOR=" + id);
                return false;

            }




        </script>
        <script type="text/javascript">
            var identificacion;
            function ShowEditForm2(id, rowIndex) {
                var grid = $find("<%= GvrHistorico.ClientID %>");
                var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element();
                identificacion = rowControl;
                grid.get_masterTableView().selectItem(rowControl, true);
                window.radopen("version.aspx?ID_INDICADOR=" + id, "rwBusqueda", "950px", "300px");
                return false;

            }




        </script>

    </telerik:RadCodeBlock>


</asp:Content>
