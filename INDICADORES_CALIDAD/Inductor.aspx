﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Inductor.aspx.cs" Inherits="INDICADORES_CALIDAD.Inductor" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <link href="Content/StyleCustom.css" rel="stylesheet" />
        <link href="Content/bootstrap.css" rel="stylesheet" />
                <asp:ScriptManager runat="server"></asp:ScriptManager>
        <div class="col-sm-12" runat="server" id="DivAlert">
            <div class="DivGeneral DivColor">
                <asp:Label Text="INDUCTOR" Font-Bold="true" Font-Size="15pt" runat="server" Font-Names="Arial Narrow"></asp:Label>

            </div>
        </div>
        <div class="col-sm-12" runat="server" id="Div1" style="align-content:center;text-align:center">
         
                <asp:Label runat="server" ID="LblNombre" Font-Size="12pt" Font-Names="Agency FB" Font-Bold="true"></asp:Label>

                <br />
                <br />
                <br />
           <telerik:RadGrid runat="server" ID="GVinductor" Width="100%" AutoGenerateColumns="false" Skin="Bootstrap" BackColor="White" Font-Bold="True">
                        <MasterTableView>
                            <Columns>

                                <telerik:GridBoundColumn DataField="NOMBRE_INDICADOR" HeaderText="INDICADOR">
                                    <HeaderStyle Font-Bold="true" ForeColor="#333333"  Font-Names="Arial Narrow"/>
                                    <ItemStyle Font-Names="Arial Narrow" />
                                </telerik:GridBoundColumn>
                                         <telerik:GridBoundColumn DataField="NOMBRE_PERSPECTIVA" HeaderText="PERSPECTIVA">
                                    <HeaderStyle Font-Bold="true" ForeColor="#333333"  Font-Names="Arial Narrow" />
                                </telerik:GridBoundColumn>
                                          <telerik:GridBoundColumn DataField="NOMBRE_INDUCTOR" HeaderText="INDUCTOR">
                                    <HeaderStyle Font-Bold="true" ForeColor="#333333"  Font-Names="Arial Narrow" />
                                                        <ItemStyle Font-Names="Arial Narrow" />
                                </telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>

                        <HeaderStyle CssClass="HeaderStyleGrid" />
                        <FooterStyle CssClass="footerStyle" />

                    </telerik:RadGrid>

            </div>
        

    </form>
</body>
</html>
