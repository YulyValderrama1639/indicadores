﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DetalleIndicador.aspx.cs" Inherits="INDICADORES_CALIDAD.DetalleIndicador" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<!DOCTYPE html>
<link href="Content/StyleCustom.css" rel="stylesheet" />
<link href="Content/bootstrap.css" rel="stylesheet" />
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server"></asp:ScriptManager>

        <telerik:RadWizard RenderMode="Lightweight" ID="RadCrearindicador" DisplayNavigationButtons="false" Localization-Previous="Anterior" OnFinishButtonClick="RadCrearindicador_FinishButtonClick" Localization-Finish="Finalizar" runat="server" Height="500px" Skin="Bootstrap">
            <WizardSteps>
                <telerik:RadWizardStep ID="RadWizardStep1" Title="MODIFICAR DATOS INDICADOR" Font-Names="Arial" ImageUrl="images/add-documents.png" Font-Bold="true" runat="server" StepType="Start">

                    <div class="col-sm-12" runat="server" id="Div1">
                        <table>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="LblNombre" Font-Bold="true" CssClass="Labels" Text="Nombre de indicador"></asp:Label>
                                </td>
                                <td>
                                    <telerik:RadTextBox runat="server" ID="TxtNombreindicador" TextMode="MultiLine" Width="450px" Skin="Bootstrap"></telerik:RadTextBox>
                                </td>


                            </tr>


                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="Lblperiodicidad" Text="Periodicidad de medición" Font-Bold="true" CssClass="Labels"></asp:Label>
                                </td>
                                <td>
                                    <telerik:RadComboBox runat="server" ID="cmboperiodicidad" Width="350px" Font-Size="12pt" Skin="Bootstrap" Font-Names="Agency FB"></telerik:RadComboBox>
                                </td>

                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lblunidad" Text="Unidad de medida" Font-Bold="true" CssClass="Labels"></asp:Label>
                                </td>
                                <td>
                                    <telerik:RadComboBox runat="server" ID="CmbUnidad" Width="350px" Skin="Bootstrap" Font-Size="12pt" Font-Names="Agency FB"></telerik:RadComboBox>
                                </td>

                            </tr>

                            <tr>

                                <td>
                                    <asp:Label runat="server" Text="Periodicidad de análisis" Font-Bold="true" CssClass="Labels"></asp:Label>

                                </td>
                                <td>
                                    <telerik:RadComboBox ID="cmbperiodicidada" Width="350px" EmptyMessage="Seleccionar periodicidad de análisis" runat="server" Skin="Bootstrap"></telerik:RadComboBox>
                                </td>


                            </tr>
                            <tr>
                                <td>

                                    <asp:Label runat="server" Text="Tipo de medición" Font-Bold="true" CssClass="Labels"></asp:Label>


                                </td>
                                <td>
                                    <telerik:RadComboBox ID="CmtipoMedicion" EmptyMessage="Seleccionar alcance" Width="350px" runat="server" Skin="Bootstrap"></telerik:RadComboBox>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" Font-Bold="true" CssClass="Labels" ID="Label3" Text="Alcance de la medición" Visible="false"></asp:Label>
                                </td>
                                <td>
                                    <telerik:RadComboBox runat="server" ID="CmbAlcance" Width="350px" Font-Size="12pt" Enabled="false" Skin="Bootstrap" Visible="false" Font-Names="Agency FB"></telerik:RadComboBox>
                                </td>

                            </tr>









                        </table>


                        <br />
                        <div style="align-content: center; text-align: center">
                            <div id="DResultado" runat="server" visible="false">
                                <div class="alert-success" role="alert" style="align-content: center; text-align: center">
                                    <span class="badge badge-pill badge-info">¡Guardado!</span>
                                    <br />
                                <asp:Label runat="server" ID="lblmensaje" Text="Se ha modificado conÉxito"></asp:Label>
                                    <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span aria-hidden="true">×</span></button>
                                </div>
                            </div>
                            <br />
                            <telerik:RadButton runat="server" Width="150px" ID="btn_actualizar" OnClick="btn_actualizar_Click" BackColor="#660066" Text="Actualizar" Skin="BlackMetroTouch">
                            </telerik:RadButton>

                            <br />
                        </div>
                    </div>
                </telerik:RadWizardStep>
                <%-- <telerik:RadWizardStep ID="RadWizardStep3" Title="RESPONSABLES" Visible="false" Font-Names="Arial" runat="server" ImageUrl="~/images/team.png">
                    <div class="ContendDivRegistro" style="width: 700px; height: auto">
                        <h2 class="Contendh3">ASIGNAR USUARIOS</h2>
                        <table style="width: 50%">

                            <tr>
                                <td>
                                    <asp:Label Text="USUARIO ASIGNADO" CssClass="Labels" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <telerik:RadComboBox runat="server" ID="CmbUsuario" Filter="Contains" EmptyMessage="Seleccionar usuario" Width="250px" Skin="Metro"></telerik:RadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label Text="ROL ASIGNADO" CssClass="Labels" Width="350px" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <telerik:RadComboBox runat="server" ID="CmbRol" Skin="Metro" EmptyMessage="Seleccionar rol" Width="250px"></telerik:RadComboBox>
                                </td>
                            </tr>

                        </table>
                        <br />
                        <div style="align-content; text-align: center">
                            <telerik:RadButton ID="btnagregar" runat="server" Text="Agregar" Skin="BlackMetroTouch" BackColor="#660066" OnClick="btnagregar_Click"></telerik:RadButton>
                            <br />
                            <br />

                            <telerik:RadGrid runat="server" ID="Gvrolesu" Visible="false" MasterTableView-DataKeyNames="ID_ROLESIN" OnDeleteCommand="Gvrolesu_DeleteCommand" AutoGenerateColumns="false" Skin="Vista">
                                <MasterTableView>
                                    <Columns>

                                        <telerik:GridButtonColumn CommandName="Delete" Text="Eliminar" HeaderText="INACTIVAR" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="Black" UniqueName="DeleteColumn" ButtonType="ImageButton" ImageUrl="images/delete.png" ItemStyle-Width="10px" />
                                        <telerik:GridBoundColumn DataField="ID_ROLES_IN" Visible="false">
                                            <HeaderStyle Font-Bold="true" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="NOMBRE" HeaderText="NOMBRE USUARIO">
                                            <HeaderStyle Font-Bold="true" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="NOMBRE_ROL" HeaderText="ROL">
                                            <HeaderStyle Font-Bold="true" />
                                        </telerik:GridBoundColumn>

                                    </Columns>
                                </MasterTableView>
                            </telerik:RadGrid>

                            <asp:Label ID="lblMensaje" CssClass="Labels" ForeColor="#009900" Width="350px" runat="server"></asp:Label>
                        </div>
                    </div>


                </telerik:RadWizardStep>--%>
            </WizardSteps>
        </telerik:RadWizard>




    </form>
</body>
</html>
