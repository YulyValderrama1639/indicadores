﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Parametros.aspx.cs" Inherits="INDICADORES_CALIDAD.Parametros" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <telerik:RadAjaxPanel runat="server">
        <link href="Content/StyleCustom.css" rel="stylesheet" />

        <div class="col-sm-12" runat="server" id="DivAlert">
            <div class="DivGeneral DivColor">
                     <asp:Image runat="server" ImageUrl="~/images/play.png" />
                <asp:Label Text="PARAMETROS INDICADORES" Font-Bold="true" Font-Size="20pt" runat="server" Font-Names="Arial Narrow"></asp:Label>
                           
            </div>

        </div>
        <div class="col-sm-12" runat="server" id="Div1">
            <telerik:RadTabStrip runat="server" ID="RadTabStrip1" Font-Names="Agency FB" MultiPageID="RadMultiPage1" Skin="Glow" SelectedIndex="1">
                <Tabs>
                    <telerik:RadTab Text="ADMINISTRACIÓN DE TABLAS MAESTRAS" Font-Bold="true" Width="250px" runat="server"></telerik:RadTab>
                </Tabs>


            </telerik:RadTabStrip>
            <telerik:RadMultiPage ID="RadMultiPage1" SelectedIndex="0" runat="server" CssClass="outerMultiPage">
                <%--BUSQUEDA TEXTO--%>
                <telerik:RadPageView ID="Pagina1" runat="server">
                    <div class="Contencheckin" style="align-content: center; text-align: center">
                        <asp:Label Text="SELECCIÓNAR TABLAS MAESTRA" Font-Bold="true" Font-Size="12pt" runat="server" Font-Names="Arial Narrow"></asp:Label>
                        <br />
                        <telerik:RadComboBox runat="server" ID="cmbtablam" Width="250px" EmptyMessage="Seleccione tabla maestra" Skin="Bootstrap">
                        </telerik:RadComboBox>
                        <br />
                        <telerik:RadButton runat="server" ID="BtnConsultar" Text="CONSULTAR" Font-Names="Arial Narrow" OnClick="BtnConsultar_Click" Skin="Glow" ButtonType="SkinnedButton"></telerik:RadButton>
                        <br />
                        <telerik:RadGrid runat="server" MasterTableView-DataKeyNames="1" ID="GVTMAESTRA" OnDeleteCommand="GVTMAESTRA_DeleteCommand" PageSize="20" AllowPaging="true" Visible="false" AllowAutomaticUpdates="true" OnInsertCommand="GVTMAESTRA_InsertCommand" OnUpdateCommand="GVTMAESTRA_UpdateCommand" OnNeedDataSource="GVTMAESTRA_NeedDataSource" Width="100%" AutoGenerateColumns="true" Skin="Bootstrap">
                            <MasterTableView AutoGenerateColumns="false" CommandItemDisplay="Top">
                                <CommandItemSettings AddNewRecordText="Agregar un nuevo item" RefreshText="Actualizar" />
                                <EditFormSettings>
                                    <EditColumn UniqueName="EditCommandColumn" ButtonType="ImageButton" CancelText="Cancelar" UpdateText="Actualizar" CancelImageUrl="images/cancel.png"
                                        UpdateImageUrl="images/Actualizar.png" InsertImageUrl="images/Add.png" ItemStyle-Width="50px">
                                    </EditColumn>
                                </EditFormSettings>
                                <Columns>
                                    <telerik:GridButtonColumn CommandName="Delete" Text="Delete" UniqueName="DeleteColumn" ButtonType="ImageButton" ImageUrl="images/delete.png" ItemStyle-Width="50px" />
                                    <telerik:GridEditCommandColumn EditImageUrl="images/editar.png" ItemStyle-Width="50px" ButtonType="ImageButton" />
                                    <telerik:GridBoundColumn DataField="1" HeaderText="ID_PARAMETRO" ReadOnly="true">
                                        <HeaderStyle Font-Bold="true" ForeColor="#333333" Font-Size="11pt" Font-Names="Arial Narrow" />
                                        <ItemStyle Font-Size="10pt" Font-Names="Arial Narrow" Width="50px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="2" HeaderText="NOMBRE_PARAMETRO">
                                        <HeaderStyle Font-Bold="true" ForeColor="#333333" Font-Size="11pt" Font-Names="Arial Narrow" />
                                        <ItemStyle Font-Size="10pt" Font-Names="Arial Narrow" Width="550px" />
                                    </telerik:GridBoundColumn>
                                </Columns>

                            </MasterTableView>

                            <HeaderStyle CssClass="HeaderStyleGrid" />
                            <FooterStyle CssClass="footerStyle" />

                        </telerik:RadGrid>

                        <telerik:RadInputManager RenderMode="Lightweight" runat="server" ID="RadInputManager1" Enabled="true" Skin="Office2010Silver">
                            <telerik:NumericTextBoxSetting BehaviorID="TxtNombreparametro" Type="Number"
                                AllowRounding="true" DecimalDigits="0" MinValue="0">
                            </telerik:NumericTextBoxSetting>
                        </telerik:RadInputManager>
                    </div>
                </telerik:RadPageView>
            </telerik:RadMultiPage>
        </div>
    </telerik:RadAjaxPanel>
</asp:Content>
