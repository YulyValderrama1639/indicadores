﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CNegocios;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Telerik.Web.UI;

namespace INDICADORES_CALIDAD
{
    public partial class Cumplimiento : System.Web.UI.Page
    {
        public SqlConnection Conexion;
        public IndicadoresBussines ObjGIndicadores = new IndicadoresBussines();
        public enum BANDERA { PEORB, MEJORB, METAB,ALCANCEB }
        protected void Page_Load(object sender, EventArgs e)
        {
            string Valor = Request.QueryString["ID_INDICADOR"];
            Session["VALOR"] = Valor;
            DataSet ds = new DataSet();
            int varalcance;
            ds = ObjGIndicadores.DBUSQUEDAVARIABLE(Convert.ToInt32(Valor));
            if (ds.Tables[0].Rows.Count > 0)
            {
                LblNombre.Text = ds.Tables[0].Rows[0]["NOMBRE_INDICADOR"].ToString();
                GVMeses.Rebind();
                GVMeses.Visible = true;

            }


        }

        private void CargarCombosMaestros(string Tabla, int campo1, int campo2, int orden, RadComboBox Combo, int? campo3 = null, int? campo4 = null)
        {
            Conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["SCORBOARD"].ConnectionString);
            SqlCommand cmd = new SqlCommand("P_CARGA_COMBOS", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TABLA", Tabla);
            cmd.Parameters.AddWithValue("@CAMPO1", campo1);
            cmd.Parameters.AddWithValue("@CAMPO2", campo2);
            cmd.Parameters.AddWithValue("@ORDEN", orden);
            cmd.Parameters.AddWithValue("@CAMPO_FILTRO", campo3);
            cmd.Parameters.AddWithValue("@VALOR_FILTRO", campo4);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {

                da.Fill(ds);
                Combo.DataSource = ds;
                Combo.DataValueField = "1";
                Combo.DataTextField = "2";
                Combo.DataBind();

            }
            catch (Exception ex)
            {

            }

        }
        
        protected void GVMeses_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {

            GVMeses.DataSource = ObjGIndicadores.DCONSULTARRESULTADO_S(Convert.ToInt32(Session["VALOR"]), Convert.ToInt32(Session["PERIODO"]));
        }

        protected void GVMeses_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {

                GridDataItem item = (GridDataItem)e.Item;
                var RESULTADO = item.OwnerTableView.DataKeyValues[item.ItemIndex]["RESULTADO"].ToString().Replace("%", "");
                var PEOR = item.OwnerTableView.DataKeyValues[item.ItemIndex]["PEOR"].ToString();
                var ROJA = item.OwnerTableView.DataKeyValues[item.ItemIndex]["ROJA"].ToString();
                var ALCANCE = item.OwnerTableView.DataKeyValues[item.ItemIndex]["ALCANCE"].ToString();
                var MEJOR = item.OwnerTableView.DataKeyValues[item.ItemIndex]["MEJOR"].ToString();

                double RESULTADO1 = Convert.ToDouble(RESULTADO);
                double PEOR1 = Convert.ToDouble(PEOR);
                double ROJA1 = Convert.ToDouble(ROJA);
                double ALCANCE1 = Convert.ToDouble(ALCANCE);
                double MEJOR1 = Convert.ToDouble(MEJOR);


                if (RESULTADO1 == 0)
                {
                    item["SEMAFORO"].Controls.Clear();
                    Image img = new Image();
                    img.ImageUrl = "~/Images/gris.png";
                    item["SEMAFORO"].Controls.Add(img);
                }
                if (RESULTADO1 <= ROJA1)
                {
                    item["SEMAFORO"].Controls.Clear();
                    Image img = new Image();
                    img.ImageUrl = "~/Images/Rojo.png";
                    item["SEMAFORO"].Controls.Add(img);
                }

                if (RESULTADO1 > ROJA1 && RESULTADO1 < PEOR1 )

                {
                    item["SEMAFORO"].Controls.Clear();
                    Image img = new Image();
                    img.ImageUrl = "~/Images/Amarillo.png";
                    item["SEMAFORO"].Controls.Add(img);
                }

                if (RESULTADO1 >= PEOR1 && RESULTADO1 <= ALCANCE1 )
                {
                    item["SEMAFORO"].Controls.Clear();
                    Image img = new Image();
                    img.ImageUrl = "~/Images/Verde.png";
                    item["SEMAFORO"].Controls.Add(img);
                }
                if (RESULTADO1 > ALCANCE1 && RESULTADO1 <= MEJOR1 || RESULTADO1 > ALCANCE1 && RESULTADO1 > MEJOR1)
                {
                    item["SEMAFORO"].Controls.Clear();
                    Image img = new Image();
                    img.ImageUrl = "~/Images/Azul.png";
                    item["SEMAFORO"].Controls.Add(img);
                }
            }
        }
    }
}