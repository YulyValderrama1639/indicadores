﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Search.aspx.cs" Inherits="INDICADORES_CALIDAD.Search" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
      <link href="Content/StyleCustom.css" rel="stylesheet" />
    <telerik:RadAjaxPanel runat="server">
        <telerik:RadWindowManager runat="server" ID="RwAyudas" Modal="True" Font-Names="Agency FB" Behaviors="Close,Move"
            RenderMode="Lightweight" Height="600px">
            <Windows>
                <telerik:RadWindow ID="rwBusqueda" IconUrl="~/images/icons/favicon.ico" Behaviors="Default" Font-Names="Agency FB" RenderMode="Mobile" Title="INDICADORES-CALIDAD" Font-Bold="true" Skin="Bootstrap" Modal="true" CenterIfModal="true" VisibleStatusbar="false"
                    runat="server">
                </telerik:RadWindow>
                <telerik:RadWindow ID="rwGuardado" IconUrl="~/Imagenes/IconosFormularios/faviconC.png" Behaviors="Close" RenderMode="Mobile" Title="Registro-Congreso" Skin="Bootstrap" Modal="true" CenterIfModal="true" VisibleStatusbar="false"
                    runat="server">
                </telerik:RadWindow>

            </Windows>
        </telerik:RadWindowManager>
  

        <div class="col-sm-12" runat="server" id="DivAlert">
            <div class="DivGeneral DivColor">
                <asp:Image runat="server" ImageUrl="~/images/play.png" />
                <asp:Label Text="CONSULTA INDICADORES" Font-Bold="true" Font-Size="20pt" runat="server" Font-Names="Arial Narrow"></asp:Label>

            </div>
        </div>

        <div class="col-sm-12" runat="server" id="Div1">
            <telerik:RadTabStrip runat="server" ID="RadTabStrip1" Font-Names="Agency FB" MultiPageID="RadMultiPage1" Skin="MetroTouch">
                <Tabs>

                    <telerik:RadTab Text="BUSQUEDA" Font-Bold="true" Width="150px" runat="server" Selected="True"></telerik:RadTab>
                    <telerik:RadTab Text="REGIONAL" Font-Bold="true" Width="150px" runat="server"></telerik:RadTab>
                    <telerik:RadTab Text="SECCIONAL" Font-Bold="true" Width="150px" runat="server"></telerik:RadTab>
                    <telerik:RadTab Text="4.PROCESOS" Font-Bold="true" Width="150px" runat="server" Visible="false"></telerik:RadTab>
                    <telerik:RadTab Text="5.TIPO" Font-Bold="true" Width="150px" runat="server" Visible="false"></telerik:RadTab>

                    
                </Tabs>
            </telerik:RadTabStrip>

            <telerik:RadMultiPage ID="RadMultiPage1" SelectedIndex="0" runat="server" CssClass="outerMultiPage">
                <%--BUSQUEDA TEXTO--%>
                <telerik:RadPageView ID="Pagina1" runat="server">                  
                <div class="ContendDivRegistro" style="width: 1050px; height:400px">
                           
                        <h2 class="Contendh3">BUSCAR POR INDICADOR</h2>
                        <div id="Div2" runat="server" visible="true">
                                 <asp:Label runat="server" ID="LblGuardado"></asp:Label>
                                 
                                </div>

                        <div id="DResultado" runat="server" visible="true">
                         
                    <div style="align-content:center;text-align:center" height: auto">
                    <table style="width: 100%">
                        <tr>
                            <td colspan="2" style="align-content:center">
                                <asp:Label runat="server" Text="Tipo Indicador" CssClass="Labels" Font-Bold="true"></asp:Label>
                                <br />
                                   <telerik:RadComboBox ID="CmbtipoIndicadors" AutoPostBack="true" EmptyMessage="Seleccionar indicador"  Filter="Contains" Width="350px" runat="server" Skin="Bootstrap"></telerik:RadComboBox>
                            </td>
                        </tr>
                      
                        <tr>
                             <td colspan="2">

                                <asp:Label runat="server" Text="Proceso" CssClass="Labels" Font-Bold="true"></asp:Label>
                                <br />
                                <telerik:RadComboBox ID="CmbProceso" AutoPostBack="true" EmptyMessage="Seleccionar proceso" OnSelectedIndexChanged="CmbProceso_SelectedIndexChanged" filter="Contains" Width="350px" runat="server" Skin="Bootstrap"></telerik:RadComboBox>
                                <br />

                            </td>
                        </tr>
                  
                        <tr>
                             <td colspan="2">

                                <asp:Label runat="server" Text="Indicador" CssClass="Labels" Font-Bold="true"></asp:Label>
                                <br />
                                <telerik:RadComboBox ID="CmbIndicadores" AutoPostBack="true" EmptyMessage="Seleccionar Indicador"  OnSelectedIndexChanged="CmbIndicadores_SelectedIndexChanged"   Filter="Contains" Width="550px" runat="server" Skin="Bootstrap"></telerik:RadComboBox>
                                <br />

                            </td>
                        </tr>
                        
                                   <tr>
                            <td>


                                <asp:Label Text="Medida" Font-Bold="true" Font-Size="12pt" runat="server" Font-Names="Arial Narrow"></asp:Label>
                                <br />
                                <telerik:RadComboBox runat="server" ID="CmbMedida"    AutoPostBack="true" Width="250px" EmptyMessage="Medida" Skin="Bootstrap">
                                </telerik:RadComboBox>


                            </td>
                            <td>
                                <asp:Label Text="Periodo" Font-Bold="true" Font-Size="12pt" runat="server" Font-Names="Arial Narrow"></asp:Label>
                                <br />
                                <telerik:RadComboBox runat="server" Filter="StartsWith" ID="CmbPeriodo" Skin="Bootstrap" Width="250px" EmptyMessage="Seleccionar Periodo" >
                                </telerik:RadComboBox>
                            </td>
                        </tr>
                     

                                                           
                    </table>
                    <br />

                    <telerik:RadButton runat="server" ID="btn_consultar" Text="Consultar" Font-Bold="true" ForeColor="#ffffff" BackColor="#25a0da"  Skin="MetroTouch"  OnClick="btn_consultar_Click" ></telerik:RadButton>

                                      <telerik:RadButton runat="server" ID="btn_limpiar" Font-Bold="true"  ForeColor="#ffffff" BackColor="#25a0da"  Text="Limpiar" Skin="MetroTouch"  OnClick="btn_limpiar_Click"  ></telerik:RadButton>
                    <br />
                        <br />
                    <asp:Label runat="server" ID="Lblresultado" Visible="false" ForeColor="#990000"></asp:Label>

                        
                               <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                        <ProgressTemplate>
                            <div class="text-center">
                                <span style="font-size: 12pt; color: grey">Procesando datos....</span>
                                <asp:Image ID="imgLoader23" runat="server" ImageUrl="~/images/Progress.gif"  Width="100px" Height="100px"/>
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <br />
                    <br />

                </div>
                    <div id="Div3" runat="server" class="col-sm-12" visible="false">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert" style="width: 400px">
                            <span class="badge badge-pill badge-danger">¡Advertencia!</span>
                            <br />
                            <asp:Label runat="server" ID="Label1"></asp:Label>
                            <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span aria-hidden="true">×</span></button>
                        </div>
                    </div>
                           </div>         
                           
                      
                      
                        <br />
                        <div id="Dvbusqueda1" runat="server" class="col-sm-12" visible="false">
                            <div class="alert alert-danger alert-dismissible fade show" role="alert" style="width: 400px">
                                <span class="badge badge-pill badge-danger">¡Advertencia!</span>
                                <br />
                                <asp:Label runat="server" ID="LblError"></asp:Label>
                                <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span aria-hidden="true">×</span></button>
                            </div>
                        </div>
                            </div>
                    <br />
                    <div style="align-content:center">
                       <telerik:RadGrid runat="server" ID="GvIndicadores"  Visible="false" MasterTableView-DataKeyNames="ID_INDICADOR"  OnNeedDataSource="GvIndicadores_NeedDataSource"  PageSize="10"  OnItemCreated="GvIndicadores_ItemCreated" OnItemDataBound="GvIndicadores_ItemDataBound"  Skin="MetroTouch">
                        <AlternatingItemStyle BackColor="#F0F0F0" />
                       <MasterTableView  Width="100%"  AutoGenerateColumns="False" DataKeyNames="RESULTADO1,PEOR,ROJA,ALCANCE,MEJOR,ID_INDICADOR,LIMITE">                                                
                            <Columns>
                                    <telerik:GridTemplateColumn  HeaderText="DETALLE">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btn_detalle" runat="server" ImageUrl="images/Detalle.png" />
                                        </ItemTemplate>
                                 
                                    </telerik:GridTemplateColumn>                  
                                    <telerik:GridBoundColumn  DataField="NOMBRE_INDICADOR" HeaderText="NOMBRE INDICADOR">
                             <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn  DataField="MES" HeaderText="MES">
                                           <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                    </telerik:GridBoundColumn>    
                                  <telerik:GridBoundColumn  DataField="LIMITE" HeaderText="LIMITE" Visible="false">
                                         
                                    </telerik:GridBoundColumn>    
                                    <telerik:GridBoundColumn DataField="NOMBRE_TIPOINDICADOR" HeaderText="TIPO INDICADOR" >
                                       <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                         </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="FRECUENCIA" HeaderText="FRECUENCIA" >
                            
                                         </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="META" HeaderText="META" >
                                       <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                         </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="RESULTADO1" HeaderText="RESULTADO" >
                                     <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                         </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CUMPLIMIENTO1" HeaderText="CUMPLIMIENTO" >
                                     <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                         </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn  HeaderText="REGIONAL">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btn_resultado" runat="server" ImageUrl="~/images/resultado.png" />
                                        </ItemTemplate>
                                     <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                         </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn  HeaderText="SECCIONAL">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btn_Cumplimiento" runat="server" ImageUrl="~/images/Cumplimiento.png" />
                                        </ItemTemplate>
                                    <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        </telerik:GridTemplateColumn>                                                            
                      
                                    <telerik:GridBoundColumn DataField="ALCANCE"   HeaderText="ALCANCE"  Visible="false">
                                    <ItemStyle Font-Size="10pt" Wrap="true" />
                                         </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="MEJOR" HeaderText="MEJOR"  Visible="false">
                              
                                        </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="SEMAFORO" HeaderText="SEMAFORO"  >
                                 <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                         </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn  HeaderText="ANÁLISIS CUALITATIVO" >
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btn_Analisis" runat="server" ImageUrl="~/images/loupe.png" />
                                        </ItemTemplate>
                                           <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn  HeaderText="GRAFICO">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btn_grafico" runat="server" ImageUrl="images/grafico.png" />
                                        </ItemTemplate>
                                          <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="DES_ALCANCE" HeaderText="ALCANCE" >
                                        <ItemStyle   Wrap="true"  />
                                        </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="PEOR" HeaderText="PEOR"  Visible="false">
                                        </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="ROJA" HeaderText="ROJA"  Visible="false">
                                        </telerik:GridBoundColumn>
                                  <telerik:GridBoundColumn DataField="ALCANCE" HeaderText="ALCANCE"  Visible="false">
                                        </telerik:GridBoundColumn>
                                  <telerik:GridBoundColumn DataField="MEJOR" HeaderText="MEJOR"  Visible="false" >
                                        </telerik:GridBoundColumn>
                                
                                
                            </Columns>
                             <HeaderStyle VerticalAlign="Middle" HorizontalAlign="Center"  Font-Bold="true" Font-Names="agency fb"/>
                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                        </MasterTableView>
                       
                      
                    </telerik:RadGrid>
                        </div>
                </telerik:RadPageView>
                <%--BUSQUEDA REGIONAL--%>
                <telerik:RadPageView ID="Pagina4" runat="server">
                    <br />
                    <div class="ContendDivRegistro" style="width: 1050px; height: auto">
                        <h2 class="Contendh3">BUSCAR POR REGIONAL</h2>
                        <div style="text-align: center; align-content: center">
                           <table style="width: 100%">
                        <tr>
                            <td colspan="3" style="align-content:center">
                                <asp:Label runat="server" Text="Tipo Indicador" CssClass="Labels" Font-Bold="true"></asp:Label>
                                <br />
                                   <telerik:RadComboBox ID="CmbTipoRegional" AutoPostBack="true" EmptyMessage="Seleccionar indicador"  Filter="Contains" Width="350px" runat="server" Skin="Bootstrap"></telerik:RadComboBox>
                            </td>
                        </tr>
                      
                        <tr>
                             <td colspan="3">

                                <asp:Label runat="server" Text="Proceso" CssClass="Labels" Font-Bold="true"></asp:Label>
                                <br />
                                <telerik:RadComboBox ID="CmbProcesoR" AutoPostBack="true" EmptyMessage="Seleccionar proceso" OnSelectedIndexChanged="CmbProcesoR_SelectedIndexChanged" filter="Contains" Width="350px" runat="server" Skin="Bootstrap"></telerik:RadComboBox>
                                <br />

                            </td>
                        </tr>
                  
                        <tr>
                             <td colspan="3">

                                <asp:Label runat="server" Text="Indicador" CssClass="Labels" Font-Bold="true"></asp:Label>
                                <br />
                                <telerik:RadComboBox ID="CmbIndicadorRegional" AutoPostBack="true" EmptyMessage="Seleccionar Indicador"  OnSelectedIndexChanged="CmbIndicadorRegional_SelectedIndexChanged"   Filter="Contains" Width="550px" runat="server" Skin="Bootstrap"></telerik:RadComboBox>
                                <br />

                            </td>
                        </tr>
                        
                                   <tr>
                                       <td>
                                  
                              <asp:Label Text="Regional" Font-Bold="true" Font-Size="12pt" runat="server" Font-Names="Arial Narrow"></asp:Label>
                               <br />
                            <telerik:RadComboBox runat="server" ID="CmbRegional"  AutoPostBack="true" Width="250px" EmptyMessage="Regional" Skin="Bootstrap">
                                </telerik:RadComboBox>
                             </td>
                                   
                            <td>


                                <asp:Label Text="Medida" Font-Bold="true" Font-Size="12pt" runat="server" Font-Names="Arial Narrow"></asp:Label>
                                <br />
                                <telerik:RadComboBox runat="server" ID="CmbMedidaR"  OnSelectedIndexChanged="CmbMedidaR_SelectedIndexChanged"  AutoPostBack="true" Width="250px" EmptyMessage="Medida" Skin="Bootstrap">
                                </telerik:RadComboBox>


                            </td>
                            <td>
                                <asp:Label Text="Periodo" Font-Bold="true" Font-Size="12pt" runat="server" Font-Names="Arial Narrow"></asp:Label>
                                <br />
                                <telerik:RadComboBox runat="server" Filter="StartsWith" ID="CmbPeriodoR" Skin="Bootstrap" Width="250px" EmptyMessage="Seleccionar Periodo" >
                                </telerik:RadComboBox>
                            </td>
                        </tr>
                  
                                                           
                    </table>
                    <br />

                          <asp:Label runat="server" ID="LblResultadoR" Visible="false" ForeColor="#990000"></asp:Label>
                            <br />
                    <telerik:RadButton runat="server" ID="btn_consultarR" Text="Consultar" Font-Bold="true" ForeColor="#ffffff" BackColor="#25a0da"  Skin="MetroTouch"  OnClick="btn_consultarR_Click" ></telerik:RadButton>

                                      <telerik:RadButton runat="server" ID="Btn_limpiaRegional" Font-Bold="true"  ForeColor="#ffffff" BackColor="#25a0da"  Text="Limpiar" Skin="MetroTouch"  OnClick="Btn_limpiaRegional_Click"  ></telerik:RadButton>
                        </div>


                    </div>
                         
                               <asp:UpdateProgress ID="UpdateProgress2" runat="server">
                        <ProgressTemplate>
                            <div class="text-center">
                                <span style="font-size: 12pt; color: grey">Procesando datos....</span>
                                <asp:Image ID="imgLoaderREGIONAL" runat="server" ImageUrl="~/images/Progress.gif"  Width="100px" Height="100px"/>
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <br />



                    <telerik:RadGrid ID="GrV_regional" runat="server" AllowPaging="true" AutoGenerateColumns="false" MasterTableView-DataKeyNames="ID_INDICADOR" OnItemDataBound="GrV_regional_ItemDataBound" OnItemCreated="GrV_regional_ItemCreated" OnNeedDataSource="GrV_regional_NeedDataSource" PageSize="10" Skin="MetroTouch" Visible="false" Width="100%">

                        <MasterTableView DataKeyNames="RESULTADO,PEOR,ROJA,ALCANCE,MEJOR,ID_INDICADOR "  Width="100%">
                              <AlternatingItemStyle BackColor="#F0F0F0" />
                           <Columns>
                                <telerik:GridTemplateColumn  HeaderText="DETALLE" HeaderStyle-Font-Names="Agency FB" HeaderStyle-Font-Bold="true"   >
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btn_detalle" runat="server" ImageUrl="images/Detalle.png" />
                                        
                                           </ItemTemplate>
                                  
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="NOMBRE_INDICADOR"  HeaderStyle-Font-Bold="true" HeaderStyle-Font-Names="Agency FB"   HeaderText="NOMBRE INDICADOR">
                                 
                                </telerik:GridBoundColumn>
                                  <telerik:GridBoundColumn DataField="REGIONAL"  HeaderStyle-Font-Bold="true" HeaderText="REGIONAL" HeaderStyle-Font-Names="Agency FB"    >
                                 </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="MES"  HeaderStyle-Font-Bold="true" HeaderText="MES" HeaderStyle-Font-Names="Agency FB"   >
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn  DataField="PERIODICIDAD" HeaderText="FRECUENCIA" HeaderStyle-Font-Names="Agency FB" HeaderStyle-Font-Bold="true"   >
                                </telerik:GridBoundColumn>
                                   <telerik:GridBoundColumn  DataField="META" HeaderText="META" HeaderStyle-Font-Names="Agency FB"   HeaderStyle-Font-Bold="true" >
                                     </telerik:GridBoundColumn>

                             
                                 <telerik:GridBoundColumn DataField="RESULTADO"  HeaderStyle-Font-Bold="true" HeaderText="RESULTADO" HeaderStyle-Font-Names="Agency FB"   >
                                </telerik:GridBoundColumn>

                                 <telerik:GridBoundColumn DataField="CUMPLIMIENTO"  HeaderStyle-Font-Bold="true" HeaderText="CUMPLIMIENTO" HeaderStyle-Font-Names="Agency FB"   >
                                </telerik:GridBoundColumn>
                              
                            
                                
                                <telerik:GridBoundColumn  DataField="RESULTADO" HeaderText="RESULTADO" Visible="false">
                                </telerik:GridBoundColumn>

                                  
                                <telerik:GridBoundColumn  DataField="SEMAFORO" HeaderText="SEMAFORO" HeaderStyle-Font-Names="Agency FB"  HeaderStyle-Font-Bold="true"   >
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn  HeaderText="ANÁLISIS CUALITATIVO" HeaderStyle-Font-Names="Agency FB" ItemStyle-VerticalAlign="Middle" HeaderStyle-Font-Bold="true"  >
                                    <ItemTemplate >
                                        <asp:ImageButton ID="btn_Analisis" runat="server" ImageUrl="~/images/loupe.png" />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn  HeaderText="GRAFICO" HeaderStyle-Font-Names="Agency FB"  HeaderStyle-Font-Bold="true" >
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btn_grafico" runat="server" ImageUrl="images/grafico.png" />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="PEOR" HeaderText="PEOR"  Visible="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ROJA" HeaderText="ROJA"  Visible="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ALCANCE" HeaderText="ALCANCE"  Visible="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="MEJOR" HeaderText="MEJOR"  Visible="false">
                                </telerik:GridBoundColumn>



                            </Columns>
                           
                           
                        </MasterTableView>                                                     
                        </telerik:RadGrid>

                    <br />
                    <div id="DivBusqueda4" runat="server" class="col-sm-12" visible="false">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <span class="badge badge-pill badge-danger">Error</span> ¡Debe seleccionar la regional para buscar!

                                <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span aria-hidden="true">×</span></button>
                        </div>
                    </div>

                </telerik:RadPageView>
                <%--BUSQUEDA SECCIONAL--%>
                <telerik:RadPageView ID="Pagina5" runat="server">
                    <br />
                    <div class="ContendDivRegistro" style="width: 1050px; height: auto">
                        <h2 class="Contendh3">BUSCAR POR SECCIONAL</h2>
                        <div style="text-align: center; align-content: center">
                            <div style="text-align: center; align-content: center">

                                    <table style="width: 100%">
                        <tr>
                            <td colspan="2" style="align-content:center">
                                <asp:Label runat="server" Text="Tipo Indicador" CssClass="Labels" Font-Bold="true"></asp:Label>
                                <br />
                                   <telerik:RadComboBox ID="CmbTipoS" AutoPostBack="true" EmptyMessage="Seleccionar indicador"  Filter="Contains" Width="350px" runat="server" Skin="Bootstrap"></telerik:RadComboBox>
                            </td>
                        </tr>
                      
                        <tr>
                             <td colspan="2">

                                <asp:Label runat="server" Text="Proceso" CssClass="Labels" Font-Bold="true"></asp:Label>
                                <br />
                                <telerik:RadComboBox ID="CmbProcesoS" AutoPostBack="true" EmptyMessage="Seleccionar proceso" OnSelectedIndexChanged="CmbProcesoS_SelectedIndexChanged" filter="Contains" Width="350px" runat="server" Skin="Bootstrap"></telerik:RadComboBox>
                                <br />

                            </td>
                        </tr>
                  
                        
                     
                                          <tr>

                                        <td colspan="3">
                                            <asp:Label runat="server" CssClass="Labels" Text="Indicador">                             
                                            </asp:Label>
                                            <br />
                                            <telerik:RadComboBox ID="CmbIndicadoresS" AutoPostBack="true" Filter="StartsWith" OnSelectedIndexChanged="CmbIndicadoresS_SelectedIndexChanged" runat="server" EmptyMessage="Seleccione el indicador" Skin="Bootstrap" Width="450px"></telerik:RadComboBox>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td >

                                            <asp:Label runat="server" CssClass="Labels" Text="Medida">                                                                     </asp:Label>
                                            <br />
                                            <telerik:RadComboBox ID="CmMedida7" runat="server" AutoPostBack="true" EmptyMessage="Seleccione frecuencia" Skin="Bootstrap"  Width="250px" ></telerik:RadComboBox>
                                        </td>
                                        <td >

                                            <asp:Label runat="server" CssClass="Labels" Text="Periodo">                             
                                            </asp:Label>
                                            <br />
                                            <telerik:RadComboBox ID="Cmperiodo7" Filter="StartsWith" runat="server" EmptyMessage="Seleccione el periodo" Skin="Bootstrap" Width="250px"></telerik:RadComboBox>
                                        </td>
                                    </tr>
                 
                                        <tr>
                                            <td colspan="2">
                                                <asp:Label runat="server" CssClass="Labels" Font-Size="12pt" Text="Seccional "></asp:Label>
                                                <br />
                                                <telerik:RadComboBox ID="cmbseccional" runat="server" EmptyMessage="Seleccione seccional" Filter="StartsWith" Skin="Bootstrap" Width="250px">
                                                </telerik:RadComboBox>
                                            </td>
                                        </tr>

                                                           
                    </table>
                                                                                                                                                                                                              
                                <br />
                                <telerik:RadButton ID="btnconsultarS" runat="server" Font-Size="12pt" Height="30px" OnClick="btnconsultar6_Click" Font-Bold="true"  ForeColor="#ffffff" BackColor="#25a0da"  Text="Consultar" Skin="MetroTouch"></telerik:RadButton>
                                <telerik:RadButton ID="btnlimpiarS" runat="server" Font-Size="12pt" Height="30px" OnClick="btnlimpiars_Click1" Font-Bold="true"  ForeColor="#ffffff" BackColor="#25a0da"  Text="Limpiar" Skin="MetroTouch"></telerik:RadButton>
                            </div>
                        </div>
               
                     
                    <br />
                          <asp:UpdateProgress ID="UpdateProgress3" runat="server">
                        <ProgressTemplate>
                            <div class="text-center">
                                <span style="font-size: 12pt; color: grey">Procesando datos....</span>
                                <asp:Image ID="imgLoaderSeccional" runat="server" ImageUrl="~/images/Progress.gif"  Width="100px" Height="100px"/>
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <telerik:RadGrid ID="GvSeccional" runat="server" AllowPaging="true" AutoGenerateColumns="false" MasterTableView-DataKeyNames="ID_INDICADOR" OnItemDataBound="GvSeccional_ItemDataBound" OnItemCreated="GvSeccional_ItemCreated" OnNeedDataSource="GvSeccional_NeedDataSource" PageSize="10" Skin="MetroTouch" Visible="false" Width="100%">

                        <MasterTableView DataKeyNames="RESULTADO,PEOR,ROJA,ALCANCE,MEJOR,ID_INDICADOR" AllowFilteringByColumn="false" Width="100%">
                           
                            <Columns>
                                <telerik:GridTemplateColumn  HeaderText="DETALLE">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btn_detalle" runat="server" ImageUrl="images/Detalle.png" />
                                    </ItemTemplate>
                                  
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="NOMBRE_INDICADOR" HeaderText="NOMBRE INDICADOR">
                                  
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn  DataField="PERIODICIDAD" HeaderText="FRECUENCIA">
                                   
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn  DataField="META" HeaderText="META">
                                    
                                </telerik:GridBoundColumn>

                                <telerik:GridBoundColumn  DataField="RESULTADO" HeaderText="RESULTADO" Visible="false">
                                   
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn  DataField="RESULTADO" HeaderText="RESULTADO">
                                    
                                </telerik:GridBoundColumn>

                                <telerik:GridBoundColumn  DataField="CUMPLIMIENTO" HeaderText="CUMPLIMIENTO">
                                   
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn  DataField="SEMAFORO" HeaderText="SEMAFORO">
                                   
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn  HeaderText="ANÁLISIS CUALITATIVO">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btn_Analisis" runat="server" ImageUrl="~/images/loupe.png" />
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="true"  Width="50px" Wrap="true" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn  HeaderText="GRAFICO">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btn_grafico" runat="server" ImageUrl="images/grafico.png" />
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="true"  Width="50px" Wrap="true" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </telerik:GridTemplateColumn>
                            </Columns>
                        </MasterTableView>     
                        <HeaderStyle VerticalAlign="Middle" HorizontalAlign="Center"  Font-Bold="true" Font-Names="agency fb"/>
                            
                     
                    </telerik:RadGrid>
                </telerik:RadPageView>
                    
            </telerik:RadMultiPage>
        </div>
          </telerik:RadAjaxPanel>

    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            var identificacion;
            function ShowEditForm(id, rowIndex) {
                var grid = $find("<%= GvIndicadores.ClientID %>");
                var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element();
                identificacion = rowControl;
                grid.get_masterTableView().selectItem(rowControl, true);
                window.radopen("DetalleIndicador1.aspx?ID_INDICADOR=" + id, "rwBusqueda", "950px", "610px");
                return false;

            }

            var identificacion;
            function ShowResultado(id, rowIndex) {
                var grid = $find("<%= GvIndicadores.ClientID %>");
                var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element();
                identificacion = rowControl;
                grid.get_masterTableView().selectItem(rowControl, true);
                window.radopen("Resultado.aspx?ID_INDICADOR=" + id, "rwBusqueda", "990px", "650px");
                return false;
            }
            function ShowAnalisis(id, rowIndex) {
                var grid = $find("<%= GvIndicadores.ClientID %>");
                var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element();
                identificacion = rowControl;
                grid.get_masterTableView().selectItem(rowControl, true);
                window.radopen("AnalisisIndicador.aspx?ID_INDICADOR=" + id, "rwBusqueda", "950px", "610px");
                return false;
            }



            var identificacion;
            function ShowCumplmiento(id, rowIndex) {
                var grid = $find("<%= GvIndicadores.ClientID %>");
                var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element();
                identificacion = rowControl;
                grid.get_masterTableView().selectItem(rowControl, true);

                window.radopen("Cumplimiento.aspx?ID_INDICADOR=" + id, "rwBusqueda", "950px", "610px");
                return false;
            }


        </script>

    </telerik:RadCodeBlock>

</asp:Content>
