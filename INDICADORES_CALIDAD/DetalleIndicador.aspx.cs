﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CNegocios;
using CEntidades;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Telerik.Web.UI;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace INDICADORES_CALIDAD
{
    public partial class DetalleIndicador : System.Web.UI.Page
    {
        public SqlConnection Conexion;
        public IndicadoresBussines ObjGIndicadores = new IndicadoresBussines();
        public IndicadoresEntities ObjGIndicadoresE = new IndicadoresEntities();
        public EEnvioCorreo ObjEntEnvioCorreo = new EEnvioCorreo();
        DateTime FECHA_CREACION;
        public string NOMBRE_INDICADOR;
        public string FORMULA;
        public string META;
        public string NOMBRE_PERODICIDAD;
        public string PROPOSITO_INDICADOR;
        public string PESO;
        public string UNIDAD;
        public string DES_ALCANCE;
        public string DES_CLASIFICACION;
        public string DESTIPOME;
        public string ETIQUETA;
        private string TIPOINDICADOR;
        private string PROCESO;
        private string NOMBREVAR;
        private string FUENTE;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                string Valor = Request.QueryString["ID_INDICADOR"];

                Session["IDINDICADOR"] = Valor;
                DataSet ds = ObjGIndicadores.DCONSULTAINDICADORESHIS(Convert.ToInt32(Valor));
                if (ds.Tables[0].Rows.Count > 0)
                {
                    TxtNombreindicador.Text = ds.Tables[0].Rows[0]["NOMBRE_INDICADOR"].ToString();



                    CargarCombosMaestros("T_PERIODO_ANALISI", 1, 2, 2, cmbperiodicidada);
                    cmbperiodicidada.SelectedValue = ds.Tables[0].Rows[0]["ID_PER_ANALISIS"].ToString();

                    CargarCombosMaestros("T_UNIDAD", 1, 2, 2, CmbUnidad);
                    CmbUnidad.SelectedValue = ds.Tables[0].Rows[0]["ID_UNIDADMEDIDA"].ToString();
                 
                    CargarCombosMaestros("T_PERIODICIDAD", 1, 2, 2, cmboperiodicidad);
                    CargarCombosMaestros("T_UNIDAD", 1, 2, 2, CmbUnidad);


                    //CargarCombosMaestros("T_ROLES", 1, 2, 2, CmbRol);
                    //CargarCombosMaestros("T_ALCANCE", 1, 2, 2, CmbAlcance);
                    CargarCombosMaestros("T_TIPO_MEDICION", 1, 2, 2, CmtipoMedicion);
                    CmtipoMedicion.SelectedValue = ds.Tables[0].Rows[0]["ID_TIPO_MEDICION"].ToString();


                    //CmbUsuario.DataSource = ObjGIndicadores.DCONSULTARUSUARIOS();
                    //CmbUsuario.DataValueField = "IDENTIFICACION";
                    //CmbUsuario.DataTextField = "NOMBRERESPONSABLE";
                    //CmbUsuario.DataBind();

                }



            }



        }

        private void CargarCombosMaestros(string Tabla, int campo1, int campo2, int orden, RadComboBox Combo, int? campo3 = null, int? campo4 = null)
        {
            Conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["SCORBOARD"].ConnectionString);
            SqlCommand cmd = new SqlCommand("P_CARGA_COMBOS", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TABLA", Tabla);
            cmd.Parameters.AddWithValue("@CAMPO1", campo1);
            cmd.Parameters.AddWithValue("@CAMPO2", campo2);
            cmd.Parameters.AddWithValue("@ORDEN", orden);
            cmd.Parameters.AddWithValue("@CAMPO_FILTRO", campo3);
            cmd.Parameters.AddWithValue("@VALOR_FILTRO", campo4);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {

                da.Fill(ds);
                Combo.DataSource = ds;
                Combo.DataValueField = "1";
                Combo.DataTextField = "2";
                Combo.DataBind();

            }
            catch (Exception ex)
            {

            }

        }

        protected void VarObjIndicaores()
        {
            ObjGIndicadoresE.IDINDICADOR = Convert.ToInt32(Session["IDINDICADOR"]);
            ObjGIndicadoresE.IDPERIODICIDAD = Convert.ToInt32(cmboperiodicidad.SelectedValue);
            if (cmboperiodicidad.SelectedValue != "0")
            {
                ObjGIndicadoresE.IDPERIODICIDAD = Convert.ToInt32(cmboperiodicidad.SelectedValue);
            }
            else
            {
                ObjGIndicadoresE.IDPERIODICIDAD = 1;

            }
            ObjGIndicadoresE.IDUNIDAD = Convert.ToInt32(CmbUnidad.SelectedValue);
            if (cmboperiodicidad.SelectedValue != "0")
            {
                ObjGIndicadoresE.IDUNIDAD = Convert.ToInt32(CmbUnidad.SelectedValue);
            }
            else
            {
                ObjGIndicadoresE.IDUNIDAD = 1;

            }
            ObjGIndicadoresE.IDPERANALISI = Convert.ToInt32(cmbperiodicidada.SelectedValue);

            if (cmbperiodicidada.SelectedValue != "0")
            {
                ObjGIndicadoresE.IDPERANALISI = Convert.ToInt32(cmbperiodicidada.SelectedValue);
            }
            else
            {
                ObjGIndicadoresE.IDPERANALISI = 1;

            }
            ObjGIndicadoresE.IDPERIODICIDAD = Convert.ToInt32(cmboperiodicidad.SelectedValue);

            if (cmbperiodicidada.SelectedValue != "0")
            {
                ObjGIndicadoresE.IDPERIODICIDAD = Convert.ToInt32(cmboperiodicidad.SelectedValue);
            }
            else
            {
                ObjGIndicadoresE.IDPERIODICIDAD = 1;

            }
            if (CmtipoMedicion.SelectedValue!="0")
            {
                ObjGIndicadoresE.IDTIPOMEDICION = Convert.ToInt32(CmtipoMedicion.SelectedValue);
            }
            else
            {
                ObjGIndicadoresE.IDTIPOMEDICION = 1;

            }

            ObjGIndicadoresE.META = "0";
            ObjGIndicadoresE.FORMULA = "0";

            ObjGIndicadoresE.NOMBREINDICADOR = TxtNombreindicador.Text;

            ObjGIndicadoresE.USUARIO = Session["ID_USUARIO"].ToString();
        }
        protected void btn_actualizar_Click(object sender, EventArgs e)
        {
            VarObjIndicaores();
            int nactualizar;
            nactualizar = ObjGIndicadores.UPDATEINDICADOR(ObjGIndicadoresE);
            if (nactualizar == -1)
            {
                DResultado.Visible = true;
            

            }
            else
            {
             
                DResultado.Visible = true;

            }
        }







        protected void GrvVariables_DeleteCommand(object sender, GridCommandEventArgs e)
        {

        }




        protected void btnagregar_Click(object sender, EventArgs e)
        {
            //if (CmbUsuario.SelectedValue != "" && CmbRol.SelectedValue != "")
            //{
            //    int nactualizar;

            //    nactualizar = ObjGIndicadores.CREATEROLESINDICADOR(Convert.ToInt32(CmbUsuario.SelectedValue), Convert.ToInt32(Session["IDINDICADOR"]), Convert.ToInt32(CmbRol.SelectedValue), false);
            //    DataSet ds = new DataSet();
            //    ds = ObjGIndicadores.DACONSULTAASINGANCION(Convert.ToInt32(Session["IDINDICADOR"]));
            //    Gvrolesu.DataSource = ds;
            //    Gvrolesu.DataBind();
            //    Gvrolesu.Visible = true;
            //    CmbUsuario.SelectedValue = "0";
            //    CmbUsuario.Text = "";
            //    CmbRol.SelectedValue = "0";
            //    CmbRol.Text = "";

            //}
            //else
            //{
            //    lblMensaje.Text = "Debe seleccionar el rol y el usuario para asignar";
            //    lblMensaje.ForeColor = System.Drawing.Color.Red;
            //    Gvrolesu.Visible = false;
            //}


        }

        protected void Gvrolesu_DeleteCommand(object sender, GridCommandEventArgs e)
        {

        }

        private void Limpiar()
        {

            CmbUnidad.SelectedValue = "0";
            CmbUnidad.Text = "";
            //Gvrolesu.Visible = false;
            //CmbRol.SelectedValue = "0";
            //CmbRol.Text = "";
            //CmbUsuario.SelectedValue = "0";
            //CmbUsuario.Text = "";
            CmbAlcance.SelectedValue = "0";
            CmbAlcance.Text = "";
            CmtipoMedicion.SelectedValue = "0";
            CmtipoMedicion.Text = "";
            //btnagregar.Enabled = true;
        }




        protected void RadCrearindicador_FinishButtonClick(object sender, WizardEventArgs e)
        {



            //DataSet ds2 = new DataSet();
            //ds2 = ObjGIndicadores.DACONSULTAASINGANCION(Convert.ToInt32(Session["IDINDICADOR"]));
            //if (ds2.Tables[0].Rows.Count > 0)
            //{

            //    for (int i = 0; i < ds2.Tables[0].Rows.Count; ++i)
            //    {

            //        Session["CorreoAdmin"] = (ds2.Tables[0].Rows[i]["CORREOADMINISTRADOR"]);



            //        (Session["NOMBRE_INDICADOR"]) = (ds2.Tables[0].Rows[0]["NOMBRE_INDICADOR"]);
            //        string archivo = "IND-" + (Session["NOMBRE_INDICADOR"]).ToString().Trim() + ".pdf";
            //        string ruta = Server.MapPath("Pdf\\") + archivo;
            //        CreatePdf();
            //        EnviarCorreo(ruta);
            //    }
            //}

            //Gvrolesu.DataSource = "";
            //Gvrolesu.Visible = false;
            //lblMensaje.Text = "Se realizo la modificación correctamentamente";





        }


        //private void EnviarCorreo(string ruta)
        //{
        //    try
        //    {

        //        NEnvioCorreo correo = new NEnvioCorreo();
        //        ObjEntEnvioCorreo.Para1 = Convert.ToString(Session["CorreoAdmin"]);
        //        ObjEntEnvioCorreo.Para2 = Convert.ToString(Session["CorreoActualizador"]);
        //        //ObjEntEnvioCorreo.CopiaO = LeerParametro(3);
        //        ObjEntEnvioCorreo.Asunto = "INDICADORES CALIDAD";

        //        ObjEntEnvioCorreo.Mensaje = "Bienvenido a la plataforma de indicadores </br> se ha creado un nuevo indicador en la plataforma con el siguiente nombre </br>" + NOMBRE_INDICADOR + " Adjunto encontrara la hoja de vida del indicador con los diferentes datos que lo componen";
        //        ObjEntEnvioCorreo.Adjunto = ruta;
        //        string textoCorreo = correo.EnvioCorreo(ObjEntEnvioCorreo);

        //        if (textoCorreo != "OK")
        //        {
        //            lblMensaje.Text = textoCorreo;
        //            lblMensaje.Visible = true;
        //            lblMensaje.ForeColor = System.Drawing.Color.Red;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        lblMensaje.Text = "Error al enviar el correo. " + e;
        //        lblMensaje.Visible = true;
        //        lblMensaje.ForeColor = System.Drawing.Color.Red;
        //    }
        //}



        protected void CreatePdf()
        {
            DataSet ds = new DataSet();
            ds = ObjGIndicadores.DCONSULTAPDF(Convert.ToInt32(Session["IDINDICADOR"]));
            if (ds.Tables[0].Rows.Count > 0)
            {

                FECHA_CREACION = Convert.ToDateTime(ds.Tables[0].Rows[0]["FECHA_CREACION"].ToString());
                NOMBRE_INDICADOR = (ds.Tables[0].Rows[0]["NOMBRE_INDICADOR"].ToString());
                FORMULA = (ds.Tables[0].Rows[0]["FORMULA"].ToString());
                META = (ds.Tables[0].Rows[0]["META"].ToString());
                NOMBRE_PERODICIDAD = (ds.Tables[0].Rows[0]["NOMBRE_PERODICIDAD"].ToString());
                PROPOSITO_INDICADOR = (ds.Tables[0].Rows[0]["PROPOSITO_INDICADOR"].ToString());
                PESO = (ds.Tables[0].Rows[0]["PESO"].ToString());
                UNIDAD = (ds.Tables[0].Rows[0]["UNIDAD_MEDIDA"].ToString());
                DES_ALCANCE = (ds.Tables[0].Rows[0]["DES_ALCANCE"].ToString());
                DES_CLASIFICACION = (ds.Tables[0].Rows[0]["DES_CLASIFICACION"].ToString());
                DESTIPOME = (ds.Tables[0].Rows[0]["DESTIPOME"].ToString());
                ETIQUETA = (ds.Tables[0].Rows[0]["NOMBRE_ETIQUETA"].ToString());
                TIPOINDICADOR = (ds.Tables[0].Rows[0]["NOMBRE_TIPOINDICADOR"].ToString());
                PROCESO = (ds.Tables[0].Rows[0]["NOMBRE_PROCESO"].ToString());
            }


            DataSet ds2 = new DataSet();
            ds2 = ObjGIndicadores.DCONSULTAPDFVAR(Convert.ToInt32(Session["IDINDICADOR"]));
            if (ds2.Tables[0].Rows.Count > 0)
            {
                NOMBREVAR = (ds2.Tables[0].Rows[0]["NOMBRE_VAR"].ToString());
                FUENTE = (ds2.Tables[0].Rows[0]["FUENTE"].ToString());

            }



            Document documento = new Document();
            Paragraph parrafo = new Paragraph();
            iTextSharp.text.Image hayimagen = default(iTextSharp.text.Image);
            PdfPTable tabla1 = new PdfPTable(3);
            PdfPTable tabla2 = new PdfPTable(3);

            PdfPCell cell;
            string path = Server.MapPath("Pdf");
            string pathimagenes = Server.MapPath("images");
            iTextSharp.text.pdf.PdfWriter pdfw = default(iTextSharp.text.pdf.PdfWriter);
            pdfw = iTextSharp.text.pdf.PdfWriter.GetInstance(documento, new System.IO.FileStream(path + "/IND-" + Session["NOMBRE_INDICADOR"] + ".pdf", System.IO.FileMode.Create));
            documento.Open();
            hayimagen = iTextSharp.text.Image.GetInstance(pathimagenes + "/membrete.png");

            hayimagen.SetAbsolutePosition(0, 0);
            hayimagen.ScaleAbsoluteWidth(600);
            hayimagen.ScaleAbsoluteHeight(845);
            documento.Add(hayimagen);

            documento.Add(new Paragraph(" "));
            documento.Add(new Paragraph(" "));
            documento.Add(new Paragraph(" "));

            parrafo.Alignment = Element.ALIGN_CENTER;
            parrafo.Font = FontFactory.GetFont("Arial", 14, Font.BOLD);
            parrafo.Add("INDICADOR MODIFICADO");
            documento.Add(parrafo);
            parrafo.Clear();

            documento.Add(new Paragraph(" "));
            documento.Add(new Paragraph(" "));

            parrafo.Alignment = Element.ALIGN_JUSTIFIED;
            parrafo.Font = FontFactory.GetFont("Arial", 9);
            parrafo.Leading = 10;
            parrafo.Add("Fecha de modificación:");
            parrafo.Add(Convert.ToString(FECHA_CREACION));
            documento.Add(parrafo);
            parrafo.Clear();

            documento.Add(new Paragraph(" "));
            documento.Add(new Paragraph(" "));

            tabla1.DefaultCell.HorizontalAlignment = Element.ALIGN_LEFT;
            tabla1.DefaultCell.BorderColor = BaseColor.BLACK;
            tabla1.DefaultCell.Border = Rectangle.BOX;
            tabla1.DefaultCell.BorderWidth = 1;


            tabla2.DefaultCell.HorizontalAlignment = Element.ALIGN_LEFT;
            tabla2.DefaultCell.BorderColor = BaseColor.BLACK;
            tabla2.DefaultCell.Border = Rectangle.BOX;
            tabla2.DefaultCell.BorderWidth = 1;



            tabla1.AddCell(new PdfPCell(new Paragraph("NOMBRE INDICADOR:", FontFactory.GetFont("Arial", 10, Font.BOLD))));
            cell = new PdfPCell(new Paragraph(NOMBRE_INDICADOR, FontFactory.GetFont("Arial", 10)));
            cell.Colspan = 2;
            cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
            tabla1.AddCell(cell);

            tabla1.AddCell(new PdfPCell(new Paragraph("PROPOSITO:", FontFactory.GetFont("Arial", 10, Font.BOLD))));
            cell = new PdfPCell(new Paragraph(PROPOSITO_INDICADOR, FontFactory.GetFont("Arial", 10)));
            cell.Colspan = 2;
            cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
            tabla1.AddCell(cell);

            tabla1.AddCell(new PdfPCell(new Paragraph("PERIODICIDAD:", FontFactory.GetFont("Arial", 10, Font.BOLD))));
            cell = new PdfPCell(new Paragraph(NOMBRE_PERODICIDAD, FontFactory.GetFont("Arial", 10)));
            cell.Colspan = 2;
            cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
            tabla1.AddCell(cell);

            tabla1.AddCell(new PdfPCell(new Paragraph("UNIDAD DE MEDIDA:", FontFactory.GetFont("Arial", 10, Font.BOLD))));
            cell = new PdfPCell(new Paragraph(UNIDAD, FontFactory.GetFont("Arial", 10)));
            cell.Colspan = 2;
            cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
            tabla1.AddCell(cell);

            tabla1.AddCell(new PdfPCell(new Paragraph("PESO:", FontFactory.GetFont("Arial", 10, Font.BOLD))));
            cell = new PdfPCell(new Paragraph(PESO, FontFactory.GetFont("Arial", 10)));
            cell.Colspan = 2;
            cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
            tabla1.AddCell(cell);

            tabla1.AddCell(new PdfPCell(new Paragraph("TIPO DE ALCANCE:", FontFactory.GetFont("Arial", 10, Font.BOLD))));
            cell = new PdfPCell(new Paragraph(DES_ALCANCE, FontFactory.GetFont("Arial", 10)));
            cell.Colspan = 2;
            cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
            tabla1.AddCell(cell);

            tabla1.AddCell(new PdfPCell(new Paragraph("CLASIFICACIÓN:", FontFactory.GetFont("Arial", 10, Font.BOLD))));
            cell = new PdfPCell(new Paragraph(DES_CLASIFICACION, FontFactory.GetFont("Arial", 10)));
            cell.Colspan = 2;
            cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
            tabla1.AddCell(cell);

            tabla1.AddCell(new PdfPCell(new Paragraph("TIPO MEDIDA:", FontFactory.GetFont("Arial", 10, Font.BOLD))));
            cell = new PdfPCell(new Paragraph(DESTIPOME, FontFactory.GetFont("Arial", 10)));
            cell.Colspan = 2;
            cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
            tabla1.AddCell(cell);

            tabla1.AddCell(new PdfPCell(new Paragraph("TENDENCIA:", FontFactory.GetFont("Arial", 10, Font.BOLD))));
            cell = new PdfPCell(new Paragraph(ETIQUETA, FontFactory.GetFont("Arial", 10)));
            cell.Colspan = 2;
            cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
            tabla1.AddCell(cell);

            tabla1.AddCell(new PdfPCell(new Paragraph("TIPO INDICADOR:", FontFactory.GetFont("Arial", 10, Font.BOLD))));
            cell = new PdfPCell(new Paragraph(TIPOINDICADOR, FontFactory.GetFont("Arial", 10)));
            cell.Colspan = 2;
            cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
            tabla1.AddCell(cell);

            tabla1.AddCell(new PdfPCell(new Paragraph("PROCESO:", FontFactory.GetFont("Arial", 10, Font.BOLD))));
            cell = new PdfPCell(new Paragraph(PROCESO, FontFactory.GetFont("Arial", 10)));
            cell.Colspan = 2;
            cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
            tabla1.AddCell(cell);



            tabla2.AddCell(new PdfPCell(new Paragraph("NOMBREVAR:", FontFactory.GetFont("Arial", 10, Font.BOLD))));
            cell = new PdfPCell(new Paragraph(PROCESO, FontFactory.GetFont("Arial", 10)));
            cell.Colspan = 2;
            cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
            tabla2.AddCell(cell);

            tabla2.AddCell(new PdfPCell(new Paragraph("FUENTE:", FontFactory.GetFont("Arial", 10, Font.BOLD))));
            cell = new PdfPCell(new Paragraph(PROCESO, FontFactory.GetFont("Arial", 10)));
            cell.Colspan = 2;
            cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
            tabla2.AddCell(cell);

            documento.Add(tabla1);
            documento.Add(tabla2);

            documento.Close();

        }
    }
}
