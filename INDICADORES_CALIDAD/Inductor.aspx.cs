﻿using System.Web.UI.WebControls;
using System.Data;
using CNegocios;
using System.Data.Entity;
using Telerik.Web.UI;

using System.Data.SqlClient;
using System.Configuration;
using System;

namespace INDICADORES_CALIDAD
{
    public partial class Inductor : System.Web.UI.Page
    {
        public IndicadoresBussines ObjGIndicadores = new IndicadoresBussines();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {


                string Valor = Request.QueryString["ID_INDICADOR"];
                Session["VALOR"] = Valor;

                DataSet ds = new DataSet();
                ds = ObjGIndicadores.DBUSQUEDAVARIABLE(Convert.ToInt32(Valor));
                if (ds.Tables[0].Rows.Count > 0)
                {
                    LblNombre.Text = "NOMBRE DE INDICADOR:" + ds.Tables[0].Rows[0]["NOMBRE_INDICADOR"].ToString();

                }

                DataSet dsin = new DataSet();
                dsin = ObjGIndicadores.DCONSULTAINDUCTORES(Convert.ToInt32(Valor));
                if (ds.Tables[0].Rows.Count > 0)
                {
                    GVinductor.DataSource = dsin;
                    GVinductor.DataBind();

                }
            }
        }
    }
}