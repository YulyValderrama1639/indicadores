﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CNegocios;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Telerik.Web.UI;
namespace INDICADORES_CALIDAD
{
    public partial class DetalleUsuario : System.Web.UI.Page
    {
        public IndicadoresBussines ObjGIndicadores = new IndicadoresBussines();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                string Valor = Request.QueryString["IDENTIFICACION"];
                Session["VALOR"] = Valor;
                DataSet ds = ObjGIndicadores.DUSUARIOS2((Valor));
                if (ds.Tables[0].Rows.Count > 0)
                {

                    lblNombre.Text = ds.Tables[0].Rows[0]["UserName"].ToString();
                    LblCargo.Text = ds.Tables[0].Rows[0]["Cargo"].ToString();
                    LblProceso.Text = ds.Tables[0].Rows[0]["company"].ToString();
                    lblNombre2.Text = ds.Tables[0].Rows[0]["NombreUsuario"].ToString();
                }
                RadtreePer.DataSource = ObjGIndicadores.DUSUARIOS3(1,Valor);
                RadtreePer.ID = "ID_MENU";
                RadtreePer.DataFieldID = "NOMBRE_MENU";
                RadtreePer.DataBind();
              
                GVpermisosind.Rebind();

            }
        }

        protected void GVpermisosind_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GVpermisosind.DataSource = ObjGIndicadores.DCONSULTAPER(1, Convert.ToString( Session["VALOR"] ));
        }
    }
}