﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CNegocios;
using CEntidades;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Telerik.Web.UI;

namespace INDICADORES_CALIDAD
{
    public partial class DetalleIndicador1 : System.Web.UI.Page
    {
        public SqlConnection Conexion;
        public IndicadoresBussines ObjGIndicadores = new IndicadoresBussines();
        public IndicadoresEntities ObjGIndicadoresE = new IndicadoresEntities();
        public EEnvioCorreo ObjEntEnvioCorreo = new EEnvioCorreo();
        DateTime FECHA_CREACION;
        public string NOMBRE_INDICADOR;
        public string FORMULA;
        public string META;
        public string NOMBRE_PERODICIDAD;
        public string PROPOSITO_INDICADOR;
        public string PESO;
        public string UNIDAD;
        public string DES_ALCANCE;
        public string DES_CLASIFICACION;
        public string DESTIPOME;
        public string ETIQUETA;
        private string TIPOINDICADOR;
        private string PROCESO;
        private string NOMBREVAR;
        private string FUENTE;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string Valor = Request.QueryString["ID_INDICADOR"];

                Session["IDINDICADOR"] = Valor;
                DataSet ds = ObjGIndicadores.DCONSULTAINDICADORESHIS(Convert.ToInt32(Valor));
                if (ds.Tables[0].Rows.Count > 0)
                {
                    TxtNombre.Text = ds.Tables[0].Rows[0]["NOMBRE_INDICADOR"].ToString();
                    TxtNombre.Enabled = false;
                    CargarCombosMaestros("T_PERIODO_ANALISI", 1, 2, 2, Cmbperiodicidada);
                    Cmbperiodicidada.SelectedValue = ds.Tables[0].Rows[0]["ID_PER_ANALISIS"].ToString();
                    Cmbperiodicidada.Enabled = false;

                                   CargarCombosMaestros("T_ALCANCE", 1, 2, 2, CmbAlcance);
                    CmbAlcance.SelectedValue = ds.Tables[0].Rows[0]["ID_ALCANCE"].ToString();
                    CmbAlcance.Enabled = false;

                    CargarCombosMaestros("T_UNIDAD", 1, 2, 2, CmbUnidad);
                    CmbUnidad.SelectedValue = ds.Tables[0].Rows[0]["ID_UNIDADMEDIDA"].ToString();
                    CmbUnidad.Enabled = false;

                    CargarCombosMaestros("T_ETIQUETAS", 1, 2, 2, CmbTendenciaI);
                    CmbTendenciaI.SelectedValue = ds.Tables[0].Rows[0]["ID_TENDENCIA"].ToString();
                    CmbTendenciaI.Enabled = false;

                    CargarCombosMaestros("T_PERIODICIDAD", 1, 2, 2, Cmbperiodicidad);
                    Cmbperiodicidad.SelectedValue = ds.Tables[0].Rows[0]["ID_PERIODICIDAD"].ToString();
                    Cmbperiodicidad.Enabled = false;

                    CargarCombosMaestros("T_TIPOINDICADOR", 1, 2, 2, CmbTipoIndicador);
                    CmbTipoIndicador.SelectedValue = ds.Tables[0].Rows[0]["ID_TIPO_INDICADOR"].ToString();
                    CmbTipoIndicador.Enabled = false;

                    CargarCombosMaestros("T_CLASIFICACION", 1, 2, 2, CmbClasificacion);
                    CmbClasificacion.SelectedValue = ds.Tables[0].Rows[0]["ID_CLASIFICACION"].ToString();
                    CmbClasificacion.Enabled = false;

                    CargarCombosMaestros("T_EMPRESA", 1, 2, 2, CmbEmpresa);
                    CmbEmpresa.SelectedValue = ds.Tables[0].Rows[0]["ID_EMPRESA"].ToString();
                    CmbEmpresa.Enabled = false;

                    CargarCombosMaestros("T_PROCESO", 1, 2, 2, CmbProceso);
                    CmbProceso.SelectedValue = ds.Tables[0].Rows[0]["ID_PROCESO"].ToString();
                    CmbProceso.Enabled = false;
                   
                    TxtMeta.Text = ds.Tables[0].Rows[0]["META"].ToString();
                    TxtMeta.Enabled = false;
                    TxtFormula.Text = ds.Tables[0].Rows[0]["FORMULA"].ToString();
                    TxtFormula.Enabled = false;
                    CargarCombosMaestros("T_UNIDAD", 1, 2, 2, CmbUnidad);
                    radFechainic.SelectedDate=Convert.ToDateTime( ds.Tables[0].Rows[0]["FECHA_CREACION"].ToString());
                    radFechainic.Enabled = false;

                }
            }


        }

            private void CargarCombosMaestros(string Tabla, int campo1, int campo2, int orden, RadComboBox Combo, int? campo3 = null, int? campo4 = null)
        {
            Conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["SCORBOARD"].ConnectionString);
            SqlCommand cmd = new SqlCommand("P_CARGA_COMBOS", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TABLA", Tabla);
            cmd.Parameters.AddWithValue("@CAMPO1", campo1);
            cmd.Parameters.AddWithValue("@CAMPO2", campo2);
            cmd.Parameters.AddWithValue("@ORDEN", orden);
            cmd.Parameters.AddWithValue("@CAMPO_FILTRO", campo3);
            cmd.Parameters.AddWithValue("@VALOR_FILTRO", campo4);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {

                da.Fill(ds);
                Combo.DataSource = ds;
                Combo.DataValueField = "1";
                Combo.DataTextField = "2";
                Combo.DataBind();

            }
            catch (Exception ex)
            {

            }

        }

    }
}
