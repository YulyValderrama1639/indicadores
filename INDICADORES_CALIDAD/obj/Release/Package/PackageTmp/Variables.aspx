﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Variables.aspx.cs" Inherits="INDICADORES_CALIDAD.Variables" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <link href="Content/StyleCustom.css" rel="stylesheet" />
    <link href="Content/bootstrap.css" rel="stylesheet" />
    <form id="form1" runat="server">

        <telerik:RadAjaxPanel runat="server">
            <asp:ScriptManager runat="server"></asp:ScriptManager>
            <br />
            <div class="col-sm-12" runat="server" id="DivAlert">
                <div class="DivGeneral DivColorr">
                    <asp:Label Text="VARIABLES" Font-Bold="true" Font-Size="15pt" runat="server" Font-Names="Arial Narrow"></asp:Label>

                </div>
            </div>
            <div class="col-sm-12" runat="server" id="Div2" style="text-align: center; align-content: center">
                <asp:Label runat="server" ID="LblNombre" Font-Size="11pt" Font-Bold="true" ForeColor="#3399ff"></asp:Label>

                <telerik:RadGrid runat="server" ID="GVariables" Visible="false" Skin="Windows7" Font-Size="11pt" AutoGenerateColumns="false" Font-Bold="True">
                    <MasterTableView>

                        <Columns>

                            <telerik:GridBoundColumn DataField="NOMBRE_VAR" HeaderText=" NOMBRE DE VARIABLES">
                                <HeaderStyle Font-Bold="true" Wrap="true" Width="50px" BackColor="#00AD93" ForeColor="White" Font-Names="Arial Narrow" Font-Size="10pt" />
                                <ItemStyle Font-Size="10pt" Wrap="true" Width="50px" HorizontalAlign="Left" Font-Names="Arial Narrow" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="DES_TIPOD" HeaderText="TIPO DE DATO">
                                <HeaderStyle Font-Bold="true" Wrap="true" Width="50px" BackColor="#00AD93" ForeColor="White" Font-Names="Arial Narrow" Font-Size="10pt" />
                                <ItemStyle Font-Size="10pt" Wrap="true" Width="50px" HorizontalAlign="Left" Font-Names="Arial Narrow" />
                            </telerik:GridBoundColumn>
                                   <telerik:GridBoundColumn DataField="FUENTE" HeaderText="FUENTE">
                                <HeaderStyle Font-Bold="true" Wrap="true" Width="50px" BackColor="#00AD93" ForeColor="White" Font-Names="Arial Narrow" Font-Size="10pt" />
                                <ItemStyle Font-Size="10pt" Wrap="true" Width="50px" HorizontalAlign="Left" Font-Names="Arial Narrow" />
                            </telerik:GridBoundColumn>
                        </Columns>
                        <PagerStyle AlwaysVisible="True" />
                    </MasterTableView>

                    <HeaderStyle CssClass="HeaderStyleGrid" BackColor="#00AD93" />
                    <FooterStyle CssClass="footerStyle" />

                    <PagerStyle AlwaysVisible="True" />

                </telerik:RadGrid>
            </div>






        </telerik:RadAjaxPanel>
    </form>

</body>
</html>
