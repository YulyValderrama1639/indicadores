﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Analisis.aspx.cs" Inherits="INDICADORES_CALIDAD.Analisis" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <link href="Content/StyleCustom.css" rel="stylesheet" />
    <link href="Content/bootstrap.css" rel="stylesheet" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>


    <form id="form1" runat="server">
        <telerik:RadAjaxPanel runat="server">
            <asp:ScriptManager runat="server"></asp:ScriptManager>

            <br />
            <div class="col-sm-12" runat="server" id="DivAlert">
                <div class="DivGeneral DivColor">
                    <asp:Label Text="CREAR ANÁLISIS CUALITATIVO" Font-Bold="true" Font-Size="15pt" runat="server" Font-Names="Arial Narrow"></asp:Label>

                </div>
            </div>
            <div class="col-sm-12" runat="server" id="Div2" style="text-align: center; align-content: center">
                <asp:Label runat="server" ID="LblNombre" Font-Size="11pt" Font-Bold="true"></asp:Label>
            </div>
           
            <div class="col-sm-12" runat="server" id="Div1" style="text-align: center; align-content: center">
                <table style="width: 100%">
                    <tr>
                        <td colspan="1">
                            <asp:Label Text="Análisis de medida" Font-Bold="true" ForeColor="#767676" runat="server" Font-Size="15pt" Font-Names="Agency FB">
                            </asp:Label>
                            <br />
                            <telerik:RadComboBox runat="server" ID="CmbVigencia" AutoPostBack="true" Width="250px" EmptyMessage="Seleccione analisis de medida" OnSelectedIndexChanged="CmbVigencia_SelectedIndexChanged" Skin="Bootstrap">
                            </telerik:RadComboBox>
                        </td>
                        <td>
                            <asp:Label Text="Periodo" Font-Bold="true" ForeColor="#767676" runat="server" Font-Size="15pt" Font-Names="Agency FB"></asp:Label>
                            <telerik:RadComboBox runat="server" ID="CmbPeriodo" Skin="Bootstrap" Width="250px" EmptyMessage="Seleccione periodo">
                            </telerik:RadComboBox>
                        </td>
                    </tr>

                </table>
                <br />
                <telerik:RadTextBox ID="TxtAnalisis"  TextMode="MultiLine" runat="server" Width="800px" Height="100px" Skin="Windows7"></telerik:RadTextBox>


            </div>
            <div>

            </div>
            <br />
            <div style="align-content: center; text-align: center">
                <telerik:RadButton runat="server" ID="btn_crear" Text="CREAR" Font-Names="Arial Narrow"  OnClick="btn_crear_Click" Skin="BlackMetroTouch"></telerik:RadButton>
                
                <telerik:RadButton runat="server" ID="btn_eliminar" Text="LIMPIAR" Font-Names="Arial Narrow" OnClick="btn_eliminar_Click" Skin="BlackMetroTouch"></telerik:RadButton>

            </div>

     

            <div style="align-content: center">
                <telerik:RadGrid runat="server" ID="GNotas" OnNeedDataSource="GNotas_NeedDataSource" OnUpdateCommand="GNotas_UpdateCommand" PageSize="3" AllowPaging="true" Visible="false" Width="100%" AutoGenerateColumns="false" Skin="Metro">
                    <MasterTableView DataKeyNames="ID_NOTA,ID_INDICADOR">
                        <EditFormSettings>
                            <EditColumn UniqueName="EditCommandColumn" ButtonType="ImageButton" CancelText="Cancelar" UpdateText="Actualizar" CancelImageUrl="images/cancel.png"
                                UpdateImageUrl="images/Actualizar.png" InsertImageUrl="~/RadControls/Grid/Skins/Default/Insert.gif" ItemStyle-Width="50px">
                            </EditColumn>
                        </EditFormSettings>
                        <Columns>
                            <telerik:GridEditCommandColumn EditImageUrl="images/editar.png" ButtonType="ImageButton" HeaderText="EDITAR" HeaderStyle-Font-Bold="true" />
                            <telerik:GridHTMLEditorColumn DataField="NOTA" HeaderText="ANALISIS CUALITATIVO" HtmlEncode="false" ConvertEmptyStringToNull="true" ItemStyle-Wrap="true">
                                <HeaderStyle HorizontalAlign="Center" Font-Bold="true" ForeColor="Black" />
                            </telerik:GridHTMLEditorColumn>


                        </Columns>

                    </MasterTableView>

                    <HeaderStyle CssClass="HeaderStyleGrid" />
                    <FooterStyle CssClass="footerStyle" />

                </telerik:RadGrid>
                <telerik:RadInputManager RenderMode="Lightweight" runat="server" ID="RadInputManager1" Enabled="true">
                    <telerik:TextBoxSetting BehaviorID="TxtNotas">
                    </telerik:TextBoxSetting>

                </telerik:RadInputManager>
        </telerik:RadAjaxPanel>
        
    </form>
</body>
</html>
