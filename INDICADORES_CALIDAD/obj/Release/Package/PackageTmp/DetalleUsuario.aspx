﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DetalleUsuario.aspx.cs" Inherits="INDICADORES_CALIDAD.DetalleUsuario" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>

<body>

    <form id="form1" runat="server">
        <telerik:RadAjaxPanel runat="server" PostBackControls="ImageButton1">
            <asp:ScriptManager runat="server"></asp:ScriptManager>
            <link href="Content/StyleCustom.css" rel="stylesheet" />
            <div class="col-sm-12" runat="server" id="DivAlert">
                <div class="DivGeneral DivColor3">
                    <asp:Label Text="Perfil de usuarios" Font-Bold="true" Font-Size="20pt"   runat="server" Font-Names="Arial Narrow"></asp:Label>
                    <asp:Image runat="server" ImageUrl="~/images/DetailUser.png" />
                </div>
            </div>
            <div>
                <div class="ContendDivRegistro" style="width:850px; height: auto">
                     <h2 class="Contendh3">PERFIL DE USUARIOS E INDICADORES</h2>
                <table style="width:100%;border:1px solid #e9ecef;text-align:left">
                    <tr>
                       <td><asp:Label runat="server" ID="LblNombre1" Text="Nombre:" CssClass="Labels"></asp:Label> </td>
                        <td> <asp:Label runat="server" ID="lblNombre2" CssClass="Labels2"></asp:Label></td>
                    </tr>
                    <tr>
                       <td>  <asp:Label runat="server" Text="Nombre usuario:" CssClass="Labels"></asp:Label></td>
                        <td><asp:Label runat="server" ID="lblNombre" CssClass="Labels2"></asp:Label></td>
                    </tr>
                    <tr>
                        <td><asp:Label runat="server" Text="Cargo:" CssClass="Labels"></asp:Label></td>
                        <td><asp:Label runat="server" ID="LblCargo" CssClass="Labels2"></asp:Label></td>
                    </tr>
                    <tr>
                        <td><asp:Label runat="server" Text="Compañia:" CssClass="Labels"></asp:Label></td>
                        <td><asp:Label runat="server" ID="LblProceso" CssClass="Labels2"></asp:Label></td>
                    </tr>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" CssClass="Labels" Text="Rol:"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="LblRol" runat="server" CssClass="Labels2"></asp:Label>
                        </td>
                    </tr>
                </table>
                    <br />
                    
                             <table style="width: 100%; border:1px solid #e9ecef;text-align:left" text-align:left">
                    <tr>
                        <td align="left">
                            <asp:Label runat="server" Text="PERMISOS DEL SISTEMA" CssClass="Labels"></asp:Label>
                            <telerik:RadTreeView RenderMode="Lightweight" EnableDragAndDropBetweenNodes="true" Skin="Office2010Black" Expanded="true" EnableDragAndDrop="True" runat="server" ID="RadtreePer">
                                <DataBindings>
                                    <telerik:RadTreeNodeBinding ModelID="ID_MENU" TextField="NOMBRE_MENU" ValueField="ID_MENU"
                                        Depth="0" FieldID="ID_MENU" />

                                </DataBindings>
                            </telerik:RadTreeView>
                        </td>
                        <td align="left">
                            <asp:Label runat="server" Text="PERMISOS INDICADORES" CssClass="Labels"></asp:Label>

                            <telerik:RadGrid runat="server" ID="GVpermisosind" AutoGenerateColumns="false" Skin="Metro" AllowFilteringByColumn="true" PageSize="10" OnNeedDataSource="GVpermisosind_NeedDataSource" Font-Size="15pt"
                                AllowSorting="True" Width="100%"
                                ShowFooter="True" AllowPaging="True" AllowMultiRowEdit="True" Culture="es-ES" FilterType="Combined" EnableLinqExpressions="False">
                                <MasterTableView AllowFilteringByColumn="true">

                                    <Columns>

                                        <telerik:GridBoundColumn AllowFiltering="true" DataField="NOMBRE_INDICADOR" HeaderText="Usuario" FilterControlWidth="150px" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                            FilterDelay="2000" ShowFilterIcon="false">
                                            <HeaderStyle Font-Bold="true" ForeColor="#333333" Font-Size="11pt" Font-Names="Arial Narrow" />
                                            <ItemStyle Font-Size="10pt" Font-Names="Arial Narrow" />
                                        </telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>

                                <HeaderStyle CssClass="HeaderStyleGrid" />
                                <FooterStyle CssClass="footerStyle" />

                            </telerik:RadGrid>

                        </td>
                    </tr>

                </table>
                 </div>

            </div>
        </telerik:RadAjaxPanel>
    </form>
</body>
</html>
