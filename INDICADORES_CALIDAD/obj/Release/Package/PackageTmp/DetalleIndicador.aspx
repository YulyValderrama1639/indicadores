﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DetalleIndicador.aspx.cs" Inherits="INDICADORES_CALIDAD.DetalleIndicador" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<!DOCTYPE html>
<link href="Content/StyleCustom.css" rel="stylesheet" />
<link href="Content/bootstrap.css" rel="stylesheet" />
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server"></asp:ScriptManager>

        <telerik:RadWizard RenderMode="Lightweight" ID="RadCrearindicador" DisplayNavigationButtons="true" Localization-Previous="Anterior" OnFinishButtonClick="RadCrearindicador_FinishButtonClick" Localization-Next="Siguiente" Localization-Finish="Finalizar" runat="server" Height="500px" Skin="Bootstrap">
            <WizardSteps>
                <telerik:RadWizardStep ID="RadWizardStep1" Title="MODIFICAR DATOS INDICADOR" Font-Names="Arial" ImageUrl="images/add-documents.png" Font-Bold="true" runat="server" StepType="Start">

                    <div class="col-sm-12" runat="server" id="Div1">
                        <table>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="LblNombre" Font-Bold="true" CssClass="Labels" Text="Nombre de indicador"></asp:Label>
                                </td>
                                <td>
                                    <telerik:RadTextBox runat="server" ID="TxtNombreindicador" Enabled="false" TextMode="MultiLine" Width="450px" Skin="Bootstrap"></telerik:RadTextBox>
                                </td>
                                
                                
                            </tr>
                            

                          



                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="Lblperiodicidad" Text="Periodicidad de medición" Font-Bold="true" CssClass="Labels"></asp:Label>
                                </td>
                                <td>
                                    <telerik:RadComboBox runat="server" ID="cmboperiodicidad" Width="350px" Font-Size="12pt" Skin="Bootstrap" Font-Names="Agency FB"></telerik:RadComboBox>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblunidad" Text="Unidad de medida" Font-Bold="true" CssClass="Labels"></asp:Label>
                                </td>
                                <td>
                                    <telerik:RadComboBox runat="server" ID="CmbUnidad" Width="350px" Skin="Bootstrap" Font-Size="12pt" Font-Names="Agency FB"></telerik:RadComboBox>
                                </td>
                            </tr>


                            <tr>

                                <td>
                                    <asp:Label runat="server" Text="Periodicidad de análisis" Font-Bold="true" CssClass="Labels"></asp:Label>

                                </td>
                                <td>
                                    <telerik:RadComboBox ID="cmbperiodicidada" Width="350px" EmptyMessage="Seleccionar periodicidad de análisis" runat="server" Skin="Bootstrap"></telerik:RadComboBox>
                                </td>
                                <td>

                                    <asp:Label runat="server" Text="Tipo de medición" Font-Bold="true" CssClass="Labels"></asp:Label>


                                </td>
                                <td>
                                    <telerik:RadComboBox ID="CmtipoMedicion" EmptyMessage="Seleccionar alcance" Width="350px" runat="server" Skin="Bootstrap"></telerik:RadComboBox>

                                </td>

                            </tr>

                            <tr>
                                <td>
                                    <asp:Label runat="server" Font-Bold="true" CssClass="Labels" ID="Label3" Text="Alcance de la medición" Visible="false"></asp:Label>
                                </td>
                                <td>
                                    <telerik:RadComboBox runat="server" ID="CmbAlcance" Width="350px" Font-Size="12pt" Enabled="false" Skin="Bootstrap" Visible="false" Font-Names="Agency FB"></telerik:RadComboBox>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="Label2" Font-Bold="true" CssClass="Labels" Text="Tendencia de resultado" Visible="false"></asp:Label>
                                </td>
                                <td>
                                    <telerik:RadComboBox runat="server" ID="CmbTendencia" Width="350px" Skin="Bootstrap" Font-Size="12pt" Visible="false" Font-Names="Agency FB"></telerik:RadComboBox>
                                </td>
                            </tr>




                            <tr>
                                <td>
                                    <asp:Label runat="server" Font-Bold="true" CssClass="Labels" ID="LblFormula" Text="Descripción de Fórmula"></asp:Label>
                                </td>
                                <td>
                                    <telerik:RadTextBox runat="server" ID="TxtFormula" TextMode="MultiLine" Width="500px" Font-Size="12pt" Height="80px" Skin="Bootstrap"></telerik:RadTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" Font-Bold="true" CssClass="Labels" ID="LbLmeta" Text="Meta" Font-Size="12pt"></asp:Label>
                                </td>
                                <td>
                                    <telerik:RadTextBox runat="server" ID="TxtMeta" Skin="Bootstrap"></telerik:RadTextBox>
                                    <br />


                                </td>
                            </tr>




                        </table>


                        <br />
                        <div style="align-content: center; text-align: center">
                            <asp:Label runat="server" ID="LblMensajes" ForeColor="Green" Font-Size="10pt" Visible="false"></asp:Label>
                            <br />
                            <telerik:RadButton runat="server" Width="150px" ID="btn_actualizar" OnClick="btn_actualizar_Click"  BackColor="#660066" Text="Actualizar" Skin="BlackMetroTouch">
                            </telerik:RadButton>

                            <br />
                        </div>
                    </div>
                </telerik:RadWizardStep>
                <telerik:RadWizardStep ID="RadWizardStep2" Title="CREAR VARIABLES" Font-Names="Arial" ImageUrl="~/images/formula.png" Font-Bold="true" runat="server">
                    <div class="ContendDivRegistro" style="width: 700px; height: auto">
                        <h2 class="Contendh3">CREAR VARIABLES</h2>
                        <div style="text-align: center; align-content: center">
                            <table style="width: 100%">
                                <tr>
                                    <td colspan="2">
                                        <br />
                                        <asp:Label runat="server" Text="Nombre de variable" CssClass="Labels" Font-Bold="true"></asp:Label>
                                        <br />
                                        <telerik:RadTextBox runat="server" ID="TxtNombreVariable" EmptyMessage="Seleccionar unidad de medida" Width="350px" Skin="Bootstrap"></telerik:RadTextBox>

                                    </td>

                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server" Text="Fuente de origen" CssClass="Labels" Font-Bold="true"></asp:Label>
                                        <br />
                                        <telerik:RadComboBox ID="CmbFuente" EmptyMessage="Seleccionar fuente" Width="250px" runat="server" Skin="Bootstrap"></telerik:RadComboBox>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" Text="Tipo de dato" CssClass="Labels" Font-Bold="true"></asp:Label>
                                        <br />
                                        <telerik:RadComboBox ID="CmbTipoDato" EmptyMessage="Seleccionar Tipo" Width="250px" runat="server" Skin="Bootstrap"></telerik:RadComboBox>
                                    </td>
                                </tr>
                            </table>






                        </div>
                    </div>
                    <br />  
                    <div style="text-align: center">
                        <telerik:RadButton ID="btn_agregar" runat="server" Text="Agregar" BackColor="#660066" Skin="BlackMetroTouch" OnClick="btn_agregar_Click"></telerik:RadButton>
                        <br />
                        <br />
                        <telerik:RadGrid runat="server" ID="GrvVariables" Visible="false" Font-Size="10pt" PageSize="3" OnDeleteCommand="GrvVariables_DeleteCommand" OnNeedDataSource="GrvVariables_NeedDataSource" AllowPaging="True" AllowSorting="True" Skin="Vista">
                            <MasterTableView AutoGenerateColumns="False" Width="100%" TableLayout="Fixed" DataKeyNames="ID_VARIABLE">
                                <Columns>

                                    <telerik:GridButtonColumn CommandName="Delete" Text="Delete" UniqueName="DeleteColumn" HeaderStyle-Width="50px" />

                                    <telerik:GridBoundColumn DataField="ID_VARIABLE" HeaderText="ID_VARIABLE">
                                        <HeaderStyle ForeColor="#003399" Width="150px" Font-Bold="true" />

                                        <ItemStyle Font-Size="10pt" ForeColor="#000000" Height="50px" Width="100px" HorizontalAlign="Justify" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridNumericColumn DataField="VALOR" HeaderText="VALOR">
                                        <HeaderStyle ForeColor="#003399" Width="150px" Font-Bold="true" />
                                        <ItemStyle Font-Size="10pt" ForeColor="#000000" Height="50px" Width="100px" HorizontalAlign="Justify" />
                                    </telerik:GridNumericColumn>


                                    <telerik:GridBoundColumn DataField="FUENTE" HeaderText="FUENTE">
                                        <HeaderStyle ForeColor="#003399" Width="150px" Font-Bold="true" />

                                        <ItemStyle Font-Size="10pt" ForeColor="#000000" Height="50px" Width="100px" HorizontalAlign="Justify" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="DES_TIPOD" HeaderText="TIPO DATO">
                                        <HeaderStyle ForeColor="#003399" Width="150px" Font-Bold="true" />

                                        <ItemStyle Font-Size="10pt" ForeColor="#000000" Height="50px" Width="100px" HorizontalAlign="Justify" />
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn DataField="FILA" HeaderText="ALIAS">
                                        <HeaderStyle ForeColor="#003399" Width="150px" Font-Bold="true" />

                                        <ItemStyle Font-Size="10pt" ForeColor="#000000" Height="50px" Width="100px" HorizontalAlign="Justify" />
                                    </telerik:GridBoundColumn>
                                </Columns>
                            </MasterTableView>
                        </telerik:RadGrid>
                    </div>
                    <br />

                    <div class="ContendDivRegistro" style="width: 700px; height: auto">
                        <h2 class="Contendh3">CONSTRUCCIÓN DE FORMULA</h2>
                        <div style="text-align: center; align-content: center">

                            <table style="width: 100%">
                                <tr>
                                    <td colspan="2">
                                        <br />
                                        <asp:Label runat="server" Text="Escribir fórmula" CssClass="Labels" Font-Bold="true"></asp:Label>
                                        <br />

                                    </td>

                                </tr>
                                <tr>
                                    <td>
                                        <telerik:RadTextBox runat="server" ID="TxtVariables" EmptyMessage="Seleccionar variables" idth="400px" Skin="Bootstrap" TextMode="MultiLine"></telerik:RadTextBox>

                                        <br />
                                        <br />
                                        <telerik:RadButton ID="btn_agregar2" runat="server" Text="Validar sintaxis" BackColor="#660066" Skin="BlackMetroTouch" OnClick="btn_agregar2_Click"></telerik:RadButton>
                                    </td>
                                    <td></td>
                                </tr>



                            </table>

                            <asp:Label runat="server" ID="LblValidarf" Visible="false" Font-Bold="true"></asp:Label>




                        </div>
                    </div>





                </telerik:RadWizardStep>
                <telerik:RadWizardStep ID="RadWizardStep3" Title="RESPONSABLES" Font-Names="Arial" runat="server" ImageUrl="~/images/team.png">
                    <div class="ContendDivRegistro" style="width: 700px; height: auto">
                        <h2 class="Contendh3">ASIGNAR USUARIOS</h2>
                        <table style="width: 50%">

                            <tr>
                                <td>
                                    <asp:Label Text="USUARIO ASIGNADO" CssClass="Labels" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <telerik:RadComboBox runat="server" ID="CmbUsuario" Filter="Contains" EmptyMessage="Seleccionar usuario" Width="250px" Skin="Metro"></telerik:RadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label Text="ROL ASIGNADO" CssClass="Labels" Width="350px" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <telerik:RadComboBox runat="server" ID="CmbRol" Skin="Metro" EmptyMessage="Seleccionar rol" Width="250px"></telerik:RadComboBox>
                                </td>
                            </tr>

                        </table>
                        <br />
                        <div style="align-content; text-align: center">
                            <telerik:RadButton ID="btnagregar" runat="server" Text="Agregar" Skin="BlackMetroTouch" BackColor="#660066" OnClick="btnagregar_Click"></telerik:RadButton>
                            <br />
                            <br />

                            <telerik:RadGrid runat="server" ID="Gvrolesu" Visible="false" MasterTableView-DataKeyNames="ID_ROLESIN" OnDeleteCommand="Gvrolesu_DeleteCommand" AutoGenerateColumns="false" Skin="Vista">
                                <MasterTableView>
                                    <Columns>

                                        <telerik:GridButtonColumn CommandName="Delete" Text="Eliminar" HeaderText="INACTIVAR" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="Black" UniqueName="DeleteColumn" ButtonType="ImageButton" ImageUrl="images/delete.png" ItemStyle-Width="10px" />
                                        <telerik:GridBoundColumn DataField="ID_ROLES_IN" Visible="false">
                                            <HeaderStyle Font-Bold="true" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="NOMBRE" HeaderText="NOMBRE USUARIO">
                                            <HeaderStyle Font-Bold="true" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="NOMBRE_ROL" HeaderText="ROL">
                                            <HeaderStyle Font-Bold="true" />
                                        </telerik:GridBoundColumn>

                                    </Columns>
                                </MasterTableView>
                            </telerik:RadGrid>

                            <asp:Label ID="lblMensaje" CssClass="Labels" ForeColor="#009900" Width="350px" runat="server"></asp:Label>
                        </div>
                    </div>


                </telerik:RadWizardStep>
            </WizardSteps>
        </telerik:RadWizard>




    </form>
</body>
</html>
