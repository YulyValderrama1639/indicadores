﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CreateIndicator.aspx.cs" Inherits="INDICADORES_CALIDAD.CreateIndicator" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <telerik:RadWindowManager runat="server" ID="RwAyudas" Modal="True" Behaviors="Close,Move"
        RenderMode="Lightweight" Width="400px" Height="200px" Skin="Bootstrap" >
        <Windows>
            <telerik:RadWindow ID="rwBusqueda" Behaviors="Default" Font-Names="Arial" RenderMode="Mobile" Title="INDICADORES-CALIDAD" Font-Bold="true" Modal="true" CenterIfModal="true" VisibleStatusbar="false" runat="server">
                <ContentTemplate>
                    <div style="align-content: center; text-align: center">
                        <asp:Label runat="server" ID="LblVerificar" Text="Se realizo la creación del indicador con éxito </br> Adjunto al correo electrónico encontrara el detalle del indicador " Font-Size="15pt" Font-Names="agency fb"></asp:Label>
                    </div>
                </ContentTemplate>

            </telerik:RadWindow>
          
    
            <telerik:RadWindow ID="rwGuardado" IconUrl="~/Imagenes/IconosFormularios/faviconC.png" Behaviors="Close" RenderMode="Mobile" Title="Registro-Congreso" Skin="Bootstrap" Modal="true" CenterIfModal="true" VisibleStatusbar="false"
                runat="server">
            </telerik:RadWindow>

        </Windows>
    </telerik:RadWindowManager>
    <telerik:RadAjaxPanel runat="server">
        <div runat="server" id="DivAlert">
            <div class="DivGeneral DivColor">
                <asp:Image runat="server" ImageUrl="~/images/play.png" />
                <asp:Label Text="CREAR INDICADORES" Font-Bold="true" Font-Size="20pt" runat="server" Font-Names="Arial Narrow"></asp:Label>

            </div>
        </div>

        <telerik:RadWizard RenderMode="Lightweight" ID="RadCrearindicador" DisplayNavigationButtons="true" Localization-Previous="Anterior" OnFinishButtonClick="RadCrearindicador_FinishButtonClick" Localization-Next="Siguiente" Localization-Finish="Finalizar" runat="server" Height="900px" Skin="Bootstrap">
            <WizardSteps>

                <telerik:RadWizardStep ID="RadWizardStep1" Title="PASO1. DATOS INDICADOR" Font-Names="Arial" ImageUrl="images/add-documents.png" Font-Bold="true" runat="server" StepType="Start">
                    <div class="col-sm-12" runat="server" id="Div1">
                        <table style="width: 90%">
                            <tr>

                                <td>
                                    <asp:Label runat="server" Text="Nombre indicador" CssClass="Labels" Font-Bold="true"></asp:Label>
                                    <br>
                                    <telerik:RadTextBox runat="server" ID="TxtNombre" EmptyMessage="Nombre de indicador" Width="350px" Skin="Bootstrap"></telerik:RadTextBox>
                                </td>

                                <td>
                                    <asp:Label runat="server" Text="Empresa" CssClass="Labels" Font-Bold="true"></asp:Label>
                                    <br />
                                    <telerik:RadComboBox ID="CmbEmpresa" Width="350px" runat="server" EmptyMessage="Seleccionar empresa" AutoPostBack="true" OnSelectedIndexChanged="CmbEmpresa_SelectedIndexChanged" Skin="Bootstrap"></telerik:RadComboBox>

                                </td>


                            </tr>
                            <tr>

                                <td>
                                    <asp:Label runat="server" Text="Clasificación del indicador" CssClass="Labels" Font-Bold="true"></asp:Label>
                                    <br />
                                    <telerik:RadComboBox ID="CmbClasificacion" Width="350px" runat="server" EmptyMessage="Seleccionar clasificación" Skin="Bootstrap"></telerik:RadComboBox>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text="Tipo de indicador" Font-Bold="true" CssClass="Labels"></asp:Label>
                                    <br />
                                    <telerik:RadComboBox ID="CmbTipoIndicador" Width="350px" runat="server" AutoPostBack="true" EmptyMessage="Seleccionar Tipo de indicador" Skin="Bootstrap" OnSelectedIndexChanged="CmbTipoIndicador_SelectedIndexChanged"></telerik:RadComboBox>

                                </td>
                            </tr>
                            <tr>

                                <td>
                                    <asp:Label runat="server" Text="Fecha creación" CssClass="Labels" Font-Bold="true"></asp:Label>
                                    <br />
                                    <telerik:RadDatePicker runat="server" ID="radFechainic" Skin="Bootstrap" Calendar-CultureInfo="es-CO"></telerik:RadDatePicker>
                                </td>

                                <td>
                                    <asp:Label runat="server" Text="Propósito" CssClass="Labels" Font-Bold="true"></asp:Label>
                                    <br />
                                    <telerik:RadTextBox runat="server" Width="350px" ID="TxtProposito" Skin="Bootstrap"></telerik:RadTextBox>
                                </td>
                            </tr>





                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lblperspectiva" Text="Perspectiva" Visible="false" Font-Bold="true" CssClass="Labels"></asp:Label>
                                    <br />
                                    <telerik:RadComboBox ID="Cmbperspectiva" Visible="false" Width="350px" runat="server" AutoPostBack="true" EmptyMessage="Seleccionar perspectiva" OnSelectedIndexChanged="Cmbperspectiva_SelectedIndexChanged" Skin="Bootstrap"></telerik:RadComboBox>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="LblInductor" Text="Inductor" Visible="false" Font-Bold="true" CssClass="Labels"></asp:Label>
                                    <br />
                                    <telerik:RadComboBox ID="cmbinductor" Visible="false" Width="350px" runat="server" EmptyMessage="Seleccione el inductor" Skin="Bootstrap"></telerik:RadComboBox>
                                </td>

                            </tr>

                            <tr>

                                <td>
                                    <asp:Label runat="server" Text="Proceso" ID="LblProceso" Font-Bold="true" CssClass="Labels"></asp:Label>
                                    <br />
                                    <telerik:RadComboBox ID="CmbProceso" Width="350px" EmptyMessage="Seleccionar proceso" runat="server" Skin="Bootstrap"></telerik:RadComboBox>
                                </td>


                            </tr>
                            <tr>

                                <td>
                                    <asp:Label runat="server" Text="Periodicidad de medición" CssClass="Labels" Font-Bold="true"></asp:Label>
                                    <telerik:RadComboBox ID="Cmbperiodicidad" EmptyMessage="Seleccionar periodicidad" Width="350px" runat="server" Skin="Bootstrap"></telerik:RadComboBox>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text="Unidad Medida" Font-Bold="true" CssClass="Labels"> </asp:Label>
                                    <br />
                                    <telerik:RadComboBox ID="CmbUnidad" Width="350px" runat="server" Skin="Bootstrap" EmptyMessage="Seleccionar unidad de medida"></telerik:RadComboBox>
                                </td>

                            </tr>

                            <tr>

                                <td>
                                    <asp:Label runat="server" Text="Periodicidad de análisis" Font-Bold="true" CssClass="Labels"></asp:Label>
                                    <br />
                                    <telerik:RadComboBox ID="Cmbperiodicidada" Width="350px" EmptyMessage="Seleccionar periodicidad de análisis" runat="server" Skin="Bootstrap"></telerik:RadComboBox>
                                </td>
                                <td>
                                    <asp:Label runat="server" Text="Alcance de medición" Font-Bold="true" CssClass="Labels"></asp:Label>
                                    <telerik:RadComboBox ID="CmbAlcance" EmptyMessage="Seleccionar alcance" Width="350px" runat="server" Skin="Bootstrap"></telerik:RadComboBox>
                                </td>

                            </tr>
                            <tr>
                                <td>

                                    <asp:Label runat="server" Text="Tipo de medición" Font-Bold="true" CssClass="Labels"></asp:Label>
                                    <br />
                                    <telerik:RadComboBox ID="CmtipoMedicion" EmptyMessage="Seleccionar alcance" Width="350px" runat="server" Skin="Bootstrap"></telerik:RadComboBox>


                                </td>

                                <td>
                                    <asp:Label runat="server" Text="Descripción de la fórmula matemática" Font-Bold="true" CssClass="Labels"></asp:Label>
                                    <br />
                                    <telerik:RadTextBox runat="server" ID="TxtFormula" Width="350px" Height="80px" TextMode="MultiLine" Skin="Bootstrap"></telerik:RadTextBox>
                                <td />


                            </tr>


                            <tr>
                                <td>
                                    <asp:Label runat="server" Text="Meta" Font-Bold="true" CssClass="Labels"></asp:Label>
                                    <br />
                                    <telerik:RadTextBox runat="server" ID="TxtMeta" Width="350px" Skin="Bootstrap"></telerik:RadTextBox>
                                </td>
                                <td>

                                    <asp:Label runat="server" Text="Tendencia del resultado" Font-Bold="true" CssClass="Labels"></asp:Label>
                                    <telerik:RadComboBox ID="CmbTendenciaI" EmptyMessage="Seleccionar tendencia" Width="350px" runat="server" Skin="Bootstrap"></telerik:RadComboBox>


                                </td>

                            </tr>


                            <tr>
                                <td>
                                    <asp:Label runat="server" Text="Limite" Font-Bold="true" CssClass="Labels"></asp:Label>
                                    <br />
                                    <telerik:RadComboBox runat="server" Width="250px" ID="CmbLimite" EmptyMessage="Seleccionar Limite" Skin="Bootstrap"></telerik:RadComboBox>
                                </td>

                            </tr>
                        </table>

                    </div>
                    <div style="align-content: center; text-align: center">
                        <telerik:RadButton runat="server" Text="Guardar" ID="btn_guardar" OnClick="btn_guardar_Click" Width="250px" Skin="BlackMetroTouch"></telerik:RadButton>
                    </div>
                    <br />
                    <div style="align-content: center; text-align: center">
                        <div class="col-sm-12" style="width: 500px" runat="server" id="DivError" visible="false">
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <span class="badge badge-pill badge-danger">Error</span>
                                <asp:Label runat="server" ID="LblValidar" Visible="false"></asp:Label>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div id="DResultado" runat="server" visible="false">
                            <div class="alert-success" role="alert" style="align-content: center; text-align: center; margin-top: 605px">
                                <span class="badge badge-pill badge-info">¡Guardado!</span>
                                <br />
                                <asp:Label runat="server" ID="LblGuardado"></asp:Label>
                                <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span aria-hidden="true">×</span></button>
                            </div>
                        </div>

                    </div>
                </telerik:RadWizardStep>
                <telerik:RadWizardStep ID="RadWizardStep4" Title="PASO2.LIMITES" Font-Names="Arial" ImageUrl="~/images/barras.png" Font-Bold="true" runat="server">
                    <div class="ContendDivRegistro" style="width: 850px; height: auto">
                        <h2 class="Contendh3">LIMITES</h2>
                        <table style="width: 100%">
                            <tr>
                                <td>
                                    <asp:Label runat="server" Text="Bandera Roja" ForeColor="Red" CssClass="Labels" Font-Bold="true"></asp:Label>
                                    <br />
                                    <telerik:RadTextBox runat="server" ID="TxtRoja" Skin="Bootstrap"></telerik:RadTextBox>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <asp:Label runat="server" Text="Peor" ForeColor="#ffcc00" CssClass="Labels" Font-Bold="true"></asp:Label>
                                    <br />
                                    <telerik:RadTextBox runat="server" ID="TxtPeor" Skin="Bootstrap"></telerik:RadTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" Text="Alcance" ForeColor="Green" CssClass="Labels" Font-Bold="true"></asp:Label>
                                    <br />
                                    <telerik:RadTextBox runat="server" ID="TxtAlcance" Skin="Bootstrap"></telerik:RadTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" Text="Mejor" ForeColor="#0066ff" CssClass="Labels" Font-Bold="true"></asp:Label>
                                    <br />
                                    <telerik:RadTextBox runat="server" ID="TxtMejor" Skin="Bootstrap"></telerik:RadTextBox>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <telerik:RadButton runat="server" ID="btnG" Text="Actualizar" Width="250px" Skin="BlackMetroTouch" OnClick="btnG_Click"></telerik:RadButton>
                    </div>
                </telerik:RadWizardStep>
                <telerik:RadWizardStep ID="RadWizardStep2" Title="PASO3. CREAR VARIABLES" Font-Names="Arial" ImageUrl="~/images/formula.png" Font-Bold="true" runat="server">
                    <div class="ContendDivRegistro" style="width: 850px; height: auto">
                        <h2 class="Contendh3">CREAR VARIABLES</h2>
                        <div style="text-align: center; align-content: center">
                            <table style="width: 100%">
                                <tr>
                                    <td colspan="3">
                                        <br />
                                        <asp:Label runat="server" Text="Nombre de variable" CssClass="Labels" Font-Bold="true"></asp:Label>
                                        <br />
                                        <telerik:RadTextBox runat="server" ID="TxtNombreVariable" EmptyMessage="Seleccionar unidad de medida" Width="350px" Skin="Bootstrap"></telerik:RadTextBox>

                                    </td>

                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server" Text="Fuente de origen" CssClass="Labels" Font-Bold="true"></asp:Label>
                                        <br />
                                        <telerik:RadComboBox ID="CmbFuente" EmptyMessage="Seleccionar fuente" Width="250px" runat="server" Skin="Bootstrap"></telerik:RadComboBox>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" Text="Tipo de dato" CssClass="Labels" Font-Bold="true"></asp:Label>
                                        <br />
                                        <telerik:RadComboBox ID="CmbTipoDato" EmptyMessage="Seleccionar Tipo" Width="250px" runat="server" Skin="Bootstrap"></telerik:RadComboBox>
                                    </td>

                                    <td>
                                        <asp:Label runat="server" Text="Observacion" CssClass="Labels" Font-Bold="true"></asp:Label>
                                        <br />
                                        <telerik:RadTextBox runat="server" ID="txtObservacion" Skin="Bootstrap" Width="250px"></telerik:RadTextBox>
                                    </td>
                                </tr>
                            </table>






                        </div>
                    </div>
                    <div style="text-align: center">
                        <telerik:RadButton ID="btn_agregar" runat="server" Text="Agregar" Skin="BlackMetroTouch" OnClick="btn_agregar_Click"></telerik:RadButton>
                        <br />
                        <telerik:RadGrid runat="server" ID="GrvVariables" Visible="false" Font-Size="10pt" PageSize="1" OnDeleteCommand="GrvVariables_DeleteCommand" OnNeedDataSource="GrvVariables_NeedDataSource" AllowPaging="True" AllowSorting="True" Skin="Vista">
                            <MasterTableView AutoGenerateColumns="False" Width="100%" TableLayout="Fixed" DataKeyNames="ID_VARIABLE">
                                <Columns>

                                    <telerik:GridButtonColumn CommandName="Delete" Text="Delete" UniqueName="DeleteColumn" HeaderStyle-Width="50px" Visible="false" />

                                    <telerik:GridBoundColumn DataField="ID_VARIABLE" HeaderText="ID_VARIABLE">
                                        <HeaderStyle ForeColor="#003399" Width="150px" Font-Bold="true" />

                                        <ItemStyle Font-Size="10pt" ForeColor="#000000" Height="50px" Width="100px" HorizontalAlign="Justify" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridNumericColumn DataField="VALOR" HeaderText="VALOR">
                                        <HeaderStyle ForeColor="#003399" Width="150px" Font-Bold="true" />
                                        <ItemStyle Font-Size="10pt" ForeColor="#000000" Height="50px" Width="100px" HorizontalAlign="Justify" />
                                    </telerik:GridNumericColumn>


                                    <telerik:GridBoundColumn DataField="FUENTE" HeaderText="FUENTE">
                                        <HeaderStyle ForeColor="#003399" Width="150px" Font-Bold="true" />

                                        <ItemStyle Font-Size="10pt" ForeColor="#000000" Height="50px" Width="100px" HorizontalAlign="Justify" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="DES_TIPOD" HeaderText="TIPO DATO">
                                        <HeaderStyle ForeColor="#003399" Width="150px" Font-Bold="true" />

                                        <ItemStyle Font-Size="10pt" ForeColor="#000000" Height="50px" Width="100px" HorizontalAlign="Justify" />
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn DataField="FILA" HeaderText="ALIAS">
                                        <HeaderStyle ForeColor="#003399" Width="150px" Font-Bold="true" />

                                        <ItemStyle Font-Size="10pt" ForeColor="#000000" Height="50px" Width="100px" HorizontalAlign="Justify" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="OBSERVACION" HeaderText="OBSERVACIÓN">
                                        <HeaderStyle ForeColor="#003399" Width="150px" Font-Bold="true" />

                                        <ItemStyle Font-Size="10pt" ForeColor="#000000" Height="50px" Width="100px" HorizontalAlign="Justify" />
                                    </telerik:GridBoundColumn>
                                </Columns>
                            </MasterTableView>
                        </telerik:RadGrid>
                    </div>
                    <br />

                    <div class="ContendDivRegistro" style="width: 700px; height: auto">
                        <h2 class="Contendh3">CONSTRUCCIÓN DE FORMULA</h2>
                        <div style="text-align: center; align-content: center">

                            <table style="width: 100%">
                                <tr>
                                    <td colspan="2">
                                        <br />
                                        <asp:Label runat="server" Text="Escribir fórmula" CssClass="Labels" Font-Bold="true"></asp:Label>
                                        <br />

                                    </td>

                                </tr>
                                <tr>
                                    <td>
                                        <telerik:RadTextBox runat="server" ID="TxtVariables" EmptyMessage="Seleccionar variables" Width="400px" Skin="Bootstrap" TextMode="MultiLine"></telerik:RadTextBox>

                                        <br />
                                        <telerik:RadButton ID="btn_agregar2" runat="server" Text="Validar sintaxis" Skin="BlackMetroTouch" OnClick="btn_agregar2_Click"></telerik:RadButton>
                                    </td>
                                    <td></td>
                                </tr>

                            </table>
                          
                        <br />
                              <div id="DivResultado" runat="server" visible="false">
                                <div class="alert-success" role="alert" style="width: 400px; align-content: center; text-align: center">
                                    <span class="badge badge-pill badge-info">¡Validación Sintaxis!</span>
                                    <br />
                                    <asp:Label runat="server" ForeColor="#009900" ID="LblValidarSin"></asp:Label>
                                </div>
                            </div>
                        </div>

                        <br />
                        <div id="DivAdvertencia" runat="server" visible="true">
                            <div class="alert-success" role="alert" style="width: 650px; align-content: center; text-align: center">
                                <span class="badge badge-pill  badge-info">¡Advertencia!</span>
                                <br />

                                <asp:Label runat="server" ForeColor="#009900" ID="LblValidarf" Text="La formula se construye con la siguiente sintaxis, <br/> utilizando la letra 'V' numerando  las variables que tenga la formula <br/>Ejemplo (V1+V2/V3) " Font-Bold="true"></asp:Label>
                                <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span aria-hidden="true">×</span></button>
                            </div>
                        </div>
                    </div>







                </telerik:RadWizardStep>
                <telerik:RadWizardStep ID="RadWizardStep3" Title="PASO4.CREAR RESPONSABLES" Enabled="false" Font-Names="Arial" runat="server" ImageUrl="~/images/team.png">
                    <div class="ContendDivRegistro" style="width: 700px; height: auto">
                        <h2 class="Contendh3">ASIGNAR USUARIOS</h2>
                        <table style="width: 50%">

                            <tr>
                                <td>
                                    <asp:Label Text="USUARIO ASIGNADO" CssClass="Labels" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <telerik:RadComboBox runat="server" ID="CmbUsuario" Filter="Contains" EmptyMessage="Seleccionar usuario" Width="250px" Skin="Metro"></telerik:RadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label Text="ROL ASIGNADO" CssClass="Labels" Width="350px" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <telerik:RadComboBox runat="server" ID="CmbRol" Skin="Metro" EmptyMessage="Seleccionar rol" Width="250px"></telerik:RadComboBox>
                                </td>
                            </tr>

                        </table>
                        <br />
                        <div style="align-content; text-align: center">
                            <telerik:RadButton ID="btnagregar" runat="server" Text="Agregar" Skin="BlackMetroTouch" OnClick="btnagregar_Click"></telerik:RadButton>
                            <br />
                            <br />

                            <telerik:RadGrid runat="server" ID="Gvrolesu" Visible="false" MasterTableView-DataKeyNames="ID_ROLESIN" OnDeleteCommand="Gvrolesu_DeleteCommand" AutoGenerateColumns="false" Skin="Vista">
                                <MasterTableView>
                                    <Columns>

                                        <telerik:GridButtonColumn CommandName="Delete" Text="Eliminar" HeaderText="INACTIVAR" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="Black" UniqueName="DeleteColumn" ButtonType="ImageButton" ImageUrl="images/delete.png" ItemStyle-Width="10px" />
                                        <telerik:GridBoundColumn DataField="ID_ROLES_IN" Visible="false">
                                            <HeaderStyle Font-Bold="true" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="NOMBRE" HeaderText="NOMBRE USUARIO">
                                            <HeaderStyle Font-Bold="true" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="NOMBRE_ROL" HeaderText="ROL">
                                            <HeaderStyle Font-Bold="true" />
                                        </telerik:GridBoundColumn>

                                    </Columns>
                                </MasterTableView>
                            </telerik:RadGrid>

                            <asp:Label ID="lblMensaje" CssClass="Labels" Width="350px" runat="server"></asp:Label>
                        </div>
                    </div>


                </telerik:RadWizardStep>
            </WizardSteps>
        </telerik:RadWizard>


    </telerik:RadAjaxPanel>


</asp:Content>
