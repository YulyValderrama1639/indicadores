﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Users.aspx.cs" Inherits="INDICADORES_CALIDAD.Users" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <telerik:RadAjaxPanel runat="server">
        <telerik:RadWindowManager runat="server" ID="RwAyudas" Modal="True" Font-Names="Agency FB" Behaviors="Close,Move"
            RenderMode="Lightweight" Height="600px" Skin="Bootstrap">
            <Windows>
                <telerik:RadWindow ID="rwBusqueda" IconUrl="~/images/icons/favicon.ico" Behaviors="Default" Font-Names="Agency FB" RenderMode="Mobile" Title="INDICADORES-CALIDAD" Font-Bold="true" Skin="Bootstrap" Modal="true" CenterIfModal="true" VisibleStatusbar="false"
                    runat="server">
                </telerik:RadWindow>
                <telerik:RadWindow ID="rwGuardado" IconUrl="~/Imagenes/IconosFormularios/faviconC.png" Behaviors="Close" RenderMode="Mobile" Title="INDICADORES-CALIDAD" Skin="Bootstrap" Modal="true" CenterIfModal="true" VisibleStatusbar="false"
                    runat="server">
                </telerik:RadWindow>
                <telerik:RadWindow ID="rw_customConfirm" IconUrl="~/Imagenes/IconosFormularios/faviconC.png" Width="300px" Height="200px" Behaviors="Close,Move" RenderMode="Lightweight" Title="Registro-Congreso" Skin="MetroTouch" Modal="true" CenterIfModal="true" VisibleStatusbar="false"
                    runat="server">
                    <ContentTemplate>
                        <div style="align-content: center; text-align: center">
                            <asp:Label runat="server" ID="LblVerificar" Font-Size="12pt" Text="la asignación fue guardada con éxito!!!" Font-Names="Agency FB"></asp:Label>
                            <br />
                            <br />
                            <br />
                            <telerik:RadButton runat="server" ID="BtnOkV" Skin="MetroTouch" Text="Ok" AutoPostBack="true">
                            </telerik:RadButton>

                        </div>

                    </ContentTemplate>
                </telerik:RadWindow>

            </Windows>
        </telerik:RadWindowManager>

        <br />
        <div class="col-sm-12" runat="server" id="DivAlert">
            <div class="DivGeneral DivColor">
                  <asp:Image runat="server" ImageUrl="~/images/play.png" />
                <asp:Label Text="USUARIOS" Font-Bold="true" Font-Size="20pt" runat="server" Font-Names="Arial Narrow"></asp:Label>
                

            </div>
        </div>
        <div class="col-sm-12">
            <telerik:RadTabStrip runat="server" ID="RadUsuarios" Font-Names="Agency FB" MultiPageID="RadMultiPage1" SelectedIndex="0" Skin="Glow" PerTabScrolling="True">
                <Tabs>
                    <telerik:RadTab Text="Usuarios" Font-Bold="true" Width="150px" runat="server" Selected="True"></telerik:RadTab>
                    <telerik:RadTab Text="Asignación  Menu" Font-Bold="true" Width="150px" runat="server" Selected="True"></telerik:RadTab>
                    <telerik:RadTab Text="Asignación Indicadores" Font-Bold="true" Width="150px" runat="server" Selected="True"></telerik:RadTab>

                </Tabs>
            </telerik:RadTabStrip>
            <telerik:RadMultiPage ID="RadMultiPage1" SelectedIndex="0" runat="server" CssClass="outerMultiPage">

                <telerik:RadPageView ID="Pagina1" runat="server">
                    <telerik:RadGrid RenderMode="Lightweight" OnItemCreated="gvUsuarios_ItemCreated" MasterTableView-DataKeyNames="IDENTIFICACION" AutoGenerateColumns="False" EnableLinqExpressions="False" ID="gvUsuarios" OnNeedDataSource="gvUsuarios_NeedDataSource"
                        AllowSorting="True" Width="100%"
                        ShowFooter="True" AllowPaging="True" runat="server" AllowMultiRowEdit="True" Culture="es-ES" FilterType="Combined" Skin="Windows7">
                        <GroupingSettings CaseSensitive="false"></GroupingSettings>
                        <ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true">
                        </ClientSettings>
                        <MasterTableView AllowFilteringByColumn="true">
                            <Columns>
                                <telerik:GridTemplateColumn HeaderText="DETALLE" AllowFiltering="false">

                                    <ItemTemplate>
                                        <asp:ImageButton ID="btn_detalle" runat="server" ImageUrl="~/images/DetalleUsu.png" />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn AllowFiltering="true" DataField="IDENTIFICACION" Visible="false" FilterControlWidth="150px" AutoPostBackOnFilter="false" CurrentFilterFunction="EqualTo"
                                    FilterDelay="2000" ShowFilterIcon="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn AllowFiltering="true" DataField="USR_NAME" HeaderText="Usuario" FilterControlWidth="150px" AutoPostBackOnFilter="false" CurrentFilterFunction="EqualTo"
                                    FilterDelay="2000" ShowFilterIcon="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn AllowFiltering="true" DataField="NOMBRE_RESPONSABLE" HeaderText="Nombre Usuario" FilterControlWidth="200px" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                    FilterDelay="2000" ShowFilterIcon="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn AllowFiltering="true" DataField="ROL" HeaderText="Rol" FilterControlWidth="200px" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                    FilterDelay="2000" ShowFilterIcon="false">
                                </telerik:GridBoundColumn>

                            </Columns>
                        </MasterTableView>
                        <FooterStyle CssClass="footerStyle" />

                        <HeaderStyle CssClass="HeaderStyleGrid" />
                        <FilterMenu RenderMode="Lightweight">
                        </FilterMenu>
                        <HeaderContextMenu RenderMode="Lightweight">
                        </HeaderContextMenu>

                    </telerik:RadGrid>

                </telerik:RadPageView>

                <telerik:RadPageView ID="Pagina2" runat="server">


                    <div class="ContendDivRegistro" style="width: 1079px; height: auto">
                        <h2 class="Contendh3">ASIGNACIÓN DE MENU</h2>
                        <br />



                        <div class="ContendDivRegistro" style="width: 500px; height: auto">
                            <asp:Label Text="Nombre usuario" CssClass="Labels" runat="server"></asp:Label>
                            <telerik:RadComboBox runat="server" ID="CmbUsuario" EmptyMessage="Seleccionar Menu" Filter="Contains" Width="250px" Skin="Metro"></telerik:RadComboBox>
                            <br />
                            <asp:Label Text="Rol de usuario"  CssClass="Labels" runat="server"></asp:Label>
                            <telerik:RadComboBox runat="server" ID="CmbRol" EmptyMessage="Seleccionar rol" Filter="Contains" Width="250px" Skin="Metro"></telerik:RadComboBox>
                            <telerik:RadTreeView RenderMode="Lightweight"  ID="RadtreePer" TriStateCheckBoxes="true" CheckChildNodes="true"  runat="server" CheckBoxes="true" Width="300px" Height="350px" on>
                                <DataBindings>
                                    <telerik:RadTreeNodeBinding Expanded="false"></telerik:RadTreeNodeBinding>
                                </DataBindings>
                            </telerik:RadTreeView>
                            <div class="col-sm-12" runat="server" id="Div1" visible="false">
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <span class="badge badge-pill badge-danger">Error</span> Debe seleccionar el usuario y el rol para asignar!
                   
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            </div>
                        </div>
                        <div class="col-sm-12" runat="server" id="Div2" visible="false">
                            <div class="alert-success" role="alert">
                                <span class="badge-success">Operación exitosa</span> ¡Se realizo asignación correctamente!
                   
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            </div>
                        </div>
                              <telerik:RadButton runat="server" ID="btn_asigMenu" Text="Asignar" Skin="BlackMetroTouch" OnClick="btn_asigMenu_Click"></telerik:RadButton>
                              
                        </div>



                      
                      
                    </div>








                </telerik:RadPageView>


                <telerik:RadPageView ID="Pagina3" runat="server">
                    <div class="ContendDivRegistro" style="width: 1079px; height: auto">
                        <h2 class="Contendh3">ASIGNACIÓN INDICADORES</h2>
                        <br />
                        <table style="width: 100%">

                            <tr>
                                <td align="center">
                                    <asp:Label Text="Tipo Indicador" CssClass="Labels" runat="server"></asp:Label>
                                    <telerik:RadComboBox runat="server" ID="CmbTipoIndicador" EmptyMessage="Seleccionar tipo indicador" Filter="Contains" Width="250px" Skin="Metro"></telerik:RadComboBox>
                                    <asp:Label Text="Proceso" CssClass="Labels" runat="server"></asp:Label>
                                    <telerik:RadComboBox runat="server" ID="Cmbproceso" EmptyMessage="Seleccionar el proceso " Filter="Contains" Width="250px" Skin="Metro"></telerik:RadComboBox>


                                </td>


                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Label Text="Usuario" CssClass="Labels" runat="server"></asp:Label>
                                    <telerik:RadComboBox runat="server" ID="CmbUsuarioP" EmptyMessage="Seleccionar Usuario" Filter="Contains" Width="250px" Skin="Metro"></telerik:RadComboBox>

                                    <asp:Label Text="Rol" CssClass="Labels" runat="server"></asp:Label>
                                    <telerik:RadComboBox runat="server" ID="CmbRolP" EmptyMessage="Seleccionar Usuario" Filter="Contains" Width="250px" Skin="Metro"></telerik:RadComboBox>

                                </td>
                            </tr>




                        </table>
                        <br>
                        <div style="text-align: center">
                            <telerik:RadButton ID="btn_consultar" runat="server" Font-Size="12pt" Height="30px" RenderMode="Lightweight" Skin="MetroTouch" Text="Consultar" Width="200px" OnClick="btn_consultar_Click"></telerik:RadButton>
                            <telerik:RadButton ID="btn_asignar" runat="server" Font-Size="12pt" Height="30px" RenderMode="Lightweight" Skin="MetroTouch" Text="Asignar" Width="200px" OnClick="btn_asignar_Click"></telerik:RadButton>
                            <telerik:RadButton ID="btn_limpiar" runat="server" Font-Size="12pt" Height="30px" RenderMode="Lightweight" Skin="MetroTouch" Text="Limpiar" Width="200px" OnClick="btn_limpiar_Click"></telerik:RadButton>
                        </div>
                        <br />
                        <div id="DivError" runat="server" style="" class="col-sm-12" visible="false">
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <span class="badge badge-pill badge-danger">Error</span> ¡Debe seleccionar la vigencia o el periodo!
                                <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span aria-hidden="true">×</span></button>
                            </div>
                        </div>

                        <telerik:RadGrid runat="server" ID="GVINDICADOR" Visible="false" PageSize="20" AllowPaging="true" Width="100%" OnPreRender="GVINDICADOR_PreRender" OnDataBound="GVINDICADOR_DataBound" OnNeedDataSource="GVINDICADOR_NeedDataSource" OnPageIndexChanged="GVINDICADOR_PageIndexChanged" AutoGenerateColumns="false" Skin="Windows7">
                            <MasterTableView AllowFilteringByColumn="true">
                                <EditFormSettings>
                                    <EditColumn UniqueName="EditCommandColumn" ButtonType="ImageButton" CancelText="Cancelar" UpdateText="Actualizar" CancelImageUrl="images/cancel.png"
                                        UpdateImageUrl="images/Actualizar.png" InsertImageUrl="~/RadControls/Grid/Skins/Default/Insert.gif" ItemStyle-Width="50px">
                                    </EditColumn>
                                </EditFormSettings>
                                <Columns>

                                    <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="Seleccionar" UniqueName="Seleccion">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="ChckTodos" AutoPostBack="true" runat="server" OnCheckedChanged="ChckTodos_CheckedChanged" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox runat="server" ID="ChckSeleecionar" />
                                        </ItemTemplate>
                                        <HeaderStyle Width="15px" />
                                        <ItemStyle Width="50px" />
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="ID_INDICADOR" AutoPostBackOnFilter="false" CurrentFilterFunction="EqualTo"
                                        FilterDelay="2000" ShowFilterIcon="false" HeaderText="ID_INDICADOR" ReadOnly="true">
                                        <HeaderStyle Font-Bold="true" ForeColor="#000000" Font-Names="Arial Narrow" />
                                        <ItemStyle Font-Names="Arial Narrow" Font-Size="10pt" />
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn AllowFiltering="true" DataField="NOMBRE_INDICADOR" HeaderText="INDICADOR" FilterControlWidth="200px" AutoPostBackOnFilter="false" CurrentFilterFunction="EqualTo"
                                        FilterDelay="2000" ShowFilterIcon="false">
                                    </telerik:GridBoundColumn>




                                </Columns>

                            </MasterTableView>

                            <HeaderStyle CssClass="HeaderStyleGrid" />
                            <FooterStyle CssClass="footerStyle" />

                        </telerik:RadGrid>
                    </div>

                </telerik:RadPageView>

            </telerik:RadMultiPage>



        </div>
    </telerik:RadAjaxPanel>
    <telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
        <script type="text/javascript">
            var identificacion;
            function ShowEditForm(id, rowIndex) {
                var grid = $find("<%= gvUsuarios.ClientID %>");
                var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element();
                identificacion = rowControl;

                grid.get_masterTableView().selectItem(rowControl, true);
                window.radopen("DetalleUsuario.aspx?IDENTIFICACION=" + id, "rwBusqueda", "950px", "610px");
                return false;
            }
        </script>


    </telerik:RadCodeBlock>



</asp:Content>
