﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdministradorResultados.aspx.cs" Inherits="INDICADORES_CALIDAD.AdministradorResultados" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
        <telerik:RadAjaxPanel runat="server">

    <div class="col-sm-12">
        <telerik:RadTabStrip runat="server" ID="RadUsuarios" Font-Names="Agency FB" MultiPageID="RadMultiPage1" SelectedIndex="0" Skin="MetroTouch" PerTabScrolling="True">
            <Tabs>
                <telerik:RadTab Text="ADMINISTRADOR DE RESULTADOS" ImageUrl="~/images/subir.png" Font-Bold="true" Width="250px" runat="server" Selected="True"></telerik:RadTab>

            </Tabs>
        </telerik:RadTabStrip>
    </div>


    <telerik:RadMultiPage ID="RadMultiPage1" SelectedIndex="0" runat="server">

        <telerik:RadPageView ID="Pagina1" runat="server">
            <div class="ContendDivRegistro" style="width: 1079px; height: auto">
                <h2 class="Contendh3">RESULTADOS INDICADORES</h2>

                <div style="align-content:center;text-align:center" height: auto">
                    <table style="width: 100%">
                        <tr>
                            <td colspan="2" style="align-content:center">
                                <asp:Label runat="server" Text="Tipo Indicador" CssClass="Labels" Font-Bold="true"></asp:Label>
                                <br />
                                   <telerik:RadComboBox ID="CmbTipoIndicador" AutoPostBack="true" EmptyMessage="Seleccionar indicador"  Filter="Contains" Width="350px" runat="server" Skin="Bootstrap"></telerik:RadComboBox>
                            </td>
                        </tr>
                      
                        <tr>
                             <td colspan="2">

                                <asp:Label runat="server" Text="Proceso" CssClass="Labels" Font-Bold="true"></asp:Label>
                                <br />
                                <telerik:RadComboBox ID="CmbProceso" AutoPostBack="true" EmptyMessage="Seleccionar proceso" OnSelectedIndexChanged="CmbProceso_SelectedIndexChanged" Filter="Contains" Width="350px" runat="server" Skin="Bootstrap"></telerik:RadComboBox>
                                <br />

                            </td>
                        </tr>
                  
                        <tr>
                             <td colspan="2">

                                <asp:Label runat="server" Text="Indicador" CssClass="Labels" Font-Bold="true"></asp:Label>
                                <br />
                                <telerik:RadComboBox ID="CmbIndicadores" AutoPostBack="true" EmptyMessage="Seleccionar Indicador" OnSelectedIndexChanged="CmbIndicadores_SelectedIndexChanged" Filter="Contains" Width="550px" runat="server" Skin="Bootstrap"></telerik:RadComboBox>
                                <br />

                            </td>
                        </tr>
                        
                                   <tr>
                            <td>


                                <asp:Label Text="Medida" Font-Bold="true" Font-Size="12pt" runat="server" Font-Names="Arial Narrow"></asp:Label>
                                <br />
                                <telerik:RadComboBox runat="server" ID="CmbMedida" OnSelectedIndexChanged="CmbMedida_SelectedIndexChanged"  AutoPostBack="true" Width="250px" EmptyMessage="Medida" Skin="Bootstrap">
                                </telerik:RadComboBox>


                            </td>
                            <td>
                                <asp:Label Text="Periodo" Font-Bold="true" Font-Size="12pt" runat="server" Font-Names="Arial Narrow"></asp:Label>
                                <br />
                                <telerik:RadComboBox runat="server" Filter="StartsWith" ID="CmbPeriodo" Skin="Bootstrap" Width="250px" EmptyMessage="Seleccionar Periodo">
                                </telerik:RadComboBox>
                            </td>
                        </tr>
                     

                        <tr>
                           
                            <td >
                                <asp:Label runat="server" Text="Regional"  ID="lblregional" CssClass="Labels" Font-Bold="true"></asp:Label>
                                <br />
                                <telerik:RadComboBox ID="CmbRegional" Filter="Contains"  EmptyMessage="Seleccionar seccional" Width="450px" runat="server" Skin="Bootstrap"></telerik:RadComboBox>
                                
                             
                            </td>
                            <td>
                                     <asp:Label runat="server" Text="Seccional" ID="LblSeccional" CssClass="Labels" Font-Bold="true"></asp:Label>
                                <br />
                                
                                <telerik:RadComboBox ID="CmbSeccional" Filter="Contains"  EmptyMessage="Seleccionar seccional" Width="450px" runat="server" Skin="Bootstrap"></telerik:RadComboBox>

                            </td>

                        </tr>
                      



                    </table>
                    <br />

                    <telerik:RadButton runat="server" ID="btn_consultar" Text="Consultar" ForeColor="#ffffff" BackColor="#000000" Skin="BlackMetroTouch" OnClick="btn_consultar_Click"></telerik:RadButton>

                    <telerik:RadButton runat="server" ID="btn_guardar" Text="Guardar" ForeColor="#ffffff" BackColor="#006600"  OnClick="btn_guardar_Click" Skin="BlackMetroTouch" >
                        
                         </telerik:RadButton>
                    <telerik:RadButton runat="server" ID="btn_limpiar" Text="Limpiar" Skin="BlackMetroTouch" OnClick="btn_limpiar_Click"></telerik:RadButton>
                    <br />
                    <asp:Label runat="server" ID="Lblresultado" Visible="false"></asp:Label>
                    <br />
                    <br />


                </div>
                    <div id="Dvbusqueda1" runat="server" class="col-sm-12" visible="false">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert" style="width: 400px">
                            <span class="badge badge-pill badge-danger">¡Advertencia!</span>
                            <br />
                            <asp:Label runat="server" ID="LblError"></asp:Label>
                            <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span aria-hidden="true">×</span></button>
                        </div>
                    </div>
            </div>
            <br />

            <telerik:RadGrid runat="server" ID="GrvCargardatos" Visible="false" Font-Size="10pt" MasterTableView-DataKeyNames="RESULTADO"  OnNeedDataSource="GrvCargardatos_NeedDataSource"  PageSize="20"  AllowAutomaticUpdates="True" OnItemDataBound="GrvCargardatos_ItemDataBound"  AllowPaging="True" AllowSorting="True" Skin="MetroTouch">
                        <AlternatingItemStyle BackColor="#F0F0F0" />
                       <MasterTableView CommandItemDisplay="None" DataKeyNames="RESULTADO,META"
                        AutoGenerateColumns="False">
                        

                         
                            <Columns>
                                      <telerik:GridBoundColumn DataField="AÑO" HeaderText="AÑO" ReadOnly="true">
                                    <HeaderStyle  Width="50px"   Font-Bold="true" />
                                    <ItemStyle Font-Size="12pt" Font-Names="Arial Narrow"  Height="50px" Width="20px" HorizontalAlign="Justify" />
                                </telerik:GridBoundColumn>

                                        <telerik:GridBoundColumn DataField="NMES" HeaderText="MES" ReadOnly="true">
                                    <HeaderStyle  Width="100px"   Font-Bold="true" />
                                    <ItemStyle Font-Size="12pt" Font-Names="Arial Narrow"  Height="50px" Width="20px" HorizontalAlign="Justify" />
                                </telerik:GridBoundColumn>


                                
                                    <telerik:GridTemplateColumn HeaderStyle-Width="80px" DataField="META" HeaderText="META" >
                                    <ItemTemplate>
                                        <asp:TextBox ID="META" runat="server" Width="50px" Font-Names="Arial Narrow" ></asp:TextBox>
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="True"  Width="50px" />
                                </telerik:GridTemplateColumn>
                                                
                        
                              
                                    <telerik:GridTemplateColumn HeaderStyle-Width="80px"  DataField="RESULTADO" HeaderText="RESULTADO" >
                                    <ItemTemplate>
                                        <asp:TextBox ID="RESULTADO" runat="server" Font-Names="Arial Narrow" ></asp:TextBox>
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="True"  Width="80px" />
                                </telerik:GridTemplateColumn>

                                
                                  


                            </Columns>
                        </MasterTableView>
                        <GroupingSettings CollapseAllTooltip="Collapse all groups" />
                        <ClientSettings AllowKeyboardNavigation="true"></ClientSettings>
                        <HeaderStyle BackColor="#25A0DA" Font-Names="Arial Narrow" ForeColor="White" />
                        <ItemStyle BackColor="White" />
                    </telerik:RadGrid>
        </telerik:RadPageView>
    </telerik:RadMultiPage>

            </telerik:RadAjaxPanel>
</asp:Content>
