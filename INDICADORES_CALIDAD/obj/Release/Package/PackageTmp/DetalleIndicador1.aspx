﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DetalleIndicador1.aspx.cs" Inherits="INDICADORES_CALIDAD.DetalleIndicador1" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server"></asp:ScriptManager>
        <div>
            <telerik:RadWizard RenderMode="Lightweight" ID="RadCrearindicador"  DisplayNavigationButtons="false" Localization-Previous="Anterior" Localization-Next="Siguiente" Localization-Finish="Finalizar" runat="server" Height="500px" Skin="Bootstrap" Font-Names="Arial Narrow">
                <WizardSteps>
                    <telerik:RadWizardStep ID="RadWizardStep1" Title="DATOS DEL INDICADOR"   Font-Names="Arial" ImageUrl="images/add-documents.png" Font-Bold="true" runat="server" StepType="Start">

                        <div class="col-sm-12" runat="server" id="Div1">
                            <table style="width: 90%">
                                <tr>

                                    <td>
                                        <asp:Label runat="server" Text="Nombre indicador" CssClass="Labels" Font-Bold="true"></asp:Label>
                                        <br>
                                        <telerik:RadTextBox runat="server" ID="TxtNombre" EmptyMessage="Nombre de indicador" Width="350px" Skin="Bootstrap"></telerik:RadTextBox>
                                    </td>

                                    <td>
                                        <asp:Label runat="server" Text="Empresa" CssClass="Labels" Font-Bold="true"></asp:Label>
                                        <br />
                                        <telerik:RadComboBox ID="CmbEmpresa" Width="350px" runat="server" EmptyMessage="Seleccionar empresa" Skin="Bootstrap" AutoPostBack="true"></telerik:RadComboBox>

                                    </td>


                                </tr>
                                <tr>

                                    <td>
                                        <asp:Label runat="server" Text="Clasificación del indicador" CssClass="Labels" Font-Bold="true"></asp:Label>
                                        <br />
                                        <telerik:RadComboBox ID="CmbClasificacion" Width="350px" runat="server" EmptyMessage="Seleccionar clasificación" Skin="Bootstrap"></telerik:RadComboBox>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" Text="Tipo de indicador" Font-Bold="true" CssClass="Labels"></asp:Label>
                                        <br />
                                        <telerik:RadComboBox ID="CmbTipoIndicador" Width="350px" runat="server" AutoPostBack="true" EmptyMessage="Seleccionar Tipo de indicador" Skin="Bootstrap"></telerik:RadComboBox>

                                    </td>
                                </tr>
                                <tr>

                                    <td>
                                        <asp:Label runat="server" Text="Fecha creación" CssClass="Labels" Font-Bold="true"></asp:Label>
                                        <br />
                                        <telerik:RadDatePicker runat="server" ID="radFechainic" Skin="Bootstrap" Calendar-CultureInfo="es-CO"></telerik:RadDatePicker>
                                    </td>

                                </tr>





                                <tr>
                                    <td>
                                        <asp:Label runat="server" ID="lblperspectiva" Text="Perspectiva" Visible="false" Font-Bold="true" CssClass="Labels"></asp:Label>
                                        <br />
                                        <telerik:RadComboBox ID="Cmbperspectiva" Visible="false" Width="350px" runat="server" AutoPostBack="true" EmptyMessage="Seleccionar perspectiva" Skin="Bootstrap"></telerik:RadComboBox>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="LblInductor" Text="Inductor" Visible="false" Font-Bold="true" CssClass="Labels"></asp:Label>
                                        <br />
                                        <telerik:RadComboBox ID="cmbinductor" Visible="false" Width="350px" runat="server" EmptyMessage="Seleccione el inductor" Skin="Bootstrap"></telerik:RadComboBox>
                                    </td>

                                </tr>

                                <tr>

                                    <td>
                                        <asp:Label runat="server" Text="Proceso" ID="LblProceso" Font-Bold="true" CssClass="Labels"></asp:Label>
                                        <br />
                                        <telerik:RadComboBox ID="CmbProceso" Width="350px" EmptyMessage="Seleccionar proceso" runat="server" Skin="Bootstrap"></telerik:RadComboBox>
                                    </td>


                                </tr>
                                <tr>

                                    <td>
                                        <asp:Label runat="server" Text="Periodicidad de medición" CssClass="Labels" Font-Bold="true"></asp:Label>
                                        <telerik:RadComboBox ID="Cmbperiodicidad" EmptyMessage="Seleccionar periodicidad" Width="350px" runat="server" Skin="Bootstrap"></telerik:RadComboBox>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" Text="Unidad Medida" Font-Bold="true" CssClass="Labels"> </asp:Label>
                                        <br />
                                        <telerik:RadComboBox ID="CmbUnidad" Width="350px" runat="server" Skin="Bootstrap" EmptyMessage="Seleccionar unidad de medida"></telerik:RadComboBox>
                                    </td>

                                </tr>

                                <tr>

                                    <td>
                                        <asp:Label runat="server" Text="Periodicidad de análisis" Font-Bold="true" CssClass="Labels"></asp:Label>
                                        <br />
                                        <telerik:RadComboBox ID="Cmbperiodicidada" Width="350px" EmptyMessage="Seleccionar periodicidad de análisis" runat="server" Skin="Bootstrap"></telerik:RadComboBox>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" Text="Alcance de medición" Font-Bold="true" CssClass="Labels"></asp:Label>
                                        <telerik:RadComboBox ID="CmbAlcance" EmptyMessage="Seleccionar alcance" Width="350px" runat="server" Skin="Bootstrap"></telerik:RadComboBox>
                                    </td>

                                </tr>
                                <tr>
                                    <td>

                                        <asp:Label runat="server" Text="Tipo de medición" Font-Bold="true" CssClass="Labels"></asp:Label>
                                        <br />
                                        <telerik:RadComboBox ID="CmtipoMedicion" EmptyMessage="Seleccionar alcance" Width="350px" runat="server" Skin="Bootstrap"></telerik:RadComboBox>


                                    </td>

                                    <td>
                                        <asp:Label runat="server" Text="Descripción de la fórmula matemática" Font-Bold="true" CssClass="Labels"></asp:Label>
                                        <br />
                                        <telerik:RadTextBox runat="server" ID="TxtFormula" Width="350px" Height="80px" TextMode="MultiLine" Skin="Bootstrap"></telerik:RadTextBox>
                                        <td />
                                </tr>


                                <tr>
                                    <td>
                                        <asp:Label runat="server" Text="Meta" Font-Bold="true" CssClass="Labels"></asp:Label>
                                        <br />
                                        <telerik:RadTextBox runat="server" ID="TxtMeta" Width="350px" Skin="Bootstrap"></telerik:RadTextBox>
                                    </td>
                                    <td>

                                        <asp:Label runat="server" Text="Tendencia del resultado" Font-Bold="true" CssClass="Labels"></asp:Label>
                                        <telerik:RadComboBox ID="CmbTendenciaI" EmptyMessage="Seleccionar tendencia" Width="350px" runat="server" Skin="Bootstrap"></telerik:RadComboBox>


                                    </td>

                                </tr>



                            </table>


                        
                        </div>
                    </telerik:RadWizardStep>

                </WizardSteps>
            </telerik:RadWizard>
        </div>
    </form>
</body>
</html>
