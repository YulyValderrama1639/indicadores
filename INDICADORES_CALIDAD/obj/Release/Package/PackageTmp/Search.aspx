﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Search.aspx.cs" Inherits="INDICADORES_CALIDAD.Search" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>




<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
        <script type="text/javascript">
            var identificacion;
            function ShowEditForm4(id, rowIndex) {
                var grid = $find("<%= GrV_regional.ClientID %>");
                var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element();
                identificacion = rowControl;
                grid.get_masterTableView().selectItem(rowControl, true);
                window.radopen("DetalleIndicador1.aspx?ID_INDICADOR=" + id, "rwBusqueda", "950px", "610px");
                return false;

            }
            var identificacion;
            function ShowResultado4(id, rowIndex) {
                var grid = $find("<%= GrV_regional.ClientID %>");
                var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element();
                identificacion = rowControl;
                grid.get_masterTableView().selectItem(rowControl, true);
                window.radopen("Resultado.aspx?ID_INDICADOR=" + id, "rwBusqueda", "950px", "610px");
                return false;
            }

            var identificacion;
            function ShowCumplmiento4(id, rowIndex) {
                var grid = $find("<%= GrV_regional.ClientID %>");
                var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element();
                identificacion = rowControl;
                grid.get_masterTableView().selectItem(rowControl, true);
                window.radopen("Cumplimiento.aspx?ID_INDICADOR=" + id, "rwBusqueda", "950px", "610px");
                return false;
            }

            var identificacion;
            function ShowResultado3(id, rowIndex) {
                var grid = $find("<%= GvIndicadoresEs.ClientID %>");
                var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element();
                identificacion = rowControl;
                grid.get_masterTableView().selectItem(rowControl, true);
                window.radopen("Resultado.aspx?ID_INDICADOR=" + id, "rwBusqueda", "950px", "610px");
                return false;
            }


        </script>

    </telerik:RadCodeBlock>
    <telerik:RadAjaxPanel runat="server" PostBackControls="ImageButton1">
        <telerik:RadWindowManager runat="server" ID="RwAyudas" Modal="True" Font-Names="Agency FB" Behaviors="Close,Move"
            RenderMode="Lightweight" Height="600px">
            <Windows>
                <telerik:RadWindow ID="rwBusqueda" IconUrl="~/images/icons/favicon.ico" Behaviors="Default" Font-Names="Agency FB" RenderMode="Mobile" Title="INDICADORES-CALIDAD" Font-Bold="true" Skin="Bootstrap" Modal="true" CenterIfModal="true" VisibleStatusbar="false"
                    runat="server">
                </telerik:RadWindow>
                <telerik:RadWindow ID="rwGuardado" IconUrl="~/Imagenes/IconosFormularios/faviconC.png" Behaviors="Close" RenderMode="Mobile" Title="Registro-Congreso" Skin="Bootstrap" Modal="true" CenterIfModal="true" VisibleStatusbar="false"
                    runat="server">
                </telerik:RadWindow>

            </Windows>
        </telerik:RadWindowManager>

        <link href="Content/StyleCustom.css" rel="stylesheet" />

        <div class="col-sm-12" runat="server" id="DivAlert">
            <div class="DivGeneral DivColor">
                <asp:Image runat="server" ImageUrl="~/images/play.png" />
                <asp:Label Text="CONSULTA INDICADORES" Font-Bold="true" Font-Size="20pt" runat="server" Font-Names="Arial Narrow"></asp:Label>

            </div>
        </div>

        <div class="col-sm-12" runat="server" id="Div1">
            <telerik:RadTabStrip runat="server" ID="RadTabStrip1" Font-Names="Agency FB" MultiPageID="RadMultiPage1" Skin="Glow">
                <Tabs>

                    <telerik:RadTab Text="1.BUSQUEDA" Font-Bold="true" Width="150px" runat="server" Selected="True"></telerik:RadTab>
                    <telerik:RadTab Text="2.REGIONAL" Font-Bold="true" Width="150px" runat="server"></telerik:RadTab>
                    <telerik:RadTab Text="3.SECCIONAL" Font-Bold="true" Width="150px" runat="server"></telerik:RadTab>
                    <telerik:RadTab Text="4.PROCESOS" Font-Bold="true" Width="150px" runat="server" Visible="false"></telerik:RadTab>
                    <telerik:RadTab Text="5.TIPO" Font-Bold="true" Width="150px" runat="server" Visible="false"></telerik:RadTab>



                </Tabs>
            </telerik:RadTabStrip>

            <telerik:RadMultiPage ID="RadMultiPage1" SelectedIndex="0" runat="server" CssClass="outerMultiPage">
                <%--BUSQUEDA TEXTO--%>
                <telerik:RadPageView ID="Pagina1" runat="server">
                    <br />
                    <div class="ContendDivRegistro" style="width: 1050px; height: auto">
                        <h2 class="Contendh3">BUSCAR POR INDICADOR</h2>
                        <div style="text-align: center; align-content: center">
                            <table>
                                <tr>
                                    <td>
                                        <telerik:RadButton RenderMode="Lightweight" ID="btnPalabraC" ForeColor="#0092AB" AutoPostBack="true" runat="server" Text="Palabra Clave" ToggleType="CheckBox"
                                            ButtonType="ToggleButton" OnCheckedChanged="btnPalabraC_CheckedChanged">
                                        </telerik:RadButton>
                                    </td>
                                    <td>
                                        <telerik:RadButton RenderMode="Lightweight" ID="btnProceso" AutoPostBack="true" ForeColor="#0092AB" runat="server" Text="Proceso" ToggleType="CheckBox"
                                            ButtonType="ToggleButton" OnCheckedChanged="btnProceso_CheckedChanged">
                                        </telerik:RadButton>
                                    </td>
                                    <td>
                                        <telerik:RadButton RenderMode="Lightweight" ID="btnTipo" AutoPostBack="true" ForeColor="#0092AB" runat="server" Text="Tipo de indicador" ToggleType="CheckBox"
                                            ButtonType="ToggleButton" OnCheckedChanged="btnTipo_CheckedChanged">
                                        </telerik:RadButton>
                                    </td>
                                </tr>
                            </table>
                            <h2 class="Contendh3"></h2>

                            <table style="width: 100%">

                                <tr>
                                    <td align="center">

                                        <asp:Label runat="server" Font-Names=" Arial, Helvetica, sans-serif;" Font-Bold="true" Font-Size="12pt" Text="Seleccionar  frecuencia">                                                                     </asp:Label>
                                        <br />
                                        <telerik:RadComboBox ID="CmbMedida" runat="server" AutoPostBack="true" EmptyMessage="Seleccione la frecuencia" Skin="Bootstrap" OnSelectedIndexChanged="CmbMedida_SelectedIndexChanged" Width="250px"></telerik:RadComboBox>
                                    </td>
                                    <td align="center">

                                        <asp:Label runat="server" Font-Names=" Arial, Helvetica, sans-serif;" Font-Size="12pt" Font-Bold="true" Text="Seleccionar el periodo">                             
                                        </asp:Label>
                                        <br />
                                        <telerik:RadComboBox ID="Cmbperiodo" Filter="StartsWith" Font-Bold="true" runat="server" EmptyMessage="Seleccione el mes" Skin="Bootstrap" Width="250px"></telerik:RadComboBox>
                                    </td>

                                </tr>
                                <tr>
                                    <td colspan="2">

                                        <div runat="server" id="Panel1">
                                            <asp:Label runat="server" Font-Names=" Arial, Helvetica, sans-serif;" Font-Bold="true" Text="Palabra clave del indicador" Font-Size="12pt" ID="LblTitulo">                                
                                            </asp:Label>
                                            <br />
                                            <telerik:RadTextBox runat="server" ID="TxtBuscar1" Skin="Bootstrap" Width="300px" OnTextChanged="TxtBuscar_TextChanged1"></telerik:RadTextBox>

                                        </div>
                                        <div runat="server" id="Panel2" visible="false">
                                            <asp:Label runat="server" Font-Names=" Arial, Helvetica, sans-serif;" Font-Bold="true" Text="Proceso" Font-Size="12pt" ID="lblproceso" Visible="true">  </asp:Label>
                                            <br />
                                            <telerik:RadComboBox ID="CmbProceso" runat="server" AutoPostBack="true" EmptyMessage="Seleccione el proceso" Skin="Bootstrap" Width="300px"  Visible="true"></telerik:RadComboBox>
                                            <br />
                                            <asp:Label runat="server" Font-Names=" Arial, Helvetica, sans-serif;" Font-Bold="true" Text="Tipo de indicador" Font-Size="12pt"  Visible="true" ID="lbltipoindicador"> 
                                            </asp:Label>
                                            <br />
                                            <telerik:RadComboBox ID="CmbTipo" runat="server" AutoPostBack="true" EmptyMessage="Seleccione el Tipo" Skin="Bootstrap" Width="300px"  Visible="true"></telerik:RadComboBox>
                                        </div>

                                        <div runat="server" id="Panel3" visible="false">
                                            <asp:Label runat="server" Font-Names=" Arial, Helvetica, sans-serif;" Font-Bold="true" Text="Tipo de indicador" Font-Size="12pt" ID="lblproceso3"> 
                                            </asp:Label>
                                            <br />
                                            <telerik:RadComboBox ID="Cmbtipo2" runat="server" AutoPostBack="true" EmptyMessage="Seleccione el Tipo" Skin="Bootstrap" Width="300px"></telerik:RadComboBox>

                                            <br />
                                            <asp:Label runat="server" Font-Names=" Arial, Helvetica, sans-serif;" Font-Bold="true" Text="Proceso" Font-Size="12pt" ID="lblproceso2"> 
                                            </asp:Label>
                                            <br />
                                            <telerik:RadComboBox ID="CmbProceso2" runat="server" AutoPostBack="true" EmptyMessage="Seleccione el proceso" Skin="Bootstrap" Width="300px"></telerik:RadComboBox>


                                        </div>

                                    </td>
                                </tr>
                            </table>

                            <br />
                            <br />

                            <div style="align-content: center; text-align: center">

                                <telerik:RadButton ID="rbConsultar" runat="server" CssClass="TextosSitio" Font-Size="12pt" Height="30px" OnClick="rbConsultar_Click" RenderMode="Lightweight" Skin="Material" Text="Consultar" Width="200px" BackColor="#3276B1" ForeColor="White"></telerik:RadButton>

                                <telerik:RadButton ID="btnlimpiar" runat="server" CssClass="TextosSitio" Font-Size="12pt" Height="30px" OnClick="btnlimpiar_Click" RenderMode="Lightweight" Skin="Material" Text="Limpiar" Width="200px" BackColor="#3276B1" ForeColor="White"></telerik:RadButton>
                            </div>

                        </div>
                    </div>
                    <br />
                    <telerik:RadGrid ID="GvIndicadores" AllowFilteringByColumn="false" runat="server" AllowPaging="true" AutoGenerateColumns="false" MasterTableView-DataKeyNames="ID_INDICADOR" OnItemCreated="GvIndicadores_ItemCreated" OnItemDataBound="GvIndicadores_ItemDataBound" OnNeedDataSource="GvIndicadores_NeedDataSource" PageSize="10" Skin="Windows7" Visible="false" Width="100%">
                        <GroupingSettings CollapseAllTooltip="Collapse all groups" />
                        <ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true">
                        </ClientSettings>
                        <PagerStyle Mode="NextPrevAndNumeric" BackColor="#CCCCCC"></PagerStyle>
                        <AlternatingItemStyle BackColor="White" ForeColor="Black" />
                        <MasterTableView DataKeyNames="PEOR,ROJA,RESULTADO1,RESULTADO,ALCANCE,MEJOR,ID_INDICADOR">
                            <PagerStyle Mode="NextPrevNumericAndAdvanced"></PagerStyle>
                            <RowIndicatorColumn Visible="False">
                            </RowIndicatorColumn>
                            <ExpandCollapseColumn Created="True">
                            </ExpandCollapseColumn>
                            <Columns>
                                <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="DETALLE">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btn_detalle" runat="server" ImageUrl="images/Detalle.png" />
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="true" Wrap="true" Width="50px" BackColor="#00AD93" ForeColor="White" Font-Names="Arial Narrow" Font-Size="9pt" />
                                    <ItemStyle Font-Size="9pt" Wrap="true" Width="50px" HorizontalAlign="Center" Font-Names="Arial Narrow" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="NOMBRE_PROCESO" HeaderText="PROCESO" AllowFiltering="false">
                                    <HeaderStyle Font-Bold="true" Wrap="true" Width="50px" BackColor="#00AD93" ForeColor="White" Font-Size="9pt" Font-Names="Arial Narrow" />
                                    <ItemStyle Font-Size="9pt" Wrap="true" Width="50px" HorizontalAlign="Center" Font-Names="Arial Narrow" />

                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn AllowFiltering="true" DataField="NOMBRE_INDICADOR" HeaderText="NOMBRE INDICADOR">
                                    <HeaderStyle Font-Bold="true" Wrap="true" BackColor="#00AD93" Width="250px" ForeColor="White" HorizontalAlign="Center" Font-Names="Arial Narrow" Font-Size="9pt" />
                                    <ItemStyle Font-Size="9pt" Wrap="true" Width="250px" Font-Names="Arial Narrow" />
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="VARIABLES">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btn_Variables" runat="server" ImageUrl="~/images/Variable.png" />
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="true" Wrap="true" BackColor="#00AD93" Font-Names="Arial Narrow" ForeColor="White" Width="50px" Font-Size="9pt" />
                                    <ItemStyle Font-Size="9pt" Wrap="true" Width="50px" Font-Bold="true" HorizontalAlign="Center" Font-Names="Arial Narrow" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="NOMBRE_TIPOINDICADOR" HeaderText="TIPO INDICADOR" AllowFiltering="false">
                                    <HeaderStyle Font-Bold="true" Wrap="true" Width="50px" Font-Names="Arial Narrow" BackColor="#00AD93" ForeColor="White" Font-Size="9pt" />
                                    <ItemStyle Font-Size="9pt" Wrap="true" Width="50px" Font-Names="Arial Narrow" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FRECUENCIA" HeaderText="FRECUENCIA" AllowFiltering="false">
                                    <HeaderStyle Font-Bold="true" Wrap="true" BackColor="#00AD93" Font-Names="Arial Narrow" Width="50px" ForeColor="White" Font-Size="9pt" />
                                    <ItemStyle Font-Size="9pt" Wrap="true" Width="50px" Font-Names="Arial Narrow" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="META" HeaderText="META" AllowFiltering="false">

                                    <HeaderStyle Font-Bold="true" Wrap="true" Width="50px" BackColor="#00AD93" Font-Names="Arial Narrow" ForeColor="White" Font-Size="9pt" />
                                    <ItemStyle Font-Size="9pt" Font-Bold="true" HorizontalAlign="Center" ForeColor="Blue" Wrap="true" Width="50px" Font-Names="Arial Narrow" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="RESULTADO1" HeaderText="RESULTADO" AllowFiltering="false">
                                    <HeaderStyle Font-Bold="true" Wrap="true" BackColor="#00AD93" Width="150px" Font-Names="Arial Narrow" ForeColor="White" Font-Size="9pt" />
                                    <ItemStyle Font-Size="9pt" Font-Bold="true" Wrap="true" Width="50px" HorizontalAlign="Center" Font-Names="Arial Narrow" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="CUMPLIMIENTO1" HeaderText="CUMPLIMIENTO" AllowFiltering="false">
                                    <HeaderStyle Font-Bold="true" Wrap="true" BackColor="#00AD93" Width="150px" Font-Names="Arial Narrow" ForeColor="White" Font-Size="9pt" />
                                    <ItemStyle Font-Size="9pt" Font-Bold="true" Wrap="true" Width="50px" HorizontalAlign="Center" Font-Names="Arial Narrow" />
                                </telerik:GridBoundColumn>


                                <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="REGIONAL">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btn_resultado" runat="server" ImageUrl="~/images/resultado.png" />
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="true" Wrap="true" BackColor="#00AD93" Font-Names="Arial Narrow" Width="50px" ForeColor="White" Font-Size="9pt" />
                                    <ItemStyle Font-Size="9pt" Wrap="true" Width="50px" HorizontalAlign="Center" Font-Names="Arial Narrow" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="SECCIONAL">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btn_Cumplimiento" runat="server" ImageUrl="~/images/Cumplimiento.png" />
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="true" Wrap="true" BackColor="#00AD93" Font-Names="Arial Narrow" Width="50px" ForeColor="White" Font-Size="9pt" />
                                    <ItemStyle Font-Size="9pt" Wrap="true" Width="50px" HorizontalAlign="Center" Font-Names="Arial Narrow" />
                                </telerik:GridTemplateColumn>

                                <telerik:GridBoundColumn DataField="RESULTADO" HeaderText="RESULTADO" AllowFiltering="false" Visible="false">
                                    <HeaderStyle Font-Bold="true" Wrap="true" BackColor="#00AD93" Width="150px" Font-Names="Arial Narrow" ForeColor="White" Font-Size="9pt" />
                                    <ItemStyle Font-Size="9pt" Font-Bold="true" Wrap="true" Width="50px" HorizontalAlign="Center" Font-Names="Arial Narrow" />
                                </telerik:GridBoundColumn>

                                <telerik:GridBoundColumn DataField="PEOR" HeaderText="PEOR" AllowFiltering="false" Visible="false">
                                    <HeaderStyle Font-Bold="true" Wrap="true" BackColor="#00AD93" Width="150px" Font-Names="Arial Narrow" ForeColor="White" Font-Size="9pt" />
                                    <ItemStyle Font-Size="9pt" Font-Bold="true" Wrap="true" Width="50px" HorizontalAlign="Center" Font-Names="Arial Narrow" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ROJA" HeaderText="ROJA" AllowFiltering="false" Visible="false">
                                    <HeaderStyle Font-Bold="true" Wrap="true" BackColor="#00AD93" Width="150px" Font-Names="Arial Narrow" ForeColor="White" Font-Size="9pt" />
                                    <ItemStyle Font-Size="9pt" Font-Bold="true" Wrap="true" Width="50px" HorizontalAlign="Center" Font-Names="Arial Narrow" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ALCANCE" HeaderText="ALCANCE" AllowFiltering="false" Visible="false">
                                    <HeaderStyle Font-Bold="true" Wrap="true" BackColor="#00AD93" Width="150px" Font-Names="Arial Narrow" ForeColor="White" Font-Size="9pt" />
                                    <ItemStyle Font-Size="9pt" Font-Bold="true" Wrap="true" Width="50px" HorizontalAlign="Center" Font-Names="Arial Narrow" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="MEJOR" HeaderText="MEJOR" AllowFiltering="false" Visible="false">
                                    <HeaderStyle Font-Bold="true" Wrap="true" BackColor="#00AD93" Width="150px" Font-Names="Arial Narrow" ForeColor="White" Font-Size="9pt" />
                                    <ItemStyle Font-Size="9pt" Font-Bold="true" Wrap="true" Width="50px" HorizontalAlign="Center" Font-Names="Arial Narrow" />
                                </telerik:GridBoundColumn>


                                <telerik:GridBoundColumn DataField="SEMAFORO" HeaderText="SEMAFORO" AllowFiltering="false">
                                    <HeaderStyle Font-Bold="true" Wrap="true" BackColor="#00AD93" Font-Names="Arial Narrow" ForeColor="White" Width="50px" Font-Size="9pt" />
                                    <ItemStyle Font-Size="9pt" Font-Bold="true" Wrap="true" HorizontalAlign="Center" Font-Names="Arial Narrow" Width="50px" />
                                </telerik:GridBoundColumn>

                                <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="ANALISIS CUALITATIVO">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btn_Analisis" runat="server" ImageUrl="~/images/loupe.png" />
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="true" Wrap="true" BackColor="#00AD93" Font-Names="Arial Narrow" ForeColor="White" Width="50px" Font-Size="9pt" />
                                    <ItemStyle HorizontalAlign="Center" Font-Names="Arial Narrow" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="GRAFICO">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btn_grafico" runat="server" ImageUrl="images/grafico.png" />
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="true" Wrap="true" BackColor="#00AD93" Font-Names="Arial Narrow" ForeColor="White" Width="50px" Font-Size="9pt" />
                                    <ItemStyle HorizontalAlign="Center" Font-Names="Arial Narrow" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="DES_ALCANCE" HeaderText="ALCANCE" AllowFiltering="false">
                                    <HeaderStyle Font-Bold="true" Wrap="true" BackColor="#00AD93" Font-Names="Arial Narrow" ForeColor="White" Width="50px" Font-Size="9pt" />
                                    <ItemStyle Font-Size="9pt" Font-Bold="true" Wrap="true" HorizontalAlign="Center" Font-Names="Arial Narrow" Width="50px" />
                                </telerik:GridBoundColumn>

                            </Columns>

                        </MasterTableView>
                        <HeaderStyle CssClass="HeaderStyleGrid" />
                        <FooterStyle CssClass="footerStyle" BackColor="#CCCCCC" />
                        <ItemStyle BackColor="#EAEAEA" ForeColor="Black" />
                    </telerik:RadGrid>
                    <br />
                    <div id="Dvbusqueda1" runat="server" class="col-sm-12" visible="false">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert" style="width: 400px">
                            <span class="badge badge-pill badge-danger">¡Advertencia!</span>
                            <br />
                            <asp:Label runat="server" ID="LblError"></asp:Label>
                            <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span aria-hidden="true">×</span></button>
                        </div>
                    </div>

                </telerik:RadPageView>
                <%--BUSQUEDA REGIONAL--%>
                <telerik:RadPageView ID="Pagina4" runat="server">
                    <br />
                    <div class="ContendDivRegistro" style="width: 1050px; height: auto">
                        <h2 class="Contendh3">BUSCAR POR REGIONAL</h2>
                        <div style="text-align: center; align-content: center">
                            <div style="text-align: center; align-content: center">

                                <table style="width: 100%">
                                    <tr>

                                        <td colspan="3">
                                            <asp:Label runat="server" Font-Names=" Arial, Helvetica, sans-serif;" Font-Size="12pt" Text="Seleccionar el Indicador">                             
                                            </asp:Label>
                                            <br />
                                            <telerik:RadComboBox ID="CmbIndicadoresR" Filter="StartsWith" runat="server" EmptyMessage="Seleccione el indicador" Skin="Bootstrap" Width="450px"></telerik:RadComboBox>
                                        </td>

                                    </tr>

                                    <tr>
                                        <td align="center">

                                            <asp:Label runat="server" Font-Names=" Arial, Helvetica, sans-serif;" Font-Size="12pt" Text="Seleccionar  frecuencia">                                                                     </asp:Label>
                                            <br />
                                            <telerik:RadComboBox ID="CmbMedida5" runat="server" AutoPostBack="true" EmptyMessage="Seleccione la medida" Skin="Bootstrap" OnSelectedIndexChanged="CmbMedida5_SelectedIndexChanged" Width="250px"></telerik:RadComboBox>
                                        </td>
                                        <td align="center">

                                            <asp:Label runat="server" Font-Names=" Arial, Helvetica, sans-serif;" Font-Size="12pt" Text="Seleccionar el periodo">                             
                                            </asp:Label>
                                            <br />
                                            <telerik:RadComboBox ID="CmbPeriodo5" Filter="StartsWith" runat="server" EmptyMessage="Seleccione el mes" Skin="Bootstrap" Width="250px"></telerik:RadComboBox>
                                        </td>

                                        <td>
                                            <asp:Label runat="server" Font-Names="Helvetica" Font-Size="12pt" Text="Seleccionar la regional  "></asp:Label>
                                            <br />
                                            <telerik:RadComboBox ID="Cmbregional" runat="server" EmptyMessage="Seleccione la regional" Skin="Bootstrap" Width="250px"></telerik:RadComboBox>

                                        </td>


                                    </tr>


                                </table>
                                <br />
                                <telerik:RadButton ID="btn_consultar4" runat="server" Font-Size="12pt" Skin="Material" BackColor="#3276B1" ForeColor="White" Height="30px" OnClick="btn_consultar4_Click" RenderMode="Lightweight" Text="Consultar" Width="200px"></telerik:RadButton>
                                <telerik:RadButton ID="btn_limpiar4" runat="server" Font-Size="12pt" Skin="Material" BackColor="#3276B1" ForeColor="White" Height="30px" RenderMode="Lightweight" Text="Limpiar" Width="200px" OnClick="btn_limpiar4_Click"></telerik:RadButton>
                            </div>
                        </div>


                    </div>
                    <br />
                    <telerik:RadGrid ID="GrV_regional" runat="server" AllowPaging="true" AllowFilteringByColumn="false" AutoGenerateColumns="false" MasterTableView-DataKeyNames="ID_INDICADOR" OnItemDataBound="GrV_regional_ItemDataBound" OnItemCreated="GrV_regional_ItemCreated" OnNeedDataSource="GrV_regional_NeedDataSource" PageSize="10" Skin="Windows7" Visible="false" Width="100%">

                        <MasterTableView DataKeyNames="RESULTADO,PEOR,ROJA,ALCANCE,MEJOR,ID_INDICADOR " AllowFilteringByColumn="false" Width="100%">
                            <PagerStyle Mode="NextPrevNumericAndAdvanced" />
                            <Columns>
                                <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="DETALLE">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btn_detalle" runat="server" ImageUrl="images/Detalle.png" />
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="true" Font-Size="9pt" Width="50px" Wrap="true" />
                                    <ItemStyle Font-Size="9pt" HorizontalAlign="Center" Width="50px" Wrap="true" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="NOMBRE_INDICADOR" AllowFiltering="false" HeaderStyle-Font-Bold="true" HeaderText="NOMBRE INDICADOR">
                                    <HeaderStyle Font-Bold="True" />
                                    <ItemStyle Width="50px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="VARIABLES">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btn_Variables" runat="server" ImageUrl="~/images/Variable.png" />
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="true" Font-Size="9pt" Width="50px" Wrap="true" />
                                    <ItemStyle Font-Bold="true" Font-Size="9pt" HorizontalAlign="Center" Width="50px" Wrap="true" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn AllowFiltering="false" DataField="FRECUENCIA" HeaderText="FRECUENCIA">
                                    <HeaderStyle Font-Bold="true" Font-Size="9pt" Width="50px" Wrap="true" />
                                    <ItemStyle Font-Size="9pt" Width="50px" Wrap="true" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn AllowFiltering="false" DataField="META" HeaderText="META">
                                    <HeaderStyle Font-Bold="true" Font-Size="9pt" Width="50px" Wrap="true" />
                                    <ItemStyle Font-Bold="true" Font-Size="9pt" ForeColor="Blue" HorizontalAlign="Center" Width="50px" Wrap="true" />
                                </telerik:GridBoundColumn>

                                <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="SECCIONAL">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btn_Cumplimiento" runat="server" ImageUrl="~/images/Cumplimiento.png" />
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="true" Font-Size="9pt" Width="50px" Wrap="true" />
                                    <ItemStyle Font-Size="9pt" HorizontalAlign="Center" Width="50px" Wrap="true" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn AllowFiltering="false" DataField="RESULTADO1" HeaderText="RESULTADO">
                                    <HeaderStyle Font-Bold="true" Font-Size="9pt" Width="50px" Wrap="true" />
                                    <ItemStyle Font-Bold="true" Font-Size="9pt" HorizontalAlign="Center" Width="50px" Wrap="true" />
                                </telerik:GridBoundColumn>

                                <telerik:GridBoundColumn AllowFiltering="false" DataField="RESULTADO" HeaderText="RESULTADO" Visible="false">
                                    <HeaderStyle Font-Bold="true" Font-Size="9pt" Width="50px" Wrap="true" />
                                    <ItemStyle Font-Bold="true" Font-Size="9pt" HorizontalAlign="Center" Width="50px" Wrap="true" />
                                </telerik:GridBoundColumn>

                                <telerik:GridBoundColumn AllowFiltering="false" DataField="CUMPLIMIENTO1" HeaderText="CUMPLIMIENTO">
                                    <HeaderStyle Font-Bold="true" Font-Size="9pt" Width="50px" Wrap="true" />
                                    <ItemStyle Font-Bold="true" Font-Size="9pt" HorizontalAlign="Center" Width="50px" Wrap="true" />
                                </telerik:GridBoundColumn>

                                <telerik:GridBoundColumn AllowFiltering="false" DataField="SEMAFORO" HeaderText="SEMAFORO">
                                    <HeaderStyle Font-Bold="true" Font-Size="9pt" Width="50px" Wrap="true" />
                                    <ItemStyle Font-Bold="true" Font-Size="9pt" HorizontalAlign="Center" Width="50px" Wrap="true" />
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="ANALISIS CUALITATIVO">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btn_Analisis" runat="server" ImageUrl="~/images/loupe.png" />
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="true" Font-Size="9pt" Width="50px" Wrap="true" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="GRAFICO">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btn_grafico" runat="server" ImageUrl="images/grafico.png" />
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="true" Font-Size="9pt" Width="50px" Wrap="true" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="PEOR" HeaderText="PEOR" AllowFiltering="false" Visible="false">
                                    <HeaderStyle Font-Bold="true" Wrap="true" BackColor="#00AD93" Width="150px" Font-Names="Arial Narrow" ForeColor="White" Font-Size="9pt" />
                                    <ItemStyle Font-Size="9pt" Font-Bold="true" Wrap="true" Width="50px" HorizontalAlign="Center" Font-Names="Arial Narrow" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ROJA" HeaderText="ROJA" AllowFiltering="false" Visible="false">
                                    <HeaderStyle Font-Bold="true" Wrap="true" BackColor="#00AD93" Width="150px" Font-Names="Arial Narrow" ForeColor="White" Font-Size="9pt" />
                                    <ItemStyle Font-Size="9pt" Font-Bold="true" Wrap="true" Width="50px" HorizontalAlign="Center" Font-Names="Arial Narrow" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ALCANCE" HeaderText="ALCANCE" AllowFiltering="false" Visible="false">
                                    <HeaderStyle Font-Bold="true" Wrap="true" BackColor="#00AD93" Width="150px" Font-Names="Arial Narrow" ForeColor="White" Font-Size="9pt" />
                                    <ItemStyle Font-Size="9pt" Font-Bold="true" Wrap="true" Width="50px" HorizontalAlign="Center" Font-Names="Arial Narrow" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="MEJOR" HeaderText="MEJOR" AllowFiltering="false" Visible="false">
                                    <HeaderStyle Font-Bold="true" Wrap="true" BackColor="#00AD93" Width="150px" Font-Names="Arial Narrow" ForeColor="White" Font-Size="9pt" />
                                    <ItemStyle Font-Size="9pt" Font-Bold="true" Wrap="true" Width="50px" HorizontalAlign="Center" Font-Names="Arial Narrow" />
                                </telerik:GridBoundColumn>



                            </Columns>
                        </MasterTableView>
                        <HeaderStyle CssClass="HeaderStyleGrid" BackColor="#00AD93" Font-Names="Arial Narrow" ForeColor="White" />
                        <FooterStyle CssClass="footerStyle" BackColor="#CCCCCC" />
                        <ItemStyle BackColor="#EAEAEA" ForeColor="Black" Font-Names="Arial Narrow" />
                    </telerik:RadGrid>

                    <br />
                    <div id="DivBusqueda4" runat="server" class="col-sm-12" visible="false">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <span class="badge badge-pill badge-danger">Error</span> ¡Debe seleccionar la regional para buscar!

                                <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span aria-hidden="true">×</span></button>
                        </div>
                    </div>

                </telerik:RadPageView>
                <%--BUSQUEDA SECCIONAL--%>
                <telerik:RadPageView ID="Pagina5" runat="server">
                    <br />
                    <div class="ContendDivRegistro" style="width: 1050px; height: auto">
                        <h2 class="Contendh3">BUSCAR POR SECCIONAL</h2>
                        <div style="text-align: center; align-content: center">
                            <div style="text-align: center; align-content: center">
                                <table style="width: 100%">
                                    <tr>

                                        <td colspan="3">
                                            <asp:Label runat="server" Font-Names=" Arial, Helvetica, sans-serif;" Font-Size="12pt" Text="Seleccionar el Indicador">                             
                                            </asp:Label>
                                            <br />
                                            <telerik:RadComboBox ID="CmbIndicadoresS" Filter="StartsWith" runat="server" EmptyMessage="Seleccione el indicador" Skin="Bootstrap" Width="450px"></telerik:RadComboBox>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td align="center">

                                            <asp:Label runat="server" Font-Names=" Arial, Helvetica, sans-serif;" Font-Size="12pt" Text="Seleccionar frecuencia">                                                                     </asp:Label>
                                            <br />
                                            <telerik:RadComboBox ID="CmMedida7" runat="server" AutoPostBack="true" EmptyMessage="Seleccione frecuencia" Skin="Bootstrap" OnSelectedIndexChanged="Cmperiodoseccional_SelectedIndexChanged" Width="250px"></telerik:RadComboBox>
                                        </td>
                                        <td align="center">

                                            <asp:Label runat="server" Font-Names=" Arial, Helvetica, sans-serif;" Font-Size="12pt" Text="Seleccionar el periodo">                             
                                            </asp:Label>
                                            <br />
                                            <telerik:RadComboBox ID="Cmperiodo7" Filter="StartsWith" runat="server" EmptyMessage="Seleccione el periodo" Skin="Bootstrap" Width="250px"></telerik:RadComboBox>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan="2">

                                            <asp:Label runat="server" Font-Names="Helvetica" Font-Size="12pt" Text="Debe seleccionar la seccional "></asp:Label>
                                            <br />
                                            <telerik:RadComboBox ID="cmbseccional" Filter="StartsWith" runat="server" EmptyMessage="Seleccione seccional" Skin="Bootstrap" Width="250px"></telerik:RadComboBox>

                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <telerik:RadButton ID="btnconsultar6" runat="server" Font-Size="12pt" Height="30px" OnClick="btnconsultar6_Click" RenderMode="Lightweight" Skin="Material" Text="Consultar" Width="200px" BackColor="#3276B1" ForeColor="White"></telerik:RadButton>
                                <telerik:RadButton ID="btnlimpiars" runat="server" Font-Size="12pt" Height="30px" OnClick="btnlimpiars_Click1" RenderMode="Lightweight" Skin="Material" Text="Limpiar" Width="200px" BackColor="#3276B1" ForeColor="White"></telerik:RadButton>
                            </div>
                        </div>
                        <br />

                        <br />
                        <div id="Divbusqueda5" runat="server" class="col-sm-12" visible="false">

                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <span class="badge badge-pill badge-danger">Error</span> ¡Debe seleccionar la seccional para buscar!
                                <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span aria-hidden="true">×</span></button>
                            </div>
                        </div>
                    </div>
                    <br />
                    <telerik:RadGrid ID="GvSeccional" runat="server" AllowPaging="true" AutoGenerateColumns="false" MasterTableView-DataKeyNames="ID_INDICADOR" OnItemDataBound="GvSeccional_ItemDataBound" OnItemCreated="GvSeccional_ItemCreated" OnNeedDataSource="GvSeccional_NeedDataSource" PageSize="10" Skin="Windows7" Visible="false" Width="100%">

                        <MasterTableView DataKeyNames="RESULTADO,PEOR,ROJA,ALCANCE,MEJOR,ID_INDICADOR" AllowFilteringByColumn="false" Width="100%">
                            <PagerStyle Mode="NextPrevNumericAndAdvanced" />
                            <Columns>
                                <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="DETALLE">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btn_detalle" runat="server" ImageUrl="images/Detalle.png" />
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="true" Font-Size="9pt" Width="50px" Wrap="true" />
                                    <ItemStyle Font-Size="9pt" HorizontalAlign="Center" Width="50px" Wrap="true" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="NOMBRE_INDICADOR" HeaderText="NOMBRE INDICADOR">
                                    <HeaderStyle Font-Bold="true" Font-Size="9pt" Width="50px" Wrap="true" />
                                    <ItemStyle Font-Size="9pt" HorizontalAlign="Center" Width="50px" Wrap="true" />
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="VARIABLES">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btn_Variables" runat="server" ImageUrl="~/images/Variable.png" />
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="true" Font-Size="9pt" Width="50px" Wrap="true" />
                                    <ItemStyle Font-Bold="true" Font-Size="9pt" HorizontalAlign="Center" Width="50px" Wrap="true" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn AllowFiltering="false" DataField="FRECUENCIA" HeaderText="FRECUENCIA">
                                    <HeaderStyle Font-Bold="true" Font-Size="9pt" Width="50px" Wrap="true" />
                                    <ItemStyle Font-Size="9pt" Width="50px" Wrap="true" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn AllowFiltering="false" DataField="META" HeaderText="META">
                                    <HeaderStyle Font-Bold="true" Font-Size="9pt" Width="50px" Wrap="true" />
                                    <ItemStyle Font-Bold="true" Font-Size="9pt" ForeColor="Blue" HorizontalAlign="Center" Width="50px" Wrap="true" />
                                </telerik:GridBoundColumn>

                                <telerik:GridBoundColumn AllowFiltering="false" DataField="RESULTADO" HeaderText="RESULTADO" Visible="false">
                                    <HeaderStyle Font-Bold="true" Font-Size="9pt" Width="50px" Wrap="true" />
                                    <ItemStyle Font-Bold="true" Font-Size="9pt" HorizontalAlign="Center" Width="50px" Wrap="true" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn AllowFiltering="false" DataField="RESULTADO1" HeaderText="RESULTADO">
                                    <HeaderStyle Font-Bold="true" Font-Size="9pt" Width="50px" Wrap="true" />
                                    <ItemStyle Font-Bold="true" Font-Size="9pt" HorizontalAlign="Center" Width="50px" Wrap="true" />
                                </telerik:GridBoundColumn>

                                <telerik:GridBoundColumn AllowFiltering="false" DataField="CUMPLIMIENTO1" HeaderText="CUMPLIMIENTO">
                                    <HeaderStyle Font-Bold="true" Font-Size="9pt" Width="50px" Wrap="true" />
                                    <ItemStyle Font-Bold="true" Font-Size="9pt" HorizontalAlign="Center" Width="50px" Wrap="true" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn AllowFiltering="false" DataField="SEMAFORO" HeaderText="SEMAFORO">
                                    <HeaderStyle Font-Bold="true" Font-Size="9pt" Width="50px" Wrap="true" />
                                    <ItemStyle Font-Bold="true" Font-Size="9pt" HorizontalAlign="Center" Width="50px" Wrap="true" />
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="ANALISIS CUALITATIVO">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btn_Analisis" runat="server" ImageUrl="~/images/loupe.png" />
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="true" Font-Size="9pt" Width="50px" Wrap="true" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="GRAFICO">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btn_grafico" runat="server" ImageUrl="images/grafico.png" />
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="true" Font-Size="9pt" Width="50px" Wrap="true" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </telerik:GridTemplateColumn>
                            </Columns>
                        </MasterTableView>
                        <HeaderStyle CssClass="HeaderStyleGrid" BackColor="#00AD93" Font-Names="Arial Narrow" ForeColor="White" />
                        <FooterStyle CssClass="footerStyle" />
                        <ItemStyle Font-Names="Arial Narrow" />
                    </telerik:RadGrid>
                </telerik:RadPageView>

                <telerik:RadPageView ID="Pagina2" runat="server" Visible="false">
                    <br />
                    <div class="ContendDivRegistro" style="width: 800px; height: auto">
                        <h2 class="Contendh3">BUSCAR POR PROCESO</h2>
                        <div style="text-align: center; align-content: center">

                            <table style="width: 100%">
                                <tr>
                                    <td align="center">

                                        <asp:Label runat="server" Font-Names=" Arial, Helvetica, sans-serif;" Font-Size="12pt" Text="Seleccionar  frecuencia">                                                                     </asp:Label>
                                        <br />
                                        <telerik:RadComboBox ID="CmbMedida2" runat="server" AutoPostBack="true" EmptyMessage="Seleccione la medida" Skin="Bootstrap" OnSelectedIndexChanged="CmbMedida2_SelectedIndexChanged" Width="250px"></telerik:RadComboBox>
                                    </td>
                                    <td align="center">

                                        <asp:Label runat="server" Font-Names=" Arial, Helvetica, sans-serif;" Font-Size="12pt" Text="Seleccionar el periodo">                             
                                        </asp:Label>
                                        <br />
                                        <telerik:RadComboBox ID="CmbPeriodo2" Filter="StartsWith" runat="server" EmptyMessage="Seleccione el mes" Skin="Bootstrap" Width="250px"></telerik:RadComboBox>
                                    </td>

                                </tr>
                                <tr>
                                    <td colspan="2">

                                        <asp:Label runat="server" Font-Names=" Arial, Helvetica, sans-serif;" Font-Size="12pt" Text="Seleccionar el proceso">                                
                                        </asp:Label>
                                        <br />
                                        <telerik:RadComboBox ID="Cmbprocesos" Filter="StartsWith" runat="server" EmptyMessage="Seleccione el proceso" Skin="Bootstrap" Width="250px"></telerik:RadComboBox>


                                    </td>
                                </tr>
                            </table>


                            <br />
                            <telerik:RadButton ID="btn_consultar2" runat="server" Font-Size="12pt" Height="30px" OnClick="btn_consultar2_Click" RenderMode="Lightweight" Skin="MetroTouch" Text="Consultar" Width="200px"></telerik:RadButton>
                            <telerik:RadButton ID="Cmb_Limpiar" runat="server" Font-Size="12pt" Height="30px" OnClick="Cmb_Limpiar_Click" RenderMode="Lightweight" Skin="MetroTouch" Text="Limpiar" Width="200px"></telerik:RadButton>

                        </div>
                    </div>
                    <br />
                    <div style="align-content: center; text-align: center">

                        <telerik:RadGrid ID="GvIndicadoresproceso" runat="server" AllowPaging="true" AutoGenerateColumns="false" MasterTableView-DataKeyNames="ID_INDICADOR" OnItemCreated="GvIndicadoresproceso_ItemCreated1" OnItemDataBound="GvIndicadoresproceso_ItemDataBound" OnNeedDataSource="GvIndicadoresproceso_NeedDataSource" PageSize="10" Skin="Windows7" Visible="false" Width="100%">
                            <GroupingSettings CollapseAllTooltip="Collapse all groups" />
                            <ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true">
                            </ClientSettings>
                            <MasterTableView Width="100%">

                                <PagerStyle Mode="NextPrevNumericAndAdvanced"></PagerStyle>
                                <Columns>
                                    <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="DETALLE">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btn_detalle" runat="server" ImageUrl="images/Detalle.png" />
                                        </ItemTemplate>
                                        <HeaderStyle Font-Bold="true" Wrap="true" Width="50px" Font-Size="9pt" />
                                        <ItemStyle Font-Size="9pt" Wrap="true" Width="50px" HorizontalAlign="Center" />
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridBoundColumn DataField="NOMBRE_INDICADOR" HeaderText="NOMBRE INDICADOR">
                                        <HeaderStyle Font-Bold="true" Wrap="true" Width="50px" Font-Size="9pt" />
                                        <ItemStyle Font-Size="9pt" Wrap="true" Width="50px" HorizontalAlign="Center" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="VARIABLES">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btn_Variables" runat="server" ImageUrl="~/images/Variable.png" />
                                        </ItemTemplate>
                                        <HeaderStyle Font-Bold="true" Wrap="true" Width="50px" Font-Size="9pt" />
                                        <ItemStyle Font-Size="9pt" Wrap="true" Width="50px" Font-Bold="true" HorizontalAlign="Center" />
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="NOMBRE_TIPOINDICADOR" HeaderText="TIPO INDICADOR">
                                        <HeaderStyle Font-Bold="true" Wrap="true" Width="50px" Font-Size="9pt" />
                                        <ItemStyle Font-Size="9pt" Wrap="true" Width="50px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="FRECUENCIA" HeaderText="FRECUENCIA">
                                        <HeaderStyle Font-Bold="true" Wrap="true" Width="50px" Font-Size="9pt" />
                                        <ItemStyle Font-Size="9pt" Wrap="true" Width="50px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="META" HeaderText="META">

                                        <HeaderStyle Font-Bold="true" Wrap="true" Width="50px" Font-Size="9pt" />
                                        <ItemStyle Font-Size="9pt" Font-Bold="true" HorizontalAlign="Center" ForeColor="Blue" Wrap="true" Width="50px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="RESULTADO">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btn_resultado" runat="server" ImageUrl="images/resultado.png" />
                                        </ItemTemplate>
                                        <HeaderStyle Font-Bold="true" Wrap="true" Width="50px" Font-Size="9pt" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="CUMPLIMIENTO">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btn_Cumplimiento" runat="server" ImageUrl="~/images/Cumplimiento.png" />
                                        </ItemTemplate>
                                        <HeaderStyle Font-Bold="true" Wrap="true" Width="50px" Font-Size="9pt" />
                                        <ItemStyle Font-Size="9pt" Wrap="true" Width="50px" HorizontalAlign="Center" />
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="RESULTADO" HeaderText="VALOR MES ACTUAL">
                                        <HeaderStyle Font-Bold="true" Wrap="true" Width="50px" Font-Size="9pt" />
                                        <ItemStyle Font-Size="9pt" Font-Bold="true" Wrap="true" Width="50px" HorizontalAlign="Center" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="SEMAFORO" HeaderText="SEMAFORO">
                                        <HeaderStyle Font-Bold="true" Wrap="true" Width="50px" Font-Size="9pt" />
                                        <ItemStyle Font-Size="9pt" Font-Bold="true" Wrap="true" HorizontalAlign="Center" Width="50px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="ANALISIS CUALITATIVO">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btn_Analisis" runat="server" ImageUrl="~/images/loupe.png" />
                                        </ItemTemplate>
                                        <HeaderStyle Font-Bold="true" Wrap="true" Width="50px" Font-Size="9pt" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="GRAFICO">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btn_grafico" runat="server" ImageUrl="images/grafico.png" />
                                        </ItemTemplate>
                                        <HeaderStyle Font-Bold="true" Wrap="true" Width="50px" Font-Size="9pt" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </telerik:GridTemplateColumn>


                                </Columns>

                            </MasterTableView>
                            <HeaderStyle CssClass="HeaderStyleGrid" BackColor="#00AD93" Font-Names="Arial Narrow" ForeColor="White" />
                            <FooterStyle CssClass="footerStyle" />
                            <ItemStyle Font-Names="Arial Narrow" />
                        </telerik:RadGrid>
                    </div>
                    <br />
                    <div id="DivBusqueda2" runat="server" class="col-sm-12" visible="false">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <span class="badge badge-pill badge-danger">Error</span> ¡Debe seleccionar el indicador para buscar!
                                <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span aria-hidden="true">×</span></button>
                        </div>
                    </div>
                </telerik:RadPageView>
                <%--BUSQUEDA TIPO--%>
                <telerik:RadPageView ID="Pagina3" runat="server" Visible="false">

                    <br />
                    <div class="ContendDivRegistro" style="width: 700px; height: auto">
                        <h2 class="Contendh3">BUSCAR POR TIPO DE INDICADOR</h2>
                        <div style="text-align: center; align-content: center">

                            <table style="width: 100%">
                                <tr>
                                    <td>
                                        <asp:Label runat="server" Font-Names="Helvetica" Font-Size="12pt" Text="Debe seleccionar el tipo de indicador "></asp:Label>
                                        <br />
                                        <telerik:RadComboBox ID="Cmbtipoindicador" AutoPostBack="true" OnSelectedIndexChanged="Cmbtipoindicador_SelectedIndexChanged" runat="server" EmptyMessage="Seleccionar tipo" Skin="Bootstrap" Width="250px"></telerik:RadComboBox>

                                    </td>
                                    <td>
                                        <asp:Label ID="lblperspectiva" runat="server" Font-Names="Helvetica" Font-Size="12pt" Text="Debe seleccionar la perspectiva" Visible="false"></asp:Label>
                                        <br />
                                        <telerik:RadComboBox ID="Cmbperspectiva" runat="server" EmptyMessage="Seleccionar tipo" Skin="Bootstrap" Width="250px" Visible="false"></telerik:RadComboBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">

                                        <asp:Label runat="server" Font-Names=" Arial, Helvetica, sans-serif;" Font-Size="12pt" Text="Seleccionar  frecuencia">                                                                     </asp:Label>
                                        <br />
                                        <telerik:RadComboBox ID="Cmbmedida3" Filter="StartsWith" runat="server" AutoPostBack="true" EmptyMessage="Seleccione la medida" Skin="Bootstrap" OnSelectedIndexChanged="Cmbmedida3_SelectedIndexChanged" Width="250px"></telerik:RadComboBox>
                                    </td>
                                    <td align="center">

                                        <asp:Label runat="server" Font-Names=" Arial, Helvetica, sans-serif;" Font-Size="12pt" Text="Seleccionar el periodo">                             
                                        </asp:Label>
                                        <br />
                                        <telerik:RadComboBox ID="CmbPeriodo3" runat="server" EmptyMessage="Seleccione el mes" Skin="Bootstrap" Width="250px"></telerik:RadComboBox>
                                    </td>

                                </tr>
                            </table>
                            <br />


                            <telerik:RadButton ID="btn_consultar3" runat="server" Font-Size="12pt" Height="30px" OnClick="btn_consultar3_Click" RenderMode="Lightweight" Skin="MetroTouch" Text="Consultar" Width="200px"></telerik:RadButton>
                            <telerik:RadButton ID="btn_limpiar3" runat="server" Font-Size="12pt" Height="30px" RenderMode="Lightweight" Skin="MetroTouch" Text="Limpiar" Width="200px" OnClick="btn_limpiar3_Click"></telerik:RadButton>

                        </div>
                    </div>
                    <br />
                    <telerik:RadGrid ID="GvIndicadoresEs" runat="server" EnableLinqExpressions="False" OnItemCreated="GvIndicadoresEs_ItemCreated2" AllowPaging="true" AutoGenerateColumns="false" MasterTableView-DataKeyNames="ID_INDICADOR" OnItemDataBound="GvIndicadoresEs_ItemDataBound" OnNeedDataSource="GvIndicadoresEs_NeedDataSource" PageSize="20" Skin="Windows7" Visible="false" Width="100%">
                        <GroupingSettings CollapseAllTooltip="Collapse all groups" />
                        <ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true">
                        </ClientSettings>
                        <MasterTableView Width="100%" AllowFilteringByColumn="true">
                            <PagerStyle Mode="NextPrevNumericAndAdvanced"></PagerStyle>
                            <Columns>
                                <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="DETALLE">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btn_detalle" runat="server" ImageUrl="images/Detalle.png" />
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="true" Wrap="true" Width="50px" Font-Size="9pt" />
                                    <ItemStyle Font-Size="9pt" Wrap="true" Width="50px" HorizontalAlign="Center" />
                                </telerik:GridTemplateColumn>


                                <telerik:GridBoundColumn AllowFiltering="true" HeaderStyle-Font-Bold="true" DataField="NOMBRE_INDICADOR" HeaderText="NOMBRE INDICADOR" FilterControlWidth="150px" AutoPostBackOnFilter="false" CurrentFilterFunction="Contains"
                                    FilterDelay="2000" ShowFilterIcon="false">
                                    <HeaderStyle Font-Bold="True" />
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="VARIABLES">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btn_Variables" runat="server" ImageUrl="~/images/Variable.png" />
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="true" Wrap="true" Width="20px" Font-Size="9pt" />
                                    <ItemStyle Font-Size="9pt" Wrap="true" Width="20px" Font-Bold="true" HorizontalAlign="Center" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="PERSPECTIVA" AllowFiltering="false" HeaderText="PERSPECTIVA">
                                    <HeaderStyle Font-Bold="true" Wrap="true" Width="50px" Font-Size="9pt" />
                                    <ItemStyle Font-Size="9pt" Wrap="true" Width="50px" HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="INDUCTOR">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btn_inductor" runat="server" ImageUrl="~/images/diagram.png" />
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="true" Wrap="true" Width="50px" Font-Size="9pt" />
                                    <ItemStyle Font-Size="9pt" Wrap="true" Width="50px" Font-Bold="true" HorizontalAlign="Center" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="FRECUENCIA" AllowFiltering="false" HeaderText="FRECUENCIA">
                                    <HeaderStyle Font-Bold="true" Wrap="true" Width="50px" Font-Size="9pt" />
                                    <ItemStyle Font-Size="9pt" Wrap="true" Width="50px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="META" HeaderText="META" AllowFiltering="false">

                                    <HeaderStyle Font-Bold="true" Wrap="true" Width="50px" Font-Size="9pt" />
                                    <ItemStyle Font-Size="9pt" Font-Bold="true" HorizontalAlign="Center" ForeColor="Blue" Wrap="true" Width="50px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="RESULTADO">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btn_resultado" runat="server" ImageUrl="images/resultado.png" />
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="true" Wrap="true" Width="50px" Font-Size="9pt" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="CUMPLIMIENTO">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btn_Cumplimiento" runat="server" ImageUrl="~/images/Cumplimiento.png" />
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="true" Wrap="true" Width="50px" Font-Size="9pt" />
                                    <ItemStyle Font-Size="9pt" Wrap="true" Width="50px" HorizontalAlign="Center" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="RESULTADO" AllowFiltering="false" HeaderText="VALOR MES ACTUAL">
                                    <HeaderStyle Font-Bold="true" Wrap="true" Width="50px" Font-Size="9pt" />
                                    <ItemStyle Font-Size="9pt" Font-Bold="true" Wrap="true" Width="50px" HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="SEMAFORO" AllowFiltering="false" HeaderText="SEMAFORO">
                                    <HeaderStyle Font-Bold="true" Wrap="true" Width="50px" Font-Size="9pt" />
                                    <ItemStyle Font-Size="9pt" Font-Bold="true" Wrap="true" HorizontalAlign="Center" Width="50px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="ANALISIS CUALITATIVO">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btn_Analisis" runat="server" ImageUrl="~/images/loupe.png" />
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="true" Wrap="true" Width="50px" Font-Size="9pt" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="GRAFICO">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btn_grafico" runat="server" ImageUrl="images/grafico.png" />
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="true" Wrap="true" Width="50px" Font-Size="9pt" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </telerik:GridTemplateColumn>


                            </Columns>

                        </MasterTableView>
                        <HeaderStyle CssClass="HeaderStyleGrid" BackColor="#00AD93" Font-Names="Arial Narrow" ForeColor="White" />
                        <FooterStyle CssClass="footerStyle" />
                    </telerik:RadGrid>

                    <div id="DivBusqueda3" runat="server" class="col-sm-12" visible="false">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <span class="badge badge-pill badge-danger">Error</span> ¡Debe seleccionar el tipo de indicador!
                                <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span aria-hidden="true">×</span></button>
                        </div>
                    </div>

                </telerik:RadPageView>



            </telerik:RadMultiPage>
        </div>
    </telerik:RadAjaxPanel>

    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            var identificacion;
            function ShowEditForm(id, rowIndex) {
                var grid = $find("<%= GvIndicadores.ClientID %>");
                var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element();
                identificacion = rowControl;
                grid.get_masterTableView().selectItem(rowControl, true);
                window.radopen("DetalleIndicador1.aspx?ID_INDICADOR=" + id, "rwBusqueda", "950px", "610px");
                return false;

            }

            function ShowEditForm10(id, rowIndex) {
                var grid = $find("<%=GvSeccional.ClientID %>");
                var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element();
                identificacion = rowControl;
                grid.get_masterTableView().selectItem(rowControl, true);
                window.radopen("DetalleIndicador1.aspx?ID_INDICADOR=" + id, "rwBusqueda", "950px", "610px");
                return false;

            }



            function ShowEditForm2(id, rowIndex) {
                var grid = $find("<%= GvIndicadoresproceso.ClientID %>");
                var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element();
                identificacion = rowControl;
                grid.get_masterTableView().selectItem(rowControl, true);
                window.radopen("DetalleIndicador1.aspx?ID_INDICADOR=" + id, "rwBusqueda", "950px", "610px");
                return false;
            }

            function ShowEditForm3(id, rowIndex) {
                var grid = $find("<%= GvIndicadoresEs.ClientID %>");
                var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element();
                identificacion = rowControl;
                grid.get_masterTableView().selectItem(rowControl, true);
                window.radopen("DetalleIndicador1.aspx?ID_INDICADOR=" + id, "rwBusqueda", "950px", "610px");
                return false;
            }



            var identificacion;
            function ShowVariables(id, rowIndex) {
                var grid = $find("<%= GvIndicadores.ClientID %>");
                var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element();
                identificacion = rowControl;
                grid.get_masterTableView().selectItem(rowControl, true);
                window.radopen("Variables.aspx?ID_INDICADOR=" + id, "rwBusqueda", "950px", "610px");
                return false;
            }


            var identificacion;
            function ShowVariables2(id, rowIndex) {
                var grid = $find("<%= GvIndicadoresproceso.ClientID %>");
                var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element();
                identificacion = rowControl;
                grid.get_masterTableView().selectItem(rowControl, true);
                window.radopen("Variables.aspx?ID_INDICADOR=" + id, "rwBusqueda", "950px", "610px");
                return false;
            }


            var identificacion;
            function ShowVariables3(id, rowIndex) {
                var grid = $find("<%= GvIndicadoresEs.ClientID %>");
                var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element();
                identificacion = rowControl;
                grid.get_masterTableView().selectItem(rowControl, true);
                window.radopen("Variables.aspx?ID_INDICADOR=" + id, "rwBusqueda", "950px", "610px");
                return false;
            }


            var identificacion;
            function ShowVariables4(id, rowIndex) {
                var grid = $find("<%= GrV_regional.ClientID %>");
                var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element();
                identificacion = rowControl;
                grid.get_masterTableView().selectItem(rowControl, true);
                window.radopen("Variables.aspx?ID_INDICADOR=" + id, "rwBusqueda", "950px", "610px");
                return false;
            }


            var identificacion;
            function ShowVariablesRegional(id, rowIndex) {
                var grid = $find("<%= GrV_regional.ClientID %>");
                var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element();
                identificacion = rowControl;
                grid.get_masterTableView().selectItem(rowControl, true);
                window.radopen("Graficaregional.aspx?ID_INDICADOR=" + id, "rwBusqueda", "950px", "610px");
                return false;
            }




            var identificacion;
            function ShowVariables10(id, rowIndex) {
                var grid = $find("<%= GvSeccional.ClientID %>");
                var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element();
                identificacion = rowControl;
                grid.get_masterTableView().selectItem(rowControl, true);
                window.radopen("Variables.aspx?ID_INDICADOR=" + id, "rwBusqueda", "950px", "610px");
                return false;
            }





            function ShowAnalisis(id, rowIndex) {
                var grid = $find("<%= GvIndicadores.ClientID %>");
                var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element();
                identificacion = rowControl;
                grid.get_masterTableView().selectItem(rowControl, true);
                window.radopen("Analisis.aspx?ID_INDICADOR=" + id, "rwBusqueda", "950px", "610px");
                return false;
            }

            function ShowAnalisis2(id, rowIndex) {
                var grid = $find("<%= GvIndicadoresproceso.ClientID %>");
                var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element();
                identificacion = rowControl;
                grid.get_masterTableView().selectItem(rowControl, true);
                window.radopen("Analisis.aspx?ID_INDICADOR=" + id, "rwBusqueda", "950px", "610px");
                return false;
            }

            var identificacion;
            function ShowAnalisis4(id, rowIndex) {
                var grid = $find("<%= GrV_regional.ClientID %>");
                var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element();
                identificacion = rowControl;
                grid.get_masterTableView().selectItem(rowControl, true);
                window.radopen("Analisis.aspx?ID_INDICADOR=" + id, "rwBusqueda", "950px", "610px");
                return false;
            }


            var identificacion;
            function ShowAnalisis10(id, rowIndex) {
                var grid = $find("<%= GvSeccional.ClientID %>");
                var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element();
                identificacion = rowControl;
                grid.get_masterTableView().selectItem(rowControl, true);
                window.radopen("Analisis.aspx?ID_INDICADOR=" + id, "rwBusqueda", "950px", "610px");
                return false;
            }




            var identificacion;
            function ShowResultado(id, rowIndex) {
                var grid = $find("<%= GvIndicadores.ClientID %>");
                var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element();
                identificacion = rowControl;
                grid.get_masterTableView().selectItem(rowControl, true);
                window.radopen("Resultado.aspx?ID_INDICADOR=" + id, "rwBusqueda", "990px", "650px");
                return false;
            }

            var identificacion;
            function ShowResultado2(id, rowIndex) {
                var grid = $find("<%= GvIndicadoresproceso.ClientID %>");
                var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element();
                identificacion = rowControl;
                grid.get_masterTableView().selectItem(rowControl, true);
                window.radopen("Resultado.aspx?ID_INDICADOR=" + id, "rwBusqueda", "950px", "610px");
                return false;
            }


            var identificacion;
            function ShowResultado10(id, rowIndex) {
                var grid = $find("<%= GvSeccional.ClientID %>");
                var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element();
                identificacion = rowControl;
                grid.get_masterTableView().selectItem(rowControl, true);
                window.radopen("Resultado.aspx?ID_INDICADOR=" + id, "rwBusqueda", "950px", "610px");
                return false;
            }




            var identificacion;
            function ShowCumplmiento(id, rowIndex) {
                var grid = $find("<%= GvIndicadores.ClientID %>");
                var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element();
                identificacion = rowControl;
                grid.get_masterTableView().selectItem(rowControl, true);

                window.radopen("Cumplimiento.aspx?ID_INDICADOR=" + id, "rwBusqueda", "950px", "610px");
                return false;
            }
            var identificacion;
            function ShowCumplmiento2(id, rowIndex) {
                var grid = $find("<%= GvIndicadoresproceso.ClientID %>");
                var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element();
                identificacion = rowControl;
                grid.get_masterTableView().selectItem(rowControl, true);
                window.radopen("Cumplimiento.aspx?ID_INDICADOR=" + id, "rwBusqueda", "950px", "610px");
                return false;
            }

            var identificacion;
            function ShowCumplmiento3(id, rowIndex) {
                var grid = $find("<%= GvIndicadoresEs.ClientID %>");
                var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element();
                identificacion = rowControl;
                grid.get_masterTableView().selectItem(rowControl, true);
                window.radopen("Cumplimiento.aspx?ID_INDICADOR=" + id, "rwBusqueda", "950px", "610px");
                return false;
            }

            var identificacion;
            function ShowCumplmiento10(id, rowIndex) {
                var grid = $find("<%= GvSeccional.ClientID %>");
                var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element();
                identificacion = rowControl;
                grid.get_masterTableView().selectItem(rowControl, true);
                window.radopen("Cumplimiento.aspx?ID_INDICADOR=" + id, "rwBusqueda", "950px", "610px");
                return false;
            }



            var identificacion;
            function Showgrafica(id, rowIndex) {
                var grid = $find("<%= GvIndicadores.ClientID %>");
                var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element();
                identificacion = rowControl;
                grid.get_masterTableView().selectItem(rowControl, true);
                window.radopen("Grafica.aspx?ID_INDICADOR=" + id, "rwBusqueda", "1020px", "640px");
                return false;
            }


            var identificacion;
            function ShowInductor(id, rowIndex) {
                var grid = $find("<%= GvIndicadoresEs.ClientID %>");
                var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element();
                identificacion = rowControl;
                grid.get_masterTableView().selectItem(rowControl, true);
                window.radopen("inductor.aspx?ID_INDICADOR=" + id, "rwBusqueda", "950px", "610px");
                return false;
            }


            function ShowInductorSecc(id, rowIndex) {
                var grid = $find("<%= GvSeccional.ClientID %>");
                var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element();
                identificacion = rowControl;
                grid.get_masterTableView().selectItem(rowControl, true);
                window.radopen("inductor.aspx?ID_INDICADOR=" + id, "rwBusqueda", "950px", "610px");
                return false;
            }




        </script>

    </telerik:RadCodeBlock>

</asp:Content>
