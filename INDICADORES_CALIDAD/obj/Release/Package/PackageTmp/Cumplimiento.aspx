﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Cumplimiento.aspx.cs" Inherits="INDICADORES_CALIDAD.Cumplimiento" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="Content/StyleCustom.css" rel="stylesheet" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">

        <asp:ScriptManager runat="server"></asp:ScriptManager>


        <br />
        <div class="DivGeneral DivColorr">
            <asp:Label Text="SECCIONAL" Font-Bold="true" Font-Size="20pt" runat="server" Font-Names="Arial Narrow"></asp:Label>

        </div>
        <div style="align-content: center; text-align: center">
           <div style="align-content: center; text-align: center">
            <asp:Label runat="server" ID="LblNombre" Font-Size="20pt" ForeColor="#00AD93" Font-Names="Arial Narrow" Font-Bold="true"></asp:Label>
        </div>
        </div>
        <telerik:RadTabStrip runat="server" ID="RadUsuarios" CssClass="html .rtsTop .rtsLevel1" Font-Names="Agency FB" MultiPageID="RadMultiPage1" SelectedIndex="0" BorderColor="#99ccff" Skin="MetroTouch" PerTabScrolling="True" ReorderTabsOnSelect="True" ShowBaseLine="True">
            <Tabs>

                <telerik:RadTab Text="SECCIONAL" ImageUrl="~/images/pie-chart.png" Font-Bold="true" Width="250px" BackColor="#00AD93" runat="server"></telerik:RadTab>


            </Tabs>
        </telerik:RadTabStrip>


        <telerik:RadMultiPage ID="RadMultiPage1" SelectedIndex="0" runat="server">

            <telerik:RadPageView ID="Pagina1" runat="server">
                <telerik:RadGrid ID="GVMeses" runat="server" AllowPaging="true" AllowFilteringByColumn="true" OnNeedDataSource="GVMeses_NeedDataSource" AutoGenerateColumns="false" PageSize="12" Skin="Metro" Visible="false" Width="100%">
                    <GroupingSettings CollapseAllTooltip="Collapse all groups"></GroupingSettings>

                    <AlternatingItemStyle BackColor="#F0F0F0" ForeColor="Black" Font-Names="Arial Narrow" />

                    <MasterTableView>
                        <Columns>

                            <telerik:GridBoundColumn DataField="NMES" HeaderText="PERIODO" AllowFiltering="true">
                                <HeaderStyle Font-Bold="true" Font-Size="12pt" />
                                <ItemStyle Font-Size="12pt" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="RESULTADO" HeaderText="RESULTADO" AllowFiltering="false">
                                <HeaderStyle Font-Bold="true" Font-Size="12pt" />
                                <ItemStyle ForeColor="#00574a" Font-Bold="true" Font-Size="11pt" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="DES_AÑO" HeaderText="AÑO" AllowFiltering="true">
                                <HeaderStyle Font-Bold="true" Font-Size="12pt" />
                                <ItemStyle Font-Size="12pt" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="NOMBRE_SECCIONAL" HeaderText="SECCIONAL" AllowFiltering="true">
                                <HeaderStyle Font-Bold="true" Font-Size="12pt" />
                                <ItemStyle Font-Size="12pt" Font-Bold="true" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="CUMPLIMIENTO" HeaderText="CUMPLIMIENTO" AllowFiltering="true">
                                <HeaderStyle Font-Bold="true" Font-Size="12pt" />
                                <ItemStyle Font-Size="12pt" Font-Bold="true" />
                            </telerik:GridBoundColumn>

                        </Columns>
                    </MasterTableView>
                    <HeaderStyle BackColor="#00AD93" ForeColor="White" Font-Names="Arial Narrow" />
                    <ItemStyle BackColor="#E0E0E0" ForeColor="Black" Font-Names="Arial Narrow" />
                </telerik:RadGrid>
            </telerik:RadPageView>
        </telerik:RadMultiPage>





    </form>
</body>
</html>
