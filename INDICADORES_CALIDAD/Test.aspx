﻿<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<!DOCTYPE html>
<html>
<head>
    <title>5 pulse</title>
    <style type="text/css">
        /*
 *  Usage:
 *
      <div class="sk-spinner sk-spinner-pulse"></div>
 *
 */
    </style>
</head>
<body>

    <form runat="server">
        <asp:ScriptManager runat="server"></asp:ScriptManager>

        <telerik:RadTreeView runat="server" ID="RadTreeView1" DataSourceID="SqlDataSource1"
            DataFieldID="id" DataFieldParentID="parentID" Skin="Vista" CheckBoxes="true">
            <DataBindings>
                <telerik:RadTreeNodeBinding TextField="Text" />
                <telerik:RadTreeNodeBinding
                    Depth="0"
                    Checkable="false"
                    TextField="Text"
                    Expanded="true"
                    CssClass="rootNode" />
            </DataBindings>
        </telerik:RadTreeView>

        <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString="<%$ ConnectionStrings:SCORBOARD %>"
            SelectCommand="SELECT [ID_MENU], [DESCRIPCION_MENU], [ID_TIPOMENU] FROM [T_MENU]"></asp:SqlDataSource>
    </form>
</body>
</html>
