﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CNegocios;
using System.Data.Entity;
using Telerik.Web.UI;

using System.Data.SqlClient;
using System.Configuration;
using System.IO;

namespace INDICADORES_CALIDAD
{
    public partial class AdministracionIndicadores : System.Web.UI.Page
    {
        public IndicadoresBussines ObjGIndicadores = new IndicadoresBussines();
        public SqlConnection Conexion;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                CargarCombosMaestros("T_TIPOINDICADOR", 1, 2, 2, CmbTipoIndicador);
                
                CargarCombosMaestros("T_PROCESO", 1, 2, 2, CmbProceso);
            }
        }



        protected void rbConsultar_Click(object sender, EventArgs e)
        {
            GvIndicadores.Rebind();
            GvIndicadores.Visible = true;
        }
        private void CargarCombosMaestros(string Tabla, int campo1, int campo2, int orden, RadComboBox Combo, int? campo3 = null, int? campo4 = null)
        {
            Conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["SCORBOARD"].ConnectionString);
            SqlCommand cmd = new SqlCommand("P_CARGA_COMBOS", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TABLA", Tabla);
            cmd.Parameters.AddWithValue("@CAMPO1", campo1);
            cmd.Parameters.AddWithValue("@CAMPO2", campo2);
            cmd.Parameters.AddWithValue("@ORDEN", orden);
            cmd.Parameters.AddWithValue("@CAMPO_FILTRO", campo3);
            cmd.Parameters.AddWithValue("@VALOR_FILTRO", campo4);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {

                da.Fill(ds);
                Combo.DataSource = ds;
                Combo.DataValueField = "1";
                Combo.DataTextField = "2";
                Combo.DataBind();

            }
            catch (Exception ex)
            {

            }

        }

        protected void GvIndicadores_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GvIndicadores.DataSource = ObjGIndicadores.DCONSULTAINDICADORES(Convert.ToInt32(CmbTipoIndicador.SelectedValue), Convert.ToInt32(CmbProceso.SelectedValue));
        }

        protected void GvIndicadores_ItemCreated(object sender, GridItemEventArgs e)
        {

            if (e.Item is GridDataItem)
            {
                ImageButton btn_editar = (ImageButton)e.Item.FindControl("btn_detalle");
                btn_editar.Attributes["href"] = "javascript:void(0);";
                btn_editar.Attributes["onclick"] = String.Format("return ShowEditForm('{0}','{1}');", e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID_INDICADOR"], e.Item.ItemIndex);
                LblInactivo.Text = "Se inactivo el indicador";
                LblInactivo.Visible = true;


            }
            else
            {
                LblInactivo.Visible = false;
            }
        }
       



        protected void btn_Consulta3_Click(object sender, EventArgs e)
        {
            RadDocumentos.Visible = true;
            RadDocumentos.Rebind();

        }

        protected void GvIndicadores_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            string ID = e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID_INDICADOR"].ToString();
            ObjGIndicadores.DELETEINDICADOR(Convert.ToInt32(ID));
            GvIndicadores.Rebind();
        }

        protected void RadDocumentos_NeedDataSource1(object sender, GridNeedDataSourceEventArgs e)
        {
            RadDocumentos.DataSource = ObjGIndicadores.DCONSULTARDOCUMENTO(Convert.ToInt32(CmbConsultarTipo.SelectedValue));


        }
        
        protected void RadDocumentos_ItemCommand1(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "DownloadMyFile")
            {
                GridDataItem item = e.Item as GridDataItem;

                string strID = item.GetDataKeyValue("ID_INDICADOR").ToString();
                string filePath = Convert.ToString(e.CommandArgument);
                if (filePath == string.Empty || filePath == "")
                {
                    string script1 = "function f(){radopen(null, 'rw_customConfirm'); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "customConfirmOpener", script1, true);

                }
                else
                {
                    Response.ContentType = ContentType;
                    Response.AppendHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(filePath));
                    Response.WriteFile(filePath);
                    Response.Flush();
                    Response.End();


                }


            }

        }

 

        protected void GvrHistorico_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GvrHistorico.DataSource = ObjGIndicadores.DCONSULTARVERSION();
        }

        protected void GvrHistorico_ItemCreated(object sender, GridItemEventArgs e)
        {

            if (e.Item is GridDataItem)
            {
                ImageButton btn_editar = (ImageButton)e.Item.FindControl("btn_detalle");
                btn_editar.Attributes["href"] = "javascript:void(0);";
                btn_editar.Attributes["onclick"] = String.Format("return ShowEditForm2('{0}','{1}');", e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID_INDICADOR"], e.Item.ItemIndex);
                
            }

        }

        protected void GvrHistorico_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            string ID = e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID_INDICADOR"].ToString();
            ObjGIndicadores.DELETEINDICADORMOD(Convert.ToInt32(ID));
            GvrHistorico.Rebind();
        }

        protected void rbEliminar_Click(object sender, EventArgs e)
        {
            CmbTipoIndicador.SelectedValue = "0";
            CmbTipoIndicador.Text = "";
            CmbProceso.SelectedValue = "";
            CmbProceso.Text = "";
            GvIndicadores.Visible = false;

        }
    }
}
